//菜单显示
$('.menu-mobile').click(function () {
    $(this).siblings('.header-menu').slideToggle();
});
//回到顶部
$(window).scroll(function () {
    var scrollTop = $(window).scrollTop();
    if(scrollTop > $(window).height()){
        $('#back-to-top').addClass('active');
    }else{
        $('#back-to-top').removeClass('active');
    }
});
$('#back-to-top').click(function () {
    $('body').animate({
        scrollTop: 0
    },500);
});
//表單驗證
$.fn.extend({
    'formConfirm': function () {
        var flag = true;//表单验证是否成功标志
        $(this).parents('form').find('.error-tip').remove();
        $(this).parents('form').find('.form-control.require').each(function () {
            if($(this).val() == '') {
                switchRexp($(this));
                flag = false;
            }else{
               var type =  $(this).attr('data-type');
                //邮箱验证
                if(type == 'email') {
                    var rexg = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/g;
                    if(!rexg.test($(this).val())) {
                        switchRexp($(this));
                    }
                }
                //电话号码正则表达式（支持手机号码，3-4位区号，7-8位直播号码，1－4位分机号）:
                //((\d{11})|^((\d{7,8})|(\d{4}|\d{3})-(\d{7,8})|(\d{4}|\d{3})-(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1})|(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1}))$)
                if(type == 'tel'){
                    var rexg = /((\d{11})|^((\d{7,8})|(\d{4}|\d{3})-(\d{7,8})|(\d{4}|\d{3})-(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1})|(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1}))$)/g;
                    if(!rexg.test($(this).val())) {
                        switchRexp($(this));
                    }
                }
            }
        });
        //失去焦点
        $(this).parents('form').find('.form-control.require').on('blur',function () {
            $(this).removeClass('has-error');
            if($(this).parent().hasClass('input-group')) {
                $(this).parent().siblings('.error-tip').remove();
            }else{
                $(this).siblings('.error-tip').remove();
            }

        });
        return flag;
        //错误提示
        function switchRexp (cur) {
            cur.addClass('has-error');
            if(cur.parent().hasClass('input-group')) {
                cur.parent().parent().append('<span class="error-tip">請輸入正確的' +  cur.attr('data-info') + '。</span>');
            }else{
                cur.parent().append('<span class="error-tip">請輸入正確的' +  cur.attr('data-info') + '。</span>');
            }

        }

    }

});