<?php

return array (
	'default' => array (
		'hostname' => '127.0.0.1',
		'port' => 3306,
		'database' => 'dev_coach',
		'username' => 'coach',
		'password' => 'coach',
		'tablepre' => 'v9_sso_',
		'charset' => 'utf8',
		'type' => 'mysqli',
		'debug' => true,
		'pconnect' => 0,
		'autoconnect' => 0
		)
);

?>