<?php
$emailData = "<!doctype html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport'
          content='width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>推薦個案</title>
</head>
<body>
    <div style='text-align: center;margin-top: 30px'>
        <h2 title='tutorseeking' style='margin-bottom: 15px'><img width='250px'  src='http://www.tutorseeking.com/statics/images/add_image/logo-sticky.png' alt=''></h2>
        <p style='margin-bottom: 20px;font-size: 18px'>導師個案推薦</p>
        <div style='overflow: auto'>
            <table border='1px' cellpadding='0' cellspacing='0' style='min-width: 700px; font-size: 16px; text-align: center;border-collapse:collapse;margin: 0 auto;' >
                <thead>
                <tr>
                    <th style='padding: 6px' >個案編號</th>
                    <th style='padding: 6px' >授課地區</th>
                    <th style='padding: 6px' >科目</th>
                    <th style='padding: 6px' >主要教學語言</th>
                    <th style='padding: 6px' >可教授日期</th>
                    <th style='padding: 6px' >每小時授課價錢</th>
                    <th style='padding: 6px' >操作</th>
                </tr>
                </thead>
                <tbody>";
                $sst_weekday_array = array('M','T','W','T','F','S','S');
                foreach ($data as $key => $value) {
                    $Tutor = pc_base::load_app_class('helpcommon');//加載配對輔助類
                    $value['tsl_locid'] = $Tutor->create_location($value['tsl_locid']);//轉化為地區名稱
                    $tst_weekday = ($value['tst_weekday'] == '')?'無要求':explode(',', $value['tst_weekday']);//转数组
                    if($tst_weekday != '無要求'){
                        $weekday = '';
                        foreach ($sst_weekday_array as $ke => $val) {
                            if($ke == 6){//星期天标识最后插到首个位置
                                if(in_array(6, $tst_weekday)){
                                    $val = '<b>'.$val.'</b>&nbsp;';
                                }else{
                                    $val = '<b style="color:#ccc;">'.$val.'</b>&nbsp;';
                                }
                                $weekday = $val.$weekday;
                            }else{
                                if(in_array($ke,$tst_weekday)){
                                    $weekday .= '<b>'.$val.'</b>&nbsp;';
                                }else{
                                    $weekday .= '<b style="color:#ccc;">'.$val.'</b>&nbsp;';
                                }
                            }
                        }
                    }else{
                        $weekday = '無要求';
                    }

                    if($value['tutor_main_lang'] == ''){
                        $value['tutor_main_lang'] = '無要求';
                    }

                	$emailData .= "<tr>
				                    <td style='padding: 6px' >".$value['tm_tutorid']."</td>
				                    <td style='padding: 6px' >".$value['loc_name']."</td>
				                    <td style='padding: 6px' >".$value['tsl_locid']."</td>
				                    <td style='padding: 6px' >".$value['tutor_main_lang']."</td>
				                    <td style='padding: 6px' >".$weekday."</td>
				                    <td style='padding: 6px' >$".$value['tstt_fee']."</td>
				                    <td style='padding: 6px' ><a href='http://www.tutorseeking.com/index.php?m=teachers&c=index&a=tutor_detail&userid=".$value['tutor_userid']."&tutor_id=".$value['tm_tutorid']."' style='text-decoration: none;color: #fbca21;'>查看詳情</a></td>
				                </tr>";

                }

$emailData .= "</tbody>
            </table>
        </div>

    </div>
</body>
</html>";


?>