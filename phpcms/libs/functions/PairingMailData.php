<?php
$email_data="<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width,initial-scale=1.0, maximum-scale=1.0,minimum-scale=1.0,user-scalable=no'>
    <title>tutorseeking</title>
    <style>
        /*.match-table td{padding: 6px;}*/
    </style>
</head>
<body>
    <div style='max-width: 880px; margin:0 auto; font-size: 14px; color: #333; text-align: justify'>
        <form action=''>
            <div style='text-align: center'>
                <br>
                <br>
                <img width='250px' src='http://www.tutorseeking.com/statics/images/add_image/logo-sticky.png' alt=''>
            </div>
            <h2 style='text-align: center; font-size: 24px;'>配對信息 —— 配對狀態：$pair[state]</h2>
            <br>
            <div style='text-align: center;'>
                <div style='display:inline-block; margin-right: 60px; margin-bottom: 10px; vertical-align: top;'>
                    <table border='1px' class='match-table' cellpadding='0' cellspacing='0' style='width: 360px; font-size: 16px; text-align: center;border-collapse:collapse' >
                        <tbody>
                        <tr>
                            <td style='padding: 6px;'  colspan='2' >學生信息</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >聯絡人姓名</td>
                            <td style='padding: 6px;' >$stu_info[username]</td>
                        </tr>
                         <tr>
                            <td style='padding: 6px;' >要求導師性別</td>
                            <td style='padding: 6px;' >$stu_info[st_req_sex]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >聯絡電話</td>
                            <td style='padding: 6px;' >$stu_info[phone]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >聯絡電郵</td>
                            <td style='padding: 6px;' >$stu_info[email]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >學習類別</td>
                            <td style='padding: 6px;' >$stu_info[st_ttid_1]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >學習項目</td>
                            <td style='padding: 6px;' >$stu_info[st_ttid_2]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >學習級別</td>
                            <td style='padding: 6px;' >$stu_info[st_ttid_3]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >上課地點</td>
                            <td style='padding: 6px;' >$stu_info[loc_name]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >每小時學費</td>
                            <td style='padding: 6px;' >HKD $stu_info[st_tutor_fee]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >要求授課語言</td>
                            <td style='padding: 6px;' >$stu_info[st_main_lang]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >可接受最低教育程度</td>
                            <td style='padding: 6px;' >$stu_info[st_min_grade]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >配對狀態</td>
                            <td style='padding: 6px;' >$stu_info[pair_sponsor]</td>
                        </tr>
                        <tr>
                            <td colspan='2' style='padding: 6px;' >要求上課時間</td>
                        </tr>
                        ".$stu_info['weekday']."
                        <tr>
                            <td style='padding: 6px;'  colspan='2'><a href='$stu_info[link]' style='text-decoration: none; color: #e39916;'>查看詳情</a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div style='display:inline-block; margin-bottom: 10px; vertical-align: top;'>
                    <table border='1px' class='match-table' cellpadding='0' cellspacing='0' style='width: 360px; font-size: 16px; text-align: center;border-collapse:collapse' >
                        <tbody>
                        <tr>
                            <td style='padding: 6px;' colspan='2' >導師信息</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >聯絡人姓名</td>
                            <td style='padding: 6px;' >$tutor_info[username]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >性別</td>
                            <td style='padding: 6px;' >$tutor_info[tutor_sex]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >聯絡電話</td>
                            <td style='padding: 6px;' >$tutor_info[phone]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >聯絡電郵</td>
                            <td style='padding: 6px;' >$tutor_info[email]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >可教授類別</td>
                            <td style='padding: 6px;' >$tutor_info[tstt_ttid_name_1]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >可教授項目</td>
                            <td style='padding: 6px;' >$tutor_info[tstt_ttid_name_2]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >可教授級別</td>
                            <td style='padding: 6px;' >$tutor_info[tstt_ttid_name_3]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >可教授地點</td>
                            <td style='padding: 6px;' >$tutor_info[tsl_locid]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >每小時學費</td>
                            <td style='padding: 6px;' >HKD $tutor_info[tstt_fee]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >可授課語言</td>
                            <td style='padding: 6px;' >$tutor_info[tutor_main_lang]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >最高學歷</td>
                            <td style='padding: 6px;' >$tutor_info[grade_name]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >配對狀態</td>
                            <td style='padding: 6px;' >$tutor_info[pair_sponsor]</td>
                        </tr>
                        <tr>
                            <td colspan='2' style='padding: 6px;' >可授課時間</td>
                        </tr>
                        ".$tutor_info['weekday']."
                        <tr>
                            <td style='padding: 6px;'  colspan='2'><a href='$tutor_info[link]' style='text-decoration: none; color: #e39916;'>查看詳情</a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>
            <br>";



           
            // <div>
            //     以下乃閣下須就Tutorseeking.com（簡稱「此網站」）及其提供之服務需要遵守之條款和規則。按下「我接受」，即表示你同意接受這些條款和規則之約束。

            //     Tutorseeking可隨時修訂合約內容，修訂條款會在此網站內張貼。新修訂條款將於張貼七天後自動生效。因此，閣下該定期檢視Tutorseeking對該等條款所作出之更改。倘 於該等條款作出任何更改後，閣下仍繼續使用本網站或本網站提供的服務，則表示接納該等更改。

            //     1. 定義

            //     1.1 詞彙之定義

            //     以下詞彙在此合約中具下列定義：

            //     「個案確認(confirm)／接受個案」指此網站職員如在電話對話中表明「個案已經confirm／確認」，如導師沒有 提出反對，從電話結束以後的一刻開始，個案已被視為「已確認／confirm／已接受／已接納」

            //     「全科」指該級別之全級常規科目，小學科目則為「中文、英文、數學、常識、普通話」，中學科目則需按個別學校情況而定，另功課輔導即等同全科補習

            //     「導師／家長/ Tutorseeking三方關係 」指導師與家長之關係為服務合作關係，其間只為獨立合作聘約，並不存在僱傭關係；導師與此網站亦只存在服務提供者與服務使用者之關係，雙方沒有任何僱傭關係 ，服務亦與「職業介紹」完全無關

            //     「會員合約 」指此網站的任何一部分所示指引、指南、內容、條款、說明，均為會員合約之一部分，此網站有權依據合約收取費用，成功登記即代表導師同意所有條款

            //     「公司商業註冊及對不法行為
            // </div>


$email_data .="        </form>
    </div>
</body>
</html>";


//導師郵件內容
$email_tut_data="<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width,initial-scale=1.0, maximum-scale=1.0,minimum-scale=1.0,user-scalable=no'>
    <title>tutorseeking</title>
    <style>
        /*.match-table td{padding: 6px;}*/
    </style>
</head>
<body>
    <div style='max-width: 880px; margin:0 auto; font-size: 14px; color: #333; text-align: justify'>
        <form action=''>
            <div style='text-align: center'>
                <br>
                <br>
                <img width='250px' src='http://www.tutorseeking.com/statics/images/add_image/logo-sticky.png' alt=''>
            </div>
            <h2 style='text-align: center; font-size: 24px;'>配對信息 —— 配對狀態：$pair[state]</h2>
            <br>
            <div style='text-align: center;'>               
                <div style='display:inline-block; margin-bottom: 10px; vertical-align: top;'>
                    <table border='1px' class='match-table' cellpadding='0' cellspacing='0' style='width: 360px; font-size: 16px; text-align: center;border-collapse:collapse' >
                        <tbody>
                        <tr>
                            <td style='padding: 6px;'  colspan='2' >導師信息</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >聯絡人姓名</td>
                            <td style='padding: 6px;' >$tutor_info[tutor_fname_chi]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >性別</td>
                            <td style='padding: 6px;' >$tutor_info[tutor_sex]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >聯絡電話</td>
                            <td style='padding: 6px;' >$tutor_info[phone]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >聯絡電郵</td>
                            <td style='padding: 6px;' >$tutor_info[email]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >可教授類別</td>
                            <td style='padding: 6px;' >$tutor_info[tstt_ttid_name_1]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >可教授項目</td>
                            <td style='padding: 6px;' >$tutor_info[tstt_ttid_name_2]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >可教授級別</td>
                            <td style='padding: 6px;' >$tutor_info[tstt_ttid_name_3]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >可教授地點</td>
                            <td style='padding: 6px;' >$tutor_info[tsl_locid]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >每小時學費</td>
                            <td style='padding: 6px;' >HKD $tutor_info[tstt_fee]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >可授課語言</td>
                            <td style='padding: 6px;' >$tutor_info[tutor_main_lang]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >最高學歷</td>
                            <td style='padding: 6px;' >$tutor_info[grade_name]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >配對狀態</td>
                            <td style='padding: 6px;' >$tutor_info[pair_sponsor]</td>
                        </tr>
                        <tr>
                            <td colspan='2' style='padding: 6px;' >可授課時間</td>
                        </tr>
                        ".$tutor_info['weekday']."
                        <tr>
                            <td style='padding: 6px;'  colspan='2'><a href='$tutor_info[link]' style='text-decoration: none; color: #e39916;'>$tutor_info[bottomName]</a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>
            <br>";



           
            // <div>
            //     以下乃閣下須就Tutorseeking.com（簡稱「此網站」）及其提供之服務需要遵守之條款和規則。按下「我接受」，即表示你同意接受這些條款和規則之約束。

            //     Tutorseeking可隨時修訂合約內容，修訂條款會在此網站內張貼。新修訂條款將於張貼七天後自動生效。因此，閣下該定期檢視Tutorseeking對該等條款所作出之更改。倘 於該等條款作出任何更改後，閣下仍繼續使用本網站或本網站提供的服務，則表示接納該等更改。

            //     1. 定義

            //     1.1 詞彙之定義

            //     以下詞彙在此合約中具下列定義：

            //     「個案確認(confirm)／接受個案」指此網站職員如在電話對話中表明「個案已經confirm／確認」，如導師沒有 提出反對，從電話結束以後的一刻開始，個案已被視為「已確認／confirm／已接受／已接納」

            //     「全科」指該級別之全級常規科目，小學科目則為「中文、英文、數學、常識、普通話」，中學科目則需按個別學校情況而定，另功課輔導即等同全科補習

            //     「導師／家長/ Tutorseeking三方關係 」指導師與家長之關係為服務合作關係，其間只為獨立合作聘約，並不存在僱傭關係；導師與此網站亦只存在服務提供者與服務使用者之關係，雙方沒有任何僱傭關係 ，服務亦與「職業介紹」完全無關

            //     「會員合約 」指此網站的任何一部分所示指引、指南、內容、條款、說明，均為會員合約之一部分，此網站有權依據合約收取費用，成功登記即代表導師同意所有條款

            //     「公司商業註冊及對不法行為
            // </div>


$email_tut_data .="        </form>
    </div>
</body>
</html>";



//學生郵件信息
$email_st_data="<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width,initial-scale=1.0, maximum-scale=1.0,minimum-scale=1.0,user-scalable=no'>
    <title>tutorseeking</title>
    <style>
        /*.match-table td{padding: 6px;}*/
    </style>
</head>
<body>
    <div style='max-width: 880px; margin:0 auto; font-size: 14px; color: #333; text-align: justify'>
        <form action=''>
            <div style='text-align: center'>
                <br>
                <br>
                <img width='250px' src='http://www.tutorseeking.com/statics/images/add_image/logo-sticky.png' alt=''>
            </div>
            <h2 style='text-align: center; font-size: 24px;'>配對信息 —— 配對狀態：$pair[state]</h2>
            <br>
            <div style='text-align: center;'>
                <div style='display:inline-block; margin-right: 60px; margin-bottom: 10px; vertical-align: top;'>
                    <table border='1px' class='match-table' cellpadding='0' cellspacing='0' style='width: 360px; font-size: 16px; text-align: center;border-collapse:collapse' >
                        <tbody>
                        <tr>
                            <td style='padding: 6px;'  colspan='2' >學生信息</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >聯絡人姓名</td>
                            <td style='padding: 6px;' >$stu_info[username]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >要求導師性別</td>
                            <td style='padding: 6px;' >$stu_info[st_req_sex]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >聯絡電話</td>
                            <td style='padding: 6px;' >$stu_info[phone]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >聯絡電郵</td>
                            <td style='padding: 6px;' >$stu_info[email]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >學習類別</td>
                            <td style='padding: 6px;' >$stu_info[st_ttid_1]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >學習項目</td>
                            <td style='padding: 6px;' >$stu_info[st_ttid_2]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >學習級別</td>
                            <td style='padding: 6px;' >$stu_info[st_ttid_3]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >上課地點</td>
                            <td style='padding: 6px;' >$stu_info[loc_name]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >每小時學費</td>
                            <td style='padding: 6px;' >HKD $stu_info[st_tutor_fee]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >要求授課語言</td>
                            <td style='padding: 6px;' >$stu_info[st_main_lang]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >最高學歷</td>
                            <td style='padding: 6px;' >$stu_info[st_min_grade]</td>
                        </tr>
                        <tr>
                            <td style='padding: 6px;' >配對狀態</td>
                            <td style='padding: 6px;' >$stu_info[pair_sponsor]</td>
                        </tr>
                        <tr>
                            <td colspan='2' style='padding: 6px;' >要求上課時間</td>
                        </tr>
                        ".$stu_info['weekday']."
                        <tr>
                            <td style='padding: 6px;'  colspan='2'><a href='$stu_info[link]' style='text-decoration: none; color: #e39916;'>$stu_info[bottomName]</a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <br>";



           
            // <div>
            //     以下乃閣下須就Tutorseeking.com（簡稱「此網站」）及其提供之服務需要遵守之條款和規則。按下「我接受」，即表示你同意接受這些條款和規則之約束。

            //     Tutorseeking可隨時修訂合約內容，修訂條款會在此網站內張貼。新修訂條款將於張貼七天後自動生效。因此，閣下該定期檢視Tutorseeking對該等條款所作出之更改。倘 於該等條款作出任何更改後，閣下仍繼續使用本網站或本網站提供的服務，則表示接納該等更改。

            //     1. 定義

            //     1.1 詞彙之定義

            //     以下詞彙在此合約中具下列定義：

            //     「個案確認(confirm)／接受個案」指此網站職員如在電話對話中表明「個案已經confirm／確認」，如導師沒有 提出反對，從電話結束以後的一刻開始，個案已被視為「已確認／confirm／已接受／已接納」

            //     「全科」指該級別之全級常規科目，小學科目則為「中文、英文、數學、常識、普通話」，中學科目則需按個別學校情況而定，另功課輔導即等同全科補習

            //     「導師／家長/ Tutorseeking三方關係 」指導師與家長之關係為服務合作關係，其間只為獨立合作聘約，並不存在僱傭關係；導師與此網站亦只存在服務提供者與服務使用者之關係，雙方沒有任何僱傭關係 ，服務亦與「職業介紹」完全無關

            //     「會員合約 」指此網站的任何一部分所示指引、指南、內容、條款、說明，均為會員合約之一部分，此網站有權依據合約收取費用，成功登記即代表導師同意所有條款

            //     「公司商業註冊及對不法行為
            // </div>


$email_st_data .="        </form>
    </div>
</body>
</html>";
