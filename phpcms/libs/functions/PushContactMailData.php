<?php
$emailData = "<!doctype html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport'
          content='width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>用戶留言</title>
</head>
<body>
    <div style='text-align: center;margin-top: 30px'>
        <h2 title='tutorseeking' style='margin-bottom: 15px'><img width='250px'  src='http://www.tutorseeking.com/statics/images/add_image/logo-sticky.png' alt=''></h2>
        <p style='margin-bottom: 20px;font-size: 18px'>".$title."</p>
        <div style='overflow: auto'>
            <table border='1px' cellpadding='0' cellspacing='0' style='min-width: 700px; font-size: 16px; text-align: center;border-collapse:collapse;margin: 0 auto;' >
                <thead>
                <tr>
                    <th style='padding: 6px' >姓名</th>
                    <td style='padding: 6px;width:500px;' >".$nickname."</td>
                </tr>
                <tr>
                    <th style='padding: 6px' >電話</th>
                    <td style='padding: 6px;width:500px;' >".$phone."</td>
                </tr>
                <tr>
                    <th style='padding: 6px' >留言</th>
                    <td style='padding: 6px;width:500px;' >
                        <textarea cols='16px;' rows='52px;' style='margin: 0px;width: 492px;height: 166px;'>".$content."</textarea>
                    </td>
                </tr>
                </thead>
                <tbody>";

$emailData .= "</tbody>
            </table>
        </div>

    </div>
</body>
</html>";


?>