<?php
$emailData = "<!doctype html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport'
          content='width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>推薦個案</title>
</head>
<body>
    <div style='text-align: center;margin-top: 30px'>
        <h2 title='tutorseeking' style='margin-bottom: 15px'><img width='250px'  src='http://www.tutorseeking.com/statics/images/add_image/logo-sticky.png' alt=''></h2>
        <p style='margin-bottom: 20px;font-size: 18px'>學生個案推薦</p>
        <div style='overflow: auto'>
            <table border='1px' cellpadding='0' cellspacing='0' style='min-width: 700px; font-size: 16px; text-align: center;border-collapse:collapse;margin: 0 auto;' >
                <thead>
                <tr>
                    <th style='padding: 6px' >個案編號</th>
                    <th style='padding: 6px' >上課地區</th>
                    <th style='padding: 6px' >科目</th>
                    <th style='padding: 6px' >要求教學語言</th>
                    <th style='padding: 6px' >要求上課日期</th>
                    <th style='padding: 6px' >每小時上課價錢</th>
                    <th style='padding: 6px' >操作</th>
                </tr>
                </thead>
                <tbody>";
                $sst_weekday_array = array('M','T','W','T','F','S','S');
                foreach ($data as $key => $value) {
                	$sst_weekday = ($value['sst_weekday'] == '')?'無要求':explode(',', $value['sst_weekday']);//转数组
                	if($sst_weekday != '無要求'){
                        $weekday = '';
                		foreach ($sst_weekday_array as $ke => $val) {
                            if($ke == 6){//星期天标识最后插到首个位置
                                if(in_array(6, $sst_weekday)){
                                    $val = '<b>'.$val.'</b>&nbsp;';
                                }else{
                                    $val = '<b style="color:#ccc;">'.$val.'</b>&nbsp;';
                                }
                                $weekday = $val.$weekday;
                            }else{
                                if(in_array($ke,$sst_weekday)){
                                    $weekday .= '<b>'.$val.'</b>&nbsp;';
                                }else{
                                    $weekday .= '<b style="color:#ccc;">'.$val.'</b>&nbsp;';
                                }
                            }
                        }
                	}else{
                        $weekday = '無要求';
                    }

                    if($value['st_main_lang'] == ''){
                        $value['st_main_lang'] = '無要求';
                    }

                	$emailData .= "<tr>
				                    <td style='padding: 6px' >".$value['st_id']."</td>
				                    <td style='padding: 6px' >".$value['loc_name']."</td>
				                    <td style='padding: 6px' >".$value['tt_name']."</td>
				                    <td style='padding: 6px' >".$value['st_main_lang']."</td>
				                    <td style='padding: 6px' >".$weekday."</td>
				                    <td style='padding: 6px' >$".$value['st_tutor_fee']."</td>
				                    <td style='padding: 6px' ><a href='http://www.tutorseeking.com/index.php?m=tparents&c=index&a=stu_detail&stuid=".$value['student_userid']."&tran_id=".$value['st_id']."' style='text-decoration: none;color: #fbca21;'>查看詳情</a></td>
				                </tr>";
                }

$emailData .= "</tbody>
            </table>
        </div>

    </div>
</body>
</html>";
// echo $emailData;exit;

?>