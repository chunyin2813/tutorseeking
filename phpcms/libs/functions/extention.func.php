<?php




function dump($data,$type=true){
    echo "<pre>";
    print_r($data);
    echo "</pre>";
    if($type==true){
        exit;
    }
}


function xml_array($xml){

    $arr = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);
              // var_dump($arr);exit;
    $lists = json_decode(json_encode($arr),true);
    return $lists;
}


function emptyreplace($str) {
    $str = str_replace('　', ' ', $str); 
    $str = str_replace(' ', ' ', $str); 
    $noe = false;
    for ($i=0 ; $i<strlen($str); $i++) { 
        if($noe && $str[$i]==' ') $str[$i] = '+'; 
        elseif($str[$i]!=' ') $noe=true; 
    }
    return $str;
}


function do_mencrypt($string,$key){

    $size = mcrypt_get_block_size ( MCRYPT_DES, MCRYPT_MODE_CBC );

    $string = pkcs5Pad ( $string, $size );
    
    $data  =  base64_encode(mcrypt_encrypt(MCRYPT_DES,$key,$string,MCRYPT_MODE_CBC,$key));
    return $data;
}

function do_mdecrypt($string,$key){
    $string = base64_decode($string);
    $result = mcrypt_decrypt(MCRYPT_DES, $key, $string, MCRYPT_MODE_CBC, $key);
    $result = pkcs5Unpad( $result );
    return $result;
}
function pkcs5Pad($text, $blocksize){
    $pad = $blocksize - (strlen ( $text ) % $blocksize);
    return $text . str_repeat ( chr ( $pad ), $pad );
}
function pkcs5Unpad($text){
    $pad = ord ( $text {strlen ( $text ) - 1} );
    if ($pad > strlen ( $text ))
        return false;
    if (strspn ( $text, chr ( $pad ), strlen ( $text ) - $pad ) != $pad)
        return false;
    return substr ( $text, 0, - 1 * $pad );
}




function request_post($url = '', $param = '') {
    if (empty($url) || empty($param)) {
        return false;
    }

    $postUrl = $url;
    $curlPost = $param;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$postUrl);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
}




function request_post_array($url = '', $post_data = array(),$ty='1') {
    if (empty($url) || empty($post_data)) {
        return false;
    }
    $o = "";
    foreach ( $post_data as $k => $v )
    {
        $o.= "$k=" . urlencode( $v ). "&" ;
    }
    $post_data = substr($o,0,-1);

    $postUrl = $url;
    $curlPost = $post_data;
    $ch = curl_init();
    if($ty == 2) {
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);  
    }
    curl_setopt($ch, CURLOPT_URL,$postUrl);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}


function simplified_conversion($data){
    $change = pc_base::load_model('FanJianConvert');
    if(is_array($data)){   

        foreach ($data as $key => $value) {     

            $data[$key]['areaname'] = $change->tradition2simple($value['areaname']);

            foreach ($value['submenu'] as $ke => $val) {    

                $data[$key]['submenu'][$ke]['areaname'] = $change->tradition2simple($val['areaname']);

                foreach ($val['submenu'] as $k => $v) {   

                    $data[$key]['submenu'][$ke]['submenu'][$k]['areaname'] = $change->tradition2simple($v['areaname']);

                }
            }
        }
    }else{
        $data = $change->tradition2simple($data);
    }
    return $data;
}


function get_first_letter($str){
    if (empty($str)) {
        return '';
    }
    $change = pc_base::load_model('FanJianConvert');
    $str = $change->tradition2simple($str);
    $fchar = ord($str{0});
    if ($fchar >= ord('A') && $fchar <= ord('z')) return strtoupper($str{0});
    $s1 = iconv('UTF-8', 'gb2312', $str);
    // $s2 = iconv('gb2312', 'UTF-8', $s1);
    // $s = $s2 == $str ? $s1 : $str;
    $asc = ord($s1{0}) * 256 + ord($s1{1}) - 65536;
    if ($asc >= -20319 && $asc <= -20284) return 'A';
    if ($asc >= -20283 && $asc <= -19776) return 'B';
    if ($asc >= -19775 && $asc <= -19219) return 'C';
    if ($asc >= -19218 && $asc <= -18711) return 'D';
    if ($asc >= -18710 && $asc <= -18527) return 'E';
    if ($asc >= -18526 && $asc <= -18240) return 'F';
    if ($asc >= -18239 && $asc <= -17923) return 'G';
    if ($asc >= -17922 && $asc <= -17418) return 'H';
    if ($asc >= -17417 && $asc <= -16475) return 'J';
    if ($asc >= -16474 && $asc <= -16213) return 'K';
    if ($asc >= -16212 && $asc <= -15641) return 'L';
    if ($asc >= -15640 && $asc <= -15166) return 'M';
    if ($asc >= -15165 && $asc <= -14923) return 'N';
    if ($asc >= -14922 && $asc <= -14915) return 'O';
    if ($asc >= -14914 && $asc <= -14631) return 'P';
    if ($asc >= -14630 && $asc <= -14150) return 'Q';
    if ($asc >= -14149 && $asc <= -14091) return 'R';
    if ($asc >= -14090 && $asc <= -13319) return 'S';
    if ($asc >= -13318 && $asc <= -12839) return 'T';
    if ($asc >= -12838 && $asc <= -12557) return 'W';
    if ($asc >= -12556 && $asc <= -11848) return 'X';
    if ($asc >= -11847 && $asc <= -11056) return 'Y';
    if ($asc >= -11055 && $asc <= -10247) return 'Z';
    return null;
}


function generate_password( $length = 8,$type=1 ) {
    
    if($type != 1){
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    }else{
        $chars = '0123456789';
    }

    $password = '';
    for ( $i = 0; $i < $length; $i++ )
    {
        
        $password .= $chars[ mt_rand(0, strlen($chars) - 1) ];
    }
    return $password;
}



function Post($curlPost,$url){
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_NOBODY, true);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $curlPost);
    $return_str = curl_exec($curl);
    curl_close($curl);
    return $return_str;
}

function xml_to_array($xml){
    
    $reg = "/<(\w+)[^>]*>([\\x00-\\xFF]*)<\\/\\1>/";
    if(preg_match_all($reg, $xml, $matches)){
        $count = count($matches[0]);
        for($i = 0; $i < $count; $i++){
            $subxml= $matches[2][$i];
            $key = $matches[1][$i];
            if(preg_match( $reg, $subxml )){
                $arr[$key] = xml_to_array( $subxml );
            }else{
                $arr[$key] = $subxml;
            }
        }
    }
    return $arr;
}


function sendTemplateSMS($post_data,$target,$user){

    $result = xml_to_array(Post($post_data, $target));

    if($result == NULL ) {
        $array['error_msg']="result error!";
        $array['error_code']="0";
        exit;
    }
    if($result['SubmitResult']['code']!=2) {

        $array['error_code']=$result['SubmitResult']['code'];
        $array['msg'] = $result['SubmitResult']['msg'];

        echo json_encode($array);
        exit;
        
    }else{
       
        $array['error_code']='2';
        $array["msg"]=$result['SubmitResult']['msg'];
        $array["smsid"]=$result['SubmitResult']['smsid'];
        
    }
    echo json_encode($array);exit;
}




function arraySequence($array, $field, $sort = 'SORT_DESC')
{
    $arrSort = array();
    foreach ($array as $uniqid => $row) {
        foreach ($row as $key => $value) {
            $arrSort[$key][$uniqid] = $value;
        }
    }
    array_multisort($arrSort[$field], constant($sort), $array);
    return $array;
}



function get_week($date){
    
    $date_str=date('Y-m-d',strtotime($date));
   
    $arr=explode("-", $date_str);
    
    $year=$arr[0];
    
    $month=sprintf('%02d',$arr[1]);
    
    $day=sprintf('%02d',$arr[2]);
    
    $hour = $minute = $second = 0;
    
    $strap = mktime($hour,$minute,$second,$month,$day,$year);
    
    $number_wk=date("w",$strap);
   
    $weekArr=array("星期日","星期一","星期二","星期三","星期四","星期五","星期六");
    
    return $weekArr[$number_wk];
}


function get_al_exchange_rate(){
    $host = "https://ali-waihui.showapi.com";
    $path = "/waihui-list";
    $method = "GET";
    $appcode = "eec76df71b61477b934e1d7e763ffa7f";
    $headers = array();
    array_push($headers, "Authorization:APPCODE " . $appcode);
    $querys = "bankCode=ICBC";
    $querys = "code=";
    $bodys = "";
    $url = $host . $path . "?" . $querys;

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_FAILONERROR, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, true);
    if (1 == strpos("$".$host, "https://"))
    {
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    }
    

    $response = curl_exec($curl);

    
    $response = strstr($response,'{');

    return json_decode($response,true);
}

function structure_al_exchange_data($key=" "){
    if(!isset($_SESSION['exchange']) || empty($_SESSION['exchange'])){       
        // echo 123;
        $data = get_al_exchange_rate();
        $data = $data['showapi_res_body']['list'];
        $arr = array();
        foreach ($data as $key => $value) {
            $arr[$value['code']]['currency_name'] = $value['code'];
            $arr[$value['code']]['buying_rate'] = $value['hui_in'];
            $arr[$value['code']]['cash_buying_rate'] = $value['chao_in'];
            $arr[$value['code']]['selling_rate'] = $value['hui_out'];
            $arr[$value['code']]['middle_rate'] = $value['zhesuan'];
            $arr[$value['code']]['pub_time'] = $value['day'].' '.$value['time'];
        }
    }else{
        $arr = $_SESSION['exchange'];
    }
  

    $_SESSION['exchange'] = $arr;
    if($key == " "){
        return $arr;
    }else{
        return $arr[$key];
    }

}

function rmb_trun_hkd($money){
    $exchange_data = structure_al_exchange_data('HKD');     
    $money = $money / $exchange_data['middle_rate'] * 100;
    return $money;
}




function mb_rtrim($string, $trim, $encoding)
{

    $mask = [];
    $trimLength = mb_strlen($trim, $encoding);
    for ($i = 0; $i < $trimLength; $i++) {
        $item = mb_substr($trim, $i, 1, $encoding);
        $mask[] = $item;
    }

    $len = mb_strlen($string, $encoding);
    if ($len > 0) {
        $i = $len - 1;
        do {
            $item = mb_substr($string, $i, 1, $encoding);
            if (in_array($item, $mask)) {
                $len--;
            } else {
                break;
            }
        } while ($i-- != 0);
    }

    return mb_substr($string, 0, $len, $encoding);
}


//月份转换英文
function Month_E($Num)
    {
        $Month_E = array('01' => "January",
        '02' => "February",
        '03' => "March",
        '04' => "April",
        '05' => "May",
        '06' => "June",
        '07' => "July",
        '08' => "August",
        '09'=> "September",
        '10' => "October",
        '11' => "November",
        '12' => "December");
        return $Month_E[$Num];
    }



function to_sql_new($data, $front = ' AND ', $in_column = false) {
    if($in_column && is_array($data)) {
        $ids = '\''.implode('\',\'', $data).'\'';
        $sql = "$in_column IN ($ids)";
        return $sql;
    } else {
        if ($front == '') {
            $front = ' AND ';
        }
        if(is_array($data) && count($data) > 0) {
            $sql = '';
            foreach ($data as $key => $val) {
                $sql .= $sql ? " $front `$key` = '$val' " : " `$key` = '$val' ";
            }
            return $sql;
        } else {
            $sql = "$in_column IN ($data)";
            return $sql;
        }
    }
}
?>