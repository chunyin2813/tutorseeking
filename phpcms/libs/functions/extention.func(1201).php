<?php
/**
 *  extention.func.php 用户自定义函数库
 *
 * @copyright			(C) 2017-2020 Teach
 * @license				http://www.teach.com
 * @lastmodify 			2017-09-07 15:16 WCJM	(最后修改时间)
 */


//调试函数
function dump($data,$type=true){
    echo "<pre>";
    print_r($data);
    echo "</pre>";
    if($type==true){
        exit;
    }
}

//xml格式转换数组
function xml_array($xml){

    $arr = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);
              // var_dump($arr);exit;
    $lists = json_decode(json_encode($arr),true);
    return $lists;
}

//把空格转成+号
function emptyreplace($str) {
    $str = str_replace('　', ' ', $str); //替换全角空格为半角
    $str = str_replace(' ', ' ', $str); //替换连续的空格为一个
    $noe = false; //是否遇到不是空格的字符
    for ($i=0 ; $i<strlen($str); $i++) { //遍历整个字符串
        if($noe && $str[$i]==' ') $str[$i] = '+'; //如果当前这个空格之前出现了不是空格的字符
        elseif($str[$i]!=' ') $noe=true; //当前这个字符不是空格，定义下 $noe 变量
    }
    return $str;
}

//加密
function do_mencrypt($string,$key){

    $size = mcrypt_get_block_size ( MCRYPT_DES, MCRYPT_MODE_CBC );

    $string = pkcs5Pad ( $string, $size );
    // echo $string;exit;
    $data  =  base64_encode(mcrypt_encrypt(MCRYPT_DES,$key,$string,MCRYPT_MODE_CBC,$key));
    return $data;
}
//解密
function do_mdecrypt($string,$key){
    $string = base64_decode($string);
    $result = mcrypt_decrypt(MCRYPT_DES, $key, $string, MCRYPT_MODE_CBC, $key);
    $result = pkcs5Unpad( $result );
    return $result;
}
function pkcs5Pad($text, $blocksize){
    $pad = $blocksize - (strlen ( $text ) % $blocksize);
    return $text . str_repeat ( chr ( $pad ), $pad );
}
function pkcs5Unpad($text){
    $pad = ord ( $text {strlen ( $text ) - 1} );
    if ($pad > strlen ( $text ))
        return false;
    if (strspn ( $text, chr ( $pad ), strlen ( $text ) - $pad ) != $pad)
        return false;
    return substr ( $text, 0, - 1 * $pad );
}



/**
 * 模拟post进行url请求
 * @param string $url
 * @param string $param
 */
function request_post($url = '', $param = '') {
    if (empty($url) || empty($param)) {
        return false;
    }

    $postUrl = $url;
    $curlPost = $param;
    $ch = curl_init();//初始化curl
    curl_setopt($ch, CURLOPT_URL,$postUrl);//抓取指定网页
    curl_setopt($ch, CURLOPT_HEADER, 0);//设置header
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);//要求结果为字符串且输出到屏幕上
    curl_setopt($ch, CURLOPT_POST, 1);//post提交方式
    curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
    $data = curl_exec($ch);//运行curl
    curl_close($ch);

    return $data;
}



/**
 * 模拟post进行url请求
 * @param string $url
 * @param array $post_data
 */
function request_post_array($url = '', $post_data = array(),$ty='1') {
    if (empty($url) || empty($post_data)) {
        return false;
    }
    $o = "";
    foreach ( $post_data as $k => $v )
    {
        $o.= "$k=" . urlencode( $v ). "&" ;
    }
    $post_data = substr($o,0,-1);

    $postUrl = $url;
    $curlPost = $post_data;
    $ch = curl_init();//初始化curl
    if($ty == 2) {
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);  // 从证书中检查SSL加密算法是否存在
    }
    curl_setopt($ch, CURLOPT_URL,$postUrl);//抓取指定网页
    curl_setopt($ch, CURLOPT_HEADER, 0);//设置header
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);//要求结果为字符串且输出到屏幕上
    curl_setopt($ch, CURLOPT_POST, 1);//post提交方式
    curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
    $data = curl_exec($ch);//运行curl
    curl_close($ch);
    return $data;
}

/** 《添加 把中文转成简体》
 * @param[string or array] [$data] 要转换的数据
 * @return [string or array]  [$data]
 * @author   《WH》
 */
function simplified_conversion($data){
    $change = pc_base::load_model('FanJianConvert');//加载简繁转换类
    if(is_array($data)){    //判断数据是数组还是字符串

        foreach ($data as $key => $value) {     //国家

            $data[$key]['areaname'] = $change->tradition2simple($value['areaname']);

            foreach ($value['submenu'] as $ke => $val) {    //城市

                $data[$key]['submenu'][$ke]['areaname'] = $change->tradition2simple($val['areaname']);

                foreach ($val['submenu'] as $k => $v) {    //地区

                    $data[$key]['submenu'][$ke]['submenu'][$k]['areaname'] = $change->tradition2simple($v['areaname']);

                }
            }
        }
    }else{
        $data = $change->tradition2simple($data);
    }
    return $data;
}

/**
 * [通过汉字的ASCII码来判断汉字的首字母的方法]
 * @param  [type] $str [中文字体]
 * @return [type]      [第一个字的首字母]
 */
function get_first_letter($str){
    if (empty($str)) {
        return '';
    }
    $change = pc_base::load_model('FanJianConvert');//加载简繁转换类
    $str = $change->tradition2simple($str);
    $fchar = ord($str{0});
    if ($fchar >= ord('A') && $fchar <= ord('z')) return strtoupper($str{0});
    $s1 = iconv('UTF-8', 'gb2312', $str);
    // $s2 = iconv('gb2312', 'UTF-8', $s1);
    // $s = $s2 == $str ? $s1 : $str;
    $asc = ord($s1{0}) * 256 + ord($s1{1}) - 65536;
    if ($asc >= -20319 && $asc <= -20284) return 'A';
    if ($asc >= -20283 && $asc <= -19776) return 'B';
    if ($asc >= -19775 && $asc <= -19219) return 'C';
    if ($asc >= -19218 && $asc <= -18711) return 'D';
    if ($asc >= -18710 && $asc <= -18527) return 'E';
    if ($asc >= -18526 && $asc <= -18240) return 'F';
    if ($asc >= -18239 && $asc <= -17923) return 'G';
    if ($asc >= -17922 && $asc <= -17418) return 'H';
    if ($asc >= -17417 && $asc <= -16475) return 'J';
    if ($asc >= -16474 && $asc <= -16213) return 'K';
    if ($asc >= -16212 && $asc <= -15641) return 'L';
    if ($asc >= -15640 && $asc <= -15166) return 'M';
    if ($asc >= -15165 && $asc <= -14923) return 'N';
    if ($asc >= -14922 && $asc <= -14915) return 'O';
    if ($asc >= -14914 && $asc <= -14631) return 'P';
    if ($asc >= -14630 && $asc <= -14150) return 'Q';
    if ($asc >= -14149 && $asc <= -14091) return 'R';
    if ($asc >= -14090 && $asc <= -13319) return 'S';
    if ($asc >= -13318 && $asc <= -12839) return 'T';
    if ($asc >= -12838 && $asc <= -12557) return 'W';
    if ($asc >= -12556 && $asc <= -11848) return 'X';
    if ($asc >= -11847 && $asc <= -11056) return 'Y';
    if ($asc >= -11055 && $asc <= -10247) return 'Z';
    return null;
}

/**
 * 《添加 获取随机字符串》
 * @param  $length    [字符串长度]
 * @rerurn $string;
 */
function generate_password( $length = 8,$type=1 ) {
    // 密码字符集，可任意添加你需要的字符
    if($type != 1){
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    }else{
        $chars = '0123456789';
    }

    $password = '';
    for ( $i = 0; $i < $length; $i++ )
    {
        // 这里提供两种字符获取方式
        // 第一种是使用 substr 截取$chars中的任意一位字符；
        // 第二种是取字符数组 $chars 的任意元素
        // $password .= substr($chars, mt_rand(0, strlen($chars) – 1), 1);
        $password .= $chars[ mt_rand(0, strlen($chars) - 1) ];
    }
    return $password;
}


/**
 * 发送验证码函数 start
 **/
function Post($curlPost,$url){
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_NOBODY, true);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $curlPost);
    $return_str = curl_exec($curl);
    curl_close($curl);
    return $return_str;
}
//将 xml数据转换为数组格式。
function xml_to_array($xml){
    // var_dump($xml);exit;
    $reg = "/<(\w+)[^>]*>([\\x00-\\xFF]*)<\\/\\1>/";
    if(preg_match_all($reg, $xml, $matches)){
        $count = count($matches[0]);
        for($i = 0; $i < $count; $i++){
            $subxml= $matches[2][$i];
            $key = $matches[1][$i];
            if(preg_match( $reg, $subxml )){
                $arr[$key] = xml_to_array( $subxml );
            }else{
                $arr[$key] = $subxml;
            }
        }
    }
    return $arr;
}

//发送验证码
function sendTemplateSMS($post_data,$target,$user){

    $result = xml_to_array(Post($post_data, $target));
//     var_dump($result);exit;
    if($result == NULL ) {
        $array['error_msg']="result error!";
        $array['error_code']="0";
        exit;
    }
    if($result['SubmitResult']['code']!=2) {

        $array['error_code']=$result['SubmitResult']['code'];
        $array['msg'] = $result['SubmitResult']['msg'];

        echo json_encode($array);
        exit;
        //TODO 添加错误处理逻辑
    }else{
        // echo "Sendind TemplateSMS success!<br/>";
        // 获取返回信息
        $array['error_code']='2';
        $array["msg"]=$result['SubmitResult']['msg'];
        $array["smsid"]=$result['SubmitResult']['smsid'];
        // return 1;
        //TODO 添加成功处理逻辑
    }
    echo json_encode($array);exit;
}



/**
 * 二维数组根据字段进行排序
 * @params array $array 需要排序的数组
 * @params string $field 排序的字段
 * @params string $sort 排序顺序标志 SORT_DESC 降序；SORT_ASC 升序
 */
function arraySequence($array, $field, $sort = 'SORT_DESC')
{
    $arrSort = array();
    foreach ($array as $uniqid => $row) {
        foreach ($row as $key => $value) {
            $arrSort[$key][$uniqid] = $value;
        }
    }
    array_multisort($arrSort[$field], constant($sort), $array);
    return $array;
}


/**
 * 时间格式获取星期
 * @param  [type] $date [Ymd时间格式]
 * @return [type]       [星期]
 */
function get_week($date){
    //强制转换日期格式
    $date_str=date('Y-m-d',strtotime($date));
    //封装成数组
    $arr=explode("-", $date_str);
    //参数赋值
    //年
    $year=$arr[0];
    //月，输出2位整型，不够2位右对齐
    $month=sprintf('%02d',$arr[1]);
    //日，输出2位整型，不够2位右对齐
    $day=sprintf('%02d',$arr[2]);
    //时分秒默认赋值为0；
    $hour = $minute = $second = 0;
    //转换成时间戳
    $strap = mktime($hour,$minute,$second,$month,$day,$year);
    //获取数字型星期几
    $number_wk=date("w",$strap);
    //自定义星期数组
    $weekArr=array("星期日","星期一","星期二","星期三","星期四","星期五","星期六");
    //获取数字对应的星期
    return $weekArr[$number_wk];
}

/**
 *阿里云获取汇率api
 *"hui_in": "6.1374", //现汇买入价
 *"chao_out": "6.1811",//现钞卖出价
 *"chao_in": "5.9798",//现钞买入价
 *"hui_out": "6.1811",//现汇卖出价
 *"name": "日元",
 *"mid_price": "--", //中间价
 * "middle_rate" : 折算价(汇率)
 *"code": "JPY"
 */
function get_al_exchange_rate(){
    $host = "https://ali-waihui.showapi.com";
    $path = "/waihui-list";
    $method = "GET";
    $appcode = "eec76df71b61477b934e1d7e763ffa7f";
    $headers = array();
    array_push($headers, "Authorization:APPCODE " . $appcode);
    $querys = "bankCode=ICBC";
    $querys = "code=";
    $bodys = "";
    $url = $host . $path . "?" . $querys;

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_FAILONERROR, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, true);
    if (1 == strpos("$".$host, "https://"))
    {
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    }
    // dump(curl_exec($curl));

    $response = curl_exec($curl);

    // if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == '200') {
    //     $headerSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    //     echo $headerSize;exit;
    //     $header = substr($response, 0, $headerSize);
    //     $body = substr($response, $headerSize);
    // }
    $response = strstr($response,'{');

    return json_decode($response,true);
}
/**
 * 重构阿里云汇率api数据
**/
function structure_al_exchange_data($key=" "){
    if(!isset($_SESSION['exchange']) || empty($_SESSION['exchange'])){       //判断是否存在汇率信息
        // echo 123;
        $data = get_al_exchange_rate();//调用阿里云汇率api
        $data = $data['showapi_res_body']['list'];
        $arr = array();
        foreach ($data as $key => $value) {
            $arr[$value['code']]['currency_name'] = $value['code'];
            $arr[$value['code']]['buying_rate'] = $value['hui_in'];
            $arr[$value['code']]['cash_buying_rate'] = $value['chao_in'];
            $arr[$value['code']]['selling_rate'] = $value['hui_out'];
            $arr[$value['code']]['middle_rate'] = $value['zhesuan'];
            $arr[$value['code']]['pub_time'] = $value['day'].' '.$value['time'];
        }
    }else{
        $arr = $_SESSION['exchange'];
    }
  

    $_SESSION['exchange'] = $arr;
    if($key == " "){
        return $arr;
    }else{
        return $arr[$key];
    }

}
/**
 * 人民币转港币
 * $money  string [转换金额]
 * return string [转换后的金额]
 **/
function rmb_trun_hkd($money){
    $exchange_data = structure_al_exchange_data('HKD');     //获取币种资料
    $money = $money / $exchange_data['middle_rate'] * 100;
    return $money;
}
?>