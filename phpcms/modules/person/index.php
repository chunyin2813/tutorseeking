<?php
//学生个人中心
defined('IN_PHPCMS') or exit('No permission resources.');
$session_storage = 'session_'.pc_base::load_config('system','session_storage');
pc_base::load_sys_class($session_storage);
pc_base::load_app_class('tutor' ,'pair');
pc_base::load_app_class('inte' ,'integration');
class index {
    function __construct() {
        header("Content-type: text/html; charset=utf-8");
    	$this->db = pc_base::load_model('member_model');
        $this->e_db = pc_base::load_model('sys_tutor_achieve_model');
        $this->s_db=pc_base::load_model('student_transaction_model');
        $this->p_db=pc_base::load_model('pair_model');
        //判断是否登陆
        if(isset($_SESSION['id']) && !empty($_SESSION['id'])){
            $id = $_SESSION['id'];
        }else{
           showmessage('請登録!','./index.php');
        }
    }

    //个人中心首页页面
    public function init(){ 
        //联动要求
      
        //得到系统设定的礼品兑换积分
        $inte=new inte();
        $gift_inte=$inte->get_inte_setting(10);

        
        //学生
        $id = $_SESSION['id'];
        // $id = 1;
        $admin_sql = "select a.personid,a.integral,a.userid,a.mobile,a.username,a.userimage,a.phone,a.email,s.student_address,s.student_locid,s.student_name,s.student_contactname,s.student_id from v9_member as a
            left join v9_student_master as s
            on a.userid = s.student_userid 
            where a.userid = $id and s.student_address!='' order by a.userid asc";   

        $admin_info = $this->db->query($admin_sql);
        $admin_list = $this->db->fetch_array();

        $sql  = "select t.st_id,t.st_new_name,t.st_new_nickname,t.st_ttid_2,t.st_ttid_1,st_matched,st_state from v9_student_transaction as t
            left join v9_student_master as m
            on m.student_id = t.st_studentid
            where m.student_userid = $id";    
        $sql= $sql.$where." order by m.student_id desc";
        $this->db = pc_base::load_model('student_master_model');
        $infos = $this->db->fetch_array($this->db->query($sql));
       

        $pages = $get_page->pages;

        //学生个案总数
        $sql_stu = "select count(*) as count from v9_student_master where student_userid = $id";
       
        $this->p_db->query($sql_stu);
        $num = $this->p_db->fetch_array();

        //已匹配个案
        $sql_stus ="select count(*) as count from v9_pair as p
            left join v9_student_transaction as s
            on p.pair_student_tranid = s.st_id
            left join v9_student_master as m
            on s.st_studentid = m.student_id
            where m.student_userid = '$id' and p.pair_state = '1' ";

        $this->p_db->query($sql_stus);
        $nums = $this->p_db->fetch_array();

   
        $this->act_db = pc_base::load_model('parent_act_model');
        $act_sql = "select r.par_status,m.username,m.personid,p.act_number,p.act_name,p.act_cut_date,p.act_closing_date from `v9_parent_act` as p 
        left join `v9_parent_act_rev` as r
        on r.par_act_num = p.act_number
        left join `v9_member` as m 
        on p.act_userid = m.userid
        where m.userid = ".$id;
        $act_sql.=" order by p.act_id desc";
        $act_infos = $this->act_db->fetch_array($this->act_db->query($act_sql));

     
        $join_sql = "select act_cut_date,act_userid,act_number,act_join_parentid from `v9_parent_act` order by act_id desc";
        $this->act_db->query($join_sql);
        $join_infos = $this->act_db->fetch_array();
        foreach ($join_infos as $k => $v) {
            if(empty($v['act_join_parentid'])){
                unset($v);
            }
            $join_arrs[] = $v;
        }
       
        $join_arrs = array_filter($join_arrs);
        if(!empty($join_arrs)){
            sort($join_arrs);
         
            foreach ($join_arrs as $k => $v) {
                $join_parentids  = $v['act_join_parentid'];
                $join_parentid_arr['join_parentid']  = explode(',', $join_parentids);
                $join_parentid_arr['act_number']  =  $v['act_number'];
              
                foreach ($join_parentid_arr as $k => $v) {
                    if(in_array($id, $v)){
                  
                        $user_act_sql = "select p.act_cut_date,p.act_number,p.act_name,p.act_date,p.act_closing_date,p.act_address,m.personid from `v9_parent_act` 
                        as p
                        left join  `v9_member` as m
                        on p.act_userid = m.userid
                        left join `v9_parent_act_rev` as r
                        on p.act_number = r.par_act_num
                        where r.par_status = '1' and act_number = "."'".$join_parentid_arr["act_number"]."'"." order by p.act_id desc";
                        $user_act_infos = $this->act_db->fetch_array($this->act_db->query($user_act_sql));
                        $user_act_all_info[] = $user_act_infos;

                  
                    }
                }
            }
        }
   
        $user_act_all_info = array_filter(array_reverse($user_act_all_info));
      
        include template('person', 'index');
     }

  

    public function index_edit(){
        $edit_info = $_POST['edit_info'];
        $id = isset($_POST['student_id'])?$_POST['student_id']:$_POST['userid'];
        $this->ma_db=pc_base::load_model('student_master_model');
      
        if(isset($edit_info['mobile'])){
            $this->m_db = pc_base::load_model('student_master_model');
            $res = $this->db->update(array('mobile'=>$edit_info['mobile']),array('userid'=>$id));
            $res2 = $this->m_db->update(array('student_housemobile'=>$edit_info['mobile']),array('student_userid'=>$id));
            if($res && $res2){
                echo "1";
                exit;
            }else{
                echo "2";
                exit;
            }
        }

        if(isset($edit_info['tutor_address_locid'])){
            $res = $this->ma_db->update(array('student_locid'=>$edit_info['tutor_address_locid']),array('student_id'=>$id));
            if($res){
                echo "1";
                exit;
            }else{
                echo "2";
                exit;
            }
        }
   

        if(isset($edit_info['student_address'])){
            $res = $this->ma_db->update(array('student_address'=>$edit_info['student_address']),array('student_id'=>$id));
            if($res){
                echo "1";
                exit;
            }else{
                echo "2";
                exit;
            }
        }   
    }

    public function edit(){
        $id = $_SESSION['id'];
   
        $st_id  = $_GET['id'];
        $info_sql = "select s.student_school,m.userid,m.userimage,t.st_new_name,t.st_new_nickname,s.student_id,s.student_locid,t.st_min_grade,s.tutor_res from v9_student_transaction as t
            left join v9_student_master as s
            on t.st_studentid = s.student_id 
            left join v9_member as m
            on s.student_userid = m.userid
            where m.userid = $id and t.st_id = '$st_id' ";
        $info = $this->db->query($info_sql);
        $list = $this->db->fetch_array();

        $time_sql = "select sst_weekday ,sst_fromtime,sst_totime from v9_student_sel_time where sst_stid = '$st_id' ";
        $this->time=pc_base::load_model('student_sel_time_model');
        $this->time->query($time_sql);
        $time_info = $this->time->fetch_array();
   
        $fromtime = explode(',',$time_info[0]['sst_fromtime']); 
        $totime= explode(',',$time_info[0]['sst_totime']);
        $tst_weekday= explode(',',$time_info[0]['sst_weekday']);
        $weeknums = array("一","二","三","四","五","六","日");

        $day = array(array(),array(),array(),array(),array(),array(),array());

        for($i =0;$i<7;$i++){
            $key = $tst_weekday[$i];
            $day[$key] = array($weeknums[$key],$fromtime[$i],$totime[$i]);
        }

    
        $day1 = $day[0];
    
        $day2 = $day[1];
        $day3 = $day[2];
        $day4 = $day[3];
        $day5 = $day[4];
        $day6 = $day[5];
        $day7 = $day[6];

        $seek_sql = "select st_ttid_1,st_ttid_2,st_ttid_3,st_num_join_m,st_num_join_f,st_class_hour,st_tutor_fee,st_area,like_collage,st_req_sex,st_main_lang from v9_student_transaction where st_id = '$st_id'";
        $this->s_db->query($seek_sql);
        $seek_info = $this->s_db->fetch_array();
        

        pc_base::load_sys_class('form','', 0);
        $timelist = array(
        '0700' =>"7:00" ,'0715' =>"7:15" ,'0730' =>"7:30",'0745' =>"7:45",
        '0800' =>"8:00" ,'0815' =>"8:15" ,'0830' =>"8:30" ,'0845' =>"8:45",
        '0900' =>"9:00" ,'0915' =>"9:15" ,'0930' =>"9:30" ,'0945' =>"9:45",
        '1000' =>"10:00" ,'1015' =>"10:15" ,'1030' =>"10:30" ,'1045' =>"10:45",
        '1100' =>"11:00" ,'1115' =>"11:15" ,'1130' =>"11:30" ,'1145' =>"11:45",
        '1200' =>"12:00" ,'1215' =>"12:15" ,'1230' =>"12:30" ,'1245' =>"12:45",
        '1300' =>"13:00" ,'1315' =>"13:15" ,'1330' =>"13:30" ,'1345' =>"13:45",
        '1400' =>"14:00" ,'1415' =>"14:15" ,'1430' =>"14:30" ,'1445' =>"14:45",
        '1500' =>"15:00" ,'1515' =>"15:15" ,'1530' =>"15:30" ,'1545' =>"15:45",
        '1600' =>"16:00" ,'1615' =>"16:15" ,'1630' =>"16:30" ,'1645' =>"16:45",
        '1700' =>"17:00" ,'1715' =>"17:15" ,'1730' =>"17:30" ,'1745' =>"17:45",
        '1800' =>"18:00" ,'1815' =>"18:15" ,'1830' =>"18:30" ,'1845' =>"18:45",
        '1900' =>"19:00" ,'1915' =>"19:15" ,'1930' =>"19:30" ,'1945' =>"19:45",
        '2000' =>"20:00" ,'2015' =>"20:15" ,'2030' =>"20:30" ,'2045' =>"20:45",
        '2100' =>"21:00" ,'2115' =>"21:15" ,'2130' =>"21:30" ,'2145' =>"21:45",
        '2200' =>"22:00" ,'2215' =>"22:15" ,'2230' =>"22:30" ,'2245' =>"22:45",
        '2300' =>"23:00");
        
        $arr_ccc = array('0.75'=>'45分鐘','1'=>'1小時','1.5'=>'1.5小時','2'=>'2小時','2.5'=>'2.5小時','3'=>'3小時','3.5'=>'3.5小時','4'=>'4小時','4.5'=>'4.5小時','5'=>'5小時');

        $arr_class = array('普通話'=>'普通話','英語'=>'英語','廣東話'=>'廣東話','其他語言'=>'其他語言');

    

        include template('person', 'edit');
     }

    public function update(){
        if(isset($_POST['info'])){

            foreach ($_POST['time'] as $key => $a_time) {
                if(empty($a_time['from_hour'])){
                    unset($_POST['time'][$key]);
                }        
            }
            $week_arr = array('mon' => 0,'tues' => 1,'wed' => 2,'thur' => 3,'fri' => 4,'sat' => 5,'sun' => 6 );
            foreach ($_POST['time'] as $k=>$v) {
               $k=$week_arr[$k];
               $b[]=$k;
            }
            $week=implode(',', $b);
    

            $info=$_POST['info'];
            $id = $_POST['userid'];
            $st_id = $_POST['st_id'];
            pc_base::load_app_func('global'); 
            $regip=ip();
       
            $student_id = $_POST['student_id'];
            $this->s_db = pc_base::load_model('student_master_model');
            $mas_where['student_id'] = $student_id;
            $mas_data['student_name'] = $info['student_name'];
            $mas_data['tutor_res'] = $info['tutor_res'];
            $mas_data['student_school'] = $info['student_school'];
            $res_master=$this->s_db->update($mas_data, $mas_where);

            if($res_master){
                 $re_info=$_POST['re_info'];
          
                $re_info['st_tutor_fee']=(int)($_POST['st_tutor_fee']);

             
                $this->stu_tran_db=pc_base::load_model('student_transaction_model'); 
                $re_where['st_id']=$st_id; 
                $re_info['st_num_join']=$re_info['st_num_join_m']+$re_info['st_num_join_f'];
                $re_info['st_new_name'] = $info['student_name'];
                $re_info['st_new_nickname']  = $_POST['nickname'];
                $re_info['st_min_grade']  = $re_info['st_min_grade'];
                $req_res=$this->stu_tran_db->update($re_info,$re_where);

               
                if($req_res){
                    $s_id['st_id'] = $st_id;
                    $s_id['student_id'] = $student_id;
                    $s_id['userid'] = $id;
            
                   
                    if(!empty($re_info['st_ttid_1'])){$type =$re_info['st_ttid_1'];}
                    if(!empty($re_info['st_ttid_2'])){$item =$re_info['st_ttid_2'];}
                    if(!empty($re_info['st_ttid_3'])){$level =$re_info['st_ttid_3'];}
                    if(!empty($info['student_locid'])){$place =$info['student_locid'];}
                    if(!empty($re_info['st_class_hour'])){$classtime =$re_info['st_class_hour'];}
                    if(!empty($re_info['st_tutor_fee'])){$hourlyfee =$re_info['st_tutor_fee'];}
                    if(!empty($re_info['st_min_grade'])){$miniEduLevel =$re_info['st_min_grade'];}
                    if(!empty($re_info['st_req_sex'])&& $re_info['st_req_sex']!='A'){$gender =$re_info['st_req_sex'];}
                    
                    if(!empty($re_info['like_collage'])){$like_collage =$re_info['like_collage'];}

                    $tutor=new tutor();
                    $teacherlist=$tutor->getSuitedTeacher($type,$item,$level,$place,$classtime,$hourlyfee,$miniEduLevel,$gender,$classlang,$like_collage,1,1,'','','',$week);

                    $list=array();
                    foreach ($teacherlist['data'] as $key=> $teacher) {
                        $star=$tutor->return_star($teacher['master_id'],$teacher['role_verify']);
                        $teacher['star']=$star;
                        
                  
                        pc_base::load_app_class('helpcommon' ,'pair');
                        $helpcommon=new helpcommon();
                        $place=$helpcommon->create_location($teacher['tsl_locid']);
                        $aa=explode(',',$place);
                        foreach($aa as $key=>$a){
                            if($key>=3){
                                $place=$aa[0].','.$aa[1].','.$aa[2].'...';
                            }
                        }
                        $teacher['teach_area']=$place;

                 
                        $subject=$helpcommon->create_subject($teacher['tstt_ttid_2']);
                        
                        $subname=explode(',',$subject);
                        if(count($subname)>=3){
                            $subname=$subname[0].','.$subname[1].','.$subname[2].'...';
                        }else{
                            foreach($subname as $sub){
                                $subname=$sub.',';
                            }  
                        }
                        $teacher['teach_type']=$subname;

                        $weeknums = array('6'=> "S",'0'=> "M",'1'=> "T",'2'=> "W",'3'=> "T",'4'=> "F",'5'=> "S");
                        //得到可教授時間
                        if($teacher['tst_weekday']==""){
                            foreach($weeknums as $num){
                                 $teacher['teach_time'].="<span>".$num."</sapn>";
                            }
                        }else{
                            
                            $week=explode(",",$teacher['tst_weekday']);
                            foreach($weeknums as $k=>$num){
                            $str="";
                            if(strpos($teacher['tst_weekday'],strval($k)) !== false ){
                                $str = "style='color: black'";
                            }
                            $teacher['teach_time'].="<span ".$str.">".$num."</span>";
                            }
                        }

                        //確定此個案未配對還是在配對中
                        $pairid=$this->p_db->get_one("pair_tutor_tranid = '$teacher[tm_tutorid]'");
                        if(!empty($pairid)){
                            $teacher['match_area']="<div class='course-match'>配對中</div>";
                        }else{
                            $teacher['match_area']="";
                        }

                        $list[]=$teacher;
                    }
           
                    $mas_arr[0]=$s_id;
                    $mas_arr[1]=$list[0];
                    echo json_encode($mas_arr);
                    exit;
                }else{
                    return;
                } 
            }else{
                return;
            }   
     }else{
            $mas_arr[0] = 'fail';
            echo json_encode($mas_arr);
            return;
        } 
    }
   
  
    public function times(){
        $time_a=$_POST['time_a'];
        $time=$_POST['time_arr'];
        $time_where['sst_stid'] = $time_a['st_id'];
        $time_where['sst_studentid'] = $time_a['student_id'];
        if(empty($time)){return '';}
        $time = explode("],", $time); 
 
        $reg="/\d+/";
        for($i=0;$i<count($time);$i++){
            preg_match_all($reg,$time[$i],$time_arr[$i]);
        }
  
        foreach ($time_arr as $key =>$v){  
            $new_arr[]=$v[0]; 
        }
        foreach($new_arr as $k=>$v){
      
            if(!$v){//判断是否为空（false）
                unset($new_arr[$k]);//删除
            }
        }
        //所有星期为空
        if(!$new_arr){
            unset($new_arr);
        }
        foreach ($new_arr as $k => $v) {
            $temp[]=(count($v));
            $pos=array_search(max($temp),$temp);
            $index= $temp[$pos];
            for ($i=0; $i < $index-1 ; $i+=2) { 
                $fromtime[$k][]=$new_arr[$k][$i];
                $totime[$k][]=$new_arr[$k][$i+1];
            }
        }
  
        $time_re['sst_weekday']=array_keys($fromtime);
        foreach ($fromtime as $key => $value) {
            $time_re['sst_fromtime'][$key]=trim(implode(';', $value),';');
        }
        foreach ($totime as $key => $value) {
            $time_re['sst_totime'][$key]=trim(implode(';', $value),';');
        }
        $time_re_arr['sst_fromtime']=implode(',', $time_re['sst_fromtime']);
        $time_re_arr['sst_totime']=implode(',', $time_re['sst_totime']);
        $time_re_arr['sst_weekday']=implode(',', $time_re['sst_weekday']);
        
        $this->time_db = pc_base::load_model('student_sel_time_model');
        $time_res=$this->time_db->update($time_re_arr,$time_where);
        if($time_res){
            echo "0";
            exit; 
        }else{
            echo "1";
            exit;
        }
    }  


    public function add(){
        //联动要求
        if(isset($_POST['tt_id'])){
            $this->tt_db= pc_base::load_model('sys_tutor_type_model');
            $tt_infos=$this->tt_db->select(array('tt_parentid'=>$_POST['tt_id']));
            $tt_infos=json_encode($tt_infos);
            echo $tt_infos;
            exit;
        }
        $id = $_SESSION['id'];
  
        $info_sql  = "select student_address,userid,username,nickname,userimage,phone,email,student_locid from v9_member as member
            left join v9_student_master as master on member.userid = master.student_userid  where userid = $id limit 1";
        $info = $this->db->query($info_sql);
        $list = $this->db->fetch_array();
        pc_base::load_sys_class('form','', 0);
        $timelist = array(
        '0700' =>"7:00" ,'0715' =>"7:15" ,'0730' =>"7:30",'0745' =>"7:45",
        '0800' =>"8:00" ,'0815' =>"8:15" ,'0830' =>"8:30" ,'0845' =>"8:45",
        '0900' =>"9:00" ,'0915' =>"9:15" ,'0930' =>"9:30" ,'0945' =>"9:45",
        '1000' =>"10:00" ,'1015' =>"10:15" ,'1030' =>"10:30" ,'1045' =>"10:45",
        '1100' =>"11:00" ,'1115' =>"11:15" ,'1130' =>"11:30" ,'1145' =>"11:45",
        '1200' =>"12:00" ,'1215' =>"12:15" ,'1230' =>"12:30" ,'1245' =>"12:45",
        '1300' =>"13:00" ,'1315' =>"13:15" ,'1330' =>"13:30" ,'1345' =>"13:45",
        '1400' =>"14:00" ,'1415' =>"14:15" ,'1430' =>"14:30" ,'1445' =>"14:45",
        '1500' =>"15:00" ,'1515' =>"15:15" ,'1530' =>"15:30" ,'1545' =>"15:45",
        '1600' =>"16:00" ,'1615' =>"16:15" ,'1630' =>"16:30" ,'1645' =>"16:45",
        '1700' =>"17:00" ,'1715' =>"17:15" ,'1730' =>"17:30" ,'1745' =>"17:45",
        '1800' =>"18:00" ,'1815' =>"18:15" ,'1830' =>"18:30" ,'1845' =>"18:45",
        '1900' =>"19:00" ,'1915' =>"19:15" ,'1930' =>"19:30" ,'1945' =>"19:45",
        '2000' =>"20:00" ,'2015' =>"20:15" ,'2030' =>"20:30" ,'2045' =>"20:45",
        '2100' =>"21:00" ,'2115' =>"21:15" ,'2130' =>"21:30" ,'2145' =>"21:45",
        '2200' =>"22:00" ,'2215' =>"22:15" ,'2230' =>"22:30" ,'2245' =>"22:45",
        '2300' =>"23:00");
        
        $arr_class = array('普通話'=>'普通話','英語'=>'英語','廣東話'=>'廣東話','其他語言'=>'其他語言');

        include template('person', 'add');
    }

    public  function add_add(){

        if(isset($_POST['info'])){
       
            foreach ($_POST['time'] as $key => $a_time) {
                if(empty($a_time['from_hour'])){
                    unset($_POST['time'][$key]);
                }        
            }
            $week_arr = array('mon' => 0,'tues' => 1,'wed' => 2,'thur' => 3,'fri' => 4,'sat' => 5,'sun' => 6 );
            foreach ($_POST['time'] as $k=>$v) {
               $k=$week_arr[$k];
               $b[]=$k;
            }
            $week=implode(',', $b);
         
            
            $info=$_POST['info'];
            $id = $_POST['userid'];
            $email = $_POST['email'];
            $phone = $_POST['phone'];
            pc_base::load_app_func('global'); 
            $regip=ip();

    
            $this->s_db = pc_base::load_model('student_master_model');
            $mas_data['student_userid'] = $id;
            $mas_data['student_name'] = $info['student_name'];
            $mas_data['student_contactname'] = $_POST['nickname'];
            $mas_data['student_locid'] = $info['student_locid'];
            $mas_data['student_school'] = $info['student_school'];
            $mas_data['tutor_res'] = $info['tutor_res'];
            $mas_data['student_mobile'] = $phone;
            $mas_data['student_email'] = $email;
            $mas_data['student_address'] = $_POST['student_address'];
            $res_master=$this->s_db->insert($mas_data);

             $re_info=$_POST['re_info'];
            //学费
            $re_info['st_tutor_fee']=(int)($_POST['st_tutor_fee']);

           
            $info_t = $this->db->query("select last_insert_id()");
            $list_t = $this->db->fetch_array();
            $student_id = $list_t[0]['last_insert_id()'];

            $this->stu_tran_db=pc_base::load_model('student_transaction_model'); 
            $re_info['st_studentid']=$student_id; 
            $re_info['st_num_join']=$re_info['st_num_join_m']+$re_info['st_num_join_f'];
            $re_info['st_area']=$info['student_locid'];
            $year=substr(date("Y"),-2);
            $newNumber = substr(strval("572"+$student_id+100000),1,5);
            $re_info['st_id']="P".$year.$newNumber;

            $re_info['st_new_name'] = $info['student_name'];
            $re_info['st_new_nickname']  = $_POST['nickname'];
          
            $req_res=$this->stu_tran_db->insert($re_info);

            
            $st_id = $re_info['st_id'];
            if($req_res){
                $s_id['st_id'] = $st_id;
                $s_id['student_id'] = $student_id;
                $s_id['userid'] = $id;

              
                if(!empty($info['student_locid'])){$place =$info['student_locid'];}
                if(!empty($re_info['st_ttid_1'])){$type =$re_info['st_ttid_1'];}
                if(!empty($re_info['st_ttid_2'])){$item =$re_info['st_ttid_2'];}
                if(!empty($re_info['st_ttid_3'])){$level =$re_info['st_ttid_3'];}
                if(!empty($re_info['st_class_hour'])){$classtime =$re_info['st_class_hour'];}
                if(!empty($re_info['st_tutor_fee'])){$hourlyfee =$re_info['st_tutor_fee'];}
                if(!empty($re_info['st_min_grade'])){$miniEduLevel =$re_info['st_min_grade'];}
                if(!empty($re_info['st_req_sex'])&& $re_info['st_req_sex']!='A'){$gender =$re_info['st_req_sex'];}
                if(!empty($re_info['like_collage'])){$like_collage =$re_info['like_collage'];}
                $tutor=new tutor();
                $teacherlist=$tutor->getSuitedTeacher($type,$item,$level,$place,$classtime,$hourlyfee,$miniEduLevel,$gender,$classlang,$like_collage,1,1,'','','',$week);
                foreach ($teacherlist['data'] as $key=> $teacher) {
                    $star=$tutor->return_star($teacher['master_id'],$teacher['role_verify']);
                    $teacher['star']=$star;
                    
            
                    pc_base::load_app_class('helpcommon' ,'pair');
                    $helpcommon=new helpcommon();
                    $place=$helpcommon->create_location($teacher['tsl_locid']);
                    $aa=explode(',',$place);
                    foreach($aa as $key=>$a){
                        if($key>=3){
                            $place=$aa[0].','.$aa[1].','.$aa[2].'...';
                        }
                    }
                    $teacher['teach_area']=$place;

              
                    $subject=$helpcommon->create_subject($teacher['tstt_ttid_2']);
                    $subname=explode(',',$subject);
                    foreach($subname as $k=>$sub){
                        if($k>=3){
                            $subname=$subname[0].','.$subname[1].','.$subname[2].'...';
                        }
                    }
                    $teacher['teach_type']=$subname;

                    $weeknums = array('6'=> "S",'0'=> "M",'1'=> "T",'2'=> "W",'3'=> "T",'4'=> "F",'5'=> "S");
              
                    if($teacher['tst_weekday']==""){
                        foreach($weeknums as $num){
                             $teacher['teach_time'].="<span>".$num."</sapn>";
                        }
                    }else{
                        
                        $week=explode(",",$teacher['tst_weekday']);
                        foreach($weeknums as $k=>$num){
                        $str="";
                        if(strpos($teacher['tst_weekday'],strval($k)) !== false ){
                            $str = "style='color: black'";
                        }
                        $teacher['teach_time'].="<span ".$str.">".$num."</span>";
                        }
                    }

          
                    $pairid=$this->p_db->get_one("pair_tutor_tranid = '$teacher[tm_tutorid]'");
                    if(!empty($pairid)){
                        $teacher['match_area']="<div class='course-match'>配對中</div>";
                    }else{
                        $teacher['match_area']="";
                    }
                    $teacherlist['data'][$key]=$teacher;
                }
      
                $mas_arr[0]=$s_id;
                $mas_arr[1]=$teacherlist;
                echo json_encode($mas_arr);
                exit;
            }else{
                return;
            }
        }else{
            $mas_arr[0] = 'fail';
            echo json_encode($mas_arr);
            return;
        } 
    }

    public function add_time(){
        $time_a=$_POST['time_a'];
        $time=$_POST['time_arr'];
        $time_re_arr['sst_stid'] = $time_a['st_id'];
        $time_re_arr['sst_studentid'] = $time_a['student_id'];
        if(empty($time)){return '';}
        $time = explode("],", $time); 

        $reg="/\d+/";
        for($i=0;$i<count($time);$i++){
            preg_match_all($reg,$time[$i],$time_arr[$i]);
        }
   
        foreach ($time_arr as $key =>$v){  
            $new_arr[]=$v[0]; 
        }
        foreach($new_arr as $k=>$v){

            if(!$v){
                unset($new_arr[$k]);//删除
            }
        }
      
        if(!$new_arr){
            unset($new_arr);
        }
        foreach ($new_arr as $k => $v) {
            $temp[]=(count($v));
            $pos=array_search(max($temp),$temp);
            $index= $temp[$pos];
            for ($i=0; $i < $index-1 ; $i+=2) { 
                $fromtime[$k][]=$new_arr[$k][$i];
                $totime[$k][]=$new_arr[$k][$i+1];
            }
        }
  
        $time_re['sst_weekday']=array_keys($fromtime);
         foreach ($fromtime as $key => $value) {
            $time_re['sst_fromtime'][$key]=trim(implode(';', $value),';');
        }
        foreach ($totime as $key => $value) {
            $time_re['sst_totime'][$key]=trim(implode(';', $value),';');
        }
        $time_re_arr['sst_fromtime']=implode(',', $time_re['sst_fromtime']);
        $time_re_arr['sst_totime']=implode(',', $time_re['sst_totime']);
        $time_re_arr['sst_weekday']=implode(',', $time_re['sst_weekday']);
        $this->time_db = pc_base::load_model('student_sel_time_model');
        $time_res=$this->time_db->insert($time_re_arr);
        if($time_res){
            echo "0";
            exit; 
        }else{
            echo "1";
            exit;
        }
    }

  
    public function suit_tutor(){
        $stid = !empty($_GET['stid']) ? $_GET['stid'] : showmessage('網絡出錯,請刷新重試!',HTTP_REFERER);
        $std_tran = pc_base::load_model("student_transaction_model");
        $info_sql = "select * from v9_student_transaction as tran
        left join v9_student_sel_time as time on time.sst_stid = tran.st_id
        left join v9_student_master as mas on mas.student_id = tran.st_studentid
        left join v9_member as mem on mem.userid = mas.student_userid
        where tran.st_id = '$stid'";
        $std_tran->query($info_sql);
        $info = array_unique($std_tran->fetch_array())[0];
        if(!empty($info)){
            if(!empty($info['student_locid'])){$place =$info['student_locid'];}
            if(!empty($info['st_ttid_1'])){$type =$info['st_ttid_1'];}
            if(!empty($info['st_ttid_2'])){$item =$info['st_ttid_2'];}
            if(!empty($info['st_ttid_3'])){$level =$info['st_ttid_3'];}
            if(!empty($info['st_class_hour'])){$classtime =$info['st_class_hour'];}
            if(!empty($info['st_tutor_fee'])){$hourlyfee =$info['st_tutor_fee'];}
            if(!empty($info['st_min_grade'])){$miniEduLevel =$info['st_min_grade'];}
            if(!empty($info['st_req_sex'])&& $info['st_req_sex']!='A'){$gender =$info['st_req_sex'];}
            if(!empty($info['like_collage'])){$like_collage =$info['like_collage'];}
            if(!empty($info['sst_weekday'])){$week =$info['sst_weekday'];}
            $tutor=new tutor();
            $teacherlist=$tutor->getSuitedTeacher($type,$item,$level,$place,$classtime,$hourlyfee,$miniEduLevel,$gender,$classlang,$like_collage,1,1,'','','',$week);

            foreach ($teacherlist['data'] as $key=> $teacher) {
                $star=$tutor->return_star($teacher['master_id'],$teacher['role_verify']);
                $teacher['star']=$star;
                
                //得到教授地区
                pc_base::load_app_class('helpcommon' ,'pair');
                $helpcommon=new helpcommon();
                $place=$helpcommon->create_location($teacher['tsl_locid']);
                $aa=explode(',',$place);
                foreach($aa as $ke=>$a){
                    if($ke>=3){
                        $place=$aa[0].','.$aa[1].','.$aa[2].'...';
                    }
                }
                $teacher['teach_area']=$place;

                //得到教授科目
                $subject=$helpcommon->create_subject($teacher['tstt_ttid_2']);
                $subname=explode(',',$subject);
                foreach($subname as $k=>$sub){
                    if($k>=3){
                        $subname=$subname[0].','.$subname[1].','.$subname[2].'...';
                    }
                }
             
                $teacher['teach_type']=$subject;

                $weeknums = array('6'=> "S",'0'=> "M",'1'=> "T",'2'=> "W",'3'=> "T",'4'=> "F",'5'=> "S");
       
                if($teacher['tst_weekday']==""){
                    foreach($weeknums as $num){
                         $teacher['teach_time'].="<span>".$num."</sapn>";
                    }
                }else{
                    
                    $week=explode(",",$teacher['tst_weekday']);
                    foreach($weeknums as $k=>$num){
                    $str="";
                    if(strpos($teacher['tst_weekday'],strval($k)) !== false ){
                        $str = "style='color: black'";
                    }
                    $teacher['teach_time'].="<span ".$str.">".$num."</span>";
                    }
                }
          
                $pairid=$this->p_db->get_one("pair_tutor_tranid = '$teacher[tm_tutorid]'");
                if(!empty($pairid)){
                    $teacher['match_area']="<div class='course-match'>配對中</div>";
                }else{
                    $teacher['match_area']="";
                }
                $teacherlist['data'][$key]=$teacher;
            }
        }
        $tor_info = $teacherlist['data'][0];
        include template('person', 'suit_tutor');
    }


    public function exchangeGift(){
        $inte=new inte();
       
        if(!empty($_POST) && !empty($_SESSION['id'])){
       
            $return['code'] = 0;
            $patternEmail = "/^(\w)+(\.\w+)*@(\w)+((\.\w{2,3}){1,3})$/";
            $patternMobile1 = "/^\d{8}$/";
            $patternMobile2 = "/^\d{11}$/";
            if(empty($_POST['redeem']['redeemName'])){
                $return['msg'] = 'name_error';
                echo json_encode($return);exit;
            }elseif(empty(preg_match($patternMobile1,$_POST['redeem']['redeemMobile']) || preg_match($patternMobile2,$_POST['redeem']['redeemMobile'])) || empty($_POST['redeem']['redeemMobile'])){
                $return['msg'] = 'mobile_error';
                echo json_encode($return);exit;
            }elseif(!preg_match($patternEmail,$_POST['redeem']['redeemEmail']) || empty($_POST['redeem']['redeemEmail'])){
                $return['msg'] = 'email_error';
                echo json_encode($return);exit;
            }elseif(empty($_POST['redeem']['redeemAddress'])){
                $return['msg'] = 'address_error';
                echo json_encode($return);exit;
            }

            $gift_inte=$inte->get_inte_setting(10);
            $userid = $_SESSION['id'];
            $user = $this->db->get_one("`userid` = $userid");              
            if($user['integral'] >= $gift_inte){                                     
                $email = $_POST['redeem']['redeemEmail'];
                pc_base::load_sys_func('mail');                                
                require('phpcms/libs/functions/Exchange.php');
                sendmail($email,'積分兌換禮品申請信息', $email_info);         
                sendmail('enquiry@tutorseeking.com','用戶積分兌換禮品申請', $email_info);

                
                $returnPoint = pc_base::load_app_class('integralOperation','pay');
                $deduction = $returnPoint->integralOperation($userid,$gift_inte,$type='-');
             
                $inte->write_inte_log('','','10',$user['personid'],$user['username'],'','-'.$gift_inte);
                if($deduction){
                    $return['code'] = 1;
                    $return['msg'] = '提交成功!';
                }else{
                    $return['code'] = 0;
                    $return['msg'] = '請刷新重試!';
                }
            }else{
                $return['code'] = 0;
                $return['msg'] = '積分不足!';
            }
            echo json_encode($return);
        }
    }

    public function getCity(){
        if(!empty($_GET)){
            $city = $this->db->fetch_array($this->db->query("select loc_id,loc_name from `v9_sys_location` where loc_parentid = $_GET[areaid] order by loc_seq asc"));
            if(!empty($city)){
                echo json_encode($city);
            }else{
                echo 0;
            }
        }
    }

}
?>