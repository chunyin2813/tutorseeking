<?php 
//导师个人中心
defined('IN_PHPCMS') or exit('No permission resources.');
$session_storage = 'session_'.pc_base::load_config('system','session_storage');
pc_base::load_sys_class($session_storage);
pc_base::load_app_class('inte' ,'integration');
class tutor_index {
    function __construct() {
        header("Content-type: text/html; charset=utf-8");
    	$this->db = pc_base::load_model('member_model');
        $this->p_db=pc_base::load_model('pair_model');
        $this->tutortype=pc_base::load_model('tutor_type_model');
        $this->tuseltype=pc_base::load_model('tutor_sel_tutor_type_model');
        $this->tumas =pc_base::load_model('tutor_master_model');
        $this->tuselach = pc_base::load_model('tutor_sel_achievement_model');
        
        if(isset($_SESSION['id']) && !empty($_SESSION['id'])){
            $id = $_SESSION['id'];
        }else{
           showmessage('請登録!','./index.php');
        }
    }


    public function init(){
        $id = $_SESSION['id'];


    	$admin_sql = "select s.tutor_id_card,a.personid,a.integral,a.userid,a.mobile,a.username,a.userimage,a.phone,a.email,s.tutor_address,s.tutor_gname,s.tutor_address_locid,s.tutor_fname_chi,s.tutor_id,s.tutor_sex from v9_member as a
            left join v9_tutor_master as s
            on a.userid = s.tutor_userid  
            where a.userid = $id order by a.userid asc";

        $admin_info = $this->db->query($admin_sql);
        $admin_list = $this->db->fetch_array();

       
        $inte=new inte();
        $gift_inte=$inte->get_inte_setting(10);
        
       
        $admin_list[0]['star'] = 0;
        $tutorClass = pc_base::load_app_class('tutor','pair');
        if(!empty($admin_list[0]['tutor_id'])){
            $admin_list[0]['star'] = $tutorClass->return_star($admin_list[0]['tutor_id'],0);
        }
        
  
        $sql_tutor = "select count(*) as count from v9_tutor_transaction as tt
                        left join v9_tutor_master as tm
                        on tt.master_id = tm.tutor_id
                        where tm.tutor_userid=$id";
        $this->p_db->query($sql_tutor);
        $num = $this->p_db->fetch_array();

    
        $sql_tutors ="select count(*) as count from v9_pair as p
            right join v9_tutor_transaction as s
            on p.pair_tutor_tranid = s.tm_tutorid
            left join v9_tutor_master as m
            on s.master_id = m.tutor_id
            where m.tutor_userid = '$id' and p.pair_state = '1' ";
       
        $this->p_db->query($sql_tutors);
        $nums = $this->p_db->fetch_array();


        $tutortype_sql  = "select tstt_ttid_1,tstt_ttid_2,tstt_ttid_3 from v9_tutor_sel_tutor_type as tuseltype
        left join v9_tutor_transaction as tutran
        on tuseltype.tstt_tmid = tutran.tm_id
        left join v9_tutor_master as tumas
        on tuseltype.tstt_tutorid = tumas.tutor_id
        where tumas.tutor_userid  = '$id'";
        $this->tuseltype->query($tutortype_sql);
        $tutor_type = $this->tuseltype->fetch_array();
        foreach ($tutor_type as $type) {
            $ttid_1 = explode(',', $type['tstt_ttid_1']);
            $ttid_2 = explode(',', $type['tstt_ttid_2']);
            $ttid_3 = explode(',', $type['tstt_ttid_3']);
        }
        foreach ($ttid_1 as $k => $v) {
            $ttid_arr1[] = $v;
        }
        foreach ($ttid_2 as $k => $v) {
            $ttid_arr2[] = $v;
        }
        foreach ($ttid_3 as $k => $v) {
            $ttid_arr3[] = $v;
        }

        //導師基本信息
        $info_sql = "select * from v9_tutor_master as tumas 
        left join v9_member as mem
        on tumas.tutor_userid = mem.userid
        left join v9_tutor_sel_achievement as ach
        on ach.tsa_userid = mem.userid
        left join v9_tutor_transaction as tutran
        on tutran.master_id = tumas.tutor_id
        left join v9_tutor_sel_location as tuloc
        on tuloc.tsl_tutorid = tumas.tutor_id and tuloc.tsl_tmid = tutran.tm_id
        where tumas.tutor_userid = '$id'";
        $this->tumas->query($info_sql);
        $infos = array_unique($this->tumas->fetch_array());
            
   

        $case_sql = "select tstt.tstt_ttid_2,tstt.tstt_ttid_1,tsl_locid,tm.tutor_id,tt.tm_tutorid,tm_matched,tm_state from v9_tutor_master as tm
        left join v9_tutor_transaction as tt
        on tt.master_id = tm.tutor_id
        left join v9_tutor_sel_tutor_type as tstt
        on tstt.tstt_tutorid = tt.tm_tutorid
        left join v9_tutor_sel_location as tsl
        on tsl.tsl_tmid = tt.tm_id
        where tm.tutor_userid = $id AND tstt.tstt_ttid_2 is not null";
        $sql= $case_sql.$where." order by tm.tutor_userid desc";

        $get_page = pc_base::load_model('get_model');
        $page = intval($_GET['page'])?intval($_GET['page']) :'1';
        $Infos = $get_page->multi_listinfo($sql,$page,5);  
        $pages = $get_page->pages;

        pc_base::load_app_class('helpcommon' ,'pair');
        $helpcommon=new helpcommon();
        foreach ($Infos as $key => $value) {
            $places[]=$helpcommon->create_location($value['tsl_locid']); 
        }


    	include template('person', 'tutor-index');
    }

    public function index_edit(){
        $edit_info = $_POST['edit_info'];
        $id = isset($_POST['tutor_id'])?$_POST['tutor_id']:$_POST['userid'];
        $this->ma_db=pc_base::load_model('tutor_master_model');
        //联络人住宅电话
        if(isset($edit_info['mobile'])){
            $res = $this->db->update(array('mobile'=>$edit_info['mobile']),array('userid'=>$id));
            if($res){
                echo "1";
                exit;
            }else{
                echo "2";
                exit;
            }
        }
        //住宅区域
        if(isset($edit_info['tutor_address_locid'])){
            $res = $this->ma_db->update(array('tutor_address_locid'=>$edit_info['tutor_address_locid']),array('tutor_id'=>$id));
            if($res){
                echo "1";
                exit;
            }else{
                echo "2";
                exit;
            }
        }   
    }

    public function tutor_level_ajax(){
     
        if(!empty($_POST['tt_id'])){
            $this->tt_db= pc_base::load_model('sys_tutor_type_model');
            $tt_infos=$this->tt_db->select(array('tt_parentid'=>$_POST['tt_id']),'*','','`tt_seq` ASC','','');
           
            $tt_infos=json_encode($tt_infos);
            echo $tt_infos;
            exit;
        }
    }
//修改导师个案信息
    public function edit(){
        $id = $_SESSION['id'];
        $main_message=$this->tumas->get_one(array('tutor_userid'=>$id));
        // $id = 3;
        $tm_tutorid  = $_GET['id'];
        //可教授列别级别项目
        $courses = $this->tuseltype->get_one(array('tstt_tutorid'=>$tm_tutorid));
        // var_dump($courses);
        $ttid_1 = $courses['tstt_ttid_1'];
        $ttid_2 = $courses['tstt_ttid_2'];
        $ttid_3 = $courses['tstt_ttid_3'];
        $this->sys_course_db = pc_base::load_model('sys_tutor_type_model');
        
        $ttid_1 = explode(',', $ttid_1);
        foreach ($ttid_1 as $kt1 => $vt1) {
            $sql1 = "select * from v9_sys_tutor_type where tt_id = $vt1";
            $this->sys_course_db->query($sql1);
            $courses_names1[] = $this->sys_course_db->fetch_array();
            
        }

        
        $ttid_2 = explode(',', $ttid_2);
        foreach ($ttid_2 as $kt2 => $vt2) {
            $sql2 = "select * from v9_sys_tutor_type where tt_id = $vt2";
            $this->sys_course_db->query($sql2);
            $courses_names2[] = $this->sys_course_db->fetch_array();
               
        }

        
        $ttid_3 = explode(',', $ttid_3);
        foreach ($ttid_3 as $kt3 => $vt3) {
            $sql3 = "select * from v9_sys_tutor_type where tt_id = $vt3";
            $this->sys_course_db->query($sql3);
            $courses_names3[] = $this->sys_course_db->fetch_array();
        }

        //地区
        $this->loc_db = pc_base::load_model('tutor_sel_location_model');
        $locs_info = $this->loc_db->get_one(array('tsl_tutorid'=>$tm_tutorid));
        

        //时间
        $this->time_db = pc_base::load_model('tutor_sel_time_model');
        $times_info = $this->time_db->get_one(array('tst_tutorid'=>$tm_tutorid));
      
        $fromtime = explode(';',$times_info['tst_fromtime']); 
        $totime= explode(';',$times_info['tst_totime']);
        $tst_weekday= explode(',',$times_info['tst_weekday']);
        $weeknums = array("一","二","三","四","五","六","日");

        $day = array(array(),array(),array(),array(),array(),array(),array());

        for($i =0;$i<7;$i++){
            $key = $tst_weekday[$i];
            $day[$key] = array($weeknums[$key],$fromtime[$i],$totime[$i]);
        }

        $day1 = $day[0];
        $day2 = $day[1];
        $day3 = $day[2];
        $day4 = $day[3];
        $day5 = $day[4];
        $day6 = $day[5];
        $day7 = $day[6];
  

        pc_base::load_sys_class('form','', 0);
        $timelist = array(
        '0700' =>"7:00" ,'0715' =>"7:15" ,'0730' =>"7:30",'0745' =>"7:45",
        '0800' =>"8:00" ,'0815' =>"8:15" ,'0830' =>"8:30" ,'0845' =>"8:45",
        '0900' =>"9:00" ,'0915' =>"9:15" ,'0930' =>"9:30" ,'0945' =>"9:45",
        '1000' =>"10:00" ,'1015' =>"10:15" ,'1030' =>"10:30" ,'1045' =>"10:45",
        '1100' =>"11:00" ,'1115' =>"11:15" ,'1130' =>"11:30" ,'1145' =>"11:45",
        '1200' =>"12:00" ,'1215' =>"12:15" ,'1230' =>"12:30" ,'1245' =>"12:45",
        '1300' =>"13:00" ,'1315' =>"13:15" ,'1330' =>"13:30" ,'1345' =>"13:45",
        '1400' =>"14:00" ,'1415' =>"14:15" ,'1430' =>"14:30" ,'1445' =>"14:45",
        '1500' =>"15:00" ,'1515' =>"15:15" ,'1530' =>"15:30" ,'1545' =>"15:45",
        '1600' =>"16:00" ,'1615' =>"16:15" ,'1630' =>"16:30" ,'1645' =>"16:45",
        '1700' =>"17:00" ,'1715' =>"17:15" ,'1730' =>"17:30" ,'1745' =>"17:45",
        '1800' =>"18:00" ,'1815' =>"18:15" ,'1830' =>"18:30" ,'1845' =>"18:45",
        '1900' =>"19:00" ,'1915' =>"19:15" ,'1930' =>"19:30" ,'1945' =>"19:45",
        '2000' =>"20:00" ,'2015' =>"20:15" ,'2030' =>"20:30" ,'2045' =>"20:45",
        '2100' =>"21:00" ,'2115' =>"21:15" ,'2130' =>"21:30" ,'2145' =>"21:45",
        '2200' =>"22:00" ,'2215' =>"22:15" ,'2230' =>"22:30" ,'2245' =>"22:45",
        '2300' =>"23:00");
    
        //時間
        $arr_ccc = array('0.75'=>'45分鐘','1'=>'1小時','1.5'=>'1.5小時','2'=>'2小時','2.5'=>'2.5小時','3'=>'3小時','3.5'=>'3.5小時','4'=>'4小時','4.5'=>'4.5小時','5'=>'5小時');

        include template('person', 'tutor-case-edit');
    }

    public function ajax_tutor_case_edit(){
        $tst_class_hour = $_POST['tst_class_hour'];
        $tstt_fee = $_POST['tstt_fee'];
        $check_val = $_POST['check_val'];
        if(empty($check_val)){
            echo '2';exit;
        }
        

        $types=explode(',', $_POST['ttid_1']);
        $subs=explode(',', $_POST['ttid_2']);
        $levels=explode(',', $_POST['ttid_3']);
       
        foreach ($types as $key => $type) {
            if($type==""){
                unset($types[$key]); 
            }
        }
         foreach ($subs as $ks => $ts) {
            if($ts==""){
                unset($subs[$ks]); 
            }
        }
         foreach ($levels as $kl => $tl) {
            if($tl==""){
                unset($levels[$kl]); 
            }
        }
        $ttid_1=implode(',', array_values($types));
        $ttid_2=implode(',', array_values($subs));
        $ttid_3 = implode(',', array_values($levels));
       

        $tm_tutorid=$_POST['tutor_id'];
        $tm_id = $_POST['tm_id'];
        //处理上课时间数据
        $time=$_POST['teachertime'];
        if(empty($time)){return '';}
        $time = explode("],", $time);

        //取出数字
        $reg="/\d+/";
        for($i=0;$i<count($time);$i++){
            preg_match_all($reg,$time[$i],$time_arr[$i]);
        }

  
        foreach ($time_arr as $key =>$v){  
            $new_arr[]=$v[0]; 
        }
        foreach($new_arr as $k=>$v){
      
            if(!$v){//判断是否为空（false）
                unset($new_arr[$k]);//删除
            }
         
        }

        if(!$new_arr){
            unset($new_arr);
        }
        $from = '';
        $to = '';
        foreach($new_arr as $key =>$value){
            foreach ($value as $ke => $val) {
                if($ke%2 == 0){
                    $from .= $val.',';
                }else{
                    $to .= $val.',';
                }
            }
            $from = rtrim($from,',');
            $to = rtrim($to,',');
            $from .= ';';
            $to .= ';';
        }
        $weekday=array_keys($new_arr);
        $weekday = implode(',',$weekday);
        $week=$weekday;
    
        $this->time_db = pc_base::load_model('tutor_sel_time_model');
        $time_res = $this->time_db->update(array('tst_weekday'=>$weekday,'tst_fromtime'=>$from,'tst_totime'=>$to,'tst_class_hour'=>$tst_class_hour),array('tst_tutorid'=>$tm_id));

  
        $this->loc_db = pc_base::load_model('tutor_sel_location_model');
        $loc_res = $this->loc_db->update(array('tsl_locid'=>$check_val),array('tsl_tutorid'=>$tm_id));

       
        $this->type_db = pc_base::load_model('tutor_sel_tutor_type_model');
        $type_res = $this->type_db->update(array('tstt_ttid_1'=>$ttid_1,'tstt_ttid_2'=>$ttid_2,'tstt_ttid_3'=>$ttid_3,'tstt_fee'=>$tstt_fee),array('tstt_tutorid'=>$tm_id));

        if($time_res && $loc_res && $type_res){
            if(!empty($_POST['ttid_1'])){$type=$ttid_1;}
            if(!empty($_POST['ttid_2'])){$item=$ttid_2;}
            if(!empty($_POST['ttid_3'])){$level=$ttid_3;}
            if(!empty($_POST['tutor_sex'])){$gender=$_POST['tutor_sex'];}
            if(!empty($_POST['tutor_college'])){$like_collage=$_POST['tutor_college'];}
            if(!empty($_POST['tutor_high_grade'])){$miniEduLevel=$_POST['tutor_high_grade'];}
            if(!empty($_POST['tutor_main_lang'])){$classlang=$_POST['tutor_main_lang'];}
            if(!empty($_POST['check_val'])){$place=$_POST['check_val'];}
            if(!empty($_POST['tstt_fee'])){$hourlyfee=$_POST['tstt_fee'];}
            if(!empty($_POST['tst_class_hour'])){$classtime=$_POST['tst_class_hour'];}
            pc_base::load_app_class('student' ,'pair');
            $stu=new student();
            $caselist=$stu->getSuitedStudent($type,$item,$level,$place,$classtime,$hourlyfee,$miniEduLevel,$gender,$classlang,$like_collage,1,1,'','',$week);
            $data=$caselist['data'];
            pc_base::load_app_class('tutor' ,'pair');
            $tutor=new tutor();
            foreach ($data as $student) {
                $star=$tutor->return_star($student['st_studentid'],$student['role_verify']);
                $student['star']=$star;

                $weeknums = array('6'=> "S",'0'=> "M",'1'=> "T",'2'=> "W",'3'=> "T",'4'=> "F",'5'=> "S");
           
                if($student['sst_weekday']==""){
                    foreach($weeknums as $num){
                        $student['stu_time'].="<span>".$num."</sapn>";
                    }
                }else{
                    
                    $week=explode(",",$student['sst_weekday']);
                    foreach($weeknums as $k=>$num){
                    $str="";
                    if(strpos($student['sst_weekday'],strval($k)) !== false ){
                        $str = "style='color: black'";
                    }
                   $student['stu_time'].="<span ".$str.">".$num."</span>";
                    }
                }

             
                $pairid=$this->p_db->get_one("pair_student_tranid = '$student[st_id]'");
                if(!empty($pairid)){
                    $student['match_area']="<div class='course-match'>配對中</div>";
                }else{
                    $student['match_area']="";
                }
                $data=$student;
            }
            echo json_encode($data);
        }else{
            echo "2";
        }

    }

    

    public function base_info(){
    
        $id = $_SESSION['id'];
        // $id = 3;
        $info_sql = "select * from v9_tutor_master as tumas 
        left join v9_member as mem
        on tumas.tutor_userid = mem.userid
        left join v9_tutor_sel_achievement as ach
        on ach.tsa_userid = mem.userid
        left join v9_tutor_transaction as tutran
        on tutran.master_id = tumas.tutor_id
        left join v9_tutor_sel_time as tst
        on tst.tst_tmid = tutran.tm_id
        left join v9_tutor_sel_tutor_type as tstt
        on tstt.tstt_tmid = tutran.tm_id
        left join v9_tutor_sel_location as tuloc
        on tuloc.tsl_tmid = tutran.tm_id
        where tumas.tutor_userid = '$id'";
        $this->tumas->query($info_sql);
        $infos = array_unique($this->tumas->fetch_array());
        

     
        $lang = array('普通話'=>'普通話','英語'=>'英語','廣東話'=>'廣東話','其他語言'=>'其他語言');
     
        $subject_type = array('文科'=>'文科','理科'=>'理科','商科'=>'商科');

     
        $now_job = array('學生'=>'學生','現職小學教師'=>'現職小學教師','現職中學教師'=>'現職中學教師','全職補習導師'=>'全職補習導師','幼稚園導師'=>'幼稚園導師','外國回流導師'=>'外國回流導師','退休老師'=>'退休老師','現職大學教授'=>'現職大學教授','教育機構老師'=>'教育機構老師','教育機構老師'=>'教育機構老師','其他職業'=>'其他職業','無業'=>'無業');
        
        //經驗
        $experience = array('一年'=>'一年','二年'=>'二年','四年'=>'四年','五年或以上'=>'五年或以上','只做過暑假工'=>'只做過暑假工','無工作經驗'=>'無工作經驗');



        include template('person', 'tutor-baseinfo');
    }

    public function ajax_info_edit(){
        $info = $_POST;
        unset($info['tstt_ttid_1']);
        unset($info['tstt_ttid_2']);
        unset($info['tstt_ttid_3']);
        unset($info['tsl_locid']);
        unset($info['tstt_fee']);
        unset($info['tst_class_hour']);
        unset($info['tst_weekday']);
        if($info['tutor_sex']=='F'){
            $masterimg['userimage'] = "/statics/images/add_image/Nvtimg.jpg";
        }else{
            $masterimg['userimage'] = "/statics/images/add_image/nantimg.jpg";
        }
        $masterimg['username'] = $info['tutor_gname'];
        $masterimg['nickname'] = $info['tutor_gname'];
      
        $tutor_id = $_POST['tutor_id'];
        $userid = $_POST['tutor_userid'];
        $MAS = $this->db->update($masterimg,array('userid'=>$userid));
        $res = $this->tumas->update($info,array('tutor_id'=>$tutor_id,'tutor_userid'=>$userid));
        if($res && $MAS){
            if(!empty($_POST['tstt_ttid_1'])){$type=$_POST['tstt_ttid_1'];}
            if(!empty($_POST['tstt_ttid_2'])){$item=$_POST['tstt_ttid_2'];}
            if(!empty($_POST['tstt_ttid_3'])){$level=$_POST['tstt_ttid_3'];}
            if(!empty($_POST['tsl_locid'])){$place=$_POST['tsl_locid'];}
            if(!empty($_POST['tstt_fee'])){$hourlyfee=$_POST['tstt_fee'];}
            if(!empty($_POST['tst_class_hour'])){$classtime=$_POST['tst_class_hour'];}
            if(!empty($_POST['tutor_sex'])){$gender=$_POST['tutor_sex'];}
            if(!empty($_POST['tutor_main_lang'])){$classlang=$_POST['tutor_main_lang'];}
            if(!empty($_POST['tutor_high_grade'])){$miniEduLevel=$_POST['tutor_high_grade'];}
            if(!empty($_POST['tutor_college'])){$like_collage=$_POST['tutor_college'];}
            if(!empty($_POST['tst_weekday'])){$week=$_POST['tst_weekday'];}
            pc_base::load_app_class('student' ,'pair');
            $stu=new student();
            $caselist=$stu->getSuitedStudent($type,$item,$level,$place,$classtime,$hourlyfee,$miniEduLevel,$gender,$classlang,$like_collage,1,1,'','',$week);
            $data=$caselist['data'];
            pc_base::load_app_class('tutor' ,'pair');
            $tutor=new tutor();
            foreach ($data as $student) {
                $star=$tutor->return_star($student['st_studentid'],$student['role_verify']);
                $student['star']=$star;

                $weeknums = array('6'=> "S",'0'=> "M",'1'=> "T",'2'=> "W",'3'=> "T",'4'=> "F",'5'=> "S");
         
                if($student['sst_weekday']==""){
                    foreach($weeknums as $num){
                        $student['stu_time'].="<span>".$num."</sapn>";
                    }
                }else{
                    
                    $week=explode(",",$student['sst_weekday']);
                    foreach($weeknums as $k=>$num){
                    $str="";
                    if(strpos($student['sst_weekday'],strval($k)) !== false ){
                        $str = "style='color: black'";
                    }
                   $student['stu_time'].="<span ".$str.">".$num."</span>";
                    }
                }

            
                $pairid=$this->p_db->get_one("pair_student_tranid = '$student[st_id]'");
                if(!empty($pairid)){
                    $student['match_area']="<div class='course-match'>配對中</div>";
                }else{
                    $student['match_area']="";
                }
                $data=$student;
            }
            echo json_encode($data);
        }else{
            echo "2";
        }
    }


    public function grade_edit(){
        $arr = array(
        '0700' =>"7:00" ,'0715' =>"7:15" ,'0730' =>"7:30",'0745' =>"7:45",
        '0800' =>"8:00" ,'0815' =>"8:15" ,'0830' =>"8:30" ,'0845' =>"8:45",
        '0900' =>"9:00" ,'0915' =>"9:15" ,'0930' =>"9:30" ,'0945' =>"9:45",
        '1000' =>"10:00" ,'1015' =>"10:15" ,'1030' =>"10:30" ,'1045' =>"10:45",
        '1100' =>"11:00" ,'1115' =>"11:15" ,'1130' =>"11:30" ,'1145' =>"11:45",
        '1200' =>"12:00" ,'1215' =>"12:15" ,'1230' =>"12:30" ,'1245' =>"12:45",
        '1300' =>"13:00" ,'1315' =>"13:15" ,'1330' =>"13:30" ,'1345' =>"13:45",
        '1400' =>"14:00" ,'1415' =>"14:15" ,'1430' =>"14:30" ,'1445' =>"14:45",
        '1500' =>"15:00" ,'1515' =>"15:15" ,'1530' =>"15:30" ,'1545' =>"15:45",
        '1600' =>"16:00" ,'1615' =>"16:15" ,'1630' =>"16:30" ,'1645' =>"16:45",
        '1700' =>"17:00" ,'1715' =>"17:15" ,'1730' =>"17:30" ,'1745' =>"17:45",
        '1800' =>"18:00" ,'1815' =>"18:15" ,'1830' =>"18:30" ,'1845' =>"18:45",
        '1900' =>"19:00" ,'1915' =>"19:15" ,'1930' =>"19:30" ,'1945' =>"19:45",
        '2000' =>"20:00" ,'2015' =>"20:15" ,'2030' =>"20:30" ,'2045' =>"20:45",
        '2100' =>"21:00" ,'2115' =>"21:15" ,'2130' =>"21:30" ,'2145' =>"21:45",
        '2200' =>"22:00" ,'2215' =>"22:15" ,'2230' =>"22:30" ,'2245' =>"22:45",
        '2300' =>"23:00");

        $id = $_SESSION['id'];
        // $id = 3;
        include template('person', 'tutor-grade');
    }
    public function ajax_grade_edit(){
        $userid = $_POST['userid'];
        //sys_achieve_score表
        $scores=explode(",", $_POST['scores']);
        foreach ($scores as $key => $v) {
            $one_score=explode("-", $v);
            $tsa['tsa_asscore'] = $one_score[1];
            $tsa['tsa_modtime']=date("Y-m-d H:i:s");
            $where['tsa_userid'] = $userid;
            $where['tsa_taid'] = $one_score[0];
            $tsa_id=$this->tuselach->get_one($where);
            if(empty($tsa_id)){
                $res =  $this->tuselach->insert(array('tsa_userid'=>$userid,'tsa_asscore'=>$one_score[1],'tsa_taid'=>$one_score[0]));
            }else{
              $res = $this->tuselach->update($tsa,$where);  
            }
            
        }
        if($res){
            echo "1";
        }else{
            echo "2";
        }
       
    }



    public function tutor_add(){
         $id = $_SESSION['id'];
        // $id = 3;
        $sql="select m.tutor_id,t.tm_id,tutor_sex,tutor_college,tutor_high_grade,tutor_main_lang from v9_tutor_master as m 
                left join v9_tutor_transaction as t
                on t.tm_tutorid = m.tutor_id
                where m.tutor_userid = '$id'";
        $this->tumas->query($sql);
        $infos = $this->tumas->fetch_array();
                
        pc_base::load_sys_class('form','', 0);
        $timelist = array(
        '0700' =>"7:00" ,'0715' =>"7:15" ,'0730' =>"7:30",'0745' =>"7:45",
        '0800' =>"8:00" ,'0815' =>"8:15" ,'0830' =>"8:30" ,'0845' =>"8:45",
        '0900' =>"9:00" ,'0915' =>"9:15" ,'0930' =>"9:30" ,'0945' =>"9:45",
        '1000' =>"10:00" ,'1015' =>"10:15" ,'1030' =>"10:30" ,'1045' =>"10:45",
        '1100' =>"11:00" ,'1115' =>"11:15" ,'1130' =>"11:30" ,'1145' =>"11:45",
        '1200' =>"12:00" ,'1215' =>"12:15" ,'1230' =>"12:30" ,'1245' =>"12:45",
        '1300' =>"13:00" ,'1315' =>"13:15" ,'1330' =>"13:30" ,'1345' =>"13:45",
        '1400' =>"14:00" ,'1415' =>"14:15" ,'1430' =>"14:30" ,'1445' =>"14:45",
        '1500' =>"15:00" ,'1515' =>"15:15" ,'1530' =>"15:30" ,'1545' =>"15:45",
        '1600' =>"16:00" ,'1615' =>"16:15" ,'1630' =>"16:30" ,'1645' =>"16:45",
        '1700' =>"17:00" ,'1715' =>"17:15" ,'1730' =>"17:30" ,'1745' =>"17:45",
        '1800' =>"18:00" ,'1815' =>"18:15" ,'1830' =>"18:30" ,'1845' =>"18:45",
        '1900' =>"19:00" ,'1915' =>"19:15" ,'1930' =>"19:30" ,'1945' =>"19:45",
        '2000' =>"20:00" ,'2015' =>"20:15" ,'2030' =>"20:30" ,'2045' =>"20:45",
        '2100' =>"21:00" ,'2115' =>"21:15" ,'2130' =>"21:30" ,'2145' =>"21:45",
        '2200' =>"22:00" ,'2215' =>"22:15" ,'2230' =>"22:30" ,'2245' =>"22:45",
        '2300' =>"23:00");

        include template('person', 'tutor-add');
    } 
    public function ajax_tutor_add(){
        $checl_val = $_POST['check_val'];
        if(empty($checl_val)){
            echo 2;die;
        }
      


        $types=explode(',', $_POST['ttid_1']);
        $subs=explode(',', $_POST['ttid_2']);
        $levels=explode(',', $_POST['ttid_3']);
        foreach ($types as $key => $type) {
            if($type==""){
                unset($types[$key]); 
            }
        }
         foreach ($subs as $ks => $ts) {
            if($ts==""){
                unset($subs[$ks]); 
            }
        }
         foreach ($levels as $kl => $tl) {
            if($tl==""){
                unset($levels[$kl]); 
            }
        }
        $ttid_1=implode(',', array_values($types));
        $ttid_2=implode(',', array_values($subs));
        $ttid_3 = implode(',', array_values($levels));

        $time=$_POST['teachertime'];
        if(empty($time)){return '';}
        $time = explode("],", $time); 

        //取出数字
        $reg="/\d+/";
        for($i=0;$i<count($time);$i++){
            preg_match_all($reg,$time[$i],$time_arr[$i]);
        }

      
        foreach ($time_arr as $key =>$v){  
            $new_arr[]=$v[0]; 
        }
        foreach($new_arr as $k=>$v){
      
            if(!$v){//判断是否为空（false）
                unset($new_arr[$k]);//删除
            }
         
        }
      
        if(!$new_arr){
            unset($new_arr);
        }
        $from = '';
        $to = '';
        foreach($new_arr as $key =>$value){
            foreach ($value as $ke => $val) {
                if($ke%2 == 0){
                    $from .= $val.',';
                }else{
                    $to .= $val.',';
                }
            }
            $from = rtrim($from,',');
            $to = rtrim($to,',');
            $from .= ';';
            $to .= ';';
        }
        $weekday=array_keys($new_arr);
        $weekday = implode(',',$weekday);
        $week=$weekday;

    
        $master_id = $_POST['tutor_id'];
        $tstt_fee = $_POST['tstt_fee'];
        $tst_class_hour = $_POST['tst_class_hour'];
    
  
        $a_sql = "select max(tm_tutorid) from v9_tutor_transaction;";
        $this->tumas->query($a_sql);
        $infos = $this->tumas->fetch_array();
        $tm_tutorid = $infos[0]['max(tm_tutorid)'];
        $t = split('T',$tm_tutorid);
        $tm = $t[1]+1;
        $tm_tutorid = 'T'.$tm;

     
        $this->t_db = pc_base::load_model('tutor_transaction_model');
        $t_res = $this->t_db->insert(array('tm_tutorid'=>$tm_tutorid,'master_id'=>$master_id));

        $tm_id = $this->t_db->select(array('tm_tutorid'=>$tm_tutorid),$data='tm_id');
        $tm_id = $tm_id[0]['tm_id'];

   
        $this->time_db = pc_base::load_model('tutor_sel_time_model');
        $time_res = $this->time_db->insert(array('tst_weekday'=>$weekday,'tst_fromtime'=>$from,'tst_totime'=>$to,'tst_tutorid'=>$tm_tutorid,'tst_tmid'=>$tm_id,'tst_class_hour'=>$tst_class_hour));

   
        $this->loc_db = pc_base::load_model('tutor_sel_location_model');
        $loc_res = $this->loc_db->insert(array('tsl_locid'=>$checl_val,'tsl_tutorid'=>$tm_tutorid,'tsl_tmid'=>$tm_id));

   
        $this->type_db = pc_base::load_model('tutor_sel_tutor_type_model');
        $type_res = $this->type_db->insert(array('tstt_ttid_1'=>$ttid_1,'tstt_ttid_2'=>$ttid_2,'tstt_ttid_3'=>$ttid_3,'tstt_fee'=>$tstt_fee,'tstt_tutorid'=>$tm_tutorid,'tstt_tmid'=>$tm_id));

        if($t_res && $time_res && $loc_res && $type_res){
            if(!empty($_POST['ttid_1'])){$type=$ttid_1;}
            if(!empty($_POST['ttid_2'])){$item=$ttid_2;}
            if(!empty($_POST['ttid_3'])){$level=$ttid_3;}
            if(!empty($_POST['tutor_sex'])){$gender=$_POST['tutor_sex'];}
            if(!empty($_POST['tutor_college'])){$like_collage=$_POST['tutor_college'];}
            if(!empty($_POST['tutor_high_grade'])){$miniEduLevel=$_POST['tutor_high_grade'];}
            if(!empty($_POST['tutor_main_lang'])){$classlang=$_POST['tutor_main_lang'];}
            if(!empty($_POST['check_val'])){$place=$_POST['check_val'];}
            if(!empty($_POST['tstt_fee'])){$hourlyfee=$_POST['tstt_fee'];}
            if(!empty($_POST['tst_class_hour'])){$classtime=$_POST['tst_class_hour'];}
            pc_base::load_app_class('student' ,'pair');
            $stu=new student();
            $caselist=$stu->getSuitedStudent($type,$item,$level,$place,$classtime,$hourlyfee,$miniEduLevel,$gender,$classlang,$like_collage,1,1,'','',$week);
            $data=$caselist['data'];
            pc_base::load_app_class('tutor' ,'pair');
            $tutor=new tutor();
            foreach ($data as $student) {
                $star=$tutor->return_star($student['st_studentid'],$student['role_verify']);
                $student['star']=$star;

                $weeknums = array('6'=> "S",'0'=> "M",'1'=> "T",'2'=> "W",'3'=> "T",'4'=> "F",'5'=> "S");
                //得到可教授時間
                if($student['sst_weekday']==""){
                    foreach($weeknums as $num){
                        $student['stu_time'].="<span>".$num."</sapn>";
                    }
                }else{
                    
                    $week=explode(",",$student['sst_weekday']);
                    foreach($weeknums as $k=>$num){
                    $str="";
                    if(strpos($student['sst_weekday'],strval($k)) !== false ){
                        $str = "style='color: black'";
                    }
                   $student['stu_time'].="<span ".$str.">".$num."</span>";
                    }
                }

                //確定此個案未配對還是在配對中
                $pairid=$this->p_db->get_one("pair_student_tranid = '$student[st_id]'");
                if(!empty($pairid)){
                    $student['match_area']="<div class='course-match'>配對中</div>";
                }else{
                    $student['match_area']="";
                }
                $data=$student;
            }
            echo json_encode($data);
        }else{
            echo "2";
        }
    }

  
    public function suit_student(){
  
        $tmid = !empty($_GET['tmid']) ? $_GET['tmid'] : showmessage('網絡出錯,請刷新重試!',HTTP_REFERER);
        $tor_tran = pc_base::load_model("tutor_transaction_model");//加載導師執行表模型
        $info_sql = "select * from v9_tutor_transaction as tran
        left join v9_tutor_sel_time as time on time.tst_tutorid = tran.tm_tutorid
        left join v9_tutor_master as mas on mas.tutor_id = tran.master_id
        left join v9_member as mem on mem.userid = mas.tutor_userid
        left join v9_tutor_sel_location as loc on loc.tsl_tutorid = tran.tm_tutorid
        left join v9_tutor_sel_tutor_type as type on type.tstt_tutorid = tran.tm_tutorid
        where tran.tm_tutorid = '$tmid'";
        $tor_tran->query($info_sql);
        $info = array_unique($tor_tran->fetch_array())[0];
        if(!empty($info)){
            if(!empty($info['tstt_ttid_1'])){$type=$info['tstt_ttid_1'];}
            if(!empty($info['tstt_ttid_2'])){$item=$info['tstt_ttid_2'];}
            if(!empty($info['tstt_ttid_3'])){$level=$info['tstt_ttid_3'];}
            if(!empty($info['tsl_locid'])){$place=$info['tsl_locid'];}
            if(!empty($info['tst_class_hour'])){$classtime=$info['tst_class_hour'];}
            if(!empty($info['tstt_fee'])){$hourlyfee=$info['tstt_fee'];}
            if(!empty($info['tutor_high_grade'])){$miniEduLevel=$info['tutor_high_grade'];}
            if(!empty($info['tutor_sex'])){$gender=$info['tutor_sex'];}
            if(!empty($info['tutor_main_lang'])){$classlang=$info['tutor_main_lang'];}
            if(!empty($info['tutor_college'])){$like_collage=$info['tutor_college'];}
            if(!empty($info['tst_weekday'])){$week=$info['tst_weekday'];}
            pc_base::load_app_class('student' ,'pair');
            $stu=new student();
            $caselist=$stu->getSuitedStudent($type,$item,$level,$place,$classtime,$hourlyfee,$miniEduLevel,$gender,$classlang,$like_collage,1,1,'','',$week);
            $data=$caselist['data'];
            pc_base::load_app_class('tutor' ,'pair');
            $tutor=new tutor();
            foreach ($data as $student) {

                $star=$tutor->return_star($student['st_studentid'],$student['role_verify']);
                $student['star']=$star;

                $weeknums = array('6'=> "S",'0'=> "M",'1'=> "T",'2'=> "W",'3'=> "T",'4'=> "F",'5'=> "S");
                //得到可教授時間
                if($student['sst_weekday']==""){
                    foreach($weeknums as $num){
                        $student['stu_time'].="<span>".$num."</sapn>";
                    }
                }else{
                    
                    $week=explode(",",$student['sst_weekday']);
                    foreach($weeknums as $k=>$num){
                    $str="";
                    if(strpos($student['sst_weekday'],strval($k)) !== false ){
                        $str = "style='color: black'";
                    }
                   $student['stu_time'].="<span ".$str.">".$num."</span>";
                    }
                }

                //確定此個案未配對還是在配對中
                $pairid=$this->p_db->get_one("pair_student_tranid = '$student[st_id]'");
                if(!empty($pairid)){
                    $student['match_area']="<div class='course-match'>配對中</div>";
                }else{
                    $student['match_area']="";
                }
                $data=$student;
            }
        }else{
            $data = "2";
        }
        $stu_info = $data;
        include template('person', 'suit_student');
    }

    

       
}

 ?>