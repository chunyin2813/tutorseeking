<?php
defined('IN_ADMIN') or exit('No permission resources.'); 
include $this->admin_tpl('header', 'admin');
?>
<style>
	thead th{
		height: 30px;
		background: #eef3f7;
		border-bottom: 1px solid #d5dfe8;
		font-weight: normal;
	}
    td{
        border-bottom: #eee 1px solid;
		padding-top: 5px;
		padding-bottom: 5px;
    }
    .email_box{
    	background-color: rgba(0,0,0,0.5);
    	width: 100%;
    	height: 100%;
    	position: absolute;
    	z-index: 99;
    }
    .email_box .email_content{
    	width: 700px;
    	margin: 0 auto;
    	margin-top: 100px;
    	background: #fff;
    	padding: 20px 14px 20px 14px;
    	overflow: hidden;
    }
    .email_box .email_content table tr td input{
    	width: 50%;
    }

    .email_box .email_content table tr td{
    	margin-left: 14px;
    }
    .email_box .email_content table tr .td_width{
    	width: 12%;
    }
    .email_box .email_content .content_bottom{
    	width: 100%;
    	height: 40px;
    }
    .email_box .email_content .content_bottom span{
    	padding: 0 14px;
    	border: 1px solid blue;
    	border-radius: 3px;
    	line-height: 24px;
    	margin-right: 16px;
    	margin-top: 14px;
    }
    .email_box .email_content .content_bottom span:hover{
    	cursor: pointer;
    	color: #000;
    }
    .email_box .email_content table tr td textarea{
    	width: 682px;
    	height: 300px;
    }
</style>
<div class="email_box" style="display:none;">
	<div class="email_content">
		<table width="100%">
			<tr>
				<td class="td_width">收件人邮箱：</td>
				<td><input type="text" name="useremail" value="" disabled></td>
			</tr>			
			<tr>
				<td class="td_width">标题：</td>
				<td><input type="text" name="email_title" value=""></td>
			</tr>		
			<tr>
				<td class="td_width">邮件内容：</td>
			</tr>
			<tr>
				<td colspan="2" class="email_text">
					<textarea></textarea>
				</td>
			</tr>
		</table>
		<div class="content_bottom">
			<span onclick="send_email_content(this)">发送</span>
			<span onclick="hide_box()">取消</span>
		</div>
	</div>
</div>
<form method="post" action="?m=comment&c=comment_admin&a=del_subscribe" id="myform">
<table width="100%">
        <thead>
            <tr>
			 <th width="10%"><input type="checkbox" value="" id="check_box" onclick="selectall('ids[]');"></th>
			<th width="20%">编号</th>
			<th width="40%">邮箱</th>
			<!-- <th width="130"><?php //echo L('phone');?></th>
			<th width="130"><?php //echo L('addtime');?></th> -->
			<th width="30%"><?php echo L('operations_manage');?></th>
            </tr>
        </thead>
		<tbody class="add_comment">
    <?php
	if(is_array($data)) {
		foreach($data as $v) {
			// $comment_info = $this->comment_db->get_one(array('commentid'=>$v['commentid']));
			// if (strpos($v['content'], '<div class="content">') !==false) {
			// 	$pos = strrpos($v['content'], '</div>');
			// 	$v['content'] = substr($v['content'], $pos+6);
			// }
	?>
     <tr id="tbody_<?php echo $v['id']?>" style="text-align: center" >
		<td align="center" width="16"><input class="inputcheckbox " name="ids[]" value="<?php echo $v['id'];?>" type="checkbox"></td> 
		<td width="130"><?php echo $v['id']?></td>
		<td width="130" class="user_email"><?php echo $v['email']?></td>	
		<td align='center' width="72"><a href="javascript:;" onclick="send_email(this)">发送邮件</a></td>
	</tr>
     <?php }
	}
	?>
	</tbody>
     </table>
     <div class="btn"><label for="check_box"><?php echo L('selected_all');?>/<?php echo L('cancel');?></label>
		<input type="hidden" value="<?php echo $_SESSION['pc_hash'];?>" name="pc_hash">
		<input type="submit" class="button" value="<?php echo L('delete');?>" />
	</div>
    <div id="pages"><?php echo $pages;?></div>
</form>
<script type="text/javascript">
	function send_email(th)
	{
		$('.email_box').show();
		var user_email = $(th).parent().siblings(".user_email").text();
		$("input[name='useremail']").val(user_email);
		// console.log(user_email);
	}
	function hide_box(){
		$('.email_box').hide();
	}
	function send_email_content()
	{
		var pc_hash = "<?php echo $_SESSION['pc_hash'];?>";
		var useremail = $("input[name='useremail']").val();
		var email_title = $("input[name='email_title']").val();
		var email_text = $('.email_content .email_text textarea').val();
		var ajaxurl = '?m=comment&c=comment_admin&a=send_email&menuid=1890&pc_hash='+pc_hash;
		// console.log(ajaxurl);
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {useremail:useremail,email_title:email_title,email_text:email_text},
            cache: false,
            error: function(){
                alert('出错了！');
                return false;
            },
            success:function(data){
            	$('.email_box').hide();
            	alert('发送成功！');
            }
        })
        return false;
	}
</script>