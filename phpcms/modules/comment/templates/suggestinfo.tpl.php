<?php
defined('IN_ADMIN') or exit('No permission resources.'); 
include $this->admin_tpl('header', 'admin');
?>
<style>
	thead th{
		height: 30px;
		background: #eef3f7;
		border-bottom: 1px solid #d5dfe8;
		font-weight: normal;
	}
    td{
        border-bottom: #eee 1px solid;
		padding-top: 5px;
		padding-bottom: 5px;
    }
</style>
<form method="post" action="?m=comment&c=comment_admin&a=delete_all" id="myform">
<table width="100%">
        <thead>
            <tr>
			 <th width="16"><input type="checkbox" value="" id="check_box" onclick="selectall('ids[]');"></th>
			<th width="130"><?php echo L('suggestor')?></th>
			<th width="130"><?php echo L('suggestion')?></th>
			<th width="130"><?php echo L('phone');?></th>
			<th width="130"><?php echo L('addtime');?></th>
			<th width="72"><?php echo L('operations_manage');?></th>
            </tr>
        </thead>
		<tbody class="add_comment">
    <?php
	if(is_array($data)) {
		foreach($data as $v) {
			// $comment_info = $this->comment_db->get_one(array('commentid'=>$v['commentid']));
			// if (strpos($v['content'], '<div class="content">') !==false) {
			// 	$pos = strrpos($v['content'], '</div>');
			// 	$v['content'] = substr($v['content'], $pos+6);
			// }
	?>
     <tr id="tbody_<?php echo $v['id']?>" style="text-align: center" >
		<td align="center" width="16"><input class="inputcheckbox " name="ids[]" value="<?php echo $v['id'];?>" type="checkbox"></td> 
		<td width="130"><?php echo $v['name']?></td>
		<td width="130"><a title="<?php echo $v['content']?>"><?php echo mb_substr($v['content'],0,8,'utf-8');?>...</a></td>
		<td width="130"><?php echo $v['phone']?></td>
		<td width="130"><?php echo $v['addtime']?></td>		
		<td align='center' width="72"><a href="?m=comment&c=comment_admin&a=delete&ids=<?php echo $v['id']?>&dosubmit=1" ><?php echo L('delete');?></a> </td>
	</tr>
     <?php }
	}
	?>
	</tbody>
     </table>
     <div class="btn"><label for="check_box"><?php echo L('selected_all');?>/<?php echo L('cancel');?></label>
		<input type="hidden" value="<?php echo $_SESSION['pc_hash'];?>" name="pc_hash">
		<input type="submit" class="button" value="<?php echo L('delete');?>" />
	</div>
    <div id="pages"><?php echo $pages;?></div>
</form>