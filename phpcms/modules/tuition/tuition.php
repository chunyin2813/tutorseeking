<?php

defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_app_class('admin','admin',0);
pc_base::load_sys_class('form','',0);

class tuition extends admin {
	
	function __construct() {
		$this->db = pc_base::load_model('tuition_model');		   
		$this->tuitype = pc_base::load_model('tuitiontype_model'); 
		$this->tortype = pc_base::load_model('teacher_type_model');
		$this->stdtype = pc_base::load_model('student_type_model');
	}

	

	function tuition_list () {
		$tuitype = $this->tuitype->select('','*','','tt_order asc');	 	
		if(empty($tuitype)){ showmessage(L('请先添加学费类型'),'?m=tuition&c=tuitiontype&menuid=1805&a=tuitiontype_list'); }
		$ttid = isset($_GET['ttid']) ? $_GET['ttid'] : $tuitype[0]['tt_id'];
	
		$current_tuitype = $this->tuitype->get_one("tt_id = $ttid");		

       	
        $tui_sql = "SELECT * FROM `v9_tuition` as A left join `v9_teacher_type` as B on B.tct_id = A.tr_gradeid left join `v9_student_type` as C on C.st_id = A.tr_levelid left join v9_tuition_type as D on A.tr_ttid = D.tt_id where tr_ttid = $ttid order by tct_order ASC";		
    																
        $tuitionsql = $this->db->query($tui_sql);
        $tuitioninfo = $this->db->fetch_array($tuitionsql);
        
		$stdtype = $this->stdtype->select("st_ttid = $ttid",'*','','st_order asc');  
        $tortype = $this->tortype->select("tct_ttid = $ttid",'*','','tct_order asc');

    	$table = array();
	    foreach ($stdtype as $key => $value) {	    
			$i = 0;
			foreach ($tuitioninfo as $k => $v) {	
				if($value['st_id'] == $v['tr_levelid']){	
					if($i == 0){
						$table[$key][$i]['price'] = $v['st_name']; 
						$i += 1;
						$table[$key][$i]['price'] = $v['tr_price'];
						$table[$key][$i]['id'] = $v['tr_typeid']; 
						$i += 1;
					}else{
						$table[$key][$i]['price'] = $v['tr_price'];
						$table[$key][$i]['id'] = $v['tr_typeid']; 
						$i++;
					}
				}
			}
		}
		include $this->admin_tpl('tuition_list');
	}


	public function tuition_edit(){
		
		if(!empty($_GET['id']) && !empty($_GET['content'])){
			$data = array('tr_price'=>intval($_GET['content']));
			$where = array('tr_typeid'=>$_GET['id']);
			if($this->db->update($data,$where)){
				echo 'success';
			}else{
				echo 'fail';	
			}
		}
	}

	
	public function tuition_show(){
		
		if(!empty($_GET['ttid']) && isset($_GET['state'])){
			$data = array('tt_show'=>intval($_GET['state']));
			$where = array('tt_id'=>$_GET['ttid']);
			if($this->tuitype->update($data,$where)){
				echo 'success'; 
			}else{
				echo 'fail';   
			}
		}
	}


}