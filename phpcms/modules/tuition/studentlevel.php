<?php

defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_app_class('admin','admin',0);
pc_base::load_sys_class('form','',0);

class studentlevel extends admin {
	
	function __construct() {
		$this->db = pc_base::load_model('student_type_model');
		$this->tortype = pc_base::load_model('teacher_type_model');
		$this->tuitiontype = pc_base::load_model('tuitiontype_model');
		$this->tuition = pc_base::load_model('tuition_model');
	}
	

	function studentlevel_list () {
		$tuitiontype = $this->tuitiontype->select('','*','','tt_order asc');
		if(empty($tuitiontype)){ showmessage(L('请先添加学费类型'),'?m=tuition&c=tuitiontype&menuid=1805&a=tuitiontype_list'); }
		$page = isset($_GET['page']) ? $_GET['page'] : 1;
		$limit = 10;
		$start = ($page-1)*$limit;
		$tuitionid = isset($_GET['ttid']) ? $_GET['ttid'] : $tuitiontype[0]['tt_id'];
		$sql = "select * from `v9_student_type` as A
						 left join `v9_tuition_type` as B on A.st_ttid = B.tt_id where st_ttid = $tuitionid order by st_order ASC";
		$studentinfo = $this->db->query($sql);
		$result = $this->db->fetch_array($studentinfo);
		$studentlevel['page_link'] = pages(count($result), $page, $limit, '', array(), 10);
		$studentlevel['data'] = array_slice($result, $start, $limit);
	
		$big_menu = array('javascript:window.top.art.dialog({id:\'add\',iframe:\'?m=tuition&c=studentlevel&a=add&ttid='.$tuitionid.'\', title:\''.L('添加学生级别').'\', width:\'600\', height:\'230\', lock:true}, function(){var d = window.top.art.dialog({id:\'add\'}).data.iframe;var form = d.document.getElementById(\'dosubmit\');form.click();return false;}, function(){window.top.art.dialog({id:\'add\'}).close()});void(0);', L('添加学生级别'));
		include $this->admin_tpl('studentlevel_list');
	}
	

	
	function add() {
		if(isset($_POST['dosubmit'])) {
			$data = array(
					'st_name'=>$_POST['info']['name'],
					'st_order'=>$_POST['info']['order'],
					'st_ttid'=>$_POST['info']['ttid']
				);
			$this->db->insert($data);
			if($this->db->insert_id()){
				$st_id= $this->db->insert_id();
				$ttid = $_POST['info']['ttid'];
				$tortype = $this->tortype->select("tct_ttid = $ttid");
				foreach ($tortype as $key => $value) {	
					$tctid = $value['tct_id'];
					if(empty($this->tuition->select("tr_levelid=$st_id and tr_gradeid=$tctid"))){
						$autoAdd = array('tr_levelid'=>$st_id,'tr_gradeid'=>$tctid,'tr_price'=>0,'tr_ttid'=>$ttid);
						$this->tuition->insert($autoAdd);
					}
				}
				showmessage(L('operation_success'),'?m=tuition&c=studentlevel&a=add', '', 'add');
			}else{
				showmessage(L('operation_failure'),'?m=tuition&c=studentlevel&a=add', '', 'add');
			}
		}
		include $this->admin_tpl('studentlevel_add');	
	}
	

	function delete() {
		$stid = isset($_GET['id']) ? $_GET['id'] : showmessage(L('illegal_parameters'), HTTP_REFERER);
		$this->db->query('START TRANSACTION');
		if($this->db->delete(array('st_id'=>$stid))){
			if($this->tuition->delete("tr_levelid=$stid")){
				$this->db->query('COMMIT');
				showmessage(L('operation_success'));
			}else{
				$this->db->query('ROLLBACK');
			}
		}else{
			showmessage(L('operation_failure'), HTTP_REFERER);
		}
	}
	

	function edit(){
		if(isset($_POST['dosubmit'])) {
			$stid = $_POST['info']['id'];
			$data = array(
					'st_name'=>$_POST['info']['name'],
					'st_order'=>$_POST['info']['order'],
				);
			$where = array('st_id'=>$stid);
			if($this->db->update($data,$where)){
				showmessage(L('operation_success'),'?m=tuition&c=studentlevel&a=edit', '', 'edit');
			}
		}
		$stid = $_GET['stid'];
		$info = $this->db->get_one("st_id=$stid");
		include $this->admin_tpl('studentlevel_edit');
	}

 }
?>