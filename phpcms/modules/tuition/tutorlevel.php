<?php

defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_app_class('admin','admin',0);
pc_base::load_sys_class('form','',0);

class tutorlevel extends admin {
	
	function __construct() {
		$this->db = pc_base::load_model('teacher_type_model');			
		$this->stdtype = pc_base::load_model('student_type_model');		
		$this->tuitiontype = pc_base::load_model('tuitiontype_model');	
		$this->tuition = pc_base::load_model('tuition_model');			
	}
	
	
	function tutorlevel_list () {
		$tuitiontype = $this->tuitiontype->select('','*','','tt_order asc');	
		if(empty($tuitiontype)){ showmessage(L('请先添加学费类型'),'?m=tuition&c=tuitiontype&menuid=1805&a=tuitiontype_list'); }
		$page = isset($_GET['page']) ? $_GET['page'] : 1;
		$limit = 10;
		$start = ($page-1)*$limit;
		$tuitionid = isset($_GET['ttid']) ? $_GET['ttid'] : $tuitiontype[0]['tt_id'];	
		$sql = "select * from `v9_teacher_type` as A
						 left join `v9_tuition_type` as B on A.tct_ttid = B.tt_id where tct_ttid = $tuitionid order by tct_order ASC";
		$tutorinfo = $this->db->query($sql);
		$result = $this->db->fetch_array($tutorinfo);
		$tutorlevel['page_link'] = pages(count($result), $page, $limit, '', array(), 10);
		$tutorlevel['data'] = array_slice($result, $start, $limit);

		$big_menu = array('javascript:window.top.art.dialog({id:\'add\',iframe:\'?m=tuition&c=tutorlevel&a=add&ttid='.$tuitionid.'\', title:\''.L('添加导师级别').'\', width:\'600\', height:\'230\', lock:true}, function(){var d = window.top.art.dialog({id:\'add\'}).data.iframe;var form = d.document.getElementById(\'dosubmit\');form.click();return false;}, function(){window.top.art.dialog({id:\'add\'}).close()});void(0);', L('添加导师级别'));
		include $this->admin_tpl('tutorlevel_list');
	}
	


	function add() {
		if(isset($_POST['dosubmit'])) {
			$data = array(
					'tct_name'=>$_POST['info']['name'],
					'tct_order'=>$_POST['info']['order'],
					'tct_ttid'=>$_POST['info']['ttid']
				);
			$this->db->insert($data);
			if($this->db->insert_id()){
				$tctid= $this->db->insert_id();
				$ttid = $_POST['info']['ttid'];
				$stdtype = $this->stdtype->select("st_ttid = $ttid");
				foreach ($stdtype as $key => $value) {
					$stid = $value['st_id'];
					if(empty($this->tuition->select("tr_levelid=$stid and tr_gradeid=$tctid"))){
						$autoAdd = array('tr_levelid'=>$stid,'tr_gradeid'=>$tctid,'tr_price'=>0,'tr_ttid'=>$ttid);
						$this->tuition->insert($autoAdd);
					}
				}
				showmessage(L('operation_success'),'?m=tuition&c=tutorlevel&a=add', '', 'add');
			}else{
				showmessage(L('operation_failure'),'?m=tuition&c=tutorlevel&a=add', '', 'add');
			}
		}
		include $this->admin_tpl('tutorlevel_add');	
	}
	

	function delete() {
		$tctid = isset($_GET['id']) ? $_GET['id'] : showmessage(L('illegal_parameters'), HTTP_REFERER);
		$this->db->query('START TRANSACTION');
		if($this->db->delete(array('tct_id'=>$tctid))){
			if($this->tuition->delete("tr_gradeid=$tctid")){
				$this->db->query('COMMIT');   //执行事务
				showmessage(L('operation_success'));
			}else{
				$this->db->query('ROLLBACK'); //回滚
			}
		}else{
			showmessage(L('operation_failure'), HTTP_REFERER);
		}
	}
	

	function edit(){
		if(isset($_POST['dosubmit'])) {
			$tctid = $_POST['info']['id'];
			$data = array(
					'tct_name'=>$_POST['info']['name'],
					'tct_order'=>$_POST['info']['order'],
				);
			$where = array('tct_id'=>$tctid);
			if($this->db->update($data,$where)){
				showmessage(L('operation_success'),'?m=tuition&c=tutorlevel&a=edit', '', 'edit');
			}
		}
		$tctid = $_GET['tctid'];
		$info = $this->db->get_one("tct_id=$tctid");
		include $this->admin_tpl('tutorlevel_edit');
	}

 }
?>