<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header');?>
<div class="pad_10">
<div class="table-list">
    <table width="100%" cellspacing="0">
        <thead>
            <tr>
            <th width="60">ID</th>
            <th>学费类型</th>
            <th>排序</th>
			<th width="100"><?php echo L('operations_manage');?></th>
            </tr>
        </thead>
    <tbody>
    
	<?php foreach($tuitiontype['data'] as $r) { ?>
        <tr>
    		<td align='center'><?php echo $r['tt_id'];?></td>
    		<td align="center"><?php echo $r['tt_name'];?></td>
    		<td align="center"><?php echo $r['tt_order'];?></td>
    		<td align='center' ><a href="javascript:edit('<?php echo $r['tt_id']?>','<?php echo $r['tt_name']; ?>')">修改</a> | <a href="javascript:confirmurl('?m=tuition&c=tuitiontype&a=delete&ttid=<?php echo $r['tt_id'];?>&menuid=<?php echo $_GET['menuid'];?>','<?php echo L('confirm',array('message'=>$r['tt_name']));?>')">删除</a></td>
    	</tr>
	<?php } ?>
	</tbody>
    </table>
  
    <div id="pages"><?php echo $tuitiontype['page_link'];?></div>
</div>
</div>
<script type="text/javascript"> 
function edit(id,name) {
	window.top.art.dialog({id:'edit'}).close();
	window.top.art.dialog({title:'<?php echo L('修改学费类型');?>《'+name+'》',id:'edit',iframe:'?m=tuition&c=tuitiontype&a=edit&ttid='+id,width:'600',height:'230'}, function(){var d = window.top.art.dialog({id:'edit'}).data.iframe;d.document.getElementById('dosubmit').click();return false;}, function(){window.top.art.dialog({id:'edit'}).close()});
}
</script>
</body>
</html>
