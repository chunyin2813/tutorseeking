<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header');?>
<style>
.table{margin-top:30px;padding:0;width:90%;height:250px;border:#eee solid 1px}
.table-list td{padding:0;}
td {text-align:center;width:10%;}
.level{margin-left:5%}
</style>
<div class="pad_10">
<div class="table-list">
    <table width="100%" class="table"cellspacing="0" border="1" align="center">
		<form action="?m=admin&c=tutor&a=edit" method="post" name="myform" id="myform">
	    <tbody>
	    	<span class="level">当前学费类型：</span>
	        <select name="level" id="level">
	            <?php foreach($tuitype as $value){ ?>
	            	<option <?php if($value['tt_id'] == $ttid){echo 'selected';} ?> value="<?php echo $value['tt_id']; ?>"><?php echo $value['tt_name'];?></option>
	            <?php }?>
	        </select>
	        &nbsp;&nbsp;&nbsp;前台显示：
	        <input type="radio" name="is_show" value="1" <?php if($current_tuitype['tt_show'] == 1){echo 'checked';} ?>>是
	        <input type="radio" name="is_show" value="0" <?php if($current_tuitype['tt_show'] == 0){echo 'checked';} ?>>否
	        <script>
		        $('#level').change(function(){
		            var ttid = $("select[name='level']").val();
		            var url = '<?php echo $_SERVER['REQUEST_URI'];?>';
		            window.location.href=url+'&ttid='+ttid;
		        });

		        $('input[type=radio][name=is_show]').change(function() {
		        	$.get('./index.php?m=tuition&c=tuition&a=tuition_show',{ttid:'<?php echo $ttid; ?>',state:$("input[name='is_show']:checked").val()},function(data){
		        		if(data != success){
		        			alert('请刷新重试!');
		        		}
		        	});
		        });
	    	</script>
	        <tr>
		    	<?php if(empty($table)){ echo'<td align="center">请先添加导师与学生类型</td>'; }else{ echo'<td align="center">学生等级\导师等级</td>'; } ?>
		        <?php foreach($tortype as $value){?>
		            <td align="center"><?php echo $value['tct_name'];?></td>
		        <?php }?>
		    </tr>
	        <?php foreach($table as $key =>$value){?>
				<?php echo '<tr>';?>
					<?php foreach($value as $k =>$v){?>
						<?php if($k != 0){echo '<td class="caname" val="'.$v['id'].'">';}else{echo '<td>';}?>
						<?php if($k != 0){echo intval($v['price']);}else{echo $v['price'];}?>
						<?php echo '</td>';?>
					<?php }?>
				<?php echo '</tr>';?>
			<?php }?>
	    </tbody>
		</form>
    </table>
</div>
</div>

<script>

$(function() { 
			//获取class为caname的元素 
			$(".caname").click(function() {
			var td = $(this);
			var typeid = td.attr('val');//当前选中学费ID值

			var current = parseInt(td.html());//选中的值

			var input = $("<input style='width:50px' type='text' value='"+current+"'/>"); //文本输入框

			td.html(input);//显示文本输入框

			input.click(function() { return false; }); 

			//获取焦点 
			input.trigger("focus"); 

			//文本框失去焦点后提交内容，重新变为文本 
			input.blur(function() { 
				var newval = parseInt($(this).val());
				//判断是否修改
				if (newval != current) {
					$.ajax({
				        type: "get",
				        url: "./index.php?m=tuition&c=tuition&a=tuition_edit",
				        data: {'id':typeid,'content':newval},
				        success: function (data) {
				        	if(data == 'success'){
				        		td.html(newval);//恢复原样
				        	}else{
				        		alert('请刷新重试!');
				        	}
				        }
			        });
				}else{ 
					td.html(newval);//恢复原样
				}
			}); 
		}); 
	}); 
</script>
</body>
</html>
