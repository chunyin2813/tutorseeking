<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header');?>
<style>
    .choice{margin-bottom:10px;}
</style>
<div class="pad_10">
<div class="table-list">
    <table width="100%" cellspacing="0">
        <thead>
            <tr>
            <th width="60">ID</th>
            <th>导师级别</th>
            <th>排序</th>
            <th>学费类型</th>
			<th width="100"><?php echo L('operations_manage');?></th>
            </tr>
        </thead>
    <tbody>
    <div class="choice">
        <b>学费类型：</b>
        <select class="level" name="level">
            <?php foreach($tuitiontype as $value){ ?>
                <option <?php if($tuitionid == $value['tt_id']){echo 'selected';}?> value="<?php echo $value['tt_id'];?>"><?php echo $value['tt_name'];?></option>
            <?php }?>
        </select>
    </div>
    <script>
        $('.level').change(function(){
            var ttid = $("select[name='level']").val();
            var url = '<?php echo $_SERVER['REQUEST_URI'];?>';
            window.location.href=url+'&ttid='+ttid;
        });
    </script>
	<?php foreach($tutorlevel['data'] as $r) { ?>
        <tr>
    		<td align='center'><?php echo $r['tct_id'];?></td>
    		<td align="center"><?php echo $r['tct_name'];?></td>
    		<td align="center"><?php echo $r['tct_order'];?></td>
    		<td align="center"><?php echo $r['tt_name'];?></td>
    		<td align='center' ><a href="javascript:edit('<?php echo $r['tct_id']?>','<?php echo $r['tct_name']; ?>')">修改</a> | <a href="javascript:confirmurl('?m=tuition&c=tutorlevel&a=delete&id=<?php echo $r['tct_id'];?>&menuid=<?php echo $_GET['menuid'];?>','<?php echo L('confirm',array('message'=>$r['tct_name']));?>')">删除</a></td>
    	</tr>
	<?php } ?>
	</tbody>
    </table>
  
    <div id="pages"><?php echo $tutorlevel['page_link'];?></div>
</div>
</div>
<script type="text/javascript"> 
function edit(id,name) {
	window.top.art.dialog({id:'edit'}).close();
	window.top.art.dialog({title:'<?php echo L('修改导师级别');?>《'+name+'》',id:'edit',iframe:'?m=tuition&c=tutorlevel&a=edit&tctid='+id,width:'600',height:'230'}, function(){var d = window.top.art.dialog({id:'edit'}).data.iframe;d.document.getElementById('dosubmit').click();return false;}, function(){window.top.art.dialog({id:'edit'}).close()});
}
</script>
</body>
</html>
