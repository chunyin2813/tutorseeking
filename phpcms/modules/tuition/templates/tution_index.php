<?php
//学费参考页面
defined('IN_PHPCMS') or exit('No permission resources.');

class index {
    function __construct() {
   		$this->db = pc_base::load_model('member_model');
    }
//学费参考页面
	function init(){	
		$tuition_type = $this->tuitype->select('tt_show = "1"','*','','tt_order asc');
		$info = array();
		foreach ($tuition_type as $key => $value) {
			$sql = "select t.tr_price,st.st_name,tea.tct_name,st.st_order,tea.tct_order,tui.tt_name from v9_tuition as t 
			left join v9_student_type as st 
			on t.tr_levelid = st.st_id 
			left join v9_teacher_type as tea 
			on t.tr_gradeid = tea.tct_id
			left join v9_tuition_type as tui 
			on t.tr_ttid = tui.tt_id
			where tr_ttid = '$value[tt_id]' order by st.st_order asc,tea.tct_order asc";
			$info[] = $this->db->fetch_array($this->db->query($sql));

		}
		 $info = $this->tuition_data($info);
		include template('tuition', 'tuition-reference');

	}
	
	function tuition_data($JsonData){
		$info = array();
		foreach($JsonData as $key => $value){
			$t = 't';
			foreach($value as $k => $val){
				$i=$val['tt_name'];
				$info[$i]['title'][$val['st_name']] = $val['st_name'];
				if($t != $val['tct_name']){
					$info[$i]['data'][$val['tct_name']]['title'] = $val['tct_name'];
					$t = $val['tct_name'];
				}
				$info[$i]['data'][$val['tct_name']][$val['st_name']] = $val['tr_price'];
			}
		}
		return $info;
	}
}
?>