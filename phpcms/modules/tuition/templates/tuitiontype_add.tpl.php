<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header');
?>
<script type="text/javascript">
<!--
	$(function(){
		$.formValidator.initConfig({formid:"myform",autotip:true,onerror:function(msg,obj){window.top.art.dialog({content:msg,lock:true,width:'200',height:'50'}, function(){this.close();$(obj).focus();})}});
		$("#name").formValidator({onshow:"请输入学费类型",onfocus:"请输入学费类型"}).inputValidator({min:1,onerror:"请输入学费类型"});
		$("#order").formValidator({onshow:"请输入排序",onfocus:"请输入排序"}).inputValidator({min:1,onerror:"请输入排序"}).regexValidator({regexp:"^([0-9]|[_]){0,20}$",onerror:"请输入0~999数字"});
	})
//-->
</script>
<style type="text/css">
.input-botton {
	border:none;
	border-bottom:1px dotted #E1A035;
	background:none;
}
</style>
<div class="pad_10">
<table width="100%" cellpadding="2" cellspacing="1" class="table_form">
<form action="?m=tuition&c=tuitiontype&a=add" method="post" name="myform" id="myform">
    <tr> 
      <th width="20%" height="50px">ID:</th>
       <td>&nbsp &nbsp 自动生成</td>
    </tr>
	<tr> 
      <th width="20%" height="50px">学费类型:</th>
       <td><input type="text" name="info[name]" id="name" size="30">&nbsp 例：体育/数学</td>
    </tr>
	<tr> 
      <th width="20%" height="50px">排序:</th>
       <td><input type="text" name="info[order]" id="order" size="30"> &nbsp 按数字先后排序</td>
    </tr>
	  <input type="submit" name="dosubmit" id="dosubmit" class="dialog" value=" <?php echo L('submit')?> ">
	</form>
</table> 

</div>
</body>
</html>