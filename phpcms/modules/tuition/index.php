<?php

defined('IN_PHPCMS') or exit('No permission resources.');

//学费参考页面
class index {
	function __construct() {
		$this->db = pc_base::load_model('tuition_model');		   //学费参考表操作模型
		$this->tuitype = pc_base::load_model('tuitiontype_model'); //学费类型表操作模型
		$this->tortype = pc_base::load_model('teacher_type_model');//导师等级表操作模型
		$this->stdtype = pc_base::load_model('student_type_model');//学生等级表操作模型
	}
	private function _session_start() {
	  	$session_storage = 'session_'.pc_base::load_config('system','session_storage');
		pc_base::load_sys_class($session_storage);
	}
	//学费参考页面
	function init(){	
		$tuition_type = $this->tuitype->select('tt_show = "1"','*','','tt_order asc');
		$info = array();
		foreach ($tuition_type as $key => $value) {
			$sql = "select t.tr_price,st.st_name,tea.tct_name,st.st_order,tea.tct_order,tui.tt_name from v9_tuition as t 
			left join v9_student_type as st 
			on t.tr_levelid = st.st_id 
			left join v9_teacher_type as tea 
			on t.tr_gradeid = tea.tct_id
			left join v9_tuition_type as tui 
			on t.tr_ttid = tui.tt_id
			where tr_ttid = '$value[tt_id]' order by st.st_order asc,tea.tct_order asc";
			$info[] = $this->db->fetch_array($this->db->query($sql));

		}


		$this->_session_start();	
		//判斷學費參考的價格是否可以點擊跳轉
		if(!empty($_SESSION['id']) && $_SESSION['role_verify']=="1" ){
			$aLinkType = 1;
		}else{
			$aLinkType = 0;
		}

		

		// var_dump($info);die;
		$info = $this->tuition_data($info);
		$catid=$_GET['catid'];
		include template('tuition', 'tuition-reference');

	}
	
	function tuition_data($JsonData){
		$info = array();
		foreach($JsonData as $key => $value){
			$t = 't';
			foreach($value as $k => $val){
				$i=$val['tt_name'];
				$info[$i]['title'][$val['tct_name']] = $val['tct_name'];
				if($t != $val['st_name']){
					$info[$i]['data'][$val['st_name']]['title'] = $val['st_name'];
					$t = $val['st_name'];
				}
				$info[$i]['data'][$val['st_name']][$val['tct_name']] = $val['tr_price'];
			}
			// foreach($value as $k => $val){
			// 	$i=$val['tt_name'];
			// 	$info[$i]['title'][$val['st_name']] = $val['st_name'];
			// 	if($t != $val['tct_name']){
			// 		$info[$i]['data'][$val['tct_name']]['title'] = $val['tct_name'];
			// 		$t = $val['tct_name'];
			// 	}
			// 	$info[$i]['data'][$val['tct_name']][$val['st_name']] = $val['tr_price'];
			// }
		}
		//var_dump($info);die;
		return $info;
	}

}
?>