<?php

defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_app_class('admin','admin',0);
pc_base::load_sys_class('form','',0);

class tuitiontype extends admin {
	
	function __construct() {
		$this->db = pc_base::load_model('tuitiontype_model');	
		$this->tuition = pc_base::load_model('tuition_model');		
		$this->tortype = pc_base::load_model('teacher_type_model'); 
		$this->stdtype = pc_base::load_model('student_type_model');
	}
	
	
	function tuitiontype_list () {
		$page = isset($_GET['page']) ? $_GET['page'] : 1;
		$limit = 10;
		$start = ($page-1)*$limit;
		$sql = "select * from `v9_tuition_type` order by tt_order ASC";
		$typeinfo = $this->db->query($sql);
		$result = $this->db->fetch_array($typeinfo);
		$tuitiontype['page_link'] = pages(count($result), $page, $limit, '', array(), 10); 
		$tuitiontype['data'] = array_slice($result, $start, $limit);
	
		$big_menu = array('javascript:window.top.art.dialog({id:\'add\',iframe:\'?m=tuition&c=tuitiontype&a=add\', title:\''.L('添加学费类型').'\', width:\'600\', height:\'230\', lock:true}, function(){var d = window.top.art.dialog({id:\'add\'}).data.iframe;var form = d.document.getElementById(\'dosubmit\');form.click();return false;}, function(){window.top.art.dialog({id:\'add\'}).close()});void(0);', L('添加学费类型'));
		
		include $this->admin_tpl('tuitiontype_list');
	}
	


	function add() {
		if(isset($_POST['dosubmit'])) {
			$data = array(
					'tt_name'=>$_POST['info']['name'],
					'tt_order'=>$_POST['info']['order'],
				);
			$this->db->insert($data);
			if($this->db->insert_id()){
				showmessage(L('operation_success'),'?m=tuition&c=tuitiontype&a=add', '', 'add');
			}
		}
		include $this->admin_tpl('tuitiontype_add');	
	}
	

	function delete() {
		$ttid = isset($_GET['ttid']) ? $_GET['ttid'] : showmessage(L('illegal_parameters'), HTTP_REFERER);
		$this->db->query('START TRANSACTION');
		if($this->db->delete("tt_id=$ttid")){
			if($this->tortype->delete("tct_ttid=$ttid")){
				if($this->stdtype->delete("st_ttid=$ttid")){
					if($this->tuition->delete("tr_ttid=$ttid")){
						$this->db->query('COMMIT');
						showmessage(L('operation_success'), HTTP_REFERER);
					}else{
						$this->db->query('ROLLBACK');
						showmessage(L('operation_failure'), HTTP_REFERER);
					}
				}else{
					$this->db->query('ROLLBACK');
					showmessage(L('operation_failure'), HTTP_REFERER);
				}
			}else{
				$this->db->query('ROLLBACK');
				showmessage(L('operation_failure'), HTTP_REFERER);
			}
		}else{
			$this->db->query('ROLLBACK');//回滚
			showmessage(L('operation_failure'), HTTP_REFERER);
		}
	}
	

	function edit(){
		if(isset($_POST['dosubmit'])) {
			$ttid = $_POST['info']['id'];
			$data = array(
					'tt_name'=>$_POST['info']['name'],
					'tt_order'=>$_POST['info']['order'],
				);
			$where = array('tt_id'=>$ttid);
			if($this->db->update($data,$where)){
				showmessage(L('operation_success'),'?m=tuition&c=tuitiontype&a=edit', '', 'edit');
			}
		}
		$ttid = $_GET['ttid'];
		$info = $this->db->get_one("tt_id=$ttid");
		include $this->admin_tpl('tuitiontype_edit');
	}

 }
?>