<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header', 'admin');
?>
<div class="content-menu ib-a blue line-x"><a class="add fb" href="?m=poster&c=popup&a=add"><em>添加popup</em></a></div>

<div class="pad-lr-10">
<form name="myform" action="?m=poster&c=poster&a=listorder" method="post">
<div class="table-list">
    <table width="100%" cellspacing="0" class="contentWrap">
        <thead>
            <tr>
            <th width="30" align="center"><input type="checkbox" value="" id="check_box" onclick="selectall('id[]');"></th>
			<th width="35">ID</th>
			<th width="50" align="center">所屬版本</th>
			<th width="50" align="center"><?php echo L('status')?></th>
			<th width="50" align="center">所在頁面</th>
			<th width="50" align="center">圖片</th>
			<th width="110" align="center"><?php echo L('operations_manage')?></th>
            </tr>
        </thead>
        <tbody>
 <?php 
if(is_array($infos)){
	foreach($infos as $info){
		switch ($info['position']) {
			case '0':
				$position='手機版';
				break;
			case '1':
				$position='ipad版';
				break;
			case '2':
				$position='電腦版';
				break;
		}
		$seat=explode(",", $info['seat']);
		in_array('0', $seat)?$pag='首頁/':$pag='';
		in_array('1', $seat)?$pag=$pag.'導師登記頁/':$pag=$pag.'';
		in_array('2', $seat)?$pag=$pag.'家長登記頁':$pag=$pag.'';
?>   
	<tr>
	<td align="center">
	<input type="checkbox" name="id[]" value="<?php echo $info['id']?>">
	</td>
	<td align="center"><?php echo $info['id']?></td>
	<td align="center"><?php echo $position;?></td>
	<td align="center"><?php $info['status']?print '啓用':print '停用';?></td>
	<td align="center"><?php echo $pag;?></td>
	<td align="center"><img src="<?php echo $info['setting']?>" width="105" height="88" onerror="this.src='<?php echo IMG_PATH;?>nopic.gif'"></td>
	<td align="center"><a href="index.php?m=poster&c=popup&a=edit&id=<?php echo $info['id'];?>&pc_hash=<?php echo $_SESSION['pc_hash'];?>&menuid=<?php echo $_GET['menuid']?>" ><?php echo L('edit')?></a></td>
	</tr>
<?php 
	}
}
?>
</tbody>
    </table>
  
    <div class="btn"><label for="check_box"><?php echo L('selected_all')?>/<?php echo L('cancel')?></label>
        <input name='submit' type='submit' class="button" value='<?php echo L('start')?>' onClick="document.myform.action='?m=poster&c=popup&a=public_approval&passed=1'">&nbsp;
        <input name='submit' type='submit' class="button" value='<?php echo L('stop')?>' onClick="document.myform.action='?m=poster&c=popup&a=public_approval&passed=0'">&nbsp;</div>  </div>
 <div id="pages"><?php echo $this->db->pages;?></div>
</form>
</div>
</body>
</html>
<script type="text/javascript">
<!--
	function edit(id, name) {
	window.top.art.dialog({id:'edit'}).close();
	window.top.art.dialog({title:'<?php echo L('edit_ads')?>--'+name, id:'edit', iframe:'?m=poster&c=poster&a=edit&id='+id ,width:'600px',height:'430px'}, function(){var d = window.top.art.dialog({id:'edit'}).data.iframe;// 使用内置接口获取iframe对象
	var form = d.document.getElementById('dosubmit');form.click();return false;}, function(){window.top.art.dialog({id:'edit'}).close()});
}
//-->
</script>