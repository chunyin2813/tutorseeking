<?php 
defined('IN_ADMIN') or exit('No permission resources.');
//$show_header = $show_validator = $show_scroll = 1;
$show_dialog = $show_header = 1; 
include $this->admin_tpl('header', 'admin');
$thisExt = isset($this->M['ext'])?$this->M['ext']:'';
$authkey = upload_key('1,'.$thisExt.',1');
?>
<script language="javascript" type="text/javascript" src="<?php echo JS_PATH;?>formvalidator.js" charset="UTF-8"></script>
<script language="javascript" type="text/javascript" src="<?php echo JS_PATH;?>formvalidatorregex.js" charset="UTF-8"></script>

<form method="post" action="?m=poster&c=popup&a=add" id="myform">


<div class="pad-10" id="imagesdiv" style="display:">
	<fieldset>
	<legend><?php echo L('photo_setting')?></legend>
	<table width="100%"  class="table_form">
	<tbody>
	<tr>
	    <th><?php echo L('for_postion')?>：</th>
		<td>
			<select name="for_postion" id="for_postion">
			<option value=''><?php echo L('please_select')?></option>
			<option value='0'>手機版</option>
			<option value='1'>ipad版</option>
			<option value='2'>電腦版</option>
			</select>
		</td>
		<td>
			<input name="seat[]" type="checkbox" value="0" />首頁
			<input name="seat[]" type="checkbox" value="1" />導師登記頁
			<input name="seat[]" type="checkbox" value="2" />家長登記頁
		</td>
	    <td rowspan="2"><a href="javascript:flashupload('imgurl<?php echo $i;?>_images', '<?php echo L('upload_photo')?>','imgurl<?php echo $i;?>',preview,'1,<?php echo $thisExt?>,1','popup', '', '<?php echo $authkey?>');void(0);"><img src="<?php echo IMG_PATH;?>icon/upload-pic.png" id="imgurl<?php echo $i;?>_s" width="105" height="88"></a><input type="hidden" id="imgurl<?php echo $i;?>" name="imageurl"></td>
	</tr>
	  
	</table>
	</fieldset>
</div>
<div class="bk15" style="margin-left:10px; line-height:30px;"><input type="submit" name="dosubmit" id="dosubmit" value=" <?php echo L('ok')?> " class="button">&nbsp;<input type="reset" value=" <?php echo L('goback')?> " class="button" onclick="history.go(-1)"></div>

	
</form>
</body>
</html>
<script type="text/javascript">
function AdsType(type) {
	$('#imagesdiv').css('display', 'none');
	$('#flashdiv').css('display', 'none');
	$('#'+type+'div').css('display', '');
}
$(document).ready(function(){
	$.formValidator.initConfig({formid:"myform",autotip:true,onerror:function(msg,obj){window.top.art.dialog({content:msg,lock:true,width:'220',height:'70'}, function(){this.close();$(obj).focus();})}});
	$('#name').formValidator({onshow:"<?php echo L('please_input_name')?>",onfocus:"<?php echo L('name_three_length')?>",oncorrect:"<?php echo L('correct')?>"}).inputValidator({min:6,onerror:"<?php echo L('adsname_no_empty')?>"}).ajaxValidator({type:"get",url:"",data:"m=poster&c=poster&a=public_check_poster",datatype:"html",cached:false,async:'true',success : function(data) {
        if( data == "1" )
		{
            return true;
		}
        else
		{
            return false;
		}
	},
	error: function(){alert("<?php echo L('server_busy')?>");},
	onerror : "<?php echo L('ads_exist')?>",
	onwait : "<?php echo L('checking')?>"
});
	$('#type').formValidator({onshow:"<?php echo L('choose_ads_type')?>",onfocus:"<?php echo L('type_selected')?>",oncorrect:"<?php echo L('correct')?>",defaultvalue:"images"}).inputValidator({min:1,onerror: "<?php echo L('choose_ads_type')?>"});
	$('#startdate').formValidator({onshow:"<?php echo L('online_time')?>",onfocus:"<?php echo L('online_time')?>",oncorrect:"<?php echo L('correct')?>"}).functionValidator({fun:isDateTime});
	$('#enddate').formValidator({onshow:"<?php echo L('one_month_no_select')?>",onfocus:"<?php echo L('down_time')?>",oncorrect:"<?php echo L('correct')?>"}).inputValidator();
	<?php if(array_key_exists('text', $setting['type'])) {?>
	<?php if($sinfo['type']=='text') {?>
	$('#title').formValidator({onshow:'<?php echo L('link_content')?>',onfoucs:'<?php echo L('link_content')?>',oncorrect:'<?php echo L('correct')?>'}).inputValidator({min:1,onerror:'<?php echo L('no_link_content')?>'});
	<?php } elseif($sinfo['type']=='code') {?>
	$('#code').formValidator({onshow:"<?php echo L('input_code')?>",onfocus:"<?php echo L('input_code')?>",oncorrect:"<?php echo L('correct')?>"}).inputValidator({min:1,onerror:'<?php echo L('input_code')?>'});
	<?php } }?>
});
 function preview(uploadid,returnid){
		var d = window.top.art.dialog({id:uploadid}).data.iframe;
		var in_content = d.$("#att-status").html().substring(1);
		$('#'+returnid).val(in_content);
		$('#'+returnid+'_s').attr('src', in_content);
}
</script>
<script type="text/javascript" src="<?php echo JS_PATH?>swfupload/swf2ckeditor.js"></script>