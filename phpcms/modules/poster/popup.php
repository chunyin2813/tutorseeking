<?php 
defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_app_class('admin','admin',0);
pc_base::load_app_func('global', 'poster');

class popup extends admin {
	private $db, $s_db;
	function __construct() {
		parent::__construct();
		$this->db=pc_base::load_model('popup_model');
		$setting = new_html_special_chars(getcache('poster', 'commons'));
		$this->M = $setting[$this->get_siteid()];
	}
	public function init(){
		$page = max(intval($_GET['page']), 1);
		$infos = $this->db->listinfo('', '', $page);
		$pages = $this->db->pages;
		include $this->admin_tpl('popup_list');
	}
	// 添加popup
	public function add(){
		if (isset($_POST['dosubmit'])) {
			$space['position'] =$_POST['for_postion'] ;
			$space['status'] = '1';
			$space['setting'] = $_POST['imageurl'];
			$space['seat']=implode(",", $_POST['seat']);
			$spaceid = $this->db->insert($space, true);
			if ($spaceid) {
				showmessage('添加成功');
			}
		}
		include $this->admin_tpl('popup_add');
	}
	public function edit(){
		$_GET['id'] = intval($_GET['id']);
		if (!$_GET['id']) showmessage(L('illegal_action'), HTTP_REFERER);
		if (isset($_POST['dosubmit'])) {
			$space['position'] =$_POST['for_postion'] ;
			// $space['status'] = '1';
			$space['seat']=implode(",", $_POST['seat']);
			$space['setting'] = $_POST['imageurl'];
			$spaceid = $this->db->update($space, array('id'=>$_GET['id']));
			if ($spaceid) {
				showmessage('更改成功');
			}
		}
		
		$info = $this->db->get_one(array('id'=>$_GET['id']));
		include $this->admin_tpl('popup_edit');
	}

	
	public function public_approval() {
		if (!isset($_POST['id']) || !is_array($_POST['id'])) {
			showmessage(L('illegal_parameters'), HTTP_REFERER);
		} else {
			array_map(array($this, _approval), $_POST['id']);
		}
		showmessage(L('operation_success'), HTTP_REFERER);
	}
	
	private function _approval($id = 0) {
		$id = intval($id);
		if (!$id) return false;
		$_GET['passed'] = intval($_GET['passed']);
		$this->db->update(array('status'=>$_GET['passed']), array('id'=>$id));
		return true;
	}
}
?>