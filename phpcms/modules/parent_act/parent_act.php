<?php
defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_app_class('admin','admin',0);
$session_storage = 'session_'.pc_base::load_config('system','session_storage');//输出值session_mysql
pc_base::load_sys_class($session_storage);//载入文件路径phpcms\libs\classes\session_mysql.class.php
class parent_act extends admin {
    function __construct() {
        header("Content-type: text/html; charset=utf-8");
    	$this->db = pc_base::load_model('member_model');
    	$this->act_db = pc_base::load_model('parent_act_model');
    	$this->act_rev_db = pc_base::load_model('parent_act_rev_model');
    	$this->place=pc_base::load_model('sys_location_model');
    }
  
    public function init() {
		$sql = "select m.personid,p.par_id,p.par_act_num,p.par_remark,p.par_status from `v9_parent_act_rev` as p 
		left join `v9_member` as m 
		on p.par_parent_id = m.userid";
		$act_number = $_GET['act_number'];
		$act_status = $_GET['act_status'];

		if(!empty($act_number) && $act_status==''){
			$sql .= " "."where p.par_act_num="."'".$act_number."'";
		}
		if($act_status!='' && empty($act_number)){	
			$sql .= " "."where p.par_status ="."'".$act_status."'";
		}
		if($act_status!='' && !empty($act_number)){
			$sql .= " "."where p.par_status ="."'".$act_status."'"."and p.par_act_num="."'".$act_number."'";
		}
        $sql.=" order by p.par_id desc";
        // var_dump($sql);
        // return;
        $get_page = pc_base::load_model('get_model');
        $page = intval($_GET['page'])?intval($_GET['page']) :'1';
        $infos = $get_page->multi_listinfo($sql,$page,20);
    	$pages = $get_page->pages;
        include $this->admin_tpl('parent_act');
    }
    
    public function agree(){
    	$par_act_num = isset($_GET['par_act_num']) ? $_GET['par_act_num'] : showmessage(L('illegal_parameters'), HTTP_REFERER);
    	$par_status = $this->act_rev_db->get_one(array('par_act_num'=>$par_act_num),'par_status')['par_status'];
    	include $this->admin_tpl('agree');
    }
    //获取家长邮箱
    public function get_stuemail($par_act_num){
    	$sql = "select m.email from `v9_member` as m
				left join `v9_parent_act_rev` as p
				on p.par_parent_id = m.userid
				where p.par_act_num = "."'".$par_act_num."'";
		$to_stumail = $this->act_rev_db->fetch_array($this->act_rev_db->query($sql))[0]['email'];
		return $to_stumail;
    }

    public function ajax_agree(){
    	if(isset($_POST) && !empty($_POST)){
    		$par_status = 1;
    		$par_remark = "";
			$par_act_num = $_POST['par_act_num'];
			//获取家长编号
	    	$par_parent_id = $this->act_rev_db->get_one(array('par_act_num'=>$par_act_num),'par_parent_id')['par_parent_id'];
	    	$personid = $this->db->get_one(array('userid'=>$par_parent_id),'personid')['personid'];

			$res = $this->act_rev_db->update(array('par_remark'=>$par_remark,'par_status'=>$par_status),array('par_act_num'=>$par_act_num));
			$to_stumail = $this->get_stuemail($par_act_num);
			$username = $this->db->get_one(array('email'=>$to_stumail,'role_verify'=>'1'),'username')['username'];

			//同意  发邮件
			if($res){
			$stu_email_data="<!DOCTYPE html>
	                <html>
	                <head>
	                    <meta charset='utf-8'>
	                    <meta name='viewport' content='width=device-width,initial-scale=1.0, maximum-scale=1.0,minimum-scale=1.0,user-scalable=no'>
	                    <title>tutorseeking</title>
	                    <style>
	                        /*.match-table td{padding: 6px;}*/
	                    </style>
	                </head>
	                <body>
	                    <div style='max-width: 880px; margin:0 auto; font-size: 14px; color: #333; text-align: justify'>
	                        <form action=''>
	                            <div style='text-align: center'>
	                                <br>
	                                <br>
	                                <img width='250px' src='https://www.tutorseeking.com/statics/images/add_image/logo-sticky.png' alt=''>
	                            </div>
	                            <h2 style='text-align: center; font-size: 24px;'>尊敬的用戶：您好！您發起TutorSeeking的家長活動审核已经通过！以下是您的活動信息！</h2>
	                            <br>
	                            <div style='text-align: center;margin-left:8%;'>
	                                <div style='display:inline-block; margin-right: 60px; margin-bottom: 10px'>
	                                    <table border='1px' class='match-table' cellpadding='0' cellspacing='0' style='width: 500px; font-size: 20px; text-align: center;border-collapse:collapse' >
	                                        <tbody>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;'  colspan='2' >用戶信息</td>
	                                        </tr>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;' >姓名</td>
	                                            <td style='padding: 6px;font-size: 20px;' >".$username."</td>
	                                        </tr>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;' >活動編號</td>
	                                            <td style='padding: 6px;font-size: 20px;' >".$par_act_num."</td>
	                                        </tr>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;' >活動状态</td>
	                                            <td style='padding: 6px;font-size: 20px;' >"."已通过"."</td>
	                                        </tr>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;'  colspan='2'><a href='".APP_PATH."index.php?m=content&c=index&a=send_url&act_id=".$par_act_num."&act_personid=".$personid."&act_userid=".$_SESSION['id']."' style='text-decoration: none; color: #e39916;'>查看詳情</a></td>
	                                        </tr>
	                                        </tbody>
	                                    </table>
	                                </div>
	                            </div>
	                            <br>";
	                $stu_email_data .="        </form>
	                    </div>
	                </body>
	                </html>";
	            pc_base::load_sys_func('mail'); //實例化郵件類
	            // $to_stumail = "2905665423@qq.com";
	            sendmail($to_stumail,'TutorSeeking家長活動信息', $stu_email_data);//發送郵件給註冊用戶
				echo "success";
			}
    		exit;
    	}
    }

    public function disagree(){
    	$par_act_num = isset($_GET['par_act_num']) ? $_GET['par_act_num'] : showmessage(L('illegal_parameters'), HTTP_REFERER);
    	$par_remark = $this->act_rev_db->get_one(array('par_act_num'=>$par_act_num),'par_remark')['par_remark'];
    	include $this->admin_tpl('disagree');
    }

    public function dis_agree(){
    	if(isset($_POST) && !empty($_POST)){
    		$par_remark = $_POST['par_remark'];
			$par_act_num = $_POST['par_act_num'];
			//获取家长编号
	    	$par_parent_id = $this->act_rev_db->get_one(array('par_act_num'=>$par_act_num),'par_parent_id')['par_parent_id'];
	    	$personid = $this->db->get_one(array('userid'=>$par_parent_id),'personid')['personid'];

			$par_status = 2;
			$act_personid = $_SESSION['act_personid'];
			$act_number = $_SESSION['act_number'];
			$res = $this->act_rev_db->update(array('par_remark'=>$par_remark,'par_status'=>$par_status),array('par_act_num'=>$par_act_num));
			if($res){
			//拒绝  发邮件
			$to_stumail = $this->get_stuemail($par_act_num);
			$username = $this->db->get_one(array('email'=>$to_stumail,'role_verify'=>'1'),'username')['username'];
			$stu_email_data="<!DOCTYPE html>
	                <html>
	                <head>
	                    <meta charset='utf-8'>
	                    <meta name='viewport' content='width=device-width,initial-scale=1.0, maximum-scale=1.0,minimum-scale=1.0,user-scalable=no'>
	                    <title>tutorseeking</title>
	                    <style>
	                        /*.match-table td{padding: 6px;}*/
	                    </style>
	                </head>
	                <body>
	                    <div style='max-width: 880px; margin:0 auto; font-size: 14px; color: #333; text-align: justify'>
	                        <form action=''>
	                            <div style='text-align: center'>
	                                <br>
	                                <br>
	                                <img width='250px' src='https://www.tutorseeking.com/statics/images/add_image/logo-sticky.png' alt=''>
	                            </div>
	                            <h2 style='text-align: center; font-size: 24px;'>尊敬的用戶：您好！您發起TutorSeeking的家長活動审核未通过！以下是您的活動信息！</h2>
	                            <br>
	                            <div style='text-align: center;margin-left:8%;'>
	                                <div style='display:inline-block; margin-right: 60px; margin-bottom: 10px'>
	                                    <table border='1px' class='match-table' cellpadding='0' cellspacing='0' style='width: 500px; font-size: 20px; text-align: center;border-collapse:collapse' >
	                                        <tbody>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;'  colspan='2' >用戶信息</td>
	                                        </tr>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;' >姓名</td>
	                                            <td style='padding: 6px;font-size: 20px;' >".$username."</td>
	                                        </tr>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;' >活動編號</td>
	                                            <td style='padding: 6px;font-size: 20px;' >".$par_act_num."</td>
	                                        </tr>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;' >活動状态</td>
	                                            <td style='padding: 6px;font-size: 20px;' >"."未通过"."</td>
	                                        </tr>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;' >未通过原因</td>
	                                            <td style='padding: 6px;font-size: 20px;' >".$par_remark."</td>
	                                        </tr>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;'  colspan='2'><a href='".APP_PATH."index.php?m=content&c=index&a=send_url&act_id=".$par_act_num."&act_personid=".$personid."&act_userid=".$_SESSION['id']."' style='text-decoration: none; color: #e39916;'>查看詳情</a></td>
	                                        </tr>
	                                        </tbody>
	                                    </table>
	                                </div>
	                            </div>
	                            <br>";
	                $stu_email_data .="        </form>
	                    </div>
	                </body>
	                </html>";
	            pc_base::load_sys_func('mail'); //實例化郵件類
	            // $to_stumail = "2905665423@qq.com";
	            sendmail($to_stumail,'TutorSeeking家長活動信息', $stu_email_data);//發送郵件給註冊用戶
				echo "success";
			}
    		exit;
    	}
    }

    
    public function act_detail(){
    	$par_act_num = isset($_GET['par_act_num']) ? $_GET['par_act_num'] : showmessage(L('illegal_parameters'), HTTP_REFERER);
    	$detail_sql = "select * from `v9_parent_act_rev` as r
    					left join `v9_parent_act` as p
    					on r.par_act_num = p.act_number
    					where p.act_number ="."'".$par_act_num."'";				
    	$detail = $this->act_db->fetch_array($this->act_db->query($detail_sql))[0];	

    	//活動地址
    	$this->loc_db = pc_base::load_model('sys_location_model');
    	$act_address = $this->loc_db->get_one(array('loc_id'=>$detail['act_area']))['loc_name'];


    	//參加者
    	$total=array();
    	$pid_arr = explode(',',$detail['act_join_parentid']);
    	foreach ($pid_arr as $key => $v) {
    		$one=$this->db->get_one(array('userid'=>$v),'personid,username,phone');
    		array_push($total, $one);
    	}
    	include $this->admin_tpl('detail');
    }


    public function edit(){
    	$timelist = array(
        '0700' =>"7:00" ,'0715' =>"7:15" ,'0730' =>"7:30",'0745' =>"7:45",
        '0800' =>"8:00" ,'0815' =>"8:15" ,'0830' =>"8:30" ,'0845' =>"8:45",
        '0900' =>"9:00" ,'0915' =>"9:15" ,'0930' =>"9:30" ,'0945' =>"9:45",
        '1000' =>"10:00" ,'1015' =>"10:15" ,'1030' =>"10:30" ,'1045' =>"10:45",
        '1100' =>"11:00" ,'1115' =>"11:15" ,'1130' =>"11:30" ,'1145' =>"11:45",
        '1200' =>"12:00" ,'1215' =>"12:15" ,'1230' =>"12:30" ,'1245' =>"12:45",
        '1300' =>"13:00" ,'1315' =>"13:15" ,'1330' =>"13:30" ,'1345' =>"13:45",
        '1400' =>"14:00" ,'1415' =>"14:15" ,'1430' =>"14:30" ,'1445' =>"14:45",
        '1500' =>"15:00" ,'1515' =>"15:15" ,'1530' =>"15:30" ,'1545' =>"15:45",
        '1600' =>"16:00" ,'1615' =>"16:15" ,'1630' =>"16:30" ,'1645' =>"16:45",
        '1700' =>"17:00" ,'1715' =>"17:15" ,'1730' =>"17:30" ,'1745' =>"17:45",
        '1800' =>"18:00" ,'1815' =>"18:15" ,'1830' =>"18:30" ,'1845' =>"18:45",
        '1900' =>"19:00" ,'1915' =>"19:15" ,'1930' =>"19:30" ,'1945' =>"19:45",
        '2000' =>"20:00" ,'2015' =>"20:15" ,'2030' =>"20:30" ,'2045' =>"20:45",
        '2100' =>"21:00" ,'2115' =>"21:15" ,'2130' =>"21:30" ,'2145' =>"21:45",
        '2200' =>"22:00" ,'2215' =>"22:15" ,'2230' =>"22:30" ,'2245' =>"22:45",
        '2300' =>"23:00");
    	$id = isset($_GET['id']) ? $_GET['id'] : showmessage(L('illegal_parameters'), HTTP_REFERER);				
    	$detail = $this->act_db->get_one(array('act_id'=>$id));

    	//地區數據
    	$area=$this->place->select(array('loc_level'=>2),'loc_id,loc_name');
    	// var_dump($area);
    	foreach ($area as $key=> $v) {
    		$palce=$this->place->select(array('loc_level'=>3,'loc_parentid'=>$v['loc_id']),'loc_id,loc_name');
    		$area[$key]['child']=$palce;
    	}
    	include $this->admin_tpl('edit');
    }


    public function editPost(){
    	// var_dump($_POST);
    	if(isset($_POST['parent_act_info'])){
			$parent_act_info = $_POST['parent_act_info'];
			//拼接郵箱需要字段
			$fromTimes = substr_replace($parent_act_info['from_time'],':','2','0');
			$toTimes = substr_replace($parent_act_info['to_time'],':','2','0');

			
			//活動詳情字段
			if(!empty($parent_act_info['to_time']) && !empty($parent_act_info['to_time'])){
				$parent_act_info['act_time'] = $parent_act_info['from_time'].",".$parent_act_info['to_time'];
			}
			unset($parent_act_info['from_time']);
			unset($parent_act_info['to_time']);
			

			$act_add_res=$this->act_db->update($parent_act_info,array('act_id'=>$parent_act_info['act_id']));
			$data=$this->act_db->get_one(array('act_id'=>$parent_act_info['act_id']));
			$exdata=explode(',', $data['act_join_parentid']);
			//发起活动成功,发生邮件给管理员与家长
			if($act_add_res){
				//活动地址
				$act_area_name = $this->place->get_one(array('loc_id'=>$parent_act_info['act_area']))['loc_name'];
				$address=$act_area_name." ".$data['act_address'];
				//活動日期
				$act_time = $parent_act_info['act_date']." ".$fromTimes."  至  ".$parent_act_info['act_closing_date']." ".$toTimes;
				//截止日期
				$act_cut_time = $parent_act_info['act_cut_date'];
				//收費情況
				if($data['act_ischarge']){
					if($data['act_charge_mode']){
						$mode=$data['act_charge'].'/每次';
					}else{
						$mode=$data['act_charge'].'/每小時';
					}
				}else{
					$mode='不收費';
				}
				//獲取家長郵箱地址
				foreach ($exdata as  $v) {
					$eData=$this->db->get_one(array('userid'=>$v),'email');
					$this->send($eData['email'],$data['act_name'],$data['act_number'],$data['act_aim'],$data['act_content'],$act_time,$act_cut_time,$address,$mode);
				}
				showmessage('更改成功', HTTP_REFERER);
			}else{
				showmessage('更改失敗', HTTP_REFERER);
			} 
		}
    }


    public function send($email,$act_name,$act_number,$act_aim,$act_content,$act_time,$act_cut_time,$address,$mode){
    	$admin_email_data="<!DOCTYPE html>
            <html>
            <head>
                <meta charset='utf-8'>
                <meta name='viewport' content='width=device-width,initial-scale=1.0, maximum-scale=1.0,minimum-scale=1.0,user-scalable=no'>
                <title>tutorseeking</title>
                <style>
                    /*.match-table td{padding: 6px;}*/
                </style>
            </head>
            <body>
                <div style='max-width: 880px; margin:0 auto; font-size: 14px; color: #333; text-align: justify'>
                    <form action=''>
                        <div style='text-align: center'>
                            <br>
                            <br>
                            <img width='250px' src='https://www.tutorseeking.com/statics/images/add_image/logo-sticky.png' alt=''>
                        </div>
                        <h2 style='text-align: center; font-size: 24px;'>您好!  您所參加的家長活動【".$act_name."】信息有改動，最新信息如下</h2>
                        <br>
                        <div style='text-align: center;margin-left:8%;'>
                            <div style='display:inline-block; margin-right: 40px; margin-bottom: 10px'>
                                <table border='1px' class='match-table' cellpadding='0' cellspacing='0' style='width: 650px; font-size: 20px; text-align: center;border-collapse:collapse' >
                                    <tbody>
                                    <tr>
                                        <td style='padding: 6px;font-size: 20px;'  colspan='2' >活動信息</td>
                                    </tr>
                                    <tr>
                                        <td style='padding: 6px;font-size: 20px;' >活動編號</td>
                                        <td style='padding: 6px;font-size: 20px;' >".$act_number."</td>
                                    </tr>
                                    <tr>
                                        <td style='padding: 6px;font-size: 20px;' >活動名稱</td>
                                        <td style='padding: 6px;font-size: 20px;' >".$act_name."</td>
                                    </tr>
                                    <tr>
                                        <td style='padding: 6px;font-size: 20px;' >活動目的</td>
                                        <td style='padding: 6px;font-size: 20px;' >".$act_aim."</td>
                                    </tr>
                                    <tr>
                                        <td style='padding: 6px;font-size: 20px;' >活動内容</td>
                                        <td style='padding: 6px;font-size: 20px;' >".$act_content."</td>
                                    </tr>
                                    <tr>
                                        <td style='padding: 6px;font-size: 20px;' >活動時間</td>
                                        <td style='padding: 6px;font-size: 20px;' >".$act_time."</td>
                                    </tr>
                                    <tr>
                                        <td style='padding: 6px;font-size: 20px;' >收費情況</td>
                                        <td style='padding: 6px;font-size: 20px;' >".$mode."</td>
                                    </tr>
                                    <tr>
                                        <td style='padding: 6px;font-size: 20px;' >活動報名截止時間</td>
                                        <td style='padding: 6px;font-size: 20px;' >".$act_cut_time."</td>
                                    </tr>
                                    <tr>
                                        <td style='padding: 6px;font-size: 20px;' >活動地址</td>
                                        <td style='padding: 6px;font-size: 20px;' >".$address."</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <br>";
            $admin_email_data .="        </form>
                </div>
            </body>
            </html>";
            pc_base::load_sys_func('mail'); //實例化郵件類
            sendmail($email,'TutorSeeking家長活動信息', $admin_email_data);//發送郵件給管理員
    }

}
?>