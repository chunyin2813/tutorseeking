<?php defined('IN_ADMIN') or exit('No permission resources.');?>
<?php include $this->admin_tpl('header', 'admin');?>
<script language="javascript" type="text/javascript" src="<?php echo JS_PATH?>member_common.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo JS_PATH?>formvalidator.js" charset="UTF-8"></script>
<script language="javascript" type="text/javascript" src="<?php echo JS_PATH?>formvalidatorregex.js" charset="UTF-8"></script>
<style type="text/css">
	.common-form {
		margin-top: 10% !important;
	}
	fieldset {
	    width: 65% !important;
	    margin:0 auto !important;
	}
</style>
<div class="pad-10">
<div class="common-form">

<fieldset>
	<legend>活動編號:&nbsp;&nbsp;<b><?php echo $par_act_num;?></b></legend>
	<input type="hidden" name="par_act_num" value="<?php echo $par_act_num;?>">
	<table width="100%" class="table_form">
		<tr>
			<td width="80">拒絕理由：</td>

			<td>
				<textarea cols="45" rows="5" class="par_remark" name="par_remark"><?php echo $par_remark;?> </textarea>
			</td>
		</tr>
		
		<tr>
			<td>
				
			</td>
			<td>
				<button class="btn_submit" onclick="disagree()">確認</button>
			</td>
		</tr>
	</table>
</fieldset>

</div>
</div>
</body>
<script type="text/javascript">
	function disagree(){
		var par_remark = $(".par_remark").val();
		var par_act_num = $("b").text();
		$.post('./index.php?m=parent_act&c=parent_act&a=dis_agree',{par_remark:par_remark,par_act_num:par_act_num},function(data){
			if(data == 'success'){
				alert('操作成功!');
			}else{
				alert('操作失败,請重試!');
				window.location.reload();
			}
		});

	}

	function cancel() {
		window.location.reload();
		window.top.art.dialog({id:'edit'}).close();
	}
</script>
</html>