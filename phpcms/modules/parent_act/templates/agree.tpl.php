<?php defined('IN_ADMIN') or exit('No permission resources.');?>
<?php include $this->admin_tpl('header', 'admin');?>
<script language="javascript" type="text/javascript" src="<?php echo JS_PATH?>member_common.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo JS_PATH?>formvalidator.js" charset="UTF-8"></script>
<script language="javascript" type="text/javascript" src="<?php echo JS_PATH?>formvalidatorregex.js" charset="UTF-8"></script>
<style type="text/css">
	.common-form {
		margin-top: 15% !important;
	}
	fieldset {
	    width: 65% !important;
	    margin:0 auto !important;
	}

</style>
<div class="pad-10">
<div class="common-form">

<fieldset>
	<legend>活動編號:&nbsp;&nbsp;<b><?php echo $par_act_num;?></b></legend>
	<table width="100%" class="table_form">
		<tr>
			<td width="80">是否同意：</td>

			<td>
				<span class="radio radio-primary">
                    <input type="radio" class="par_status"  value="1" name="par_status" checked>
                <label for="">是</label>
                </span>
			</td>
		</tr>
	</table>
</fieldset>

</div>
</div>
</body>
</html>