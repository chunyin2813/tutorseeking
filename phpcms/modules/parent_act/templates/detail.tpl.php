<?php defined('IN_ADMIN') or exit('No permission resources.');?>
<?php include $this->admin_tpl('header', 'admin');?>
<script language="javascript" type="text/javascript" src="<?php echo JS_PATH?>member_common.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo JS_PATH?>formvalidator.js" charset="UTF-8"></script>
<script language="javascript" type="text/javascript" src="<?php echo JS_PATH?>formvalidatorregex.js" charset="UTF-8"></script>
<div class="pad-10">
<div class="common-form">	
<?php if(!empty($detail)){?>
<fieldset>
	<legend>活動編號:&nbsp;&nbsp;<?php echo $par_act_num;?></legend>
	<table  class="table_form" style="table-layout:fixed" width="100%">
		<tr>
			<td>活動名稱</td> 
			<td><?php echo $detail['act_name'];?></td>
		</tr>
		<tr>
			<td>活動舉行地區</td> 
			<td><?php echo $act_address;?></td>
		</tr>
		<tr>
			<td>活動舉行地址</td>
            <td <td width="70%" style="word-break : break-all; overflow:hidden; "><?php echo $detail['act_address'];?></td> 
		</tr>
		<tr>
			<td>活動是否收費</td> 
			<td><?php $detail['act_ischarge']=='1'?print '是':print '否'?></td>
		</tr>
		<?php if($detail['act_ischarge']=='1'){?>
		<tr>
			<td>活動收費模式</td> 
			<td><?php echo $detail['act_charge']; ?>/<?php $detail['act_charge_mode']=='1'?print '每次':print '每小時'?></td>
		</tr>
		<?php }?>
		<tr>
			<td>活動人數限額</td> 
			<td>成年人:<?php echo $detail['act_adult_limit'];?>&nbsp;人&nbsp;&nbsp;&nbsp;&nbsp;儿童:<?php echo $detail['act_child_limit'];?>&nbsp;人</td>
		</tr>
		<tr>
			<td>活動時間</td> 
			<?php 
                $time_arr = explode(',',$detail['act_time']);
            ?>
			<?php if(!empty($detail['act_time'])){?>
            <td><?php echo substr_replace($time_arr[0],':','2','0');?>&nbsp;-&nbsp;<?php echo substr_replace($time_arr[1],':','2','0');?></td>
            <?php }else{?>
            <td></td>
            <?php }?>
		</tr>
		<tr>
			<td>活動日期</td> 
			<td><?php echo $detail['act_date'];?>&nbsp;&nbsp;至&nbsp;&nbsp;<?php echo $detail['act_closing_date'];?></td>
		</tr>
		<tr>
			<td>活動報名截止日期</td> 
			<td><?php echo $detail['act_cut_date'];?></td>
		</tr>
		<tr>
			<td>活動目的</td> 
			<td <td width="70%" style="word-break : break-all; overflow:hidden; "><?php echo $detail['act_aim'];?></td>
		</tr>
		<tr>
			<td>活動內容</td> 
            <td <td width="70%" style="word-break : break-all; overflow:hidden; "><?php echo $detail['act_content'];?></td>
		</tr>
		<tr>
			<td>活動狀態</td> 
			<?php if($detail['par_status']=='0'){?>
            	<td>待審核</td>
            <?php }elseif($detail['par_status']=='1'){?>
            	<td>同意</td>
            <?php }else{?>
            	<td>拒絕</td>
            <?php }?>
		</tr>
		<?php if($detail['par_status']=='2'){?>
		<tr>
			<td>活動備註</td> 
            <td width="70%" style="word-break : break-all; overflow:hidden; "><?php echo $detail['par_remark'];?></td>
		</tr>
		<?php }?>
		
	</table>
</fieldset>

<fieldset style="margin-top: 20px">
	<legend>參加活動著</legend>
	<table width="100%" class="table_form">
		<tr>
			<td width="30%">參加活動者編號</td>
			<td width="30%">參加活動者姓名</td>
			<td width="30%">聯係方式</td>
		</tr>
		<?php foreach ($total as $one): ?>
			<tr>
			<td width="30%" ><?php echo $one['personid'];?></td>
			<td width="30%" ><?php echo $one['username'];?></td>
            <td width="30%" ><?php echo $one['phone'];?></td>
			</tr>
		<?php endforeach ?>
		
	</table>
</fieldset>
<?php }?>
</div>
</div>
</body>
</html>