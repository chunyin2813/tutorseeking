<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header','admin');
?>
<div class="pad-lr-10">
	<table width="100%" cellspacing="0" class="search-form">
	    <tbody>
			<tr>
				<td>
					<div class="explain-col">
						<input type="text" name="" placeholder="活動編號" class="act_number" value="<?php if(isset($_GET['act_number'])){ print $_GET['act_number'];}?>">
						<select>
							<option value="" <?php  if(isset($_GET['act_status']) && $_GET['act_status']==""){?>selected<?php }?>>請選擇活動狀態</option>
							<option value="0" <?php if(isset($_GET['act_status']) && $_GET['act_status']=='0'){?>selected<?php }?>>未確認</option>
							<option value="1" <?php if(isset($_GET['act_status']) && $_GET['act_status']=='1'){?>selected<?php }?>>已同意</option>
							<option value="2" <?php if(isset($_GET['act_status']) && $_GET['act_status']=='2'){?>selected<?php }?>>已拒絕</option>
						</select>
						<input type="button" name="search" class="button act_search" value="筛选" />
						<input type="button" name="reset" class="button act_reset" value="重置" onclick="javascript:window.location.href='./index.php?m=parent_act&c=parent_act&a=init'"/>
						<input type="button" value="刷新" class="button" style="float:right;" onclick="Refresh();">
					</div>
				</td>
			</tr>
	    </tbody>
	</table>
	<script>
		$('.act_search').click(function(){
			var url = './index.php?m=parent_act&c=parent_act&a=init';
			var act_number = $(".act_number").val();
			var select_status = $("select  option:selected").val();
			if(act_number != '' || select_status!=''){ url = url+'&act_number='+act_number+'&act_status='+select_status; }
			window.location.href=url;
		});
	</script>

	<div class="table-list">
	    <table width="100%" cellspacing="0" class="contentWrap">
	        <thead>
	            <tr>
				<th width="50">ID</th>
				<th width="50">發起者編號</th>
				<th width="170">活動編號</th>
				<th width="50">狀態</th>
				<th width="370">備註</th>
				<th width="170">操作</th>
	            </tr>
	        </thead>
	        <tbody> 
	        <?php foreach($infos as $info){?>
				<tr>
				<td align="center"><?php echo $info['par_id']; ?></td>
				<td align="center"><?php $info['personid']?print $info['personid']:print '已注銷'; ?></td>
				<td align="center"><?php echo $info['par_act_num']; ?></td>
                <?php if($info['par_status']=='0'){?>
                	<td align="center">待審核</td>
                <?php }elseif($info['par_status']=='1'){?>
                	<td align="center">同意</td>
                <?php }else{?>
                	<td align="center">拒絕</td>
                <?php }?>
                <?php if(strlen($info['par_remark'])>100){ ?>
                    <td align="center"><?php echo mb_substr($info['par_remark'],0,8,'utf-8');?>...</td>
                <?php }else{?>
                    <td align="center"><?php echo $info['par_remark']; ?></td>
                <?php }?>
				<td align="center"><?php if($info['par_status']=='2'){?> <?php }elseif($info['par_status']=='1'){?> <?php }else{?><a href="javascript:agree('<?php echo $info['par_act_num']?>')">同意</a> | <a href="javascript:disagree('<?php echo $info['par_act_num']?>')">拒絕</a> | <?php }?><a href="javascript:detail('<?php echo $info['par_act_num']?>')">[查看詳情]</a> | <a href="?m=parent_act&c=parent_act&a=edit&id=<?php echo $info['par_id']?>'">[修改]</a></td>
				</tr>
			<?php }?>	
			</tbody>
	    </table> 
	</div>
	<div id="pages"><?php echo $pages;?></div>
</div>
<script type="text/javascript">
	function Refresh(){
		window.location.reload();
	}

	function disagree(par_act_num){
		window.top.art.dialog({id:'edit'}).close();
		window.top.art.dialog({
			title:'拒絕理由',
			id:'edit',
			iframe:'?m=parent_act&c=parent_act&a=disagree&par_act_num='+par_act_num,
			width:'700',
			height:'500',
			cancelVal: '关闭',
		    cancel: true ,
		},
		function(){
			location.href = "?m=parent_act&c=parent_act&a=init";
		});

	}


	function detail(par_act_num){
		window.top.art.dialog({id:'edit'}).close();
		window.top.art.dialog({title:'活動詳情',id:'edit',iframe:'?m=parent_act&c=parent_act&a=act_detail&par_act_num='+par_act_num,width:'700',height:'500'}, function(){window.top.art.dialog({id:'edit'}).close()});
	}

	function edit(id){
		window.top.art.dialog({title:'修改活動',id:'edit',iframe:''+id,width:'700',height:'500'}, function(){window.top.art.dialog({id:'edit'}).close()});	
	}
	function agree(par_act_num){
		window.top.art.dialog({id:'edit'}).close();
		window.top.art.dialog({title:'',id:'edit',iframe:'?m=parent_act&c=parent_act&a=agree&par_act_num='+par_act_num,width:'700',height:'500',
		    cancelVal: '关闭',
		    cancel: true ,
		}, function(){
			var par_status = '1';
			$.post('./index.php?m=parent_act&c=parent_act&a=ajax_agree',{par_status:par_status,par_act_num:par_act_num},function(data){
			if(data == 'success'){
				alert('操作成功!');
				location.href = "?m=parent_act&c=parent_act&a=init";
			}else{
				alert('操作失败,請重試!');
				window.location.reload();
			}
		});
			// window.top.art.dialog({id:'edit'}).close();
		});
	}

</script>
</body>