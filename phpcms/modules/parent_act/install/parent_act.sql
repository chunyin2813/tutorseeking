DROP TABLE IF EXISTS `phpcms_parent_act`;
CREATE TABLE IF NOT EXISTS `v9_parent_act` (
  `act_number` varchar(60) NOT NULL COMMENT '活動編號',
  `act_name` varchar(60) NOT NULL COMMENT '活動名稱',
  `act_date` date NOT NULL COMMENT '活動日期',
  `act_time` varchar(60) NOT NULL COMMENT '活動時間段',
  `act_area` varchar(60) NOT NULL COMMENT '活動地區',
  `act_address` varchar(60) NOT NULL COMMENT '活動地址',
  `act_content` text NOT NULL COMMENT '活動內容',
  `act_aim` text NOT NULL COMMENT '活動目的',
  `act_ischarge` enum('1','0') NOT NULL COMMENT '活動是否收費:1(是),0(否)',
  `act_closing_date` varchar(60) NOT NULL COMMENT '活動截止日期',
  `act_adult_limit` int(11) NOT NULL COMMENT '活動成年人人數限制',
  `act_child_mode` int(11) NOT NULL COMMENT '活動兒童人數限制',
  `act_hour_charge` int(11) NOT NULL COMMENT '按時收費費用',
  `act_frequency_charge` int(11) NOT NULL COMMENT '按次數收費費用',
  `act_id` int(11) NOT NULL,
  PRIMARY KEY (`act_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
