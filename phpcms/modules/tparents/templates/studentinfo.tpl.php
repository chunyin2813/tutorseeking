<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header','admin');
?>
<!--学生详情主页面-->
<div class="pad_10">
<div class="table-list">
		<?php foreach ($infos as $info) {	?>
	     <table width="70%" class="table"cellspacing="0" border="1" align="center">
	     <caption><strong>学生基本信息</strong></caption>
	    <tbody>
            <tr width="100">
            	<th height="30">联系人名称:</th>
				<th height="60"><?php empty($info['student_contactname'])?print "未填寫":print $info['student_contactname'] ; ?></th>
				<th height="30">學生名稱:</th>
				<th height="60"><?php echo $info['student_name']?></th>
			</tr>
			<tr width="100">
            	<th height="30">學生昵稱:</th>
				<th height="60"><?php empty($tutor_type_info1['st_new_nickname'])?print "未填寫":print $tutor_type_info1['st_new_nickname'] ?></th>
				<th height="30">学生就读学校名称:</th>
				<th height="60"><?php empty($info['student_school'])?print "未填寫":print $info['student_school'] ; ?></th>
			</tr>
            <tr width="100">
            	<th height="30">住宅电话号码:</th>
            	<th height="60"><?php empty($info['student_housemobile'])?print "未填寫":print $info['student_housemobile'] ; ?></th>
            	<th height="30">手提電話:</th>
            	<th height="60"><?php empty($info['student_mobile'])?print "未填寫":print $info['student_mobile'] ; ?></th>
			</tr>
			<tr width="100">
            	<th height="30">電郵地址:</th>
            	<th height="60"><?php empty($info['student_email'])?print "未填寫":print $info['student_email'] ; ?></th>
				<th height="30">学生与联络人关系:</th>
				<th height="60"><?php empty($info['student_relation'])?print "未填寫":print $info['student_relation']; ?></th>
			</tr>	
            <tr width="100">
            	<th height="30">居住地区:</th>
            	<th height="60"><?php empty($loc_info['loc_name'])?print "未填寫":print $loc_info['loc_name']; ?></th>
				<th height="30">居住地址:</th>
				<th height="60"><?php empty($info['student_address'])?print "未填寫":print $info['student_address']; ?></th>
			</tr>
            <!-- <tr width="100">
            	<th height="30">学生出生年月:</th>
            	<th height="60"><?php //empty($info['student_birth_year'])?print "未填寫":print $info['student_birth_year'] ; ?></th>
            	<th height="30">更新时间:</th>
            	<th height="60"><?php //echo $info['student_modtime']?></th>
             </tr> -->
	     <tbody>      
	    </table> 
	    <?php }?>
	    <br /><br />
	     <table width="70%" class="table"cellspacing="0" border="1" align="center">
	     <caption><strong>学生基本要求信息</strong></caption>
	    <tbody>
            <tr width="100">
				<th height="30">要求教授类别:</th>
				<th height="60"><?php echo $tutor_type_info1['tt_name']?></th>
				<th height="30">要求教授项目:</th>
				<th height="60"><?php echo $tutor_type_info2['tt_name']?></th>
			</tr>
            <tr width="100">
            	<th height="30">要求教授级别:</th>
            	<th height="60"><?php echo $tutor_type_info3['tt_name']?></th>
				<th height="30">要求上课地区:</th>
            	<th height="60"><?php echo $stu_loc_info['loc_name']?></th>
			</tr>	
            <tr width="100">
            	<th height="30">男学生人数:</th>
            	<th height="60"><?php echo $trans_info['st_num_join_m']?>人</th>
				<th height="30">女学生人数:</th>
				<th height="60"><?php echo $trans_info['st_num_join_f']?>人</th>
			</tr>	
            <tr width="100">
            	<th height="30">总人数:</th>
            	<th height="60"><?php echo $trans_info['st_num_join']?>人</th>
				<th height="30">要求每次上课时间:</th>
				<th height="60"><?php empty($trans_info['st_class_hour'])?print "未填寫":print $trans_info['st_class_hour']."小时" ; ?></th>
			</tr>	
            <tr width="100">
            	<th height="30">要求学费/每小时:</th>
            	<th height="60"><?php echo $trans_info['st_tutor_fee']?>元/每小时</th>
            	<th height="30">要求最低教育程度:</th>

            	<th height="60"><?php empty($min_grade['grade_name'])?print "未填寫":print $min_grade['grade_name'] ; ?></th>
             </tr>
             <tr width="100">
             	<th height="30">要求导师性别:</th>
             	<?php if($trans_info['st_req_sex']=='M'){ ?>
             	<th height="60">男</th>
             	<?php }elseif($trans_info['st_req_sex']=='F'){?>
             	<th height="60">女</th>
             	<?php }elseif($trans_info['st_req_sex']=='A'){?>
             	<th height="60">都可以</th>
             	<?php }?>
            	
            	<th height="30">要求授课语言:</th>
            	<th height="60"><?php empty($trans_info['st_main_lang'])?print "未填寫":print $trans_info['st_main_lang'] ; ?></th>
         
             </tr>
              <tr width="100">
              	<th height="30">心儀大學:</th>
            	<th height="60"><?php empty($collage['c_name'])?print "未填寫":print $collage['c_name'] ?></th>
             	<th height="30">配对情况:</th>
            	<?php if($trans_info['st_matched']==0){?>
             	<th height="60">未成功</th>
             	<?php }elseif($trans_info['st_matched']==1){?>
             	<th height="60">已成功</th>
             	<?php }?>
             </tr>
             <tr width="100">
              	<th height="30">期望補習成果:</th>
            	<th height="60" colspan="3"><?php empty($info['tutor_res'])?print "未填寫":print $info['tutor_res'] ?></th>
             </tr>
	     <tbody>      
	    </table> 
	    <br /><br />
	     <table width="70%" class="table"cellspacing="0" border="1" align="center">
	     <caption><strong>学生时间要求信息</strong></caption>
		
	    <tbody>
            <tr width="100">
	    		<th height="60">星期</th>
	    		<th height="60">开始时间</th>
	    		<th height="60">结束时间</th>
	    		<th height="60">更新时间</th>
	    	</tr>
	    	<?php $str=":"?>
	    	<?php //var_dump($Arr4);exit;?>
	    	<?php foreach ($Arr4 as $info) {	?>
	    	<?php if(!empty($info[1])){?>
            <tr width="100" >
            	<th height="60">星期<?php echo $info[0];?></th>
				<th height="60"><?php 
						$arr = explode(';', $info[1]);
						foreach ($arr as $key => $value) {
							if(!empty($value)){
								echo substr_replace($value,$str,2,0);
								echo '<br >';
							}
						}
				?></th>
				<th height="60"><?php 

						$arr = explode(';', $info[2]);
						foreach ($arr as $key => $value) {
							if(!empty($value)){
								echo substr_replace($value,$str,2,0);
								echo '<br >';
							}
							
						}

				?></th>
				<?php if(!empty($info[1]) && !empty($info[2])){?>
				<th height="60"><?php echo  $time_info['sst_modtime'];?></th>
				<?php }else{?>
				<th height="60"></th>
				<?php }?>
			</tr>
			<?php }?>  
			<?php }?>   
	     <tbody> 
	     
	    </table> 
	    
	</div>
</div>
</body>
</html>