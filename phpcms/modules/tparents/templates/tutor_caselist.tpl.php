<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header','admin');
?>
<div class="pad-lr-10">
	<div class="table-list1">
		<table width="70%" cellspacing="0" class="table" align="center" border="1" style="table-layout:fixed;margin-bottom: 20px;">
	    <caption><strong>导师基本信息</strong></caption>
    		<tr width="100">
    			<th height="30">导师昵称:</th>
    			<?php if(empty($baseinfo[0]['tutor_gname'])){ ?>
    			<th height="60"><?php echo $baseinfo[0]['tutor_fname_eng']; ?></th>
    			<?php }else{ ?>
    			<th height="60"><?php echo $baseinfo[0]['tutor_gname']; ?></th>
    			<?php }?>
                <th height="30">性别:</th>
                <?php if($baseinfo[0]['tutor_sex']=="M"){ ?>
                    <th height="60">男</th>
                <?php }elseif($baseinfo[0]['tutor_sex']=="F"){ ?>
                    <th height="60">女</th>
                <?php }elseif($baseinfo[0]['tutor_sex']==""){ ?>
                    <th height="60"></th>
                <?php }?>
    			
    		</tr>
    		<tr width="100">
    			<th height="30">中文名称:</th>
    			<th height="60"><?php echo $baseinfo[0]['tutor_sname_chi'].$baseinfo[0]['tutor_fname_chi']; ?></th>
				<th height="30">英文名称:</th>
				<th height="60"><?php echo $baseinfo[0]['tutor_fname_eng'].' '.$baseinfo[0]['tutor_sname_eng']; ?></th>
    		</tr>
    		<tr width="100">
    			<th height="30">导师邮箱:</th>
                <th height="60"><?php echo $baseinfo[0]['tutor_email']; ?></th>
                <th height="30">联络电话号码:</th>
                <th height="60"><?php echo $baseinfo[0]['tutor_mobile']; ?></th>
				
    		</tr>
    		<!-- <tr width="100">
    			<th height="30">居住地址:</th>
                <th height="60"><?php //echo $baseinfo[0]['tutor_address']; ?></th>
    		</tr>-->
            <tr width="100">
                <th height="30">居住地點:</th>
                <?php if($baseinfo[0]['tutor_address_type']==""){ ?>
                    <th height="60">未填寫</th>
                <?php }elseif($baseinfo[0]['tutor_address_type']=="0"){ ?>
                    <th height="60">住宅</th>
                <?php }elseif($baseinfo[0]['tutor_address_type']=="1"){ ?>
                    <th height="60">學校宿舍</th>
                <?php }?>
                <th height="30">宿舍地址:</th>
                <th height="60"><?php empty($baseinfo[0]['tutor_address'])?print "未填寫":print $baseinfo[0]['tutor_address'] ; ?></th>
            </tr>
    		<tr width="100">
				<th height="30">出生年份:</th>
				<th height="60"><?php echo $baseinfo[0]['tutor_birth_year']; ?></th>
                <th height="30">居住地区:</th>
                <th height="60"><?php echo $loc['loc_name']; ?></th>
    		</tr>
    		<tr width="100">
    			<th height="30">国籍:</th>
    			<th height="60"><?php empty($baseinfo[0]['tutor_nation'])?print "未填寫":print $baseinfo[0]['tutor_nation'] ; ?></th>
                <th height="30">宗教信仰:</th>
                <th height="60"><?php empty($baseinfo[0]['tutor_believe'])?print "未填寫":print $baseinfo[0]['tutor_believe'] ; ?></th>
    			
    		</tr>
    		<tr width="100">
    			<th height="30">就读小学:</th>
    			<th height="60"><?php empty($baseinfo[0]['tutor_hschool1'])?print "未填寫":print $baseinfo[0]['tutor_hschool1'] ; ?></th>
				<th height="30">就读中学:</th>
				<th height="60"><?php empty($baseinfo[0]['tutor_hschool2'])?print "未填寫":print $baseinfo[0]['tutor_hschool2'] ; ?></th>
    		</tr>
            <tr width="100">
                <th height="30">最高教育程度:</th>
                <th height="60">
                    <?php empty($baseinfo[0]['grade_name'])?print "未填寫":print $baseinfo[0]['grade_name'] ; ?>
                </th>
                <th height="30">性罪行犯罪記録:</th>
                <?php if($baseinfo[0]['tutor_sexually']==""){ ?>
                    <th height="60">未填寫</th>
                <?php }elseif($baseinfo[0]['tutor_sexually']=="0"){ ?>
                    <th height="60">無</th>
                <?php }elseif($baseinfo[0]['tutor_sexually']=="1"){ ?>
                    <th height="60">有</th>
                <?php }?>
            </tr>
            <tr width="100">
                <th height="30">主要語言:</th>
                <th height="60"><?php print $baseinfo[0]['tutor_main_lang'] ; ?></th>
                <th height="30">中學主要教學語言:</th>
                <th height="60"><?php empty($baseinfo[0]['tutor_hschool_lang'])?print "未填寫":print $baseinfo[0]['tutor_hschool_lang'] ; ?></th>
            </tr>
            <tr width="100">
                <th height="30">就读大學:</th>
                <th height="60"><?php empty($baseinfo[0]['collage'])?print "未填寫":print $baseinfo[0]['collage'] ; ?>
                </th>
                <th height="30">其他大學:</th>
                <th height="60"><?php empty($baseinfo[0]['tutor_college_other'])?print "未填寫":print $baseinfo[0]['tutor_college_other'] ; ?></th> 
            </tr>
            <tr width="100">
                <th height="30">現實職業:</th>
                <th height="60"><?php echo $baseinfo[0]['tutor_now_career']; ?></th>
                <th height="30">工作经验:</th>
                <th height="60" style="word-wrap : break-word ;"><?php echo $baseinfo[0]['tutor_work_exp']; ?></th>
            </tr>
    		<tr width="100">
                <th height="30">补习经验及年资:</th>
                <th height="60"><?php echo $baseinfo[0]['tutor_teach_exp']; ?></th>
    			<th height="30">教育機構經驗:</th>
    			<th height="60"><?php echo $baseinfo[0]['tutor_edu_exp']; ?></th>
    		</tr>
            <tr width="100">
                <th height="30">最高可補習年級:</th>
                <th height="60"><?php echo $baseinfo[0]['st_name']; ?></th>
                <th height="30">提供笔记:</th>
                <?php if($baseinfo[0]['tutor_proivde_note']==1){?>
                    <th height="60">有</th>
                <?php }elseif($baseinfo[0]['tutor_proivde_note']==0){?>
                    <th height="60">没有</th>
                <?php }?>
            </tr>
    		<tr width="100">
    			<th height="30">过去工作资料(1):</th>
    			<th height="60" style="word-wrap : break-word ;"><?php empty($baseinfo[0]['tutor_working1'])?print "未填寫":print $baseinfo[0]['tutor_working1'];?></th>
				<th height="30">过去工作资料(2):</th>
				<th height="60" style="word-wrap : break-word ;"><?php empty($baseinfo[0]['tutor_working2'])?print "未填寫":print $baseinfo[0]['tutor_working2'];?></th>
    		</tr>
    		<tr width="100">
    			<th height="50">自我介绍:</th>
    			<th height="50" style="word-wrap : break-word ;"><?php echo $baseinfo[0]['tutor_self_describe'];?></th>
				<th height="50">住宅電話:</th>
				<th height="50"><?php empty($baseinfo[0]['mobile'])?print "未填寫":print $baseinfo[0]['mobile'];?></th>
    		</tr>
	    </table>
        <table width="70%" cellspacing="0"  class="table" align="center" border="1" style="table-layout:fixed ;margin-bottom: 20px;">
            <tr>
                <th width="300">導師考試類別</th>
                <th align="center">導師考試類型</th>
                <th align="center">導師考試科目</th>
                <th align="center">導師考試成績</th>
            </tr>
            
            <?php foreach ($scores as $score) {   ?>  
            <tr>  
                <td align="center"><?php echo $score['level1']?></td>
                <td align="center"><?php echo $score['level2']?></td>
                <td align="center"><?php echo $score['level3']?></td>
                <td align="center"><?php echo $score['tsa_asscore']?></td>
            </tr>
            <?php } ?>  
                
        </table> 
	    <table width="70%" cellspacing="0"  class="table" align="center" border="1" style="table-layout:fixed ;margin-bottom: 20px;">
	            <tr>
				<th width="100">个案编号</th>
				<th align="center">新增时间</th>
				<th align="center">操作</th>
	            </tr>
	        <?php foreach ($infos as $info) {	?>
				<tr>
				<td align="center"><?php echo $info['tm_tutorid']?></td>
				<td align="center"><?php echo $info['tm_modtime']?></td>
				<td align="center"><a href="?m=tparents&c=customer&a=tutorinfo&id=<?php echo $info['tm_id']?>&tm_tutorid=<?php echo $info['tm_tutorid']?>">个案详情>></a></td>
				</tr>
			<?php } ?>	
	    </table> 
	</div>
	<div id="pages"><?php echo $pages;?></div>
</div>
</body>
</html>