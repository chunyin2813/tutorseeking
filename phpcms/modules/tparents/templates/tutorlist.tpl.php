<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header','admin');
?>
<!--导师列表主页面-->
<div class="pad-lr-10">
	<form name="myform"  method="post" >
		<table width="100%" cellspacing="0" class="search-form">
	    <tbody>
			<tr>
				<td>
					<div class="explain-col">

						導師編號：<input name="tutor_id" type="text" value="<?php if(isset($_POST['tutor_id'])) {echo $_POST['tutor_id'];}?>" class="input-text" />
						導師昵稱：<input name="contact_name" type="text" value="<?php if(isset($_POST['contact_name'])) {echo $_POST['contact_name'];}?>" class="input-text" />
						聯絡電話：<input name="contact_phone" type="text" value="<?php if(isset($_POST['contact_phone'])) {echo $_POST['contact_phone'];}?>" class="input-text" />
						<!-- 地區：<input name="location" type="text" value="<?php if(isset($_POST['location'])) {echo $_POST['location'];}?>" class="input-text" /> -->

						<select name="location">
							<option value=''>地區(全部)</option>
							<!-- 港島區 -->
							<optgroup label="港島區">
								<option value='12' <?php if(isset($_POST['location']) && $_POST['location']==12){?>selected<?php }?>>中半山</option>
								<option value='13' <?php if(isset($_POST['location']) && $_POST['location']==13){?>selected<?php }?>>薄扶林</option>
								<option value='14' <?php if(isset($_POST['location']) && $_POST['location']==14){?>selected<?php }?>>中上環</option>
								<option value='15' <?php if(isset($_POST['location']) && $_POST['location']==15){?>selected<?php }?>>西環</option>
								<option value='16' <?php if(isset($_POST['location']) && $_POST['location']==16){?>selected<?php }?>>灣仔</option>
								<option value='17' <?php if(isset($_POST['location']) && $_POST['location']==17){?>selected<?php }?>>銅鑼灣</option>
								<option value='18' <?php if(isset($_POST['location']) && $_POST['location']==18){?>selected<?php }?>>跑馬地</option>
								<option value='47' <?php if(isset($_POST['location']) && $_POST['location']==47){?>selected<?php }?>>北角</option>
								<option value='49' <?php if(isset($_POST['location']) && $_POST['location']==48){?>selected<?php }?>>鰂魚涌</option>
								<option value='49' <?php if(isset($_POST['location']) && $_POST['location']==49){?>selected<?php }?>>太古</option>
								<option value='50' <?php if(isset($_POST['location']) && $_POST['location']==50){?>selected<?php }?>>西灣河</option>
								<option value='51' <?php if(isset($_POST['location']) && $_POST['location']==51){?>selected<?php }?>>筲箕灣</option>
								<option value='52' <?php if(isset($_POST['location']) && $_POST['location']==52){?>selected<?php }?>>小西灣</option>
								<option value='53' <?php if(isset($_POST['location']) && $_POST['location']==53){?>selected<?php }?>>柴灣</option>
								<option value='54' <?php if(isset($_POST['location']) && $_POST['location']==54){?>selected<?php }?>>香港仔</option>
								<option value='55' <?php if(isset($_POST['location']) && $_POST['location']==55){?>selected<?php }?>>鴨利洲</option>
								<option value='56' <?php if(isset($_POST['location']) && $_POST['location']==56){?>selected<?php }?>>赤柱</option>
							</optgroup>

							<!-- 九龍區 -->
							<optgroup label="九龍區">
								<option value='19' <?php if(isset($_POST['location']) && $_POST['location']==19){?>selected<?php }?>>藍田</option>
								<option value='20' <?php if(isset($_POST['location']) && $_POST['location']==20){?>selected<?php }?>>油塘</option>
								<option value='21' <?php if(isset($_POST['location']) && $_POST['location']==21){?>selected<?php }?>>觀塘</option>
								<option value='22' <?php if(isset($_POST['location']) && $_POST['location']==22){?>selected<?php }?>>牛頭角</option>
								<option value='23' <?php if(isset($_POST['location']) && $_POST['location']==23){?>selected<?php }?>>九龍灣</option>
								<option value='24' <?php if(isset($_POST['location']) && $_POST['location']==24){?>selected<?php }?>>彩虹</option>
								<option value='25' <?php if(isset($_POST['location']) && $_POST['location']==25){?>selected<?php }?>>牛池灣</option>
								<option value='26' <?php if(isset($_POST['location']) && $_POST['location']==26){?>selected<?php }?>>慈雲山</option>
								<option value='57' <?php if(isset($_POST['location']) && $_POST['location']==57){?>selected<?php }?>>秀茂坪</option>
								<option value='58' <?php if(isset($_POST['location']) && $_POST['location']==58){?>selected<?php }?>>黃大仙</option>
								<option value='59' <?php if(isset($_POST['location']) && $_POST['location']==59){?>selected<?php }?>>鑽石山</option>
								<option value='60' <?php if(isset($_POST['location']) && $_POST['location']==60){?>selected<?php }?>>新蒲崗</option>
								<option value='61' <?php if(isset($_POST['location']) && $_POST['location']==61){?>selected<?php }?>>樂富</option>
								<option value='62' <?php if(isset($_POST['location']) && $_POST['location']==62){?>selected<?php }?>>九龍塘</option>
								<option value='63' <?php if(isset($_POST['location']) && $_POST['location']==63){?>selected<?php }?>>石硤尾</option>
								<option value='64' <?php if(isset($_POST['location']) && $_POST['location']==64){?>selected<?php }?>>何文田</option>
								<option value='65' <?php if(isset($_POST['location']) && $_POST['location']==65){?>selected<?php }?>>九龍城</option>
								<option value='66' <?php if(isset($_POST['location']) && $_POST['location']==66){?>selected<?php }?>>土瓜灣</option>
								<option value='67' <?php if(isset($_POST['location']) && $_POST['location']==67){?>selected<?php }?>>紅磡</option>
								<option value='68' <?php if(isset($_POST['location']) && $_POST['location']==68){?>selected<?php }?>>油麻地</option>
								<option value='69' <?php if(isset($_POST['location']) && $_POST['location']==69){?>selected<?php }?>>佐敦</option>
								<option value='70' <?php if(isset($_POST['location']) && $_POST['location']==70){?>selected<?php }?>>柯士甸</option>
								<option value='71' <?php if(isset($_POST['location']) && $_POST['location']==71){?>selected<?php }?>>尖沙咀</option>
								<option value='72' <?php if(isset($_POST['location']) && $_POST['location']==72){?>selected<?php }?>>旺角</option>
								<option value='73' <?php if(isset($_POST['location']) && $_POST['location']==73){?>selected<?php }?>>太子</option>
								<option value='74' <?php if(isset($_POST['location']) && $_POST['location']==74){?>selected<?php }?>>長沙灣</option>
								<option value='75' <?php if(isset($_POST['location']) && $_POST['location']==75){?>selected<?php }?>>大角咀</option>
								<option value='76' <?php if(isset($_POST['location']) && $_POST['location']==76){?>selected<?php }?>>深水埗</option>
								<option value='77' <?php if(isset($_POST['location']) && $_POST['location']==77){?>selected<?php }?>>荔枝角</option>
								<option value='78' <?php if(isset($_POST['location']) && $_POST['location']==78){?>selected<?php }?>>美孚</option>
								<option value='79' <?php if(isset($_POST['location']) && $_POST['location']==79){?>selected<?php }?>>九龍站</option>
							</optgroup>

							<!-- 新界區 -->
							<optgroup label="新界區">
								<option value='27' <?php if(isset($_POST['location']) && $_POST['location']==27){?>selected<?php }?>>將軍澳</option>
								<option value='28' <?php if(isset($_POST['location']) && $_POST['location']==28){?>selected<?php }?>>西貢</option>
								<option value='29' <?php if(isset($_POST['location']) && $_POST['location']==29){?>selected<?php }?>>荃灣</option>
								<option value='30' <?php if(isset($_POST['location']) && $_POST['location']==30){?>selected<?php }?>>青衣</option>
								<option value='31' <?php if(isset($_POST['location']) && $_POST['location']==31){?>selected<?php }?>>葵涌</option>
								<option value='32' <?php if(isset($_POST['location']) && $_POST['location']==32){?>selected<?php }?>>葵芳</option>
								<option value='33' <?php if(isset($_POST['location']) && $_POST['location']==33){?>selected<?php }?>>荔景</option>
								<option value='34' <?php if(isset($_POST['location']) && $_POST['location']==34){?>selected<?php }?>>大圍</option>
								<option value='35' <?php if(isset($_POST['location']) && $_POST['location']==35){?>selected<?php }?>>沙田</option>
								<option value='36' <?php if(isset($_POST['location']) && $_POST['location']==36){?>selected<?php }?>>小瀝源</option>
								<option value='37' <?php if(isset($_POST['location']) && $_POST['location']==37){?>selected<?php }?>>火炭</option>
								<option value='38' <?php if(isset($_POST['location']) && $_POST['location']==38){?>selected<?php }?>>馬鞍山</option>
								<option value='39' <?php if(isset($_POST['location']) && $_POST['location']==39){?>selected<?php }?>>大埔</option>
								<option value='80' <?php if(isset($_POST['location']) && $_POST['location']==80){?>selected<?php }?>>粉嶺</option>
								<option value='81' <?php if(isset($_POST['location']) && $_POST['location']==81){?>selected<?php }?>>上水</option>
								<option value='82' <?php if(isset($_POST['location']) && $_POST['location']==82){?>selected<?php }?>>元朗</option>
								<option value='83' <?php if(isset($_POST['location']) && $_POST['location']==83){?>selected<?php }?>>天水圍</option>
								<option value='84' <?php if(isset($_POST['location']) && $_POST['location']==84){?>selected<?php }?>>青龍頭</option>
								<option value='85' <?php if(isset($_POST['location']) && $_POST['location']==85){?>selected<?php }?>>深井</option>
								<option value='86' <?php if(isset($_POST['location']) && $_POST['location']==86){?>selected<?php }?>>屯門</option>
								<option value='87' <?php if(isset($_POST['location']) && $_POST['location']==87){?>selected<?php }?>>馬灣</option>
								<option value='88' <?php if(isset($_POST['location']) && $_POST['location']==88){?>selected<?php }?>>東涌</option>
								<option value='89' <?php if(isset($_POST['location']) && $_POST['location']==89){?>selected<?php }?>>長洲</option>
							</optgroup>
						</select> 
						<input type="hidden" name="dosubmit" value="1">
						<input type="submit" name="search" class="button pair_search" value="筛选" />
						<input type="button" value="刷新" class="button" style="float:right;" onclick="Refresh();">
					</div>
				</td>
			</tr>
	    </tbody>
		</table>
	</form>
	<div class="table-list">
	    <table width="100%" cellspacing="0" class="contentWrap">
	        <thead>
	            <tr>
				<th width="35">ID</th>
				<th width="50">导师编号</th>
				<th width="100">导师昵称</th>
				<th width="170">导师邮箱</th>
				<th width="170">联络电话号码</th>
				<th width="120">积分(点击修改)</th>
				<th align="center">更新时间</th>
				<th align="center">操作</th>
	            </tr>
	        </thead>
	        <tbody> 
	        <?php foreach ($infos as $info) {	?>
				<tr class="memList">
				<td align="center"><?php echo $info['tutor_id']?></td>
				<td align="center"><?php echo $info['personid']?></td>
				<?php if(empty($info['tutor_gname'])){ ?>
    			<td align="center"><?php echo $info['tutor_fname_eng']; ?></td>
    			<?php }else{ ?>
    			<td align="center"><?php echo $info['tutor_gname']; ?></td>
    			<?php }?>
				<td align="center"><?php echo $info['tutor_email']?></td>
				<td align="center"><?php echo $info['tutor_mobile']?></td>
				<td align="center" class="editIntegral" onclick="editIntegral(this,'<?php echo $info[tutor_userid];?>','<?php echo $info[integral];?>');"><?php echo $info['integral'];?>积分</td>
				<td align="center"><?php echo $info['tutor_modtime']?></td>
				<td align="center"><a href="?m=tparents&c=customer&a=delete&userid=<?php echo $info['tutor_userid']?>" onclick="return confirm('<?php echo "確定刪除此導師？這將會刪除此導師所有的個案。"?>')">刪除>></a>
					<a href="?m=tparents&c=customer&a=tutor_caselist&id=<?php echo $info['tutor_id']?>">个案列表>></a></td>
				</tr>
			<?php } ?>	
			</tbody>
	    </table> 
	</div>
	<div id="pages"><?php echo $pages;?></div>
</div>
</body>
<script>

	//编辑积分
	function editIntegral(th,uid,integral){
		if(!$('.memList').find('.integralVal').val()){
			$(th).html('<input type="text" class="integralVal" value="'+integral+'" style="width:40px;height:20px;"><input type="button" onclick="subEditIntegral('+uid+');" class="button" value="提交"><input type="button" onclick="cancelEdit();" class="button" value="取消">');
			$(th).removeAttr('onclick');
		}
	}

	//提交编辑
	function subEditIntegral(uid){
		var integral = $('.integralVal').val();//修改后的积分
		$.post('./index.php?m=tparents&c=index&a=editIntegral',{uid:uid,integralVal:integral},function(res){
			if(res){
				window.location.reload();
			}else{
				alert('请刷新重试!');
			}
		},'json');
	}

	//取消编辑
	function cancelEdit(){
		window.location.reload();
	}

</script>
</html>