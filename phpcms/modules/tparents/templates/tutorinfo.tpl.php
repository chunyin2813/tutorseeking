<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header','admin');
?>
<!--导师详情主页面-->
<div class="pad_10">
<div class="table-list">
	    <table width="70%" class="table" cellspacing="0" border="1" align="center">
	    <caption><strong>导师基本要求信息</strong></caption>
	    <tbody>
            <tr width="100">
				<th height="30">可教授类别:</th>
				<th height="60"><?php echo $tt_sub1?></th>
				<th height="30">可教授项目:</th>
				<th height="60"><?php echo $tt_sub2?></th>
			</tr>
            <tr width="100">
            	<th height="30">可教授级别:</th>
            	<th height="60"><?php echo $tt_sub3?></th>
				<th height="30">可教授地区:</th>
            	<th height="60"><?php echo $loc_name?></th>
			</tr>
			<tr width="100">
            	<th height="30">每次上课時間:</th>
            	<th height="60"><?php empty($time_info['tst_class_hour'])?print "未填寫":print $time_info['tst_class_hour']."小時";?></th>
				<th height="30">每小時學費:</th>
            	<th height="60"><?php echo $tutor_type_info['tstt_fee']?></th>
			</tr>
			<tr width="100">
             	<th height="30">配对情况:</th>
             	<th height="60"><?php echo $state?></th>
            	
            	<th height="30">更新时间:</th>
            	<th height="60"><?php echo $tutor_type_info['tstt_modtime']?></th>
             </tr>	
		 <tbody>      
    	</table> 
    	<br /><br />
	    <table width="70%" cellspacing="0" class="table" align="center" border="1" style="table-layout:fixed">
	    <caption><strong>导师可授课时间</strong></caption>
	    	<tr width="100">
	    		<th height="60">星期</th>
	    		<th height="60">开始时间</th>
	    		<th height="60">结束时间</th>
	    		<th height="60">更新时间</th>
	    	</tr>
	    	<?php $str=":"?>
            <?php foreach ($Arr4 as $info) {
            	if(!empty($info[1])){?>
            <tr width="100" >
            	<th height="60">星期<?php echo $info[0];?></th>
				<th height="60"><?php 
						$arr = explode(',', $info[1]);
						foreach ($arr as $key => $value) {
							if(!empty($value)){
								echo substr_replace($value,$str,2,0);
								echo '<br >';
							}
						}
				?></th>
				<th height="60"><?php 
					
						$arr = explode(',', $info[2]);
						foreach ($arr as $key => $value) {
							if(!empty($value)){
								echo substr_replace($value,$str,2,0);
								echo '<br >';
							}	
						}

				?></th>
				<?php if(!empty($info[1]) && !empty($info[2])){?>
				<th height="60"><?php echo  $time_info['tst_modtime'];?></th>
				<?php }else{?>
				<th height="60"></th>
				<?php }?>
			</tr>
			<?php }?> 
			<?php }?>     
	     <tbody>      
	    </table> 
	</div>
</div>
</body>
</html>