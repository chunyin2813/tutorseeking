<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header','admin');
?>
<div class="pad-lr-10">
	<div class="table-list">
	    <table width="100%" cellspacing="0" class="contentWrap">
	        <thead>
	            <tr>
				<th width="50">学生编号</th>
				<th width="150">学生姓名</th>
				<th width="170">联络人姓名</th>
				<th width="170">住宅联络电话号码</th>
				<th align="center">更新时间</th>
				<th align="center">操作</th>
	            </tr>
	        </thead>
	        <tbody> 
	        <?php foreach ($infos as $info) {	?>
				<tr>
				<td align="center"><?php echo $info['student_id']?></td>
				<td align="center"><?php empty($info['student_name'])?print "未填寫":print $info['student_name'];?></td>
				<td align="center"><?php empty($info['student_contactname'])?print "未填寫":print $info['student_contactname'];?></td>
				<td align="center"><?php empty($info['student_mobile'])?print "未填寫":print $info['student_mobile'];?></td>
				<td align="center"><?php echo $info['student_modtime']?></td>
				<td align="center"><a href="?m=tparents&c=customer&a=studentinfo&id=<?php echo $info['student_id']?>">个案详情>></a></td>
				</tr>
			<?php } ?>	
			</tbody>
	    </table> 
	</div>
	<div id="pages"><?php echo $pages;?></div>
</div>
</body>
</html>