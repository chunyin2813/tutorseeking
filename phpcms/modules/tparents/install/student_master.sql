DROP TABLE IF EXISTS `phpcms_student_master`;
CREATE TABLE `v9_student_master` (
  `student_id` int(8) NOT NULL COMMENT '家長學生主健編號',
  `student_email` int(100) NOT NULL COMMENT '電郵及登錄 ID',
  `student_mobile` int(15) NOT NULL COMMENT '手機號及登錄ID',
  `student_pwd` int(100) DEFAULT NULL COMMENT '密碼 (加密)',
  `student_verify` text NOT NULL COMMENT '首次認證狀態 (0=未認證，1=已認證）',
  `student_contactname` varchar(100) NOT NULL COMMENT '聯絡人姓名 (家長暱稱）',
  `student_name` varchar(100) NOT NULL COMMENT '首名學生名稱',
  `student_contactno` varchar(20) DEFAULT NULL COMMENT '聯絡電話號碼',
  `student_relation` varchar(20) DEFAULT NULL COMMENT '聯絡人與學生的關係',
  `student_locid` int(8) NOT NULL COMMENT '居住地區 ID (loc_level = 3 的 loc_id)',
  `student_living_locid` int(8) NOT NULL COMMENT '居住屋苑 ID (loc_level = 4 的 loc_id)',
  `student_address` varchar(250) DEFAULT NULL COMMENT '居住地址',
  `student_birth_year` int(4) NOT NULL COMMENT '首位學生出生年份',
  `student_school` varchar(100) DEFAULT NULL COMMENT '首位學生就讀學校名稱',
  `student_modtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新時間'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='家長學生主資料表';