<?php
defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_app_class('admin','admin',0);
class customer extends admin {
    function __construct() {
        parent::__construct();
        $this->db = pc_base::load_model('tutor_master_model');
        $this->s_db = pc_base::load_model('student_master_model');
        $this->loc_db = pc_base::load_model('sys_location_model');
		$this->module_db = pc_base::load_model('module_model');
        $this->mem_db = pc_base::load_model('member_model');
        $this->tran_db = pc_base::load_model('tutor_transaction_model');
        $this->pair_db = pc_base::load_model('pair_model');
        $this->grade_db = pc_base::load_model('grade_model');
        $this->collage_db = pc_base::load_model('collage_model');
        $this->student_type_db = pc_base::load_model('student_type_model');
        $this->tutor_sel_achievement_db = pc_base::load_model('tutor_sel_achievement_model');
        $this->sys_tutor_achieve_db = pc_base::load_model('sys_tutor_achieve_model');
    }
    //导师列表
    public function init() {
        $page = isset($_GET['page']) && intval($_GET['page']) ? intval($_GET['page']) : 1;
        $condition = 'where 1=1 ';
        if(isset($_POST['dosubmit'])){
            if(!empty($_POST['tutor_id'])){
                $condition.="and personid='".$_POST['tutor_id']."'";
            }
            if(!empty($_POST['contact_name'])){
                $condition.="and tutor_gname='".$_POST['contact_name']."'";
            }
            if(!empty($_POST['contact_phone'])){
                $condition.="and tutor_mobile='".$_POST['contact_phone']."'";
            }
            if(!empty($_POST['location'])){
                $condition.="and tutor_address_locid='".$_POST['location']."'";
            }
        }
        $limit = 20; //每页显示条数
        $start = ($page-1)*$limit;
        $sql = "select * from `v9_tutor_master` as master 
                left join `v9_member` as mem on mem.userid = master.tutor_userid $condition
                order by tutor_modtime desc";
        $typeinfo = $this->db->query($sql);
        $result = $this->db->fetch_array($typeinfo);
        $results = arraySequence($result, 'tutor_modtime', $sort = 'SORT_DESC');
        $pages = pages(count($result), $page, $limit, '', array(), 10); //分页
        $infos = array_slice($results, $start, $limit);


        // $infos = $this->db->listinfo($where,$order = 'tutor_modtime desc',$page, $pages = '20');
        // foreach ($infos as $key => $value) {
        //     $otherInfo = $this->mem_db->get_one("userid = $value[tutor_userid] ".$condition,"integral,personid");
        //     if(empty($otherInfo)){
        //         unset($infos[$key]);
        //     }else{
        //         $infos[$key]['personid'] = $otherInfo['personid'];//导师编号
        //         $infos[$key]['integral'] = $otherInfo['integral'];//导师积分 
        //     }
        // }
        // $pages = $this->db->pages;
        include $this->admin_tpl('tutorlist');
    }
    public function count() {
        $page = isset($_GET['page']) && intval($_GET['page']) ? intval($_GET['page']) : 1;
        $condition = 'where 1=1 ';
        $limit = 999999; //每页显示条数
        $start = ($page-1)*$limit;

        $sql = "select tutor_know_way from `v9_tutor_master` WHERE tutor_know_way = 1";
        $typeinfo = $this->db->query($sql);
         $result = $this->db->fetch_array($typeinfo);
        $results = arraySequence($result);
         $infos = array_slice($results, $start, $limit);

         $sql2 = "select tutor_know_way from `v9_tutor_master` WHERE tutor_know_way = 2";
        $typeinfo2 = $this->db->query($sql2);
         $result2 = $this->db->fetch_array($typeinfo2);
        $results2 = arraySequence($result2);
         $infos2 = array_slice($results2, $start, $limit);

         $sql3 = "select tutor_know_way from `v9_tutor_master` WHERE tutor_know_way = 3";
        $typeinfo3 = $this->db->query($sql3);
         $result3 = $this->db->fetch_array($typeinfo3);
        $results3 = arraySequence($result3);
         $infos3 = array_slice($results3, $start, $limit);

         $sql4 = "select tutor_know_way from `v9_tutor_master` WHERE tutor_know_way = 4";
        $typeinfo4 = $this->db->query($sql4);
         $result4 = $this->db->fetch_array($typeinfo4);
        $results4 = arraySequence($result4);
         $infos4 = array_slice($results4, $start, $limit);
         
         $sql5 = "select tutor_know_way from `v9_tutor_master` WHERE tutor_know_way = 5";
        $typeinfo5 = $this->db->query($sql5);
         $result5 = $this->db->fetch_array($typeinfo5);
        $results5 = arraySequence($result5);
         $infos5 = array_slice($results5, $start, $limit);

         $sql6 = "select tutor_know_way from `v9_tutor_master` WHERE tutor_know_way = 6";
        $typeinfo6 = $this->db->query($sql6);
         $result6 = $this->db->fetch_array($typeinfo6);
        $results6 = arraySequence($result6);
         $infos6 = array_slice($results6, $start, $limit);

        include $this->admin_tpl('countlist');
    }
    //导师个案列表页
    public function tutor_caselist() {
        $page = isset($_GET['page']) && intval($_GET['page']) ? intval($_GET['page']) : 1;
        if (isset($_GET)) {
            $baseinfo=$this->db->select(array("tutor_id"=>$_GET['id']));
            
            foreach ($baseinfo as $key=>$binfo) {
                //住宅電話
                $mem=$this->mem_db->get_one(array("userid"=>$binfo["tutor_userid"]));
                $baseinfo[$key][mobile]=$mem[mobile];

                //居住地区
                $loc=$this->loc_db->get_one(array("loc_id"=>$binfo["tutor_address_locid"]));
                $baseinfo[$key][loc_name]=$loc[loc_name];

                //最高教育程度
                $grade=$this->grade_db->get_one(array("grade_id"=>$binfo["tutor_high_grade"]));
                $baseinfo[$key][grade_name]=$grade[grade_name];

                //就讀大學
                $collage=$this->collage_db->get_one(array("id"=>$binfo["tutor_college"]));
                $baseinfo[$key][collage]=$collage[c_name];

                //最高可補習年級
                $st=$this->student_type_db->get_one(array("st_id"=>$binfo["tutor_high_teach"]));
                $baseinfo[$key][st_name]=$st[st_name];

                //導師考試成績
                $scores=$this->tutor_sel_achievement_db->select(array("tsa_userid"=>$binfo["tutor_userid"]));
                foreach ($scores as $key => $score) {
                    if(empty($score['tsa_asscore'])){
                        unset($scores[$key]);
                    }
                }
                foreach ($scores as $key => $score) {
                    $level3=$this->sys_tutor_achieve_db->get_one(array("ta_id"=>$score["tsa_taid"]));
                    $scores[$key]['level3']=$level3['ta_name'];

                    $level2=$this->sys_tutor_achieve_db->get_one(array("ta_id"=>$level3['ta_parentid']));
                    $scores[$key]['level2']=$level2['ta_name'];

                    $level1=$this->sys_tutor_achieve_db->get_one(array("ta_id"=>$level2['ta_parentid']));
                    $scores[$key]['level1']=$level1['ta_name'];
                }
            }
            $infos = $this->tran_db->listinfo(array("master_id"=>$_GET['id']),$order = '',$page, $pages = '20');
            $pages = $this->tran_db->pages;
        }
        include $this->admin_tpl('tutor_caselist');  
    }
    //导师详细信息
    public function tutorinfo(){
    	if (isset($_GET)) {
            $tm_id=$_GET['id']; 
            $tm_tutorid=$_GET['tm_tutorid'];

            //查詢個案配對狀態 
            $pair_where['pair_tutor_tranid']=$tm_tutorid;
            $pair_state=$this->pair_db->get_one($pair_where);
            if(empty($pair_state)){
                $state="未配對";
            }elseif($pair_state['pair_state']=='0'){
                $state="配對中";
            }elseif ($pair_state['pair_state']=='1') {
                $state="已配對";
            }
            //可教授项目
            $type_where['tstt_tutorid']=$tm_tutorid;
            $type_where['tstt_tmid']=$tm_id;
            
            
            pc_base::load_app_class('helpcommon' ,'pair');
            $helpcommon=new helpcommon();
            //可教授科目
            $this->tutor_type_info = pc_base::load_model('tutor_sel_tutor_type_model');
            $tutor_type_info=$this->tutor_type_info->get_one($type_where);
            $tt_id1=$tutor_type_info['tstt_ttid_1'];
            $tt_id2=$tutor_type_info['tstt_ttid_2'];
            $tt_id3=$tutor_type_info['tstt_ttid_3'];
            $tt_sub1=$helpcommon->create_subject($tt_id1);
            $tt_sub2=$helpcommon->create_subject($tt_id2);
            $tt_sub3=$helpcommon->create_subject($tt_id3);

            //可教授地区

            $this->loc_info_db = pc_base::load_model('tutor_sel_location_model');
            $loc_info = $this->loc_info_db->get_one(array("tsl_tutorid"=>$tm_tutorid,"tsl_tmid"=>$tm_id));
            $loc_id=$loc_info['tsl_locid'];
            $loc_name=$helpcommon->create_location($loc_id);

            //可教授时间
            $time_where['tst_tutorid']=$tm_tutorid;
            $time_where['tst_tmid']=$tm_id;
            $this->time_info = pc_base::load_model('tutor_sel_time_model');
            $time_info=$this->time_info->get_one($time_where);
            $fromtime = explode(';',$time_info['tst_fromtime']); 
            $totime= explode(';',$time_info['tst_totime']);
            $weekday=explode(',',$time_info['tst_weekday']); 
            $weeknums = array('0'=>"一",'1'=>"二",'2'=>"三",'3'=>"四",'4'=>"五",'5'=>"六",'6'=>"日");
            $Arr4 = array();
            foreach ($weeknums as $k => $r) {
                 $Arr4[] = array($weeknums[$weekday[$k]],$fromtime[$k],$totime[$k]);
            }
        	include $this->admin_tpl('tutorinfo');
        }
    }
    //学生列表
    public function studentlist() {
        $page = isset($_GET['page']) && intval($_GET['page']) ? intval($_GET['page']) : 1;
        if(isset($_POST['dosubmit'])){
            if(!empty($_POST['st_id'])){
                $where['personid']=$_POST['st_id'];
            }
            if(!empty($_POST['contact_name'])){
                $where['username']=$_POST['contact_name'];
            }
            if(!empty($_POST['contact_phone'])){
                $where['phone']=$_POST['contact_phone'];
            }
            if(!empty($_POST['location'])){
                $where['student_locid']=$_POST['location'];
            }
        }
        $where['role_verify']='1';
        $infos = $this->mem_db->listinfo($where,$order = 'regdate desc',$page, $pages = '20');
        $pages = $this->mem_db->pages;
        include $this->admin_tpl('studentlist');  
    }
    //学生个案列表
    public function student_caselist() {
        $page = isset($_GET['page']) && intval($_GET['page']) ? intval($_GET['page']) : 1;
        if (isset($_GET)) {
            $where['student_userid']=$_GET['id'];
            $infos = $this->s_db->listinfo($where,$order = '',$page, $pages = '20');
            $pages = $this->s_db->pages;
        } 
        include $this->admin_tpl('student_caselist');  
    }
    //学生详细信息
    public function studentinfo(){
        if (isset($_GET)) {
            //var_dump($_GET);die;
            $where['student_id']=$_GET['id'];
            $infos=$this->s_db->select($where);

        }
        //居住地区
        foreach ($infos as $info_a) {
            $locid=$info_a["student_locid"];
        }
        $loc_where['loc_id']=$locid;
        $loc_where['loc_level']=3;
        $loc_info=$this->loc_db->get_one($loc_where);
        
        //基本要求信息
        $this->trans_db = pc_base::load_model('student_transaction_model'); 
        $trans_where['st_studentid']=$_GET['id']; 
        $trans_info=$this->trans_db->get_one($trans_where);
        // var_dump($trans_info['st_modtime']);
        //要求最低教育程度
        $grade_id=$trans_info['st_min_grade'];
        $this->grade_db = pc_base::load_model('grade_model');
        $min_grade=$this->grade_db->get_one(array("grade_id"=>$grade_id));
        //要求教授类别
        $tt_id1=$trans_info['st_ttid_1'];
        $tt_id2=$trans_info['st_ttid_2'];
        $tt_id3=$trans_info['st_ttid_3'];
        $this->sys_tutor_type_info = pc_base::load_model('sys_tutor_type_model');
        $where1['tt_id']=$tt_id1;
        $where1['tt_level']=1; 
        $tutor_type_info1=$this->sys_tutor_type_info->get_one($where1);
        $where2['tt_id']=$tt_id2;
        $where2['tt_level']=2; 
        $tutor_type_info2=$this->sys_tutor_type_info->get_one($where2);
        $where3['tt_id']=$tt_id3;
        $where3['tt_level']=3; 
        $tutor_type_info3=$this->sys_tutor_type_info->get_one($where3);

        //上课地区
        $stuloc_where['loc_id']=$trans_info['st_area'];
        $stu_loc_info=$this->loc_db->get_one($stuloc_where);

        //心儀大學
        $collage=$this->collage_db->get_one(array("id"=>$trans_info['like_collage']));

        //时间信息  
        $time_where['sst_stid']=$trans_info['st_id'];
        $time_where['sst_studentid']=$_GET['id'];
        $this->time_db = pc_base::load_model('student_sel_time_model'); 
        $time_info=$this->time_db->get_one($time_where);

        $fromtime = explode(',',$time_info['sst_fromtime']); 
        $totime= explode(',',$time_info['sst_totime']);
        $weekday=explode(',',$time_info['sst_weekday']);
        $weeknums = array('0'=>"一",'1'=>"二",'2'=>"三",'3'=>"四",'4'=>"五",'5'=>"六",'6'=>"日");
        $Arr4 = array();
        foreach ($weeknums as $k => $r) {
             $Arr4[] = array($weeknums[$weekday[$k]],$fromtime[$k],$totime[$k]);
        }
        include $this->admin_tpl('studentinfo');
    }
//刪除用戶
   public function delete(){
        $this->db = pc_base::load_model('member_model');
        $uidarr = isset($_GET['userid']) ? $_GET['userid'] : showmessage(L('illegal_parameters'), HTTP_REFERER);
        
        $uidarr = intval($uidarr);
        $where = to_sql_new($uidarr, '', 'userid');
        $phpsso_userinfo = $this->db->listinfo($where);

        $phpssouidarr = array();
        if(is_array($phpsso_userinfo)) {
            foreach($phpsso_userinfo as $v) {
                if(!empty($v['phpssouid'])) {
                    $phpssouidarr[] = $v['phpssouid'];
                }
            }
        }

        //查询用户信息
        $userinfo_arr = $this->db->select($where, "userid, modelid");
        $userinfo = array();
        if(is_array($userinfo_arr)) {
            foreach($userinfo_arr as $v) {
                $userinfo[$v['userid']] = $v['modelid'];
            }
        }
        //删除用户关联表

        $res=$this->db->select($where,"role_verify");
        $this->pair_db=pc_base::load_model('pair_model');
        $this->p_fail_db=pc_base::load_model('pair_fail_model');
        $this->p_comment_db=pc_base::load_model('pair_comment_model');
        foreach ($res as $v) {
            if($v['role_verify']=="0"){
                $this->mdb=pc_base::load_model('tutor_master_model');
                $this->ldb=pc_base::load_model('tutor_sel_location_model');
                $this->tdb=pc_base::load_model('tutor_sel_time_model');
                $this->tydb=pc_base::load_model('tutor_sel_tutor_type_model');
                $this->tradb=pc_base::load_model('tutor_transaction_model');
                $this->adb=pc_base::load_model('tutor_sel_achievement_model');
                $condition = to_sql_new($uidarr, '', 'tutor_userid');
                $masterid=$this->mdb->select($condition,"tutor_id");
                foreach ($masterid as $key => $m_id) {
                    $midarr[$key]=$m_id['tutor_id'];
                }
                $where_master = to_sql_new($midarr, '', 'master_id');
                $tmid=$this->tradb->select($where_master,"tm_id,tm_tutorid");
                foreach ($tmid as $k => $tm_id) {
                    $tmidarr[$k]=$tm_id['tm_id'];
                    $tutoridarr[$k]=$tm_id['tm_tutorid'];
                }
                $this->mdb->delete(to_sql_new($uidarr, '', 'tutor_userid'));
                $this->ldb->delete(to_sql_new($tmidarr, '', 'tsl_tmid'));
                $this->tdb->delete(to_sql_new($tmidarr, '', 'tst_tmid'));
                $this->tydb->delete(to_sql_new($tmidarr, '', 'tstt_tmid'));
                $this->tradb->delete(to_sql_new($midarr, '', 'master_id'));
                $this->adb->delete(to_sql_new($uidarr, '', 'tsa_userid'));
                $this->pair_db->delete(to_sql_new($tutoridarr, '', 'pair_tutor_tranid'));
                $this->p_fail_db->delete(to_sql_new($tutoridarr, '', 'pair_tutor_tranid'));
                $this->p_comment_db->delete(to_sql_new($tutoridarr, '', 'pc_tutorid'));
            }elseif($v['role_verify']=="1"){
                $this->mdb=pc_base::load_model('student_master_model');
                $this->tradb=pc_base::load_model('student_transaction_model');
                $this->tdb=pc_base::load_model('student_sel_time_model');

                $condition = to_sql_new($uidarr, '', 'student_userid');
                $studentid=$this->mdb->select($condition,"student_id");
                foreach ($studentid as $key => $student_id) {
                    $studentid_arr[$key]=$student_id['student_id'];
                }
                $where_tran=to_sql_new($studentid_arr, '', 'st_studentid');
                $stid=$this->tradb->select($where_tran,"st_id");
                foreach ($stid as $key => $st_id) {
                    $stidarr[$key]=$st_id['st_id'];
                }

                $this->mdb->delete(to_sql_new($uidarr, '', 'student_userid'));
                $this->tradb->delete(to_sql_new($studentid_arr, '', 'st_studentid'));
                $this->tdb->delete(to_sql_new($studentid_arr, '', 'sst_studentid'));
                $this->pair_db->delete(to_sql_new($stidarr, '', 'pair_student_tranid'));
                $this->p_fail_db->delete(to_sql_new($stidarr, '', 'pair_student_tranid'));
                $this->p_comment_db->delete(to_sql_new($stidarr, '', 'pc_studentid'));
            }
        }
                            
        //delete phpsso member first
        if(!empty($phpssouidarr)) {
             $status = $this->client->ps_delete_member($phpssouidarr, 1);
             if($status > 0) {
                 if ($this->db->delete($where)) {

                    
                    //删除用户模型用户资料
                    foreach($uidarr as $v) {
                        if(!empty($userinfo[$v])) {
                            $this->db->set_model($userinfo[$v]);
                            $this->db->delete(array('userid'=>$v));

                        }
                    }
                
                    showmessage(L('operation_success'), HTTP_REFERER);
                } else {
                    showmessage(L('operation_failure'), HTTP_REFERER);
                }
            } else {
                showmessage(L('operation_failure'), HTTP_REFERER);
            }
         } else {
            if ($this->db->delete($where)) {
                showmessage(L('operation_success'), HTTP_REFERER);
            } else {
                showmessage(L('operation_failure'), HTTP_REFERER);
            }
         }
    }
}

 

?>