<?php
//学生家长注册
defined('IN_PHPCMS') or exit('No permission resources.');
header("Content-Type:text/html;charset:utf8");
pc_base::load_app_class('tutor' ,'pair');
class index {
    function __construct() {
        $this->db = pc_base::load_model('member_model');
        $this->p_db = pc_base::load_model('pair_model');
        pc_base::load_app_class('inte' ,'integration');
        $session_storage = 'session_'.pc_base::load_config('system','session_storage');
        pc_base::load_sys_class($session_storage);
    }
    private function _session_start() {
        $session_storage = 'session_'.pc_base::load_config('system','session_storage');
        pc_base::load_sys_class($session_storage);
    }

    // public function test(){
    //     $inte=new inte();
    //     $condition['phone']='15017890764';
    //     $num=$this->db->count(array('Referee'=>$condition['phone']));
    //     echo $num;
    //     if((($num+1)%$spread_num)==0){
    //         $spread_inte=$inte->get_inte_setting(11);
    //         echo $spread_inte;
    //         //$inte->write_inte_log($person[0]['personid'],$person[0]['username'],'14','','','',$spread_inte);
    //     }
    // }
    //ajax验证图形验证码
    public function ajax_checkcode(){
        if(!empty($_POST['code'])){
            $session_storage = 'session_'.pc_base::load_config('system','session_storage');
            pc_base::load_sys_class($session_storage);
            if(strtolower($_SESSION['code']) == strtolower($_POST['code'])){
                echo 1;
            }else{
                echo 0;
            }
        }
    }

    //ajax验证短信验证码
    public function check_msg_code(){
        if(!empty($_POST['code']) && !empty($_POST['phone'])){
            $session_storage = 'session_'.pc_base::load_config('system','session_storage');
            pc_base::load_sys_class($session_storage);
            if(!empty($_SESSION['msg_phone']) && $_SESSION['msg_phone'] != $_POST['phone']){
                echo 2;exit;//注册提交手机号与发送短信手机号不同
            }
            if(strtolower($_SESSION['msg_code']) == strtolower($_POST['code'])){
                echo 0;exit;//验证成功
            }else{
                echo 1;exit;//验证码错误
            }
        }
    }

    //发送短信验证码
    public function send_msg_code(){
        if(!empty($_POST['phone'])){
            $phone = $_POST['phone'];
            $phone_type = substr($phone,0,3);//截取前三位(国家号)
            if($phone_type == '852'){//若有国家号
                $phone = substr($phone, 3);
            }
            
            // $code = generate_password(5);

            // $_SESSION['msg_code'] = $code;//存入session
            // $_SESSION['msg_phone'] = $phone;//存入session
            if(strlen($phone) == '8'){
                $code = generate_password(5);
                $_SESSION['msg_code'] = $code;//存入session
                $_SESSION['msg_phone'] = $phone;//存入session
                $phone = '852 '.$phone;
                $target = "http://api.isms.ihuyi.com/webservice/isms.php?method=Submit";
                $account = 'I90828156';
                $password = '6af5f84dfc96367f520468f50e769383';
                $post_data = "account=".$account."&password=".$password."&mobile=".$phone."&content=".rawurlencode("您的短信驗證碼是".$code."，請於10分钟內輸入。如非本人操作，請忽略此短信【TutorSeeking】");
            }else{
                $code = '12306';
                $_SESSION['msg_code'] = $code;//存入session
                $_SESSION['msg_phone'] = $phone;//存入session
                $target = "http://106.ihuyi.cn/webservice/sms.php?method=Submit";
                $account = 'C04712274';
                $password = 'bca84ef22bd5a4d852ba05b2e1b9f9cc';
                $post_data = "account=".$account."&password=".$password."&mobile=".$phone."&content=".rawurlencode("您的驗證碼是：".$code."。請不要把驗證碼泄露給其他人。");
            }
            return sendTemplateSMS($post_data, $target);
        }
    }


     //学生注册页
    public function init(){


        pc_base::load_sys_class('form','', 0);
        $timelist = array(
        '0700' =>"7:00" ,'0715' =>"7:15" ,'0730' =>"7:30",'0745' =>"7:45",
        '0800' =>"8:00" ,'0815' =>"8:15" ,'0830' =>"8:30" ,'0845' =>"8:45",
        '0900' =>"9:00" ,'0915' =>"9:15" ,'0930' =>"9:30" ,'0945' =>"9:45",
        '1000' =>"10:00" ,'1015' =>"10:15" ,'1030' =>"10:30" ,'1045' =>"10:45",
        '1100' =>"11:00" ,'1115' =>"11:15" ,'1130' =>"11:30" ,'1145' =>"11:45",
        '1200' =>"12:00" ,'1215' =>"12:15" ,'1230' =>"12:30" ,'1245' =>"12:45",
        '1300' =>"13:00" ,'1315' =>"13:15" ,'1330' =>"13:30" ,'1345' =>"13:45",
        '1400' =>"14:00" ,'1415' =>"14:15" ,'1430' =>"14:30" ,'1445' =>"14:45",
        '1500' =>"15:00" ,'1515' =>"15:15" ,'1530' =>"15:30" ,'1545' =>"15:45",
        '1600' =>"16:00" ,'1615' =>"16:15" ,'1630' =>"16:30" ,'1645' =>"16:45",
        '1700' =>"17:00" ,'1715' =>"17:15" ,'1730' =>"17:30" ,'1745' =>"17:45",
        '1800' =>"18:00" ,'1815' =>"18:15" ,'1830' =>"18:30" ,'1845' =>"18:45",
        '1900' =>"19:00" ,'1915' =>"19:15" ,'1930' =>"19:30" ,'1945' =>"19:45",
        '2000' =>"20:00" ,'2015' =>"20:15" ,'2030' =>"20:30" ,'2045' =>"20:45",
        '2100' =>"21:00" ,'2115' =>"21:15" ,'2130' =>"21:30" ,'2145' =>"21:45",
        '2200' =>"22:00" ,'2215' =>"22:15" ,'2230' =>"22:30" ,'2245' =>"22:45",
        '2300' =>"23:00");


        // $this->_session_start();
        // dump($_SESSION);

        //生成随机推荐码
        // $myReferee = 'TS'.generate_password(6);
        // $is_exist = $this->db->get_one(array('Referee'=>$myReferee));
        // if($is_exist){
        //     echo '<script>window.location.reload();</script>';exit;//重新加载页面
        // }
        include template('tparents', 'parent-regis');
    }
     
    public function stu_reg(){

        $inte=new inte();

        //联动要求
        if(isset($_POST['tt_id'])){
            $this->tt_db= pc_base::load_model('sys_tutor_type_model');
            $tt_infos=$this->tt_db->select(array('tt_parentid'=>$_POST['tt_id']),'*','','`tt_seq` ASC','','');
            $tt_infos=json_encode($tt_infos);
            echo $tt_infos;
            exit;
        }


        if(isset($_POST['info'])){

            // return;
            //验证码
            $session_storage = 'session_'.pc_base::load_config('system','session_storage');
            pc_base::load_sys_class($session_storage);
            $code=$_POST['code'];
            if(strtolower($_SESSION['code'])!= strtolower($code)) {
                $s_id=0;
                $mas_arr[0]=$s_id;
                echo json_encode($mas_arr);
               exit();
            }
            //验证短信验证码
            if(strtolower($_SESSION['msg_code'])!= strtolower($_POST['msg_code'])) {
                $s_id=3;
                $mas_arr[0]=$s_id;
                echo json_encode($mas_arr);
                return;
            }

            //验证注册手机号与接收短信手机号是否一致
            if(strtolower($_SESSION['msg_phone'])!= strtolower($_POST['info']['student_mobile'])) {
                $s_id=4;
                $mas_arr[0]=$s_id;
                echo json_encode($mas_arr);
                return;
            }
            //处理上课时间数组，得到天数的字符串
            foreach ($_POST['time'] as $key => $a_time) {
                if(empty($a_time['from_hour'])){
                    unset($_POST['time'][$key]);
                }
            }
            $week_arr = array('mon' => 0,'tues' => 1,'wed' => 2,'thur' => 3,'fri' => 4,'sat' => 5,'sun' => 6 );
            foreach ($_POST['time'] as $k=>$v) {
               $k=$week_arr[$k];
               $b[]=$k;
            }
            $week=implode(',', $b);
            //处理上课时间数组，得到天数的字符串结束

            $info=$_POST['info'];
            $re_info=$_POST['re_info'];

            $user_IP = ($_SERVER["HTTP_VIA"]) ? $_SERVER["HTTP_X_FORWARDED_FOR"] : $_SERVER["REMOTE_ADDR"];
            $regip = ($user_IP) ? $user_IP : $_SERVER["REMOTE_ADDR"];

            //黑名单验证
            $verify_num=count($this->db->select(array('regip'=>$regip,'verify'=>1)));
            $where['regip']=$regip;
            $infos=$this->db->select($where);

            //根据学生性别去选择男女头像
            if($re_info['st_num_join_m']!="" && $re_info['st_num_join_f']==""){
                $userimage="/statics/images/add_image/student_m.jpg";
            }
            if($re_info['st_num_join_m']=="" && $re_info['st_num_join_f']!=""){
                $userimage="/statics/images/add_image/student_f.jpg";
            }
            if($re_info['st_num_join_m']!="" && $re_info['st_num_join_f']!=""){
                $userimage="/statics/images/add_image/student_both.jpg";
            }
            if($re_info['st_num_join_m']=="" && $re_info['st_num_join_f']==""){
                $userimage="/statics/images/add_image/student_both.jpg";
            }
            if($verify_num<3 || empty($infos)){

                //member表需要的数据
                $data['username']=$info['student_contactname'];
                $data['mobile']=$info['student_housemobile'];
                $data['password']=md5($_POST['student_pwd']);
                $data['role_verify']=$_POST['role_verify'];
                $data['regip']=$regip;
                $data['phone']=$info['student_mobile'];
                $data['verify']='0';
                $data['email'] = $info['student_email'];
                $to_sendmail=$data['email'];
                $data['groupid'] = $_POST['groupid'];
                $data['nickname'] = $_POST['nickname'];
                $data['regdate'] = time();
                $data['userimage'] =$userimage;
                $data['Referee'] = $info['Referee'];//推荐码
                $Referee=$data['Referee'];
                // return;
                //用户名或邮箱唯一
                $where_info['role_verify']= 1;
                $infos=$this->db->select($where_info);
                foreach ($infos as $info2) {
                    if($info['student_contactname']==$info2['username']){
                        $s_id=1;
                        $mas_arr[0]=$s_id;
                        echo json_encode($mas_arr);
                        return;
                    }
                    if($info['student_email']==$info2['email']){
                        $s_id=2;
                        $mas_arr[0]=$s_id;
                        echo json_encode($mas_arr);
                        return;
                    }
                    if($info['student_mobile']==$info2['phone']){
                        $s_id=5;
                        $mas_arr[0]=$s_id;
                        echo json_encode($mas_arr);
                        return;
                    }
                }

                //member表插入数据
                $member_res=$this->db->insert($data);
                if($member_res){
                   $info=$_POST['info'];

                   //移除主资料表不需要的字段
                   unset($info['imgurl']);
                   unset($info['Referee']);
                   unset($info['myReferee']);
                    /*新增*/
                    $re_info['st_new_name'] = $info['student_name'];
                    $re_info['st_new_nickname']  = $_POST['nickname'];

                    //每小時学费
                    $re_info['st_tutor_fee']=(int)($_POST['st_tutor_fee']);
                    //主资料表
                    $this->s_db = pc_base::load_model('student_master_model');
                    $where_name['email']= $data['email'];
                    $where_name['role_verify']= '1';
                    $member_info=$this->db->select($where_name,'*',$limit='1',$order='');
                    $info['student_userid']=$member_info[0]['userid'];

                    //将注册信息保存到session中，注册成功后自动登录
                    $this->_session_start();
                    $_SESSION['id']=$info['student_userid'];
                    $_SESSION['username']=$member_info[0]['username'];
                    $_SESSION['role_verify']='1';


                    //形成家長編號
                    $hou = substr(strval("572"+$member_info[0]['userid']+1000000),1,6);
                    $personid="S".$hou;
                    $this->db->update(array('personid'=>$personid),$where_name);
                    $res_master=$this->s_db->insert($info);


                    if($res_master){
                        $where_stuid['student_userid']=$member_info[0]['userid'];
                        $stuid=$this->s_db->select($where_stuid);
                        /*从master表拿出student_id，再形成个案编号插入transaction表中*/
                        $year=substr(date("Y"),-2);
                        $newNumber = substr(strval("572"+$stuid[0]['student_id']+100000),1,5);
                        $re_info['st_id']="P".$year.$newNumber;
                        //授课要求表
                        $userid = $member_info[0]['userid'];
                        $where_userid['student_userid'] = $userid;
                        $stu_info=$this->s_db->select($where_userid,$data='student_id',$limit='1',$order='');
                        $student_id = $stu_info[0]['student_id'];
                        $this->stu_tran_db=pc_base::load_model('student_transaction_model');
                        $re_info['st_studentid']=$student_id;
                        $re_info['st_num_join']=$re_info['st_num_join_m']+$re_info['st_num_join_f'];
                        $re_info['st_area']=$info['student_locid'];
                        $re_info['st_main_lang']=$re_info['st_main_lang'];
                        $req_res=$this->stu_tran_db->insert($re_info);

                        //授课时间要求
                        $where_student['st_studentid'] = $student_id;
                        $req_info=$this->stu_tran_db->select($where_student,$data='st_id',$limit='1',$order='');
                        $st_id = $req_info[0]['st_id'];


                        if($req_res){
                            $integration=$inte->get_inte_setting(2);
                            $sp_inte=$inte->get_inte_setting(14);
                            $spread_num=$inte->get_inte_num(14);
                            //為推薦人賬戶加10積分
                            if($Referee!=""){
                                //推荐人号码与学生注册号码一致时，将积分给导师用户
                                if($data['phone']==$Referee){
                                    $spread_man=$this->db->get_one(array('phone'=>$Referee,'role_verify'=>0),"role_verify,integral,personid,username");
                                    //推薦人同時推薦多少會員之後額外奬勵積分
                                    $num=$this->db->count(array('Referee'=>$Referee));
                                    if(!empty($spread_num) && !empty($sp_inte)){
                                        if(($num%$spread_num)==0){
                                            $spread_inte=$inte->get_inte_setting(14);
                                            $inte->write_inte_log($spread_man['personid'],$spread_man['username'],'14','','','',$spread_inte);
                                        }
                                    }
                                    $integral=$spread_man['integral']+$integration+$spread_inte;
                                    $this->db->update(array('integral'=>$integral),array('phone'=>$Referee,'role_verify'=>0));
                                    //将积分记录在日志表
                                    $inte->write_inte_log($spread_man['personid'],$spread_man['username'],'2',$personid,$_SESSION['username'],'1',$integration);
                                }else{
                                    $condition['phone']=$Referee;
                                    $role=$this->db->select($condition,"role_verify,integral,personid,username");
                                    foreach ($role as $v) {
                                        if(count($role)>1){
                                           if($v['role_verify']=='0'){
                                            $condition['role_verify']="0";
                                            //得到推荐号码的导师账号信息
                                            $person=$this->db->select($condition,"role_verify,integral,personid,username");
                                            //推薦人同時推薦多少會員之後額外奬勵積分
                                            $num=$this->db->count(array('Referee'=>$condition['phone']));
                                            if(!empty($spread_num) && !empty($sp_inte)){
                                                if(($num>=$spread_num) && ($num%$spread_num)==0){
                                                    $spread_inte=$inte->get_inte_setting(14);
                                                    $inte->write_inte_log($person[0]['personid'],$person[0]['username'],'14','','','',$spread_inte);
                                                }
                                            }
                                                $integral=$v['integral']+$integration+$spread_inte;

                                                //为推荐人账户添加积分
                                                $this->db->update(array('integral'=>$integral),$condition);
                                                //将积分记录在日志表
                                                $inte->write_inte_log($person[0]['personid'],$person[0]['username'],'2',$personid,$_SESSION['username'],'1',$integration);

                                            }
                                        }else{
                                            //推薦人同時推薦多少會員之後額外奬勵積分
                                            $num=$this->db->count(array('Referee'=>$condition['phone']));
                                            if(!empty($spread_num) && !empty($sp_inte)){
                                                if(($num>=$spread_num) && ($num%$spread_num)==0){
                                                    $spread_inte=$inte->get_inte_setting(14);
                                                    $inte->write_inte_log($v['personid'],$v['username'],'14','','','',$spread_inte);
                                                }
                                            }
                                            $integral=$v['integral']+$integration+$spread_inte;
                                            $this->db->update(array('integral'=>$integral),$condition);
                                            //将积分记录在日志表
                                            $inte->write_inte_log($v['personid'],$v['username'],'2',$personid,$_SESSION['username'],'1',$integration);
                                        }
                                    }
                                }
                            }
                            //给注册成功的用户添加积分
                            $reg_inte=$inte->get_inte_setting(1);
                            $this->db->update(array('integral'=>$reg_inte),array('userid'=>$userid));
                            if(!empty($reg_inte)){
                                $inte->write_inte_log($personid,$_SESSION['username'],'1','','','',$reg_inte);
                            }

                            $s_id['st_id'] = $st_id;
                            $s_id['student_id'] = $student_id;
                            $s_id['userid'] = $userid;
                            //echo json_encode($s_id);
                            if(!empty($info['student_locid'])){$place =$info['student_locid'];}
                            if(!empty($re_info['st_ttid_1'])){$type =$re_info['st_ttid_1'];}
                            if(!empty($re_info['st_ttid_2'])){$item =$re_info['st_ttid_2'];}
                            if(!empty($re_info['st_ttid_3'])){$level =$re_info['st_ttid_3'];}
                            if(!empty($re_info['st_class_hour'])){$classtime =$re_info['st_class_hour'];}
                            if(!empty($re_info['st_tutor_fee'])){$hourlyfee =$re_info['st_tutor_fee'];}
                            if(!empty($re_info['st_min_grade'])){$miniEduLevel =$re_info['st_min_grade'];}
                            if(!empty($re_info['st_req_sex'])&& $re_info['st_req_sex']!='A'){$gender =$re_info['st_req_sex'];}
                            if(!empty($re_info['like_collage'])){$like_collage =$re_info['like_collage'];}
                            $tutor=new tutor();
                            $teacherlist=$tutor->getSuitedTeacher($type,$item,$level,$place,$classtime,$hourlyfee,$miniEduLevel,$gender,$classlang,$like_collage,1,1,'','','',$week);
                            foreach ($teacherlist['data'] as $key=> $teacher) {
                                $star=$tutor->return_star($teacher['master_id'],$teacher['role_verify']);
                                $teacher['star']=$star;

                                //得到教授地区
                                pc_base::load_app_class('helpcommon' ,'pair');
                                $helpcommon=new helpcommon();
                                $place=$helpcommon->create_location($teacher['tsl_locid']);
                                $aa=explode(',',$place);
                                foreach($aa as $key=>$a){
                                    if($key>=3){
                                        $place=$aa[0].','.$aa[1].','.$aa[2].'...';
                                    }
                                }
                                $teacher['teach_area']=$place;

                                //得到教授科目
                                $subject=$helpcommon->create_subject($teacher['tstt_ttid_2']);
                                $subname=explode(',',$subject);
                                foreach($subname as $k=>$sub){
                                    if($k>=3){
                                        $subname=$subname[0].','.$subname[1].','.$subname[2].'...';
                                    }
                                }
                                $teacher['teach_type']=$subname;

                                $weeknums = array('6'=> "S",'0'=> "M",'1'=> "T",'2'=> "W",'3'=> "T",'4'=> "F",'5'=> "S");
                                //得到可教授時間
                                if($teacher['tst_weekday']==""){
                                    foreach($weeknums as $num){
                                         $teacher['teach_time'].="<span>".$num."</sapn>";
                                    }
                                }else{

                                    $week=explode(",",$teacher['tst_weekday']);
                                    foreach($weeknums as $k=>$num){
                                    $str="";
                                    if(strpos($teacher['tst_weekday'],strval($k)) !== false ){
                                        $str = "style='color: black'";
                                    }
                                    $teacher['teach_time'].="<span ".$str.">".$num."</span>";
                                    }
                                }

                                //確定此個案未配對還是在配對中
                                $pairid=$this->p_db->get_one("pair_tutor_tranid = '$teacher[tm_tutorid]'");
                                if(!empty($pairid)){
                                    $teacher['match_area']="<div class='course-match'>配對中</div>";
                                }else{
                                    $teacher['match_area']="";
                                }
                                $teacherlist['data'][$key]=$teacher;
                            }
                            //echo json_encode($teacherlist);
                            $mas_arr[0]=$s_id;
                            $mas_arr[1]=$teacherlist;
                            echo json_encode($mas_arr);
                            //注册成功后发送邮件
                            $stu_email_data="<!DOCTYPE html>
                            <html>
                            <head>
                                <meta charset='utf-8'>
                                <meta name='viewport' content='width=device-width,initial-scale=1.0, maximum-scale=1.0,minimum-scale=1.0,user-scalable=no'>
                                <title>tutorseeking</title>
                                <style>
                                    /*.match-table td{padding: 6px;}*/
                                </style>
                            </head>
                            <body>
                                <div style='max-width: 880px; margin:0 auto; font-size: 14px; color: #333; text-align: justify'>
                                    <form action=''>
                                        <div style='text-align: center'>
                                            <br>
                                            <br>
                                            <img width='250px' src='".APP_PATH."statics/images/add_image/logo-sticky.png' alt=''>
                                        </div>
                                        <h2 style='text-align: center; font-size: 24px;'>尊敬的用戶：您好！您于 ". date('Y-m-d H:i')." 註冊了TutorSeeking的家長帳號！以下是您的帳戶信息！</h2>
                                        <br>
                                        <div style='text-align: center;margin-left:8%;'>
                                            <div style='display:inline-block; margin-right: 60px; margin-bottom: 10px'>
                                                <table border='1px' class='match-table' cellpadding='0' cellspacing='0' style='width: 500px; font-size: 20px; text-align: center;border-collapse:collapse' >
                                                    <tbody>
                                                    <tr>
                                                        <td style='padding: 6px;font-size: 20px;'  colspan='2' >用戶信息</td>
                                                    </tr>
                                                    <tr>
                                                        <td style='padding: 6px;font-size: 20px;' >姓名</td>
                                                        <td style='padding: 6px;font-size: 20px;' >".$info['student_contactname']."</td>
                                                    </tr>
                                                    <tr>
                                                        <td style='padding: 6px;font-size: 20px;' >註冊電話</td>
                                                        <td style='padding: 6px;font-size: 20px;' >".$info['student_mobile']."</td>
                                                    </tr>
                                                    <tr>
                                                        <td style='padding: 6px;font-size: 20px;' >註冊電郵</td>
                                                        <td style='padding: 6px;font-size: 20px;' >".$to_sendmail."</td>
                                                    </tr>
                                                    <tr>
                                                        <td style='padding: 6px;font-size: 20px;' >登録密碼</td>
                                                        <td style='padding: 6px;font-size: 20px;' >".$_POST['student_pwd']."</td>
                                                    </tr>
                                                    <tr>
                                                        <td style='padding: 6px;font-size: 20px;'  colspan='2'><a href='".APP_PATH."index.php?m=content&c=index&a=std_login' style='text-decoration: none; color: #e39916;'>前往登録</a></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <br>";
                            $stu_email_data .="        </form>
                                </div>
                            </body>
                            </html>";
                            pc_base::load_sys_func('mail'); //實例化郵件類
                            sendmail($to_sendmail,'TutorSeeking用戶註冊信息', $stu_email_data);//發送郵件給註冊用戶
                            exit;
                        }else{
                            return;
                        }
                    }else{
                        return;
                    }
                 }else{
                    return;
                }
            }else{
            //黑名单也插入一次
            //$this->member->insert($data);
            $s_id=array();
            $mas_arr[0]=$s_id;
            echo json_encode($mas_arr);
            }
            exit;
        }
   
    include template('tparents', 'parent-regis');
    }


    //最佳授课时间
    public function time(){
        $time_a=$_POST['time_a'];
        $time=$_POST['time_arr'];
        $time_re_arr['sst_stid'] = $time_a['st_id'];
        $time_re_arr['sst_studentid'] = $time_a['student_id'];
        if(empty($time)){return '';}
        $time = explode("],", $time); 
        //取出数字
        $reg="/\d+/";
        for($i=0;$i<count($time);$i++){
            preg_match_all($reg,$time[$i],$time_arr[$i]);
        }
        //删除星期值都为空的值
        foreach ($time_arr as $key =>$v){  
            $new_arr[]=$v[0]; 
        }
        foreach($new_arr as $k=>$v){
            //删除星期值都为空的值
            if(!$v){//判断是否为空（false）
                unset($new_arr[$k]);//删除
            }
            //删除星期不完整的值
            //js判断必须填完整
        }
        //所有星期为空
        if(!$new_arr){
            unset($new_arr);
        }
        foreach ($new_arr as $k => $v) {
            $temp[]=(count($v));
            $pos=array_search(max($temp),$temp);
            $index= $temp[$pos];
            for ($i=0; $i < $index-1 ; $i+=2) { 
                $fromtime[$k][]=$new_arr[$k][$i];
                $totime[$k][]=$new_arr[$k][$i+1];
            }
        }
        //获取索引值
        $time_re['sst_weekday']=array_keys($fromtime);
        foreach ($fromtime as $key => $value) {
            $time_re['sst_fromtime'][$key]=trim(implode(';', $value),';');
        }
        foreach ($totime as $key => $value) {
            $time_re['sst_totime'][$key]=trim(implode(';', $value),';');
        }


        // dump($time_re);
        $time_re_arr['sst_fromtime']=implode(',', $time_re['sst_fromtime']);
        $time_re_arr['sst_totime']=implode(',', $time_re['sst_totime']);
        $time_re_arr['sst_weekday']=implode(',', $time_re['sst_weekday']);
        $this->time_db = pc_base::load_model('student_sel_time_model');
        $time_res=$this->time_db->insert($time_re_arr);
        if($time_res){
            //注册完成.发送短信认证
            //短信操作成功
            //$data2['verify'] = $SMS==true?'2':'1';
            $data2['verify'] = 2;
            $where_info['userid']= $time_a['userid'];
            $time_update = $this->db->update($data2, $where_info);
            if($time_update){
              //返回短信操作值 
                echo "認證成功";
            }
        }
        exit;
    }


    public function stu_detail(){
        if(isset($_GET['stuid'])){
            $stuid=$_GET['stuid'];
            $tran_id=$_GET['tran_id'];
            //查詢個案詳情
            $sql="select * from `v9_student_transaction` as tran 
            left join `v9_student_master` as master on master.student_id=tran.st_studentid 
            left join `v9_sys_location` as loc on loc.loc_id = tran.st_area 
            left join `v9_sys_tutor_type` as type on type.tt_id = tran.st_ttid_3 
            left join `v9_member` as mem on mem.userid =master.student_userid
            left join `v9_grade` as g on g.grade_id =tran.st_min_grade
            left join `v9_student_sel_time` as time on time.sst_studentid =master.student_id
            where mem.userid='$stuid' and tran.st_id='$tran_id' order by st_date desc ";
            $this->db=pc_base::load_model('student_transaction_model');
            $this->db->query($sql);
            $stu= $this->db->fetch_array();



            if(!empty($_SESSION['id'])){
                $userid=$_SESSION['id'];
            //查詢pair表中是否存在當前登陸人與此個案的配對
            $sql_case="select tm_tutorid from `v9_tutor_transaction` as tran 
            left join `v9_tutor_master` as master on tran.master_id = master.tutor_id
            left join `v9_member` as member on master.tutor_userid=member.userid where member.userid = $userid
            ";
            $this->db=pc_base::load_model('tutor_transaction_model');
            $this->db->query($sql_case);
            $caselist= $this->db->fetch_array();
                foreach ($caselist as  $value) {
                    $pair_tutor_tranid=$value['tm_tutorid'];
                    $sql_isset="select * from v9_pair where pair_student_tranid='$tran_id' and pair_tutor_tranid='$pair_tutor_tranid' ";
                    
                    $this->db=pc_base::load_model('pair_model');
                    // $this->db->query($sql_isset);
                    $pair= $this->db->fetch_array($this->db->query($sql_isset));
                    
                    // if(!empty($pair_stu)&&empty($pair)){ $flag=true;}
                    //登陸者有個案與此個案匹配  
                    if(count($pair) >0){ 
                        $current_pair = $pair[0];//當前用戶的個案與當前個案配對信息  
                        $tm_tutorid=$value['tm_tutorid'];
                        $stid=$tran_id;
                    }   
                }
                
            }
            $sql_stu="select pair_id,pair_state from v9_pair where pair_student_tranid='$tran_id'";
            $pair_stu= $this->db->fetch_array($this->db->query($sql_stu))[0]; 
           include template('tparents', 'stu_detail');  
        }    
    }


    public function search_all(){
        $userid=$_SESSION['id'];
        $role_verify=$_SESSION['role_verify'];
        //未登录状态
        if(!isset($role_verify)){echo "0";exit();}
        if($role_verify=="1"){
            //查询当前登录学生的未匹配个案
            $sql="select st_id,st_ttid_1,st_ttid_2,st_ttid_3,st_tutor_fee,st_area from `v9_student_transaction` as tran 
                left join `v9_student_master` as master on master.student_id=tran.st_studentid 
                left join `v9_member` as mem on mem.userid =master.student_userid
                where mem.userid='$userid' and st_matched='0'";
            $this->db=pc_base::load_model('student_transaction_model');
            $count= $this->db->fetch_array($this->db->query($sql));

            $this->locdb= pc_base::load_model('sys_location_model');
            $this->sudb= pc_base::load_model('sys_tutor_type_model');
            $this->pairdb= pc_base::load_model('pair_model');
            foreach ($count as $key => $value) {
                 //由科目、地区id查询科目地区名称
                    $sql="select loc_name from v9_sys_location where loc_id=$value[st_area]";
                    $this->locdb->query($sql);
                    $loclist=$this->locdb->fetch_array();
                    $count[$key]['loc_name']=$loclist[0]['loc_name'];

                    $sql="select tt_name from v9_sys_tutor_type where tt_id=$value[st_ttid_1]";
                    $this->sudb->query($sql);
                    $sublist=$this->sudb->fetch_array();
                    $count[$key]['sub_name_1']=$sublist[0]['tt_name'];

                    $sql="select tt_name from v9_sys_tutor_type where tt_id=$value[st_ttid_2]";
                    $this->sudb->query($sql);
                    $sublist=$this->sudb->fetch_array();
                    $count[$key]['sub_name_2']=$sublist[0]['tt_name'];

                    $sql="select tt_name from v9_sys_tutor_type where tt_id=$value[st_ttid_3]";
                    $this->sudb->query($sql);
                    $sublist=$this->sudb->fetch_array();
                    $count[$key]['sub_name']=$sublist[0]['tt_name'];

                 //查詢pair表中是否存在此個案
                 $sql_isset="select pair_id from v9_pair where pair_student_tranid='$value[st_id]'";
                    $this->pairdb->query($sql_isset);
                    $pair=$this->pairdb->fetch_array();
                    if(!empty($pair[0])){
                        unset($count[$key]);
                    }
                }
            $list['list']=$count;
            $list['count']=count($count);
        }elseif ($role_verify=="0") {
            //查询当前登录导师的未匹配个案
            $sql="select tm_tutorid,tsl_locid,tstt_ttid_1,tstt_ttid_2,tstt_ttid_3,tstt_fee from `v9_member` as member 
            left join `v9_tutor_master` as master on master.tutor_userid=member.userid 
            left join `v9_tutor_transaction` as tran on tran.master_id = master.tutor_id 
            left join `v9_tutor_sel_location` as loc on loc.tsl_tutorid = tran.tm_tutorid 
            left join `v9_tutor_sel_tutor_type` as tstt on tstt.tstt_tutorid =tran.tm_tutorid
                where member.userid='$userid' and tran.tm_matched='0'";   
            $this->db=pc_base::load_model('tutor_transaction_model');
            $count= $this->db->fetch_array($this->db->query($sql));
          


            //得到导师的地区、科目id，处理后查询相应名称
            foreach ($count as $key => $value) {
                pc_base::load_app_class('helpcommon' ,'pair');
                $helpcommon=new helpcommon();
                $place=$helpcommon->create_location($value['tsl_locid']);
                $aa=explode(',',$place);
                foreach($aa as $k=>$a){
                    if($k>=3){
                        $area[$key]='<p>'.$aa[0].'</p><p>'.$aa[1].'</p><p>'.$aa[2].'</p><p>...</p>';
                    }else{
                       $area[$key].= '<p>'.$a.'</p>';
                    }  
                }
                $count[$key]['loc_name']=$area[$key];


                $subject1=$helpcommon->create_subject($value['tstt_ttid_1']);
                $sub1=explode(',',$subject1);
                foreach($sub1 as $k=>$s){
                    if($k>=3){
                        $subname1[$key]='<p>'.$sub1[0].'</p><p>'.$sub1[1].'</p><p>'.$sub1[2].'</p><p>...</p>';
                    }else{
                       $subname1[$key].= '<p>'.$s.'</p>';
                    }  
                }
                $count[$key]['sub_name_1']=$subname1[$key];


                $subject2=$helpcommon->create_subject($value['tstt_ttid_2']);
                $sub2=explode(',',$subject2);
                foreach($sub2 as $k=>$s){
                    if($k>=3){
                        $subname2[$key]='<p>'.$sub2[0].'</p><p>'.$sub2[1].'</p><p>'.$sub2[2].'</p><p>...</p>';
                    }else{
                       $subname2[$key].= '<p>'.$s.'</p>';
                    }  
                }
                $count[$key]['sub_name_2']=$subname2[$key];


                $subject3=$helpcommon->create_subject($value['tstt_ttid_3']);
                $sub3=explode(',',$subject3);
                foreach($sub3 as $k=>$s){
                    if($k>=3){
                        $subname3[$key]='<p>'.$sub3[0].'</p><p>'.$sub3[1].'</p><p>'.$sub3[2].'</p><p>...</p>';
                    }else{
                       $subname3[$key].= '<p>'.$s.'</p>';
                    }  
                }
                $count[$key]['sub_name']=$subname3[$key];


                //查詢pair表中是否存在此個案
                $sql_isset="select pair_id from v9_pair where pair_tutor_tranid ='$value[tm_tutorid]'";
                $this->pairdb= pc_base::load_model('pair_model');
                $this->pairdb->query($sql_isset);
                $pair=$this->pairdb->fetch_array();
                if(!empty($pair[0])){
                    unset($count[$key]);
                } 
        }
            $list['list']=$count;
            $list['count']=count($count);
        }
        echo json_encode($list);
    }

    /*
        獲取地區
    */
     public function getLocation(){
        // echo 123;exit;
       $id = $_POST['id'];
       $sql = "SELECT loc_name,loc_id FROM v9_sys_location where loc_parentid=$id";
        $this->db->query($sql);
        $location= $this->db->fetch_array();
        echo json_encode($location);exit;
    }

    //導師發起匹配申請
    public function tor_request(){
        if(!empty($_POST['tmid']) && !empty($_POST['stid']) && !empty($_POST['url'])){
            $this->tor_class = pc_base::load_app_class('tutor','pair');//加載導師公共操作類
            $res = $this->tor_class->tor_send_request($_POST['tmid'],$_POST['stid'],$_POST['url'],$_POST['amount'],$_POST['points']);//調用導師匹配導師方法
            echo json_encode($res);//返回支付JS或失敗狀態
        }
    }

    //導師審核配對申請
    public function tor_respond(){
        if(!empty($_POST['tmid']) && !empty($_POST['stid']) && !empty($_POST['respond'])){
            $this->tor_class = pc_base::load_app_class('tutor','pair');//加載導師公共操作類
            $res = $this->tor_class->tor_respond($_POST['tmid'],$_POST['stid'],$_POST['respond'],$_POST['url'],$_POST['amount'],$_POST['points']);//調用導師審核配對方法
            echo json_encode($res);//返回支付JS或失敗狀態 
        }
    }

    //導師免費發起匹配申請
    public function tor_request_free(){
        $PairModel = pc_base::load_model('pair_model');//加載配對表模型
        $FailModel = pc_base::load_model('pair_fail_model');//加載配對副表模型
        $this->tran = pc_base::load_model('tutor_transaction_model');//加載導師執行表模型
        $this->S_tran = pc_base::load_model('student_transaction_model');//加載學生要求表模型
        if(!empty($_POST['tmid']) && !empty($_POST['stid'])){
            $tmid = $_POST['tmid'];
            $stid = $_POST['stid'];
            $where = " `pair_tutor_tranid` = '$tmid' && `pair_student_tranid` = '$stid' && `pair_state` = '0'";
            //查看是否曾經配對過
            // $fail_where = " `pair_tutor_tranid` = '$tmid' && `pair_student_tranid` = '$stid'";
            // $is_paired = $FailModel->get_one($fail_where);
            // if(!empty($is_paired)){
            //     $return['msg'] = '曾經配對過的個案不可再次配對!';
            //     echo json_encode($return);exit;
            // }
            $is_exist = $PairModel->get_one($where);
            if(!$is_exist){ //不存在配對信息時 新增配對信息

                //防止已經在配對的個案再次配對
                $tutor_is_pair = $PairModel->get_one("`pair_tutor_tranid` = '$tmid'");
                $student_is_pair = $PairModel->get_one("`pair_student_tranid` = '$stid'");
                if(!empty($tutor_is_pair) || !empty($student_is_pair)){
                    $return = array('msg'=>'fail');
                    return $return;exit;
                }
                
                $this->tran->update("`tm_state` = 0","`tm_tutorid` = '$tmid'");//导师执行表是否被拒绝状态改为0:否
                $this->S_tran->update("`st_state` = 0","`st_id` = '$stid'");//学生要求表是否被拒绝状态改为0:否
                $data = array(
                    'pair_tutor_tranid'=>$tmid,
                    'pair_student_tranid'=>$stid,
                    'pair_sponsor'=>1,
                    'tutor_pay'=>1,
                );
                if($PairModel->insert($data)){
                    //發起成功 扣除免費配對次數
                    $pairid = $PairModel->insert_id();
                    $this->deductTorPairfreetime($pairid);

                    //发送郵件
                    pc_base::load_app_class('tutor','pair');//實例化導師類
                    $tutor = new tutor();
                    $Pairing = $tutor->order_send_email($pairid);
                    $return = array('msg'=>'success');
                }else{
                    $return = array('msg'=>'fail');
                }
            }else{ //已存在配對信息時 直接更改付款狀態

                //異常情況返回系統繁忙：導師或學生已付款
                if($is_exist['tutor_pay'] == '1' || $is_exist['student_pay'] == '1'){
                    $return = array('msg'=>'fail');
                    return $return;exit;
                }

                if($PairModel->update("`tutor_pay` = 1","`pair_id` = $is_exist[pair_id]")){
                    //發起成功 扣除免費配對次數
                    $this->deductTorPairfreetime($is_exist['pair_id']);

                    //发送郵件
                    pc_base::load_app_class('tutor','pair');//實例化導師類
                    $tutor = new tutor();
                    $Pairing = $tutor->order_send_email($is_exist['pair_id']);
                    $return = array('msg'=>'success');
                }else{
                    $return = array('msg'=>'fail');
                }
            }
            echo json_encode($return);
        }
    }

    //導師免費同意配對申請
    public function tor_respond_free(){
        $PairModel = pc_base::load_model('pair_model');//加載配對表模型
        if(!empty($_POST['tmid']) && !empty($_POST['stid']) && !empty($_POST['respond'])){
            $tmid = $_POST['tmid'];
            $stid = $_POST['stid'];
            $where = " `pair_tutor_tranid` = '$tmid' && `pair_student_tranid` = '$stid' && `pair_state` = '0'";
            $is_exist = $PairModel->get_one($where);
            $success = array('tutor_pay'=>'1','pair_state'=>'1');//配對成功狀態
            if($PairModel->update($success,"`pair_id` = $is_exist[pair_id]")){
                //發起成功 扣除免費配對次數
                $this->deductTorPairfreetime($is_exist['pair_id']);

                //更改配對狀態和執行表狀態
                $torTranModel = pc_base::load_model('tutor_transaction_model');       //加載導師執行表模型
                $stdTranModel = pc_base::load_model('student_transaction_model');     //加載學生要求表模型
                $torTranModel->update("`tm_matched` = '1'","`tm_tutorid` = '$is_exist[pair_tutor_tranid]'");
                $stdTranModel->update("`st_matched` = '1'","`st_id` = '$is_exist[pair_student_tranid]'");

                //发送郵件
                pc_base::load_app_class('tutor','pair');//實例化導師類
                $tutor = new tutor();
                $Pairing = $tutor->order_send_email($is_exist['pair_id']);

                $HelpCommon = pc_base::load_app_class('helpcommon','pair');//加載配對輔助類
                //配对成功推荐人积分奖励
                $HelpCommon->activityReward($is_exist['pair_id']);
                $return = array('msg'=>'success');
            }else{
                $return = array('msg'=>'fail');
            }
            echo json_encode($return);
        }
    }

    //扣除導師免費配對次數 並將被拒絕的其中一個配對訂單的導師付款狀態改為金額已使用
    private function deductTorPairfreetime($pairid){
        $returnPoint = pc_base::load_app_class('integralOperation','pay');
        $userData= $returnPoint->GetUserData($pairid,'tor_agree_pay');              //获取導師userid
        $deduct = $returnPoint->freetimeOperation($userData['userid'],1,$type='-'); //扣除導師一次免費配對次數
        
        $torModel = pc_base::load_model('tutor_master_model');                  //加載導師主资料表模型
        $torTranModel = pc_base::load_model('tutor_transaction_model');         //加載導師執行表模型
        $pairFail = pc_base::load_model('pair_fail_model');                     //加載配對副表模型
        $torMaster = $torModel->get_one("`tutor_userid` = $userData[userid]");  //獲取導師主資料信息
        $allTran = $torTranModel->select("`master_id` = $torMaster[tutor_id]"); //獲取所有執行表
        $allTranid = '';//所有執行表ID
        foreach ($allTran as $key => $value) {
            $allTranid .= "'".$value['tm_tutorid']."',";
        }
        $allTranid = rtrim($allTranid,',');//移除右邊的逗號
        $unrefundPair = $pairFail->select("`pair_tutor_tranid` in ($allTranid) and `tutor_pay` = '1'")[0];//獲取其中一個配對信息
        if(!empty($unrefundPair)){
            $pairFail->update("`tutor_pay` = '4'","`pair_id` = $unrefundPair[pair_id]");//更改狀態為4(金額已被使用)
            //将退款金额被使用的配对个案ID存入使用免费配对次数的配对个案
            if(!empty($unrefundPair['tor_freepair_id'])){
                $this->p_db->update("`tor_freepair_id` = '$unrefundPair[tor_freepair_id]'","`pair_id` = $pairid");
            }else{
                $this->p_db->update("`tor_freepair_id` = '$unrefundPair[pair_id]'","`pair_id` = $pairid");
            }
        }
    }


    //修改用户积分方法
    public function editIntegral(){
        $inte=new inte();
        //存在用户ID及即将修改的积分值时
        if(!empty($_POST['uid']) && isset($_POST['integralVal'])){
            $old=$this->db->get_one("`userid`=".$_POST['uid']);
            $updateInfo = array('integral'=>$_POST['integralVal']);
            $integration=$_POST['integralVal']-$old['integral'];
            //将积分记录在日志表
            $inte->write_inte_log($old['personid'],$old['username'],'13','','','0',$integration);
            $res = $this->db->update($updateInfo,"userid = $_POST[uid]");
            if($res){
                echo 1;
            }else{
                echo 0;
            }
        }
    }

}

?>