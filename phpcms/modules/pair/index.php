<?php



defined('IN_PHPCMS') or exit('No permission resources.');
class index {

	function __construct() {
		$this->Pair = pc_base::load_model('pair_model');
		$this->Fail = pc_base::load_model('pair_fail_model');
		$this->ttype = pc_base::load_model('sys_tutor_type_model');
		$this->location = pc_base::load_model('sys_location_model');
		$this->Tutor = pc_base::load_model('tutor_master_model');
		$this->T_tran = pc_base::load_model('tutor_transaction_model');
		$this->Std = pc_base::load_model('student_master_model');
		$this->S_tran = pc_base::load_model('student_transaction_model');
		pc_base::load_app_class('inte' ,'integration');
		$this->mem = pc_base::load_model('mem_model');
		$session_storage = 'session_'.pc_base::load_config('system','session_storage');
		pc_base::load_sys_class($session_storage);
	}


	public function test_location(){
	}

	public function init(){
		$userid = $_SESSION['id'];
		if(empty($userid)){
			showmessage(L('login_website'),APP_PATH);
		}

		$indentity = $this->Std->fetch_array($this->Std->query("select * from `v9_member` where `userid` = $userid"))[0];

		$pairid = isset($_GET['pairid']) ? $_GET['pairid'] : showmessage(L('illegal_parameters'), HTTP_REFERER);
		$pair = $this->Pair->get_one("pair_id = '$pairid'");
		if(empty($pair) || $pair['pair_state'] != '1'){ showmessage(L('illegal_parameters'), HTTP_REFERER);exit; }

	
		$stu_sql = "select * from `v9_student_transaction` as tran 
					left join `v9_student_master` as stu on stu.student_id = tran.st_studentid 
					left join `v9_member` as mem on mem.userid = stu.student_userid
					left join `v9_sys_location` as loc on loc.loc_id = tran.st_area
					left join `v9_grade` as grade on grade.grade_id = tran.st_min_grade
				    where st_id = '$pair[pair_student_tranid]'";
		$stu_info = $this->Std->fetch_array($this->Std->query($stu_sql))[0];

	
		$stu_info['st_ttid_1'] = $this->ttype->get_one("tt_id = ".$stu_info['st_ttid_1'])['tt_name'];
		$stu_info['st_ttid_2'] = $this->ttype->get_one("tt_id = ".$stu_info['st_ttid_2'])['tt_name'];
		$stu_info['st_ttid_3'] = $this->ttype->get_one("tt_id = ".$stu_info['st_ttid_3'])['tt_name'];

	
		$tutor_sql = "select * from `v9_tutor_transaction` as tran 
					  left join `v9_tutor_master` as tor on tor.tutor_id = tran.master_id 
					  left join `v9_member` as mem on mem.userid = tor.tutor_userid
					  left join `v9_tutor_sel_time` as time on time.tst_tutorid = tran.tm_tutorid
					  left join `v9_tutor_sel_location` as loc on loc.tsl_tutorid = tran.tm_tutorid
					  left join `v9_tutor_sel_tutor_type` as type on type.tstt_tutorid = tran.tm_tutorid
					  left join `v9_grade` as grade on grade.grade_id = tor.tutor_high_grade
					  where tm_tutorid = '$pair[pair_tutor_tranid]'";
		$tutor_info = $this->Tutor->fetch_array($this->Tutor->query($tutor_sql))[0];

		
		$tstt_ttid_1 = array_unique(explode(',', $tutor_info['tstt_ttid_1']));
		$tstt_ttid_2 = array_unique(explode(',', $tutor_info['tstt_ttid_2']));
		$tstt_ttid_3 = array_unique(explode(',', $tutor_info['tstt_ttid_3']));
		foreach ($tstt_ttid_1 as $key => $value) {
			$tutor_info['tstt_ttid_name_1'] .= $this->ttype->get_one("tt_id = ".$value)['tt_name'].',';
		}
		foreach ($tstt_ttid_2 as $key => $value) {
			$tutor_info['tstt_ttid_name_2'] .= $this->ttype->get_one("tt_id = ".$value)['tt_name'].',';
		}
		foreach ($tstt_ttid_3 as $key => $value) {
			$tutor_info['tstt_ttid_name_3'] .= $this->ttype->get_one("tt_id = ".$value)['tt_name'].',';
		}
		$tutor_info['tstt_ttid_name_1'] = rtrim($tutor_info['tstt_ttid_name_1'],',');
		$tutor_info['tstt_ttid_name_2'] = rtrim($tutor_info['tstt_ttid_name_2'],',');
		$tutor_info['tstt_ttid_name_3'] = rtrim($tutor_info['tstt_ttid_name_3'],',');

	
		$HelpCommon_Model = pc_base::load_app_class('helpcommon');
		$tutor_info['tsl_locid'] = $HelpCommon_Model->create_location($tutor_info['tsl_locid']);

		$com_sql = "select * from `v9_pair_comment` where pc_pairid = '$pairid'";
		$comments = $this->Pair->fetch_array($this->Pair->query($com_sql))[0];
		include template('pair', 'pair_detail');
	}

	public function addComment(){
		$inte=new inte();
		if($_POST){
			$pairid = $_POST['pairid'];
			$tutorid = $_POST['tutorid'];
			$studentid = $_POST['studentid'];
			$star = $_POST['star'];
	
			$sql = "select * from `v9_pair_comment` where pc_pairid = $pairid and pc_type = 2";
			$is_exist = $this->Pair->fetch_array($this->Pair->query($sql));
			if(!$is_exist){
				$sql = "INSERT INTO `v9_pair_comment` (`pc_pairid`, `pc_tutorid`, `pc_studentid`, `pc_star`, `pc_type`) VALUES ($pairid, '$tutorid', '$studentid', $star, '2');";
				$res = $this->Pair->query($sql);

				$masterid = $this->T_tran->get_one("`tm_tutorid` = '$tutorid'")['master_id'];
				$userid = $this->Tutor->get_one("`tutor_id` = $masterid")['tutor_userid'];
				
				$memberModel = pc_base::load_model('member_model');
				$owner=$memberModel->get_one("`userid` = $userid");
				$provider=$memberModel->get_one("`userid` = ".$_SESSION['id']);
				if($star > 16){
					$integration=$inte->get_inte_setting(5);
					$memberModel->update("`integral` = `integral` + ".$integration,"`userid` = $userid");
					if(!empty($integration)){
						$inte->write_inte_log($owner['personid'],$owner['username'],'5',$provider['personid'],$provider['username'],'',$integration);
					}
				}elseif($star > 12){
					$integration=$inte->get_inte_setting(6);
					$memberModel->update("`integral` = `integral` + ".$integration,"`userid` = $userid");
					if(!empty($integration)){
						$inte->write_inte_log($owner['personid'],$owner['username'],'6',$provider['personid'],$provider['username'],'',$integration);
					}
				}elseif($star > 8){
					$integration=$inte->get_inte_setting(7);
					$memberModel->update("`integral` = `integral` + ".$integration,"`userid` = $userid");
					if(!empty($integration)){
						$inte->write_inte_log($owner['personid'],$owner['username'],'7',$provider['personid'],$provider['username'],'',$integration);
					}
				}elseif($star > 4){
					$integration=$inte->get_inte_setting(8);
					$memberModel->update("`integral` = `integral` + ".$integration,"`userid` = $userid");
					if(!empty($integration)){
						$inte->write_inte_log($owner['personid'],$owner['username'],'8',$provider['personid'],$provider['username'],'',$integration);
					}
				}else{
					$integration=$inte->get_inte_setting(9);
					$memberModel->update("`integral` = `integral` + ".$integration,"`userid` = $userid");
					if(!empty($integration)){
						$inte->write_inte_log($owner['personid'],$owner['username'],'9',$provider['personid'],$provider['username'],'',$integration);
					}
				}
				
				if($res){
					$comment_inte=$inte->get_inte_setting(4);
					$Return = pc_base::load_app_class('integralOperation','pay');
					$Return->integralOperation($_SESSION['id'],$comment_inte,$type='+');
					if(!empty($comment_inte)){
						$inte->write_inte_log($provider['personid'],$provider['username'],'4','','','',$comment_inte);
					}
					$return = array('code'=>1,'msg'=>'評價成功');
				}else{
					$return = array('code'=>0,'msg'=>'請刷新重試');
				}
			}else{
				$return = array('code'=>0,'msg'=>'您已評價過');
			}
			echo json_encode($return);exit;
		}
	}

	

	
	public function pay_fee(){
 
		$tmid = isset($_GET['tmid']) ? $_GET['tmid'] : showmessage(L('illegal_parameters'), HTTP_REFERER);
		$stid = isset($_GET['stid']) ? $_GET['stid'] : showmessage(L('illegal_parameters'), HTTP_REFERER);
		$uid = !empty($_SESSION['id']) ? $_SESSION['id'] : showmessage('請先登陸', HTTP_REFERER);
		$role = $_SESSION['role_verify'];
		$memberModel = pc_base::load_model('member_model');
		$memberinfo = $memberModel->get_one("userid = $uid");
		$settingModel = pc_base::load_model('extend_setting_model');
		$service_charge = floatval($settingModel->get_one("`key` = 'Service_Charge'")['data']);

	
		$pair = $this->Pair->get_one("`pair_tutor_tranid` = '$tmid' and `pair_student_tranid` = '$stid'");
		if(!empty($pair)){
			$Return = pc_base::load_app_class('integralOperation','pay');
			$Return->returnIntegralOperation($pair['pair_id']);
		}

		
		$pair = $this->Pair->get_one("`pair_tutor_tranid` = '$tmid' and `pair_student_tranid` = '$stid'");

		if($role == 0){
			$transaction = $this->T_tran->get_one("tm_tutorid = '$tmid'");
			$master = $this->Tutor->get_one("tutor_id = $transaction[master_id]");
			if($master['tutor_userid'] != $uid){
				showmessage(L('illegal_parameters'), HTTP_REFERER);
			}
		}else{
			$transaction = $this->S_tran->get_one("st_id = '$stid'");
			$master = $this->Std->get_one("student_id = $transaction[st_studentid]");
			if($master['student_userid'] != $uid){
				showmessage(L('illegal_parameters'), HTTP_REFERER);
			}
		}
		
	
		$stu_sql = "select *,men.phone,men.email,men.userid from `v9_student_transaction` as tran 
					left join `v9_student_master` as stu on stu.student_id = tran.st_studentid 
					left join `v9_sys_location` as loc on loc.loc_id = tran.st_area
					left join `v9_grade` as grade on grade.grade_id = tran.st_min_grade
					left join `v9_member` as men on men.userid = stu.student_userid
				    where st_id = '$stid'";
		$stu_info = $this->Std->fetch_array($this->Std->query($stu_sql))[0];


		$stu_info['st_ttid_1'] = $this->ttype->get_one("tt_id = ".$stu_info['st_ttid_1'])['tt_name'];
		$stu_info['st_ttid_2'] = $this->ttype->get_one("tt_id = ".$stu_info['st_ttid_2'])['tt_name'];
		$stu_info['st_ttid_3'] = $this->ttype->get_one("tt_id = ".$stu_info['st_ttid_3'])['tt_name'];

	
		$tutor_sql = "select *,men.phone,men.email,men.userid from `v9_tutor_transaction` as tran 
					  left join `v9_tutor_master` as tor on tor.tutor_id = tran.master_id 
					  left join `v9_tutor_sel_time` as time on time.tst_tutorid = tran.tm_tutorid
					  left join `v9_tutor_sel_location` as loc on loc.tsl_tutorid = tran.tm_tutorid
					  left join `v9_tutor_sel_tutor_type` as type on type.tstt_tutorid = tran.tm_tutorid
					  left join `v9_grade` as grade on grade.grade_id = tor.tutor_high_grade
					  left join `v9_member` as men on men.userid = tor.tutor_userid
					  where tm_tutorid = '$tmid'";
		$tutor_info = $this->Tutor->fetch_array($this->Tutor->query($tutor_sql))[0];

		$paymentType = 0;
		if($role == 0){
			if(empty($pair)){	
				$paymentType  = 1;
			}else{
				if($pair['pair_sponsor'] == 1){		
					if($pair['tutor_pay'] == 0){
						$paymentType  = 2;	
					}else{
						$paymentType = 0;	
					}
				}else{			
					$paymentType  = 3;		
				}
			}	
		}else{
			if(empty($pair)){	
				$paymentType  = 4;			
			}else{
				if($pair['pair_sponsor'] == 2){		
					if($pair['student_pay'] == 0){
						$paymentType  = 5;	
					}else{
						$paymentType = 0;	
					}
				}else{		
					$paymentType  = 6;	
				}
			}	
		}

	
		$tstt_ttid_1 = array_unique(explode(',', $tutor_info['tstt_ttid_1']));
		$tstt_ttid_2 = array_unique(explode(',', $tutor_info['tstt_ttid_2']));
		$tstt_ttid_3 = array_unique(explode(',', $tutor_info['tstt_ttid_3']));
		foreach ($tstt_ttid_1 as $key => $value) {
			$tutor_info['tstt_ttid_name_1'] .= $this->ttype->get_one("tt_id = ".$value)['tt_name'].',';
		}
		foreach ($tstt_ttid_2 as $key => $value) {
			$tutor_info['tstt_ttid_name_2'] .= $this->ttype->get_one("tt_id = ".$value)['tt_name'].',';
		}
		foreach ($tstt_ttid_3 as $key => $value) {
			$tutor_info['tstt_ttid_name_3'] .= $this->ttype->get_one("tt_id = ".$value)['tt_name'].',';
		}
		$tutor_info['tstt_ttid_name_1'] = rtrim($tutor_info['tstt_ttid_name_1'],',');
		$tutor_info['tstt_ttid_name_2'] = rtrim($tutor_info['tstt_ttid_name_2'],',');
		$tutor_info['tstt_ttid_name_3'] = rtrim($tutor_info['tstt_ttid_name_3'],',');

		
		$tsl_locid = array_unique(explode(',', $tutor_info['tsl_locid']));
		foreach ($tsl_locid as $key => $value) {
			$tutor_info['tsl_locid'] = $this->location->get_one("loc_id = ".$value)['loc_name'].',';
		}
		$tutor_info['tsl_locid'] = rtrim($tutor_info['tsl_locid'],',');
		$total_data = array(
					'tutor_info' => $tutor_info,
					'stu_info' => $stu_info,
					'pair' => $pair
				);
		include template('pair', 'pay_fee');
	}


	public function student_refund(){
		if(!empty($_POST['id'])){
			$res = $this->Fail->update("`student_pay` = '2'","pair_id = $_POST[id]");
			if($res){
				$pairinfo = $this->Fail->get_one("`pair_id` = '$_POST[id]'");
				$studentid = $this->S_tran->get_one("`st_id` = '$pairinfo[pair_student_tranid]'")['st_studentid'];
				$userid = $this->Std->get_one("`student_id` = $studentid")['student_userid'];
			
				$memberModel = pc_base::load_model('member_model');
				$memberModel->update("`freepair_time`=`freepair_time`-1","userid = ".$userid);
				$return['msg'] = 'application_success';
			}else{
				$return['msg'] = 'update_fail';
			}
		}else{
			$return['msg'] = 'error_pairid';
		}
		echo json_encode($return);
	}


	public function tutor_refund(){
		if(!empty($_POST['id'])){
			$res = $this->Fail->update("`tutor_pay` = '2'","pair_id = $_POST[id]");
			if($res){
				$pairinfo = $this->Fail->get_one("`pair_id` = '$_POST[id]'");
				$tutorid = $this->T_tran->get_one("`tm_tutorid` = '$pairinfo[pair_tutor_tranid]'")['master_id'];
				$userid = $this->Tutor->get_one("`tutor_id` = $tutorid")['tutor_userid'];
				
				$memberModel = pc_base::load_model('member_model');
				$memberModel->update("`freepair_time`=`freepair_time`-1","userid = ".$userid);
				$return['msg'] = 'application_success';
			}else{
				$return['msg'] = 'update_fail';
			}
		}else{
			$return['msg'] = 'error_pairid';
		}
		echo json_encode($return);
	}
}
?>