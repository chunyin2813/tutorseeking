<?php
defined('IN_PHPCMS') or exit('No permission resources.');

class tutor {

	private $tutor, $pair, $tran;

	public function __construct() {
		$this->mem=pc_base::load_model('member_model');
		$this->tutor = pc_base::load_model('tutor_model');
		$this->pair = pc_base::load_model('pair_model');
		$this->pair_fail = pc_base::load_model('pair_fail_model');
		$this->tran = pc_base::load_model('tutor_transaction_model');
		$this->S_tran = pc_base::load_model('student_transaction_model');
		$this->type = pc_base::load_model('tutor_sel_tutor_type_model');
		$this->loc = pc_base::load_model('tutor_sel_location_model');
		$this->time = pc_base::load_model('tutor_sel_time_model');
		$this->ttype = pc_base::load_model('sys_tutor_type_model');
		$this->location = pc_base::load_model('sys_location_model');
		$this->Std = pc_base::load_model('student_model');
		pc_base::load_app_class('inte' ,'integration');
	}

	
	public function tor_public_transaction($tutorid,$ttid_1,$ttid_2,$ttid_3,$locid,$weekday,$fromtime,$totime){
		$this->tran->query('START TRANSACTION');

		$transaction = array('tm_tutorid'=>$tutorid);
		$this->tran->insert($transaction);
		$tmid = $this->tran->insert_id();

		$tutortype = array('tstt_tutorid'=>$tutorid,'tstt_tmid'=>$tmid,'tstt_ttid_1'=>$ttid_1,'tstt_ttid_2'=>$ttid_2,'tstt_ttid_3'=>$ttid_3);

		$tutorloc = array('tsl_tutorid'=>$tutorid,'tsl_tmid'=>$tmid,'tsl_locid'=>$locid);

		$tutortime = array('tst_tutorid'=>$tutorid,'tst_tmid'=>$tmid,'tst_weekday'=>$weekday,'tst_fromtime'=>$fromtime,'tst_totime'=>$totime);
		if(!$this->type->insert($tutortype) || !$this->loc->insert($tutorloc) || !$this->time->insert($tutortime)){
			$this->tran->query('ROLLBACK');//回滚
		}else{
			$this->tran->query('COMMIT');//执行事务
			$paypal = pc_base::load_app_class('paypal','pay');
			$order = array(
					'item_name'=>'tutor_public_pay',
					'amount'=>'0.01',
					'currency_code'=>'HKD',
					'invoice'=>'10086',
					'no_note'=>'导师发布要求支付',
				);
			return $paypal->paypal($order);
		}
	}


	public function tor_transaction($tutorid='',$tmid='',$page='1',$limit='10'){
		if(!empty($tmid)){
			$sql = 'select * from `v9_tutor_transaction` as tran
						 left join `v9_tutor_master` as master on master.tutor_id = tran.tm_tutorid
						 left join `v9_tutor_sel_tutor_type` as type on type.tstt_tmid = tran.tm_id
						 left join `v9_tutor_sel_location` as loc on loc.tsl_tmid = tran.tm_id
						 left join `v9_tutor_sel_time` as time on time.tst_tmid = tran.tm_id where tran.tm_id = '.$tmid;
			$tutorinfo = $this->tutor->query($sql);
			$transaction = $this->tutor->fetch_array($tutorinfo);
			$transaction = $transaction[0];
			return $transaction;die;
		}else{
			$where = array('tm_tutorid'=>$tutorid);
			$tranlist['data'] = $this->tran->listinfo($where, 'tm_modtime DESC', $page, $limit);
			$tranlist['page_link'] = $this->tran->pages;//分页
			return $tranlist;die;
		}
		
	}


	public function tor_sponsored_list($tutorid,$sponsor,$page='1',$limit='10'){
		$tutorid = isset($tutorid) ? $tutorid : showmessage(L('illegal_parameters'), HTTP_REFERER);
		$sponsor = isset($sponsor) ? $sponsor : showmessage(L('illegal_parameters'), HTTP_REFERER);
		if($sponsor == '3'){
			$where = " tm_tutorid = $tutorid";
		}else{
			$where = " tm_tutorid = $tutorid && pair_sponsor = $sponsor";
		}

		$sql = "select * from `v9_pair` as pair
						 left join `v9_tutor_transaction` as tran on pair.pair_tmid = tran.tm_id
						 left join `v9_tutor_master` as master on master.tutor_id = tran.tm_tutorid where".$where;
		$tutorinfo = $this->tutor->query($sql);
		$pairlist['data'] = $this->tutor->fetch_array($tutorinfo);
		$pairlist['page_link'] = pages($num = count($pairlist['data']), $page, $limit, '', array(), 10);
		return $pairlist;
	}

	
	public function getSuitedTeacher($type='',$item='',$level='',$place='',$classtime='',$hourlyfee='',$miniEduLevel='',$gender='',$classlang='',$like_collage='',$page='0',$limit='10',$order='DESC',$personid='',$tutor_hschool2='',$week='',$maxpage='10',$teachstar){
		
		$where = " tm_matched = '0' and role_verify='0'" ;

		
		if(!empty($hourlyfee)){	

			if(is_array($hourlyfee)){
				$where .= " and tstt_fee >= $hourlyfee[0] AND tstt_fee <= $hourlyfee[1]";
			}else{
				$where .= " and tstt_fee <= $hourlyfee";
			}
		}

	
		if(!empty($miniEduLevel)){	$where .= " and tutor_high_grade >= $miniEduLevel" ;}


		if(!empty($gender) && $gender != 'A'){ $where .= " and tutor_sex = '$gender'"; }

	
		if(!empty($classlang)){ $where .= " and tutor_main_lang in ('$classlang','')"; }


		if(!empty($like_collage)){	$where .= " and tutor_college in ('$like_collage','')";	}


		if(!empty($personid)){	$where .= " and personid = '$personid'";}


		if(!empty($tutor_hschool2)){	$where .= " and tutor_hschool2 like '%$tutor_hschool2%'";}

		$sql = "select SQL_CALC_FOUND_ROWS * from `v9_member` as member 
            left join `v9_tutor_master` as master on master.tutor_userid=member.userid 
            left join `v9_tutor_transaction` as tran on tran.master_id = master.tutor_id 
            left join `v9_tutor_sel_location` as loc on loc.tsl_tutorid = tran.tm_tutorid 
            left join `v9_tutor_sel_time` as time on time.tst_tutorid = tran.tm_tutorid 
            left join `v9_tutor_sel_tutor_type` as tstt on tstt.tstt_tutorid =tran.tm_tutorid 
            left join `v9_student_type` as stype on stype.st_id = master.tutor_high_teach
            left join `v9_sys_location` as place on place.loc_id = master.tutor_address_locid 
            left join `v9_grade` as grade on grade.grade_id = master.tutor_high_grade
            left join `v9_collage` as collage on collage.id = master.tutor_college 
            left join `v9_sys_tutor_type` as tut_type on tstt.tstt_ttid_2  = tut_type.tt_id 
            where".$where.' order by tstt_fee DESC,tutor_high_grade '.$order;

		$pairinfo = $this->tutor->query($sql);
		$tutorList = $this->tutor->fetch_array($pairinfo);

		if(!empty($type)){
			foreach ($tutorList as $key => $value) {
				$tutor_ttid_1 = explode(',', $value['tstt_ttid_1']);
				if(!in_array($type,$tutor_ttid_1)){
					unset($tutorList[$key]);
				}
			}
		}
		if(!empty($item)){
			foreach ($tutorList as $key => $value) {
				$tutor_ttid_2 = explode(',', $value['tstt_ttid_2']);
				if(!in_array($item,$tutor_ttid_2)){
					unset($tutorList[$key]);
				}
			}
		}
		if(!empty($level)){
			$parentid = $this->ttype->get_one("`tt_id` = $level");
			$all_level = $this->ttype->select("`tt_parentid` = $parentid[tt_parentid]");
			$suit_level = array();
			foreach ($all_level as $key => $value) {
				if($value['tt_seq'] >= $parentid['tt_seq']){
					$suit_level[] = $value['tt_id'];
				}
			}
			foreach ($tutorList as $key => $value) {
				$tutor_ttid_3 = explode(',', $value['tstt_ttid_3']);
				$intersection = array_intersect($suit_level, $tutor_ttid_3);
				if(empty($intersection)){
					unset($tutorList[$key]);
				}
			}
		}

		
		if(!empty($place)){
			foreach ($tutorList as $key => $value) {
				$tutor_place = explode(',', $value['tsl_locid']);
				if(!in_array($place,$tutor_place)){
					unset($tutorList[$key]);
				}
			}
		}

	
		if($week != ''){
			foreach ($tutorList as $key => $value) {
				$tutor_week = explode(',', $value['tst_weekday']);
				$std_week = explode(',', $week);
				if($tutor_week[0] != ''){
					$intersection = array_intersect($tutor_week, $std_week);
					if(empty($intersection)){
						unset($tutorList[$key]);
					}else{
						$tutorList[$key]['intersect_time'] = count($intersection);
					}
				}
			}
		}

	
		
		if(!empty($teachstar)){
			foreach ($tutorList as $key => $value) {
				$re_star=$this->return_star($value['master_id'],'0');
				if($teachstar>$re_star){
					unset($tutorList[$key]);
				}
			}
			
		 }
	
		if(!empty($page)){
			$count = count($tutorList);
			$limit = !empty($limit) ? $limit : 10;
			$start = ($page-1)*$limit;
			$tutorList = array_slice($tutorList, $start,$limit);
		}

		$transaction['data'] = $tutorList;
		$transaction['page_link'] = pages($num = $count, $page, $limit, '', array(), $maxpage);
		return $transaction;
	}


	public function tor_send_request($tmid,$stid,$url,$amount,$points=0){
		$inte=new inte();

		$tmid = isset($tmid) ? $tmid : showmessage(L('illegal_parameters'), HTTP_REFERER);
		$stid = isset($stid) ? $stid : showmessage(L('illegal_parameters'), HTTP_REFERER);

		$where = " `pair_tutor_tranid` = '$tmid' && `pair_student_tranid` = '$stid' && `pair_state` = '0'";
		

        $is_exist = $this->pair->get_one($where);
		if(!$is_exist){	
			$tutor_is_pair = $this->pair->get_one("`pair_tutor_tranid` = '$tmid'");
			$student_is_pair = $this->pair->get_one("`pair_student_tranid` = '$stid'");
			if(!empty($tutor_is_pair) || !empty($student_is_pair)){
				$return['code'] = 0;
            	$return['msg'] = '系統繁忙，請刷新重試!';
            	return $return;exit;
			}

			$this->tran->update("`tm_state` = '0'","`tm_tutorid` = '$tmid'");
			$this->S_tran->update("`st_state` = '0'","`st_id` = '$stid'");
			$data = array(
				'pair_tutor_tranid'=>$tmid,
				'pair_student_tranid'=>$stid,
				'pair_sponsor'=>1,
				'tutor_use_points'=>$points,
			);
			$this->pair->insert($data);
			if($this->pair->insert_id()){
				$pairid = $this->pair->insert_id();

				$returnPoint = pc_base::load_app_class('integralOperation','pay');
				$userData= $returnPoint->GetUserData($pairid,'tor_agree_pay');

				$HelpCommon = pc_base::load_app_class('helpcommon','pair');		
		        $results = $HelpCommon->Payment_Security_Mechanism($amount,$points,$userData['userid']);
		        if($results['code'] != 0){
		        	$return['code'] = 0;
		            $return['msg'] = $results['msg'];
		            return $return;exit;
		        }
		        
				$returnPoint->integralOperation($userData['userid'],$userData['Integral'],$type='-');
				$provider=$this->mem->get_one("`userid` =". $userData['userid']);
		
				if(!empty($userData['Integral'])){
	                $inte->write_inte_log('','','11',$provider['personid'],$provider['username'],'','-'.$userData['Integral']);
                }
				if($results['amount'] == 0){				
			
					$this->pair->update("`tutor_pay` = '1'","`pair_id` = $pairid");
		
					pc_base::load_app_class('tutor','pair');
	                $tutor = new tutor();
	                $Pairing = $tutor->order_send_email($pairid);
					$return['code'] = 1;
					$return['msg'] = '成功發起配對';
					return $return;exit;
				}else{
		
					$paypal = pc_base::load_app_class('Paypal','pay');
					$order = array(
						'item_name'=>'tor_public_pay',
						'amount'=>$results['amount'],
						'currency_code'=>'HKD',
						'invoice'=>'T'.$pairid,
						'no_note'=>'导师发起配对支付',
						'return_url'=>$url,
					);
					return $paypal->paypal($order);
				}
			}
		}else{	
			if($is_exist['tutor_pay'] == '1' || $is_exist['student_pay'] == '1'){
				$return['code'] = 0;
	            $return['msg'] = '系統繁忙，請刷新重試!';
	            return $return;exit;
			}

			$returnPoint = pc_base::load_app_class('integralOperation','pay');
			$userData= $returnPoint->GetUserData($is_exist[pair_id],'tor_agree_pay');

			$HelpCommon = pc_base::load_app_class('helpcommon','pair');
	        $results = $HelpCommon->Payment_Security_Mechanism($amount,$points,$userData['userid']);
	        if($results['code'] != 0){
	        	$return['code'] = 0;
	            $return['msg'] = $results['msg'];
	            return $return;exit;
	        }

	        if($results['amount'] == 0){
	        	$returnPoint->integralOperation($userData['userid'],$points,$type='-');
	        	$provider=$this->mem->get_one("`userid` =". $userData['userid']);
		
				if(!empty($points)){
	                $inte->write_inte_log('','','11',$provider['personid'],$provider['username'],'','-'.$points);
                }
	        
				$this->pair->update(array('tutor_pay'=>'1','tutor_use_points'=>$points),"`pair_id` = $is_exist[pair_id]");
	
				pc_base::load_app_class('tutor','pair');		
                $tutor = new tutor();
                $Pairing = $tutor->order_send_email($is_exist['pair_id']);
				$return['code'] = 1;
				$return['msg'] = '成功發起配對';
				return $return;exit;
	        }else{
	       
				$this->pair->update("`tutor_use_points` = $results[points]","`pair_id` = $is_exist[pair_id]");
				
				$returnPoint->integralOperation($userData['userid'],$results['points'],$type='-');
				$provider=$this->mem->get_one("`userid` =". $userData['userid']);
			
				if(!empty($results['points'])){
	                $inte->write_inte_log('','','11',$provider['personid'],$provider['username'],'','-'.$results['points']);
                }
				$paypal = pc_base::load_app_class('Paypal','pay');
				$order = array(
					'item_name'=>'tor_public_pay',
					'amount'=>$results['amount'],
					'currency_code'=>'HKD',
					'invoice'=>'T'.$is_exist['pair_id'],
					'no_note'=>'导师发起配对支付',
					'return_url'=>$url,
				);
				return $paypal->paypal($order);
	        }
		}
	}

	
	public function tor_respond($tmid,$stid,$respond,$url='',$amount=0,$points=0){
		$inte=new inte();
		$tmid = isset($tmid) ? $tmid : showmessage(L('illegal_parameters'), HTTP_REFERER);
		$stid = isset($stid) ? $stid : showmessage(L('illegal_parameters'), HTTP_REFERER);
		$respond = isset($respond) ? $respond : showmessage(L('illegal_parameters'), HTTP_REFERER);

		$where = " `pair_tutor_tranid` = '$tmid' && `pair_student_tranid` = '$stid'";
		$pairinfo = $this->pair->get_one($where);
		if(!empty($pairinfo)){
			if($respond == 'agree'){
				$returnPoint = pc_base::load_app_class('integralOperation','pay');
				$userData= $returnPoint->GetUserData($pairinfo['pair_id'],'tor_agree_pay');

				$HelpCommon = pc_base::load_app_class('helpcommon','pair');
		        $results = $HelpCommon->Payment_Security_Mechanism($amount,$points,$userData['userid']);
		        if($results['code'] != 0){
		        	$return['code'] = 0;
		            $return['msg'] = $results['msg'];
		            return $return;exit;
		        }

				$this->pair->update("`tutor_use_points` = $results[points]","`pair_id` = $pairinfo[pair_id]");
				if($results['points'] != 0){
					$returnPoint->integralOperation($userData['userid'],$results['points'],$type='-');
					$provider=$this->mem->get_one("`userid` =". $userData['userid']);
		
	                $inte->write_inte_log('','','11',$provider['personid'],$provider['username'],'','-'.$results['points']);
				}

				if($results['amount'] == 0){
		
					$pair_data = array('tutor_pay'=>'1','pair_state'=>'1');
					$this->pair->update($pair_data,"`pair_id` = $pairinfo[pair_id]");
					$this->tran->update("`tm_matched` = '1'","`tm_tutorid` = '$tmid'");
					$this->S_tran->update("`st_matched` = '1'","`st_id` = '$stid'");
		 
					pc_base::load_app_class('tutor','pair');
	                $tutor = new tutor();
	                $Pairing = $tutor->order_send_email($pairinfo['pair_id']);
					$return['code'] = 1;
					$return['msg'] = '配對成功';
		
					$HelpCommon->activityReward($pairinfo['pair_id']);
					return $return;exit;
				}else{

					$paypal = pc_base::load_app_class('Paypal','pay');
					$order = array(
						'item_name'=>'tor_agree_pay',
						'amount'=>$results['amount'],
						'currency_code'=>'HKD',
						'invoice'=>'T'.$pairinfo['pair_id'],
						'no_note'=>'导师同意配对支付',
						'return_url'=>$url,
					);
					$return['code'] = 'agree_success';
					$return['msg'] = $paypal->paypal($order);
					return $return;exit;
				}
			}else{

				$Disagree = pc_base::load_model('pair_fail_model');
				$StuModel = pc_base::load_model('student_transaction_model');
				$pairinfo['pair_state'] = 2;
				$stu_tran['st_state'] = '1';
				if($Disagree->insert($pairinfo)){
					$where = "`pair_id` = $pairinfo[pair_id]";
					$this->pair->update($pairinfo,$where);
					$StuModel->update($stu_tran,"`st_id` = '$pairinfo[pair_student_tranid]'");
					
					$Pairing = $this->getPairingData($pairinfo['pair_id']);	    
					$Pairing = $this->structurePairingData($Pairing);			
					pc_base::load_sys_func('mail');								
					$PairingData = getPairingMailData($Pairing['info']);		
					sendmail($Pairing['email']['stu_email'],'TutorSeekIng配對信息', $PairingData);
					sendmail('enquiry@tutorseeking.com','TutorSeekIng配對信息', $PairingData);  
					if($this->pair->delete($where)){
					
						$memberModel = pc_base::load_model('member_model');
						$memberModel->update("`freepair_time`=`freepair_time`+1","userid = ".$Pairing['info']['stu_info']['student_userid']);
						$return = array('code'=>'disagree_success','msg'=>'拒絕成功');
					}
				}
			}
			return $return;exit;
		}else{
			$return = array('code'=>'illegal_request','msg'=>'非法操作');
			return $return;exit;
		}
	}

	
	public function tor_public_comment($pairid,$studentid,$star,$content){
		$this->pc = pc_base::load_model('pair_comment_model');
		$data = array(
				'pc_pairid'=>$pairid,
				'pc_tutorid'=>$_SESSION['uid'],
				'pc_studentid'=>$studentid,
				'pc_type'=>'1',
				'pc_star'=>$star,
				'pc_content'=>$content,
			);
		if($this->pc->insert($data)){
			showmessage(L('operation_success'),HTTP_REFERER);
		}else{
			showmessage(L('operation_failure'),HTTP_REFERER);
		}
	}


	
	public function RefundMail(){

	}



	public function RefuseMail($id,$state){

		$tutor_sql = "select *,men.phone,men.email,men.userid from `v9_tutor_transaction` as tran 
					  left join `v9_tutor_master` as tor on tor.tutor_id = tran.master_id 
					  left join `v9_tutor_sel_time` as time on time.tst_tutorid = tran.tm_tutorid
					  left join `v9_tutor_sel_location` as loc on loc.tsl_tutorid = tran.tm_tutorid
					  left join `v9_tutor_sel_tutor_type` as type on type.tstt_tutorid = tran.tm_tutorid
					  left join `v9_grade` as grade on grade.grade_id = tor.tutor_high_grade
					  left join `v9_member` as men on men.userid = tor.tutor_userid
					  where tm_tutorid = '$id'";
		$tutor_info = $this->tutor->fetch_array($this->tutor->query($tutor_sql))[0];

		$tutor_info['email'] = "******";
		$tutor_info['phone'] = "******";
		$tutor_info['mobile'] = "******";
		$tutor_info['tutor_email'] = "******";
		$tutor_info['tutor_mobile'] = "******";


		dump($tutor_info);
		
	}



	function getPairingData($pairid,$type = 1){


		if($type == 1){
			$pair = $this->pair->get_one("pair_id = '$pairid'");
		}else{
			$pair = $this->pair_fail->get_one("pair_id = '$pairid'");
		}
		
		if(empty($pair)){ showmessage(L('illegal_parameters'), HTTP_REFERER);exit; }


		$stu_sql = "select *,men.phone,men.email,men.userid from `v9_student_transaction` as tran 
					left join `v9_student_sel_time` as time on time.sst_stid = tran.st_id 
					left join `v9_student_master` as stu on stu.student_id = tran.st_studentid 
					left join `v9_member` as mem on mem.userid = stu.student_userid
					left join `v9_sys_location` as loc on loc.loc_id = tran.st_area
					left join `v9_grade` as grade on grade.grade_id = tran.st_min_grade
					left join `v9_member` as men on men.userid = stu.student_userid
				    where st_id = '$pair[pair_student_tranid]'";
		$stu_info = $this->Std->fetch_array($this->Std->query($stu_sql))[0];

	
		$stu_info['st_ttid_1'] = $this->ttype->get_one("tt_id = ".$stu_info['st_ttid_1'])['tt_name'];
		$stu_info['st_ttid_2'] = $this->ttype->get_one("tt_id = ".$stu_info['st_ttid_2'])['tt_name'];
		$stu_info['st_ttid_3'] = $this->ttype->get_one("tt_id = ".$stu_info['st_ttid_3'])['tt_name'];

		if(isset($stu_info['sst_weekday'])){
			$allweek_cn = array('一','二','三','四','五','六','日');
			$stu_weekday = explode(',', $stu_info['sst_weekday']);	
		
			foreach ($stu_weekday as $key => $value) {
				$stu_week_cn[] = $allweek_cn[$value];
			}
			$fromtime = rtrim($stu_info['sst_fromtime'],';');		
			$fromtime = explode(',', $fromtime);					
			$totime = rtrim($stu_info['sst_totime'],';');			
			$totime = explode(',', $totime);						
			$stu_info['weekday'] ='';								
			if($stu_weekday[0] != ''){

		
				foreach ($fromtime as $key => $value) {
					if(strpos($value, ';')){
						$stu_from[$key] = explode(';', $value);
					}else{
						$stu_from[$key] = $value;
					}
				}

	
				foreach ($totime as $key => $value) {
					if(strpos($value, ';')){
						$to[$key] = explode(';', $value);
					}else{
						$to[$key] = $value;
					}
				}

		
				foreach ($stu_from as $key => $value) {
					$weekdate = '';
					if(is_array($value)){
						foreach ($value as $ke => $val) {
							$weekdate .= substr($val,0,2).':'.substr($val,2,2).' —— '.substr($to[$key][$ke],0,2).':'.substr($to[$key][$ke],2,2).'<br>';
						}
					}else{
						$weekdate .= substr($value,0,2).':'.substr($value,2,2).' —— '.substr($to[$key],0,2).':'.substr($to[$key],2,2);
					}
					$stu_info['weekday'] .= "<tr><td style='padding:6px 6px;' >星期".$stu_week_cn[$key]."</td><td style='padding:6px 6px;' >".$weekdate."</td></tr>";
				}
				
			}else{
				$stu_info['weekday'] = "<tr ><td colspan='2' style='padding:6px 6px;'>未填寫</td></tr>";
			}
		}


		$tutor_sql = "select *,men.phone,men.email,men.userid from `v9_tutor_transaction` as tran 
					  left join `v9_tutor_master` as tor on tor.tutor_id = tran.master_id
					  left join `v9_member` as mem on mem.userid = tor.tutor_userid
					  left join `v9_tutor_sel_time` as time on time.tst_tutorid = tran.tm_tutorid
					  left join `v9_tutor_sel_location` as loc on loc.tsl_tutorid = tran.tm_tutorid
					  left join `v9_tutor_sel_tutor_type` as type on type.tstt_tutorid = tran.tm_tutorid
					  left join `v9_grade` as grade on grade.grade_id = tor.tutor_high_grade
					  left join `v9_member` as men on men.userid = tor.tutor_userid
					  where tm_tutorid = '$pair[pair_tutor_tranid]'";
		$tutor_info = $this->tran->fetch_array($this->tran->query($tutor_sql))[0];

	
		$tstt_ttid_1 = array_unique(explode(',', $tutor_info['tstt_ttid_1']));
		$tstt_ttid_2 = array_unique(explode(',', $tutor_info['tstt_ttid_2']));
		$tstt_ttid_3 = array_unique(explode(',', $tutor_info['tstt_ttid_3']));
		foreach ($tstt_ttid_1 as $key => $value) {
			$tutor_info['tstt_ttid_name_1'] .= $this->ttype->get_one("tt_id = ".$value)['tt_name'].',';
		}
		foreach ($tstt_ttid_2 as $key => $value) {
			$tutor_info['tstt_ttid_name_2'] .= $this->ttype->get_one("tt_id = ".$value)['tt_name'].',';
		}
		foreach ($tstt_ttid_3 as $key => $value) {
			$tutor_info['tstt_ttid_name_3'] .= $this->ttype->get_one("tt_id = ".$value)['tt_name'].',';
		}
		$tutor_info['tstt_ttid_name_1'] = rtrim($tutor_info['tstt_ttid_name_1'],',');
		$tutor_info['tstt_ttid_name_2'] = rtrim($tutor_info['tstt_ttid_name_2'],',');
		$tutor_info['tstt_ttid_name_3'] = rtrim($tutor_info['tstt_ttid_name_3'],',');

		
		if(isset($tutor_info['tst_weekday'])){
			$allweek_cn = array('一','二','三','四','五','六','日');
			$tor_weekday = explode(',', $tutor_info['tst_weekday']);
		
			foreach ($tor_weekday as $key => $value) {
				$tor_week_cn[] = $allweek_cn[$value];
			}
			$fromtime = rtrim($tutor_info['tst_fromtime'],';');		
			$fromtime = explode(';', $fromtime);					
			$totime = rtrim($tutor_info['tst_totime'],';');			
			$totime = explode(';', $totime);						
			$tutor_info['weekday'] ='';								
			if($tor_weekday[0] != ''){

			
				foreach ($fromtime as $key => $value) {
					if(strpos($value, ',')){
						$tor_from[$key] = explode(',', $value);
					}else{
						$tor_from[$key] = $value;
					}
				}

	
				foreach ($totime as $key => $value) {
					if(strpos($value, ',')){
						$to[$key] = explode(',', $value);
					}else{
						$to[$key] = $value;
					}
				}

			
				foreach ($tor_from as $key => $value) {
					$weekdate = '';
					if(is_array($value)){
						foreach ($value as $ke => $val) {
							$weekdate .= substr($val,0,2).':'.substr($val,2,2).' —— '.substr($to[$key][$ke],0,2).':'.substr($to[$key][$ke],2,2).'<br>';
						}
					}else{
						$weekdate .= substr($value,0,2).':'.substr($value,2,2).' —— '.substr($to[$key],0,2).':'.substr($to[$key],2,2);
					}
					$tutor_info['weekday'] .= "<tr><td style='padding:6px 6px;' >星期".$tor_week_cn[$key]."</td><td style='padding:6px 6px;' >".$weekdate."</td></tr>";
				}
				
			}else{
				$tutor_info['weekday'] = "<tr ><td colspan='2' style='padding:6px 6px;'>未填寫</td></tr>";
			}
		}


		$padding = 17;
		
		if(count($stu_weekday) > count($tor_weekday)){
			$gap = (count($stu_weekday)-count($tor_weekday))*17;
			$everyAdd = $gap/count($tor_weekday)+6;
			$tutor_info['weekday'] = str_replace("padding:6px", "padding:".$everyAdd."px", $tutor_info['weekday']);
		}
	
		if(count($tor_weekday) > count($stu_weekday)){
			$gap = (count($tor_weekday)-count($stu_weekday))*17;
			$everyAdd = $gap/count($stu_weekday)+6;
			$stu_info['weekday'] = str_replace("padding:6px", "padding:".$everyAdd."px", $stu_info['weekday']);
		}


		
		$tsl_locid = array_unique(explode(',', $tutor_info['tsl_locid']));
		foreach ($tsl_locid as $key => $value) {
			$tutor_info['tsl_locid'] = $this->location->get_one("loc_id = ".$value)['loc_name'].',';
		}
		$tutor_info['tsl_locid'] = rtrim($tutor_info['tsl_locid'],',');

		return array(
			'tutor_info' => $tutor_info,
			'stu_info' => $stu_info,
			'pair' => $pair
		);
	}









	
	function structurePairingData($data,$emailtype=1){

				$tutor_email = $data['tutor_info']['email'];
				$stu_email = $data['stu_info']['email'];
		if($data['pair']['pair_state'] != 1){		

				$data['tutor_info']['email'] = "******";
				$data['tutor_info']['phone'] = "******";
				$data['tutor_info']['mobile'] = "******";
				$data['tutor_info']['tutor_email'] = "******";
				$data['tutor_info']['tutor_mobile'] = "******";
			
				$data['stu_info']['email'] = "******";
				$data['stu_info']['phone'] = "******";
				$data['stu_info']['mobile'] = "******";
				$data['stu_info']['student_mobile'] = "******";
				$data['stu_info']['student_email'] = "******";
		}

		$type = 0;

		$data['tutor_info']['link'] = APP_PATH."index.php?m=teachers&c=index&a=tutor_detail&userid=".$data['tutor_info']['userid']."&tutor_id=".$data['tutor_info']['tm_tutorid'];		
		$data['stu_info']['link'] = APP_PATH."index.php?m=tparents&c=index&a=stu_detail&stuid=".$data['stu_info']['userid']."&tran_id=".$data['stu_info']['st_id'];	

		
		if($data['pair']['pair_sponsor'] == 1){							
			$data['tutor_info']['pair_sponsor'] = "發起匹配";
			switch ($data['pair']['pair_state']) {
				case '0':											
					$data['stu_info']['pair_sponsor'] = "等待匹配";
					$data['pair']['state'] = '等待家長配對';			
					break;
				case '1':					       					 	
				$data['stu_info']['pair_sponsor'] = "同意匹配";
				$data['pair']['state'] = '成功配對';					
					break;
				case '2':																
				$data['tutor_info']['pair_sponsor'] = "家長拒絕配對";
				$data['tutor_info']['bottomName'] = "點擊查看詳情";
				$data['pair']['state'] = '家長拒絕配對';			
				$type = 1;	
				$data['tutor_info']['link'] = APP_PATH."index.php?m=person&c=tutor_index&a=init";
					break;	
				case '3':											
				if($data['pair']['tutor_pay'] == '0'){
					$data['tutor_info']['pair_sponsor'] = "導師發起配對付款超時";
					$data['pair']['state'] = '導師發起配對付款超時';				
				}else{
					$data['tutor_info']['pair_sponsor'] = "家長超時未同意配對";
					$data['pair']['state'] = '家長超時未同意配對';					
				}
				$data['tutor_info']['bottomName'] = "點擊查看詳情";
				$type = 1;	
				$data['tutor_info']['link'] =  APP_PATH."index.php?m=person&c=tutor_index&a=init";
					break;						
			}



				if($emailtype != 1){
					
					$data['tutor_info']['pair_sponsor'] = "已退款";
					$data['tutor_info']['bottomName'] = "立即匹配新的學生";
					$data['pair']['state'] = '已退款';						
					$type = 1;	
					$data['tutor_info']['link'] = APP_PATH."index.php?m=content&c=index&a=tut_matching&id=".$data['tutor_info']['tm_tutorid']."&yid=".$data['stu_info']['st_id'];
				}


		}else{				

				$data['stu_info']['pair_sponsor'] = "發起匹配";
			switch ($data['pair']['pair_state']) {
				case '0':												
					$data['tutor_info']['pair_sponsor'] = "等待匹配";		
					$data['pair']['state'] = '等待導師配對';		
					break;
				case '1':					        				
				$data['tutor_info']['pair_sponsor'] = "同意匹配";
				$data['pair']['state'] = '成功配對';						
					break;
				case '2':															
				$data['stu_info']['pair_sponsor'] = "導師拒絕配對";
				$data['pair']['state'] = '導師拒絕配對';				
				$data['stu_info']['bottomName'] = "點擊查看詳情";
				$type = 2;		
				$data['stu_info']['link'] = APP_PATH."index.php?m=person&c=index&a=init";
					break;	
				case '3':											
				if($data['pair']['student_pay'] == '0'){
					$data['stu_info']['pair_sponsor'] = "學生發起配對付款超時";
					$data['pair']['state'] = '學生發起配對付款超時';			
				}else{
					$data['stu_info']['pair_sponsor'] = "導師超時未同意配對";
					$data['pair']['state'] = '導師超時未同意配對';				
				}
				$data['stu_info']['bottomName'] = "點擊查看詳情";
				$type = 2;		
				$data['stu_info']['link'] = APP_PATH."index.php?m=person&c=index&a=init";
					break;						
			}

				if($emailtype != 1){
					// echo 123;exit;
					$data['stu_info']['pair_sponsor'] = "已退款";
					$data['stu_info']['bottomName'] = "立即匹配新的導師";
					$data['pair']['state'] = '已退款';					
					$type = 2;	
					$data['stu_info']['link'] = APP_PATH."index.php?m=content&c=index&a=st_matching&id=".$data['stu_info']['st_id']."&yid=".$data['tutor_info']['tm_tutorid'];
				}


		}


		$grade = array('小學','中學','大學','研究生','博士生');

		$data['stu_info']['st_min_grade'] = $grade[$data['stu_info']['st_min_grade']];		


		$data['tutor_info']['tutor_sex'] = ($data['tutor_info']['tutor_sex'] == 'M')?'男':'女';		

		switch ($data['stu_info']['st_req_sex']) {			
			case 'M':
				$data['stu_info']['st_req_sex'] = '男';
				break;
			case 'F':
				$data['stu_info']['st_req_sex'] = '女';
				break;			
			default:
				$data['stu_info']['st_req_sex'] = '無限制';
				break;
		}


		$data['stu_info']['st_req_sex'] = ($data['stu_info']['st_req_sex'] == 'M')?'男':'女';		

		
		
		foreach ($data['stu_info'] as $key => $value) {
			if($value == '' || $value == null){
				$data['stu_info'][$key] = '未填寫';
			}
		}
		foreach ($data['tutor_info'] as $key => $value) {
			if($value == '' || $value == null){
				$data['stu_info'][$key] = '未填寫';
			}
		}

		$data['type'] = $type;

		return array(
			'info'=>$data,	
			'email'=>array(		
				'tutor_email'=>$tutor_email,
				'stu_email'=>$stu_email
				),
					
			);

	}


	public function return_star($id,$type){
		$comments = pc_base::load_model('pair_comment_model');
		if($type == 0){
			$where = array('master_id'=>$id,'tm_matched'=>1);
			$transaction = $this->tran->select($where);
			if(empty($transaction)){
				return '0';exit;
			}else{
			
				foreach ($transaction as $key => $value) {
					$where = array('pair_tutor_tranid'=>$value['tm_tutorid']);
					$pairinfo = $this->pair->get_one($where);
					if(!empty($pairinfo)){
						$pairid[] = $pairinfo['pair_id'];
					}
				}
				$his_star = 0;
				$his_count = 0;
				foreach ($pairid as $key => $value) {
					$where = array('pc_pairid'=>$value,'pc_type'=>2);
					$comment = $comments->get_one($where);
					if(!empty($comment)){
						$his_star += $comment['pc_star'];
						$his_count += 1;
					}
				}
				$total = $his_count*20;
				$res = ceil(($his_star/$total)*100);
				if($res > 0){
					if($res > 80){
						$res = 100;
					}elseif($res > 60){
						$res = 80;
					}elseif($res > 40){
						$res = 60;
					}elseif($res > 20){
						$res = 40;
					}elseif($res > 0){
						$res = 20;
					}else{
						$res = 0;
					}
				}
				return $res;exit;
			}
		}else{
			$stu_tran = pc_base::load_model('student_transaction_model');
			$where = array('st_studentid'=>$id,'st_matched'=>1);
			$transaction = $stu_tran->select($where);
			if(empty($transaction)){
				return '0';exit;
			}else{
		
				foreach ($transaction as $key => $value) {
					$where = array('pair_student_tranid'=>$value['st_id']);
					$pairinfo = $this->pair->get_one($where);
					if(!empty($pairinfo)){
						$pairid[] = $pairinfo['pair_id'];
					}
				}
			
				$his_star = 0;
				$his_count = 0;
				foreach ($pairid as $key => $value) {
					$where = array('pc_pairid'=>$value,'pc_type'=>1);
					$comment = $comments->get_one($where);
					if(!empty($comment)){
						$his_star += $comment['pc_star'];
						$his_count += 1;
					}
				}
				$total = $his_count*20;//总分数
				$res = ceil(($his_star/$total)*100);//计算最后评级 向上取整
				if($res > 0){
					if($res > 80){
						$res = 100;//五星
					}elseif($res > 60){
						$res = 80;//四星
					}elseif($res > 40){
						$res = 60;//三星
					}elseif($res > 20){
						$res = 40;//二星
					}elseif($res > 0){
						$res = 20;//一星
					}else{
						$res = 0;//無星級
					}
				}
				return $res;exit;
			}
		}
	}

	
	public function order_send_email($pairid){
		
        $Pairing = $this->getPairingData($pairid);                 
        $Pairing = $this->structurePairingData($Pairing);          
        pc_base::load_sys_func('mail');                           
        $PairingData = getPairingMailData($Pairing['info']);     
        sendmail($Pairing['email']['tutor_email'],'TutorSeekIng配對信息', $PairingData);      
        sendmail($Pairing['email']['stu_email'],'TutorSeekIng配對信息', $PairingData);        
        sendmail('enquiry@tutorseeking.com','TutorSeekIng配對信息', $PairingData);  
	}
	






}