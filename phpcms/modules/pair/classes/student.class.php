<?php
defined('IN_PHPCMS') or exit('No permission resources.');

class student {
	public function __construct() {
		$this->mem=pc_base::load_model('member_model');
		$this->tor = pc_base::load_model('tutor_master_model');//加载导师信息模块
		$this->std = pc_base::load_model('student_master_model');//加载学生信息模块
		$this->pair = pc_base::load_model('pair_model');
		$this->tran = pc_base::load_model('tutor_transaction_model');
		$this->S_tran = pc_base::load_model('student_transaction_model');
		$this->time = pc_base::load_model('student_sel_time_model');
		pc_base::load_app_class('inte' ,'integration');
	}

	
	public function tutor_public_transaction($studentid,$ttid_1,$ttid_2,$ttid_3,$mannum,$femalenum,$weekclass,$classhour,$tutorfee,$mingrade,$reqsex,$mainlang,$contacttime,$otherreq,$weekday,$fromtime,$totime){
		$this->tran->query('START TRANSACTION');

		$transaction = array('st_studentid'=>$studentid,'st_ttid_1'=>$ttid_1,'st_ttid_2'=>$ttid_2,'st_ttid_3'=>$ttid_3,'st_num_join_m'=>$mannum,'st_num_join_f'=>$femalenum,'st_week_class'=>$weekclass,'st_class_hour'=>$classhour,'st_tutor_fee'=>$tutorfee,'st_min_grade'=>$mingrade,'st_req_sex'=>$reqsex,'st_main_lang'=>$mainlang,'st_contact_time'=>$contacttime,'st_other_req'=>$otherreq);
		$this->tran->insert($transaction);
		$stid = $this->tran->insert_id();

		$studenttime = array('sst_studentid'=>$studentid,'sst_stid'=>$stid,'sst_weekday'=>$weekday,'sst_fromtime'=>$fromtime,'sst_totime'=>$totime);

		if(!$this->time->insert($studenttime)){
			$this->tran->query('ROLLBACK');//回滚
		}else{
			$this->tran->query('COMMIT');//执行事务
		}
	}

	
	public function std_transaction($studentid,$page='1',$limit='10'){
		$studentid = isset($studentid) ? $studentid : showmessage(L('illegal_parameters'), HTTP_REFERER);
		$where = array('st_studentid'=>$studentid);
		$tranlist['data'] = $this->tran->listinfo($where, 'tm_modtime DESC', $page, $limit);//每页10条记录
		$tranlist['page_link'] = $this->tran->pages;//分页
		return $tranlist;
	}

	
	public function std_sponsored_list($studentid,$sponsor,$page='1',$limit='10'){
		$studentid = isset($studentid) ? $studentid : showmessage(L('illegal_parameters'), HTTP_REFERER);
		$sponsor = isset($sponsor) ? $sponsor : showmessage(L('illegal_parameters'), HTTP_REFERER);
		if($sponsor == '3'){
			$where = " pair_studentid = $studentid";
		}else{
			$where = " pair_studentid = $studentid && pair_sponsor = $sponsor";
		}

		$sql = "select * from `v9_pair` as pair
						 left join `v9_tutor_transaction` as tran on pair.pair_tmid = tran.tm_id
						 left join `v9_tutor_master` as master on master.tutor_id = tran.tm_tutorid where".$where;
		$tutorinfo = $this->std->query($sql);
		$pairlist['data'] = $this->std->fetch_array($tutorinfo);
		$pairlist['page_link'] = pages($num = count($pairlist['data']), $page, $limit, '', array(), 10);
		return $pairlist;
	}

	
	public function std_send_request($tmid,$stid,$url,$amount,$points=0){
		$inte=new inte();
		$tmid = isset($tmid) ? $tmid : showmessage(L('illegal_parameters'), HTTP_REFERER);
		$stid = isset($stid) ? $stid : showmessage(L('illegal_parameters'), HTTP_REFERER);
		$where = " `pair_tutor_tranid` = '$tmid' && `pair_student_tranid` = '$stid' && `pair_state` = '0'";
		$this->pair_fail = pc_base::load_model('pair_fail_model');		   	
		

		$is_exist = $this->pair->get_one($where);
		if(!$is_exist){													     
	
			$tutor_is_pair = $this->pair->get_one("`pair_tutor_tranid` = '$tmid'");
			$student_is_pair = $this->pair->get_one("`pair_student_tranid` = '$stid'");
			if(!empty($tutor_is_pair) || !empty($student_is_pair)){
				$return['code'] = 0;
            	$return['msg'] = '系統繁忙，請刷新重試!';
            	return $return;exit;
			}

			$this->tran->update("`tm_state` = '0'","`tm_tutorid` = '$tmid'");
			$this->S_tran->update("`st_state` = '0'","`st_id` = '$stid'");   
			$data = array(
				'pair_tutor_tranid'=>$tmid,
				'pair_student_tranid'=>$stid,
				'pair_sponsor'=>2,
				'student_use_points'=>$points,
			);
			$this->pair->insert($data);
			if($this->pair->insert_id()){
				$pairid = $this->pair->insert_id();
				$returnPoint = pc_base::load_app_class('integralOperation','pay');
				$userData= $returnPoint->GetUserData($pairid,'std_agree_pay');

				$HelpCommon = pc_base::load_app_class('helpcommon','pair');	
		        $results = $HelpCommon->Payment_Security_Mechanism($amount,$points,$userData['userid']);
		        if($results['code'] != 0){
		        	$return['code'] = 0;
		            $return['msg'] = $results['msg'];
		            return $return;exit;
		        }

				$returnPoint->integralOperation($userData['userid'],$results['points'],$type='-');
				$provider=$this->mem->get_one("`userid` =". $userData['userid']);
				if(!empty($results['points'])){
				
                $inte->write_inte_log('','','11',$provider['personid'],$provider['username'],'','-'.$results['points']);
				}
				if($results['amount'] == 0){				
					$this->pair->update("`student_pay` = '1'","`pair_id` = $pairid");
					
					pc_base::load_app_class('tutor','pair');
	                $tutor = new tutor();
	                $Pairing = $tutor->order_send_email($pairid);
					$return['code'] = 1;
					$return['msg'] = '成功發起配對';
					return $return;exit;
				}else{
					
					$paypal = pc_base::load_app_class('Paypal','pay');
					$order = array(
						'item_name'=>'std_public_pay',
						'amount'=>$results['amount'],
						'currency_code'=>'HKD',
						'invoice'=>'P'.$pairid,
						'no_note'=>'学生发起配对支付',
						'return_url'=>$url,
					);
					return $paypal->paypal($order);
				}
			}
		}else{ 
			if($is_exist['tutor_pay'] == '1' || $is_exist['student_pay'] == '1'){
				$return['code'] = 0;
	            $return['msg'] = '系統繁忙，請刷新重試!';
	            return $return;exit;
			}

			$returnPoint = pc_base::load_app_class('integralOperation','pay');
			$userData= $returnPoint->GetUserData($is_exist[pair_id],'std_agree_pay');

			$HelpCommon = pc_base::load_app_class('helpcommon','pair');
	        $results = $HelpCommon->Payment_Security_Mechanism($amount,$points,$userData['userid']);
	       
	        if($results['code'] != 0){
	        	$return['code'] = 0;
	            $return['msg'] = $results['msg'];
	            return $return;exit;
	        }

			if($results['amount'] == 0){
				if(!empty($points)){
					$returnPoint->integralOperation($userData['userid'],$points,$type='-');
					$provider=$this->mem->get_one("`userid` =". $userData['userid']);
				
	                $inte->write_inte_log('','','11',$provider['personid'],$provider['username'],'','-'.$points);
                }
			
				$this->pair->update(array('student_pay'=>'1','student_use_points'=>$points),"`pair_id` = $is_exist[pair_id]");

			
				pc_base::load_app_class('tutor','pair');		
                $tutor = new tutor();
                $Pairing = $tutor->order_send_email($is_exist['pair_id']);
				$return['code'] = 1;
				$return['msg'] = '成功發起配對';
				return $return;exit;
			}else{

			
				$this->pair->update("`student_use_points` = $results[points]","`pair_id` = $is_exist[pair_id]");
				if(!empty($results['points'])){
					$returnPoint->integralOperation($userData['userid'],$results['points'],$type='-');
					$provider=$this->mem->get_one("`userid` =". $userData['userid']);
				
	                $inte->write_inte_log('','','11',$provider['personid'],$provider['username'],'','-'.$results['points']);
                }
				$paypal = pc_base::load_app_class('Paypal','pay');
				$order = array(
					'item_name'=>'std_public_pay',
					'amount'=>$results['amount'],
					'currency_code'=>'HKD',
					'invoice'=>'P'.$is_exist['pair_id'],
					'no_note'=>'学生发起配对支付',
					'return_url'=>$url,
				);
				return $paypal->paypal($order);
			}
		}
	}

	
	public function std_respond($tmid,$stid,$respond,$url='',$amount=0,$points=0){
		$inte=new inte();
		$tmid = isset($tmid) ? $tmid : showmessage(L('illegal_parameters'), HTTP_REFERER);
		$stid = isset($stid) ? $stid : showmessage(L('illegal_parameters'), HTTP_REFERER);
		$respond = isset($respond) ? $respond : showmessage(L('illegal_parameters'), HTTP_REFERER);

		$where = " `pair_tutor_tranid` = '$tmid' && `pair_student_tranid` = '$stid'";
		$pairinfo = $this->pair->get_one($where);
		if(!empty($pairinfo)){
			if($respond == 'agree'){
				$returnPoint = pc_base::load_app_class('integralOperation','pay');
				$userData= $returnPoint->GetUserData($pairinfo['pair_id'],'std_agree_pay');

				$HelpCommon = pc_base::load_app_class('helpcommon','pair');
		        $results = $HelpCommon->Payment_Security_Mechanism($amount,$points,$userData['userid']);
		        if($results['code'] != 0){
		        	$return['code'] = 0;
		            $return['msg'] = $results['msg'];
		            return $return;exit;
		        }

		        $this->pair->update("`student_use_points` = $results[points]","`pair_id` = $pairinfo[pair_id]");
		        if($results['points'] != 0){
		        	$returnPoint->integralOperation($userData['userid'],$results['points'],$type='-');
		        	$provider=$this->mem->get_one("`userid` =". $userData['userid']);
					
	                $inte->write_inte_log('','','11',$provider['personid'],$provider['username'],'','-'.$results['points']);
		        } 

		        if($results['amount'] == 0){ 
		        	
					$pair_data = array('student_pay'=>'1','pair_state'=>'1');
					$this->pair->update($pair_data,"`pair_id` = $pairinfo[pair_id]");
					$this->tran->update("`tm_matched` = '1'","`tm_tutorid` = '$tmid'");
					$this->S_tran->update("`st_matched` = '1'","`st_id` = '$stid'");
		        
					pc_base::load_app_class('tutor','pair');
	                $tutor = new tutor();
	                $Pairing = $tutor->order_send_email($pairinfo['pair_id']);
					$return['code'] = 1;
					$return['msg'] = '配對成功';
					
					$HelpCommon->activityReward($pairinfo['pair_id']);
					return $return;exit;
		        }else{
				
					$paypal = pc_base::load_app_class('Paypal','pay');
					$order = array(
						'item_name'=>'std_agree_pay',
						'amount'=>$results['amount'],
						'currency_code'=>'HKD',
						'invoice'=>'P'.$pairinfo['pair_id'],
						'no_note'=>'学生同意配对支付',
						'return_url'=>$url,
					);
					$return['code'] = 'agree_success';
					$return['msg'] = $paypal->paypal($order);
					return $return;exit;
		        }

				
			}else{
			
				$Disagree = pc_base::load_model('pair_fail_model');
				$tutorModel = pc_base::load_model('tutor_transaction_model');
				$pairinfo['pair_state'] = 2;
				$tor_tran['tm_state'] = '1';
				if($Disagree->insert($pairinfo)){
					$where = "`pair_id` = $pairinfo[pair_id]";
					$this->pair->update($pairinfo,$where);
					$tutorModel->update($tor_tran,"`tm_tutorid` = '$pairinfo[pair_tutor_tranid]'");
		
					pc_base::load_app_class('tutor','pair');					
					$tutor = new tutor();
					$Pairing = $tutor->getPairingData($pairinfo['pair_id']);	
					$Pairing = $tutor->structurePairingData($Pairing);			
					
					pc_base::load_sys_func('mail');								
					$PairingData = getPairingMailData($Pairing['info']);		
					sendmail($Pairing['email']['tutor_email'],'TutorSeekIng配對信息', $PairingData);
					sendmail('enquiry@tutorseeking.com','TutorSeekIng配對信息', $PairingData);  
					if($this->pair->delete($where)){
					
						$memberModel = pc_base::load_model('member_model');
						$memberModel->update("`freepair_time`=`freepair_time`+1","userid = ".$Pairing['info']['tutor_info']['tutor_userid']);
						$return = array('code'=>'disagree_success','msg'=>'拒絕成功');
					}
				}
			}
			return $return;exit;
		}else{
			$return = array('code'=>'illegal_request','msg'=>'非法操作');
			return $return;exit;
		}
	}


	
	public function getSuitedStudent($type='',$item='',$level='',$place='',$classtime='',$hourlyfee='',$miniEduLevel='',$gender='',$classlang='',$like_collage='',$page='0',$limit='10',$order='DESC',$personid='',$week='',$maxpage='10'){
		
		$where = " st_matched = '0' and role_verify='1'" ;
	
		if(!empty($personid)){	$where .= " and personid = '$personid'";	}
		
		if(!empty($type)){	$where .= " and tran.st_ttid_1 in ($type)";	}
		if(!empty($item)){	$where .= " and tran.st_ttid_2 in ($item)";	}

		
		if(!empty($place)){	$where .= " and st_area in ($place)"; }

		
		if(!empty($hourlyfee)){	

			if(is_array($hourlyfee)){
				$where .= " and st_tutor_fee >= $hourlyfee[0] AND st_tutor_fee <= $hourlyfee[1]";
			}else{
				$where .= " and st_tutor_fee >= $hourlyfee";
			}			
		}

	
		if(!empty($miniEduLevel)){	$where .= " and st_min_grade <= $miniEduLevel" ;}

		
		if(!empty($gender)){ $where .= " and st_req_sex in ('$gender','A')"; }

	
		if(!empty($classlang)){ $where .= " and st_main_lang in ('$classlang','')"; }

	
		if(!empty($like_collage)){	$where .= " and like_collage in ('$like_collage','')";	}
		$sql = "select * from `v9_student_transaction` as tran
						 left join `v9_student_master` as master on master.student_id = tran.st_studentid 
						 left join `v9_member` as member on member.userid = master.student_userid 
						 left join `v9_student_sel_time` as time on time.sst_stid = tran.st_id
						 left join `v9_sys_tutor_type` as type on type.tt_id = tran.st_ttid_2
						 left join `v9_sys_location` as loc on loc.loc_id = tran.st_area
						 where".$where.' order by st_tutor_fee ASC,st_min_grade '.$order;

		$studentsql = $this->std->query($sql);
		$studentList = $this->std->fetch_array($studentsql);

	
		if(!empty($level)){
			$this->ttype = pc_base::load_model('sys_tutor_type_model');
			$tutor_level = explode(',', $level);
			$suit_level = array();
			foreach ($tutor_level as $key => $value) {
				$parentid = $this->ttype->get_one("`tt_id` = $value");
				$all_level = $this->ttype->select("`tt_parentid` = $parentid[tt_parentid]");
				foreach ($all_level as $ke => $val) {
					if($val['tt_seq'] <= $parentid['tt_seq']){
						$suit_level[] = $val['tt_id'];
					}
				}
			}

			foreach ($studentList as $key => $value) {
				if(!in_array($value['st_ttid_3'],$suit_level)){
					unset($studentList[$key]);
				}
			}
		}

		
		if($week != ''){
			foreach ($studentList as $key => $value) {
				$std_week = explode(',', $value['sst_weekday']);
				$tutor_week = explode(',', $week);
				if($std_week[0] != ''){
					$intersection = array_intersect($std_week, $tutor_week);
					if(empty($intersection)){
						unset($studentList[$key]);
					}else{
						$studentList[$key]['intersect_time'] = count($intersection);
					}
				}
			}
		}


		if(!empty($page)){
			$count = count($studentList);
			$limit = !empty($limit) ? $limit : 10;
			$start = ($page-1)*$limit;
			$studentList = array_slice($studentList, $start,$limit);
		}

		$transaction['data'] = $studentList;
		$transaction['page_link'] = pages($num = $count, $page, $limit, '', array(), $maxpage);
		return $transaction;
	}

	
	public function std_public_comment($pairid,$tutorid,$star,$content){
		$this->pc = pc_base::load_model('pair_comment_model');
		$data = array(
				'pc_pairid'=>$pairid,
				'pc_tutorid'=>$tutorid,
				'pc_studentid'=>$_SESSION['uid'],
				'pc_type'=>'2',
				'pc_star'=>$star,
				'pc_content'=>$content,
			);
		if($this->pc->insert($data)){
			showmessage(L('operation_success'),HTTP_REFERER);
		}else{
			showmessage(L('operation_failure'),HTTP_REFERER);
		}
	}



}