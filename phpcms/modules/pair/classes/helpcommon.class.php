<?php
defined('IN_PHPCMS') or exit('No permission resources.');

class helpcommon {
	public function __construct() {

	}


	
	public function create_location($place){

		$sysLocModel = pc_base::load_model('sys_location_model');
		$return = '';

		if(!strpos($place,',')){
			return $return = $sysLocModel->get_one("loc_id = $place")['loc_name'];exit;
		}
		$res['city'] = '';
		$res['area'] = '';
		$allCity = $sysLocModel->select("`loc_level` = 2");		 
		
		foreach ($allCity as $key => $value) {
			$place = explode(',',$place);
			if(in_array($value['loc_id'],$place)){
				$city_area = $sysLocModel->select("`loc_parentid` = $value[loc_id]",'loc_id,loc_parentid');

				foreach ($city_area as $ke => $val) {
					$all_area[] = $val['loc_id'];
				}
				foreach ($place as $k => $v) {
					if(in_array($v,$all_area)){
						unset($place[$k]);
					}
				}
				foreach ($place as $kk => $vv) {
					if($vv == $value['loc_id']){
						unset($place[$kk]);
					}
				}
				$res['city'] .= '全'.$value[loc_name].',';
				$place = implode($place, ',');
				$place = str_replace(','.$value[loc_id], "", $place);
			}else{
				$place = implode($place, ',');
			}
		}
		if(!empty($place)){							
			$place = explode(',', $place);			
			foreach ($place as $key => $value) {
				
				if(!empty($value)){
					$area = $sysLocModel->get_one("`loc_id` = $value");
					if(!empty($area)){
						$res['area'] .= $area['loc_name'].',';
					}
				}
			}
			$res['area'] = rtrim($res['area'],',');
			return $return = $res['city'].$res['area'];exit;
		}else{
			return $return = rtrim($res['city'],',');exit;
		}
	}

	
	public function create_subject($subject){
		if($subject!=""){
			$sub=explode(",", $subject);
	        foreach ($sub as  $sv) {
	        	//若科目ID不为空
	        	if(!empty($sv)){
	        		$sql="select tt_name from v9_sys_tutor_type where tt_id=$sv";
		            $this->sudb= pc_base::load_model('sys_tutor_type_model');
		            $this->sudb->query($sql);
		            $sublist=$this->sudb->fetch_array();
		            $sub_arr[]=$sublist[0]['tt_name'];
	        	}
	        }
	        $subname = rtrim(implode($sub_arr, ','),',');
        }else{
        	$subname ="";
        }
        return $subname;
	}
	
	public function Payment_Security_Mechanism($amount,$points,$userid){
		$Extend_Setting_Model = pc_base::load_model('extend_setting_model');
		$Service_Charge = $Extend_Setting_Model->get_one("`key` = 'Service_Charge'")['data'];
		if(intval($amount) == intval($Service_Charge)){	
			if($points == 0){
				$res['code'] = 0;
				$res['points'] = $points;
				$res['amount'] = $amount;
			}else{
				$res['code'] = 1;
				$res['msg'] = '積分有誤';
			}
		}else{											
			$real_points = intval($Service_Charge)-intval($amount);		
			if($real_points != $points){				
				$res['code'] = 1;
				$res['msg'] = '積分有誤';
			}else{										
				$Member_Model = pc_base::load_model('member_model');
				$integral = $Member_Model->get_one("`userid` = $userid")['integral'];
				if($integral >= $points){
					$res['code'] = 0;
					$res['points'] = $points;
					$res['amount'] = $Service_Charge-$points;
				}else{
					$res['code'] = 1;
					$res['msg'] = '用戶積分不足';
				}
			}
		}
		return $res;
	}

	
	public function activityReward($pairid){
		$pairModel    = pc_base::load_model('pair_model');				  
		$torTranModel = pc_base::load_model('tutor_transaction_model');	 
		$stuTranModel = pc_base::load_model('student_transaction_model');
		$torMasModel  = pc_base::load_model('tutor_master_model');     
		$stuMasModel  = pc_base::load_model('student_master_model');   
		$memModel 	  = pc_base::load_model('member_model');    	  
		pc_base::load_app_class('inte' ,'integration');
		$inte=new inte();
		$pairInfo = $pairModel->get_one("pair_id = $pairid");
		if(!empty($pairInfo) && $pairInfo['pair_state'] == '1'){
		
			$torTran = $torTranModel->get_one("tm_tutorid = '$pairInfo[pair_tutor_tranid]'",'master_id');
			$torUserid = $torMasModel->get_one("tutor_id = $torTran[master_id]",'tutor_userid')['tutor_userid'];
			$torMemInfo = $memModel->get_one("userid = $torUserid"); 
		
			if(!empty($torMemInfo['Referee'])){
				
				if(empty($torMemInfo['first_success'])){
				
					if($torMemInfo['Referee']==$torMemInfo['phone']){
						$userInfo = $memModel->select("phone = $torMemInfo[Referee],role_verify=1",'userid,role_verify,username,personid');
					}else{
						$userInfo = $memModel->select("phone = $torMemInfo[Referee]",'userid,role_verify,username,personid');
						if(count($userInfo) > 1){
							foreach ($userInfo as $key => $value) {
								if($value['role_verify'] == '1'){
									unset($userInfo[$key]);
								}
							}
						}
					}
					foreach ($userInfo as $key => $value) {

						$spead_inte=$inte->get_inte_setting(3);
						$memModel->update("`integral`=`integral`+".$spead_inte,"userid = ".$value['userid']); 
						if(!empty($spead_inte)){
		                $inte->write_inte_log($value['personid'],$value['username'],'3',$torMemInfo['personid'],$torMemInfo['username'],'',$spead_inte);
		                }
					}
					$memModel->update("first_success = '1'","userid = ".$torMemInfo['userid']);
				}
			}

			
			$stuTran = $stuTranModel->get_one("st_id = '$pairInfo[pair_student_tranid]'",'st_studentid');
			$stuUserid = $stuMasModel->get_one("student_id = $stuTran[st_studentid]",'student_userid')['student_userid'];
			$stuMemInfo = $memModel->get_one("userid = $stuUserid"); 
			
			if(!empty($torMemInfo['Referee'])){
				
				if(empty($stuMemInfo['first_success'])){
				
					if($torMemInfo['Referee']==$torMemInfo['phone']){
						$userInfo = $memModel->select("phone = $torMemInfo[Referee],role_verify=0",'userid,role_verify,username,personid');
					}else{
						$userInfo = $memModel->select("phone = $stuMemInfo[Referee]",'userid,role_verify,username,personid');
						if(count($userInfo) > 1){
							foreach ($userInfo as $key => $value) {
								if($value['role_verify'] == '1'){
									unset($userInfo[$key]);
								}
							}
						}
					}
					foreach ($userInfo as $key => $value) {
						$spead_inte=$inte->get_inte_setting(3);	
						$memModel->update("`integral`=`integral`+".$spead_inte,"userid = ".$value['userid']); 
						if(!empty($spead_inte)){
					
		                $inte->write_inte_log($value['personid'],$value['username'],'3',$stuMemInfo['personid'],$stuMemInfo['username'],'',$spead_inte);
		                }
					}
					$memModel->update("first_success = '1'","userid = ".$stuMemInfo['userid']);
				}
			}
		}
	}



}