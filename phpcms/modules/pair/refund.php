<?php


defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_app_class('admin', 'admin', 0);
pc_base::load_sys_class('form', '', 0);
pc_base::load_app_class('inte' ,'integration');
class refund extends admin{

	function __construct() {
		$this->pair = pc_base::load_model('pair_model');			
		$this->pair_fail = pc_base::load_model('pair_fail_model');	
		$this->tor = pc_base::load_model('tutor_master_model');		
		$this->std = pc_base::load_model('student_master_model');	
		$this->paypal = pc_base::load_model('paypal_return_model');	
	}


	public function refund(){
		$pairid = isset($_GET['pairid']) ? $_GET['pairid'] : showmessage(L('illegal_parameters'), HTTP_REFERER);
		$state = isset($_GET['state']) ? $_GET['state'] : showmessage(L('illegal_parameters'), HTTP_REFERER);

		if(in_array($state, array('0','1'))){
			$pair = $this->pair->fetch_array($this->pair->query("select * from `v9_pair` as pair 
								left join `v9_tutor_transaction` as tt on tt.tm_tutorid = pair.pair_tutor_tranid 
								left join `v9_student_transaction` as st on st.st_id = pair.pair_student_tranid 
								where pair_id = $pairid"))[0];
		}else{
			$pair = $this->pair->fetch_array($this->pair_fail->query("select * from `v9_pair_fail` as pair 
								left join `v9_tutor_transaction` as tt on tt.tm_tutorid = pair.pair_tutor_tranid 
								left join `v9_student_transaction` as st on st.st_id = pair.pair_student_tranid 
								where pair_id = $pairid"))[0];
		}
		
		$tutorid = $pair['master_id'];
		$studentid = $pair['st_studentid'];
		$tutor = $this->tor->get_one("tutor_id = $tutorid");
		$student = $this->std->get_one("student_id = $studentid");

		$torpay = $this->paypal->get_one("`item_name` like '%tor%' and `order_sn` = $pairid");			
		$stdpay = $this->paypal->get_one("`item_name` like '%std%' and `order_sn` = $pairid");			

		$torpayinfo = json_decode($torpay['json_data'],true);	
		$stdpayinfo = json_decode($stdpay['json_data'],true);	

		
		if(!empty($pair['tor_freepair_id']) && in_array($pair['tutor_pay'], array('2','3'))){
			$tutor_pair = $this->pair_fail->get_one("`pair_id` = $pair[tor_freepair_id]");				
			$pair['tutor_use_points'] = $tutor_pair['tutor_use_points'];								
			$torpay = $this->paypal->get_one("item_name LIKE '%tor%' and order_sn = $pairid");			
		}

	
		if(!empty($pair['std_freepair_id']) && in_array($pair['student_pay'], array('2','3'))){
			$student_pair = $this->pair_fail->get_one("`pair_id` = $pair[std_freepair_id]");			
			$pair['student_use_points'] = $student_pair['student_use_points'];							
			$torpay = $this->paypal->get_one("item_name LIKE '%std%' and order_sn = $pairid");			
		}

		include $this->admin_tpl('refund');
	}


	public function refund_operate(){
		$inte=new inte();
		$memberModel = pc_base::load_model('member_model');		
		$Extend_Setting_Model = pc_base::load_model('extend_setting_model');
		$Service_Charge = $Extend_Setting_Model->get_one("`key` = 'Service_Charge'")['data'];
		if(!empty($_POST)){
			$pairinfo = $this->pair_fail->get_one("pair_id = $_POST[id]");
	
			if($_POST['item'] == 'std_refund'){
				$data = array('student_pay'=>'3');		
				$matched = array('st_matched'=>'0');	
				if($this->pair_fail->update($data,"`pair_id` = $_POST[id]")){
					$stu_tran = pc_base::load_model('student_transaction_model');
					if($stu_tran->update($matched,"st_id = '$pairinfo[pair_student_tranid]'")){
						pc_base::load_app_class('tutor','pair');					
						$tutor = new tutor();
						$Pairing = $tutor->getPairingData($_POST['id'],2);	    	
						$Pairing = $tutor->structurePairingData($Pairing,'2');		
						pc_base::load_sys_func('mail');								
						$PairingData = getPairingMailData($Pairing['info']);		

						
						if(!empty($pairinfo['std_freepair_id'])){
							$pairinfo['student_use_points'] = $this->pair_fail->get_one("`pair_id` = $pairinfo[std_freepair_id]")['student_use_points'];
						}
					
						if(!empty($pairinfo['student_use_points'])){
							$memberModel->update("`integral`=`integral`+$pairinfo[student_use_points]","userid = ".$Pairing['info']['stu_info']['student_userid']);
							$owner=$memberModel->get_one("`userid` =".$Pairing['info']['stu_info']['student_userid']);
					
							$inte->write_inte_log($owner['personid'],$owner['username'],'12','','','',$pairinfo['student_use_points']);
						}

					
						sendmail($Pairing['email']['stu_email'],'TutorSeekIng配對信息', $PairingData);
						sendmail('enquiry@tutorseeking.com','TutorSeekIng配對信息', $PairingData);  
					}
					exit('success');
				}else{
					exit('fail');
				}

	
			}elseif($_POST['item'] == 'tor_refund'){
				$data = array('tutor_pay'=>'3');
				$matched = array('tm_matched'=>'0');
				if($this->pair_fail->update($data,"`pair_id` = $_POST[id]")){
					$tor_tran = pc_base::load_model('tutor_transaction_model');
					if($tor_tran->update($matched,"tm_tutorid = '$pairinfo[pair_tutor_tranid]'")){
						pc_base::load_app_class('tutor','pair');					
						$tutor = new tutor();
						$Pairing = $tutor->getPairingData($_POST['id'],2);	    	
						$Pairing = $tutor->structurePairingData($Pairing,'2');		
						pc_base::load_sys_func('mail');							
						$PairingData = getPairingMailData($Pairing['info']);		

					
						if(!empty($pairinfo['tor_freepair_id'])){
							$pairinfo['tutor_use_points'] = $this->pair_fail->get_one("`pair_id` = $pairinfo[tor_freepair_id]")['tutor_use_points'];
						}
				
						if(!empty($pairinfo['tutor_use_points'])){
							$memberModel->update("`integral`=`integral`+$pairinfo[tutor_use_points]","userid = ".$Pairing['info']['tutor_info']['tutor_userid']);
							$owner=$memberModel->get_one("`userid` =".$Pairing['info']['tutor_info']['tutor_userid']);
				
							$inte->write_inte_log($owner['personid'],$owner['username'],'12','','','',$pairinfo['tutor_use_points']);
						}


						sendmail($Pairing['email']['tutor_email'],'TutorSeekIng配對信息', $PairingData);
						sendmail('enquiry@tutorseeking.com','TutorSeekIng配對信息', $PairingData); 
					}
					exit('success');
				}else{
					exit('fail');
				}
			}else{
				exit('fail');
			}
		}else{
			exit('fail');
		}
	}

}
?>