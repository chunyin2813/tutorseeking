<?php


defined('IN_PHPCMS') or exit('No permission resources.');

pc_base::load_app_class('admin', 'admin', 0);
pc_base::load_sys_class('form', '', 0);

class pair_admin extends admin {
	
	private $db, $verify_db;
	
	function __construct() {
		$this->db = pc_base::load_model('pair_model');
		$this->ttype = pc_base::load_model('sys_tutor_type_model');
		$this->grade = pc_base::load_model('grade_model');
		$this->collage = pc_base::load_model('collage_model');
	}

	
	
	function pairlist() {
		$where = 'where 1=1';
		if(isset($_GET['tor'])){ $where .= ' and tutor_pay = '."'".$_GET['tor']."'";}
		if(isset($_GET['std'])){ $where .= ' and student_pay = '."'".$_GET['std']."'";}
		if(isset($_GET['pairtype'])){ $where .= ' and pair_state = '."'".$_GET['pairtype']."'";}
		if(isset($_GET['keyword'])){
			if($_GET['type'] == '1'){
				$where = 'where tutor_sname_chi = '."'".$_GET['keyword']."'";
			}elseif($_GET['type'] == '2'){
				$where = 'where tutor_fname_chi = '."'".$_GET['keyword']."'";
			}elseif($_GET['type'] == '3'){
				$where = 'where student_name = '."'".$_GET['keyword']."'";
			}elseif($_GET['type'] == '4'){
				$where = 'where st_id = '."'".$_GET['keyword']."'";
			}elseif($_GET['type'] == '5'){
				$where = 'where tm_tutorid = '."'".$_GET['keyword']."'";
			}
		}
		$page = isset($_GET['page']) ? intval($_GET['page']) : 1;//当前页码
		$limit = 20; //每页显示条数
		$start = ($page-1)*$limit;
		if(in_array($_GET['pairtype'], array('0','1'))){
			$sql = "select * from `v9_pair` as pair 
				left join `v9_student_transaction` as St on St.st_id = pair.pair_student_tranid 
				left join `v9_tutor_transaction` as Tt on Tt.tm_tutorid = pair.pair_tutor_tranid
				left join `v9_student_master` as Std on Std.student_id = St.st_studentid
				left join `v9_tutor_master` as Tor on Tor.tutor_id = Tt.master_id $where 
				order by pair_modtime desc";
			$typeinfo = $this->db->query($sql);
			$result = $this->db->fetch_array($typeinfo);
		}elseif(in_array($_GET['pairtype'], array('2','3'))){
			$sql = "select * from `v9_pair_fail` as pair 
				left join `v9_student_transaction` as St on St.st_id = pair.pair_student_tranid 
				left join `v9_tutor_transaction` as Tt on Tt.tm_tutorid = pair.pair_tutor_tranid
				left join `v9_student_master` as Std on Std.student_id = St.st_studentid
				left join `v9_tutor_master` as Tor on Tor.tutor_id = Tt.master_id $where 
				order by pair_modtime desc";
			$typeinfo = $this->db->query($sql);
			$result = $this->db->fetch_array($typeinfo);
		}else{
			$sql1 = "select * from `v9_pair` as pair 
				left join `v9_student_transaction` as St on St.st_id = pair.pair_student_tranid 
				left join `v9_tutor_transaction` as Tt on Tt.tm_tutorid = pair.pair_tutor_tranid
				left join `v9_student_master` as Std on Std.student_id = St.st_studentid
				left join `v9_tutor_master` as Tor on Tor.tutor_id = Tt.master_id $where 
				order by pair_modtime desc";
			$sql2 = "select * from `v9_pair_fail` as pair 
				left join `v9_student_transaction` as St on St.st_id = pair.pair_student_tranid 
				left join `v9_tutor_transaction` as Tt on Tt.tm_tutorid = pair.pair_tutor_tranid
				left join `v9_student_master` as Std on Std.student_id = St.st_studentid
				left join `v9_tutor_master` as Tor on Tor.tutor_id = Tt.master_id $where 
				order by pair_modtime desc";
			$typeinfo1 = $this->db->query($sql1);
			$result1 = $this->db->fetch_array($typeinfo1);
			$typeinfo2 = $this->db->query($sql2);
			$result2 = $this->db->fetch_array($typeinfo2);
			$result = array_merge($result1,$result2);
		}
		$results = arraySequence($result, 'pair_modtime', $sort = 'SORT_DESC');
		$pairlist['page_link'] = pages(count($result), $page, $limit, '', array(), 10); //分页
		$pairlist['data'] = array_slice($results, $start, $limit);
		
 		include $this->admin_tpl('pair_list');
	}
		
	
	function tutor_traninfo() {
		$tranid = isset($_GET['tranid']) ? $_GET['tranid'] : showmessage(L('illegal_parameters'), HTTP_REFERER);
		$traninfo = $this->db->fetch_array($this->db->query(
			"select * from `v9_tutor_transaction` as tran 
			left join `v9_tutor_master` as tor on tran.master_id = tor.tutor_id
			left join `v9_tutor_sel_time` as time on tran.tm_id = time.tst_tmid 
			left join `v9_tutor_sel_location` as loc on tran.tm_id = loc.tsl_tmid 
			left join `v9_tutor_sel_tutor_type` as type on tran.tm_id = type.tstt_tmid 
			left join `v9_sys_location` as loca on loca.loc_id = loc.tsl_locid
			where tm_id = $tranid order by tm_modtime desc LIMIT 1"
		))[0];

		if(empty($traninfo)) {
			showmessage(L('要求表不存在').L('or').L('no_permission'), HTTP_REFERER);
		}

		//大學
		$collage = $traninfo['tutor_college'];
		$traninfo['tutor_college'] = $this->collage->get_one("id = $collage")['c_name'];

		//學歷
		$grade = $traninfo['tutor_high_grade'];
		$traninfo['tutor_high_grade'] = $this->grade->get_one("grade_id = $grade")['grade_name'];
		
		$weekday = array('星期一','星期二','星期三','星期四','星期五','星期六','星期天');
		$weekarray = explode(',', $traninfo['tst_weekday']);//字符串转数组
		$traninfo['tst_weekday'] = '';
		foreach ($weekarray as $key => $value) {
			$traninfo['tst_weekday'] .= $weekday[$value].','; 
		}
		$traninfo['tst_weekday'] = rtrim($traninfo['tst_weekday'],',');

		$ttid_1 = $traninfo['tstt_ttid_1'];
		$traninfo['tstt_ttid_1'] = $this->ttype->get_one("tt_id in ('$ttid_1')")['tt_name'];
		$ttid_2 = $traninfo['tstt_ttid_2'];
		$traninfo['tstt_ttid_2'] = $this->ttype->get_one("tt_id in ('$ttid_2')")['tt_name'];
		$ttid_3 = $traninfo['tstt_ttid_3'];
		$traninfo['tstt_ttid_3'] = $this->ttype->get_one("tt_id in ('$ttid_3')")['tt_name'];

		include $this->admin_tpl('tor_transaction');
	}


	function student_traninfo() {
		$tranid = isset($_GET['tranid']) ? $_GET['tranid'] : showmessage(L('illegal_parameters'), HTTP_REFERER);
		$traninfo = $this->db->fetch_array($this->db->query(
			"select * from `v9_student_transaction` as tran 
			left join `v9_student_master` as std on tran.st_studentid = std.student_id
			left join `v9_member` as mem on mem.userid = std.student_userid
			left join `v9_student_sel_time` as time on tran.st_id = time.sst_stid 
			where st_id = '$tranid' order by st_date desc LIMIT 1"
		))[0];

		$grade = $traninfo['st_min_grade'];
		$traninfo['st_min_grade'] = $this->grade->get_one("grade_id = $grade")['grade_name'];

		$weekday = array('星期一','星期二','星期三','星期四','星期五','星期六','星期天');
		$weekarray = explode(',', $traninfo['sst_weekday']);//字符串转数组
		$traninfo['sst_weekday'] = '';
		foreach ($weekarray as $key => $value) {
			$traninfo['sst_weekday'] .= $weekday[$value].','; 
		}
		$traninfo['sst_weekday'] = rtrim($traninfo['sst_weekday'],',');

		$ttid_1 = $traninfo['st_ttid_1'];
		$traninfo['st_ttid_1'] = $this->ttype->get_one("tt_id = $ttid_1")['tt_name'];
		$ttid_2 = $traninfo['st_ttid_2'];
		$traninfo['st_ttid_2'] = $this->ttype->get_one("tt_id = $ttid_2")['tt_name'];
		$ttid_3 = $traninfo['st_ttid_3'];
		$traninfo['st_ttid_3'] = $this->ttype->get_one("tt_id = $ttid_3")['tt_name'];

	
		if(empty($traninfo)) {
			showmessage(L('要求表不存在').L('or').L('no_permission'), HTTP_REFERER);
		}
		include $this->admin_tpl('std_transaction');
	}
	
}
?>