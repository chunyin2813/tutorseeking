<?php


defined('IN_PHPCMS') or exit('No permission resources.');
class autoRecommend {
    
	function __construct() {
		$this->tutor = pc_base::load_app_class('tutor');			     //初始化导师操作类
		$this->std = pc_base::load_app_class('student');			     //初始化学生操作类
		$this->t_tran = pc_base::load_model('tutor_transaction_model');  //初始化导师执行表模型
		$this->s_tran = pc_base::load_model('student_transaction_model');//初始化学生要求表模型
	}

	
	public function recommend_to_tutor(){
	
		$sql = "select * from `v9_tutor_transaction` as tran 
				left join `v9_tutor_master` as ttr on ttr.`tutor_id` = tran.`master_id`
				left join `v9_tutor_sel_location` as loc on loc.`tsl_tmid` = tran.`tm_id` 
				left join `v9_tutor_sel_time` as time on time.`tst_tmid` = tran.`tm_id` 
				left join `v9_tutor_sel_tutor_type` as type on type.`tstt_tmid` = tran.`tm_id` 
				where `tm_matched` = '0'";
		$tran = $this->t_tran->fetch_array($this->t_tran->query($sql));

		foreach ($tran as $key => $value) {
	
			$sql = "select * from `v9_email_queue` where num = '$value[tm_tutorid]'";
			$isExist = $this->s_tran->fetch_array($this->t_tran->query($sql));
			if(!empty($isExist)){
				continue;
			}
		
			if(!empty($value['tutor_email'])){
				$pair_student = $this->std->getSuitedStudent($value['tstt_ttid_1'],$value['tstt_ttid_2'],$value['tstt_ttid_3'],$value['tsl_locid'],$value['tst_class_hour'],$value['tstt_fee'],$value['tutor_high_grade'],$value['tutor_sex'],$value['tutor_main_lang'],'','','','','',$value['tst_weekday']);
				if(!empty($pair_student['data'])){
					$content = $this->returnEmailData($pair_student['data'],1);
					$email = array(
							'email_addr' => $value['tutor_email'],//郵箱地址
							'content' => $content,//郵件內容
						);
		
					if(!empty($email['content'])){
						$send_data = str_replace('"','\"', serialize($email));
						$insert = 'insert into `v9_email_queue` (`send_data`,`num`) values ("'.$send_data.'","'.$value['tm_tutorid'].'")';
						$this->t_tran->query($insert);
					}
				}
			}
		}
	}


	
	public function recommend_to_student(){

		$sql = "select * from `v9_student_transaction` as tran 
				left join `v9_student_master` as std on std.`student_id` = tran.`st_studentid`
				left join `v9_student_sel_time` as time on time.`sst_stid` = tran.`st_id` 
				where `st_matched` = '0'";
		$tran = $this->s_tran->fetch_array($this->s_tran->query($sql));

		foreach ($tran as $key => $value) {

			$sql = "select * from `v9_email_queue` where num = '$value[st_id]'";
			$isExist = $this->s_tran->fetch_array($this->t_tran->query($sql));
			if(!empty($isExist)){
				continue;
			}

			if(!empty($value['student_email'])){
				$pair_tutor = $this->tutor->getSuitedTeacher($value['st_ttid_1'],$value['st_ttid_2'],$value['st_ttid_3'],$value['student_locid'],$value['st_class_hour'],$value['st_tutor_fee'],$value['st_min_grade'],$value['st_req_sex'],$value['st_main_lang']);
				if(!empty($pair_tutor['data'])){
					$content = $this->returnEmailData($pair_tutor['data'],2);
					$email = array(
							'email_addr' => $value['student_email'],//郵箱地址
							'content' => $content,//郵件內容
						);

					if(!empty($email['content'])){
						$send_data = str_replace('"','\"', serialize($email));
						$insert = 'insert into `v9_email_queue` (`send_data`,`num`) values ("'.$send_data.'","'.$value['st_id'].'")';
						$this->s_tran->query($insert);
					}
				}
			}
		}
	}



	public function auto_sendEmail(){
		if($_GET['pass'] != '123'){
			exit;
		}
		$queue = $this->t_tran->fetch_array($this->t_tran->query("select * from `v9_email_queue` where status = '0'"))[0];
		if(!empty($queue)){
			//发送郵件
			pc_base::load_sys_func('mail');//實例化郵件類
			$send_data = unserialize($queue['send_data']);
			if(!empty($send_data['email_addr'])){
				$subject = 'Tutorseeking個案推薦';
				sendmail($send_data['email_addr'],$subject,$send_data['content']);//發送郵件
				$changeStatus = $this->t_tran->query("update `v9_email_queue` set status = '1' where queue_id = $queue[queue_id]");
			}else{
				$changeStatus = $this->t_tran->query("update `v9_email_queue` set status = '1' where queue_id = $queue[queue_id]");
			}
		}
	}

	
	public function auto_overtime_15m(){
		if($_GET['pass'] != '123'){
			exit;
		}
		$PairModel = pc_base::load_model('pair_model');			
		$FailModel = pc_base::load_model('pair_fail_model');	
		$all_pair = $PairModel->select(array('pair_state'=>0,'student_pay'=>0,'tutor_pay'=>0)); 

		if(!empty($all_pair)){
	
			foreach ($all_pair as $key => $value) {
				if(intval(time()) - intval(strtotime($value['pair_modtime'])) > 900){
					$value['pair_state'] = 3;					
					if($FailModel->insert($value)){				
						$where = "`pair_id` = $value[pair_id]";
						$PairModel->update($value,$where);		
						//发送郵件
				        $Pairing = $this->tutor->getPairingData($value['pair_id']);
				        $Pairing = $this->tutor->structurePairingData($Pairing);   
				        pc_base::load_sys_func('mail');                           
				        $PairingData = getPairingMailData($Pairing['info']);      
				        sendmail($Pairing['email']['tutor_email'],'TutorSeekIng配對信息', $PairingData);
				        sendmail($Pairing['email']['stu_email'],'TutorSeekIng配對信息', $PairingData);  
				        sendmail('enquiry@tutorseeking.com','TutorSeekIng配對信息', $PairingData);  
				        sendmail('451870143@qq.com','TutorSeekIng配對信息', $PairingData);  
						if($PairModel->delete($where)){
					
							$memberModel = pc_base::load_model('member_model');			
							if($value['pair_sponsor'] == '1' && $value['tutor_pay'] == '1'){		
								$tor_tran['tm_state'] = '1';	
								$this->t_tran->update($tor_tran,"`tm_tutorid` = '$value[pair_tutor_tranid]'");	
								$memberModel->update("`freepair_time`=`freepair_time`+1","userid = ".$Pairing['info']['tutor_info']['tutor_userid']);
							}elseif($value['pair_sponsor'] == '2' && $value['student_pay'] == '1'){		
								$std_tran['st_state'] = '1';	
								$this->s_tran->update($std_tran,"`st_id` = '$value[pair_student_tranid]'");		
								$memberModel->update("`freepair_time`=`freepair_time`+1","userid = ".$Pairing['info']['stu_info']['student_userid']);
							}
						}
					}
				}
			}
		}
	}

	
	public function auto_overtime_48h(){
		if($_GET['pass'] != '123'){
			exit;
		}
		$PairModel = pc_base::load_model('pair_model');			
		$FailModel = pc_base::load_model('pair_fail_model');	
		$all_pair = $PairModel->select(array('pair_state'=>'0')); 
		
		$this->auto_overtime_15m();
		
		if(!empty($all_pair)){
	
			foreach ($all_pair as $key => $value) {
			
				if(!empty($value['tutor_pay']) || !empty($value['student_pay'])){
					if(intval(time()) - intval(strtotime($value['pair_modtime'])) > 172800){
						$value['pair_state'] = 3;					//超時狀態
						if($FailModel->insert($value)){				//將被拒絕的配對信息轉移到配對副表
							$where = "`pair_id` = $value[pair_id]";
							$PairModel->update($value,$where);		
							//发送郵件
					        $Pairing = $this->tutor->getPairingData($value['pair_id']);
					        $Pairing = $this->tutor->structurePairingData($Pairing);   
					        pc_base::load_sys_func('mail');                            
					        $PairingData = getPairingMailData($Pairing['info']);      
					        sendmail($Pairing['email']['tutor_email'],'TutorSeekIng配對信息', $PairingData);
					        sendmail($Pairing['email']['stu_email'],'TutorSeekIng配對信息', $PairingData); 
					        sendmail('enquiry@tutorseeking.com','TutorSeekIng配對信息', $PairingData);  
					        sendmail('451870143@qq.com','TutorSeekIng配對信息', $PairingData);  
							if($PairModel->delete($where)){
								
								$memberModel = pc_base::load_model('member_model');		
								if($value['pair_sponsor'] == '1' && $value['tutor_pay'] == '1'){	
									$tor_tran['tm_state'] = '1';	
									$this->t_tran->update($tor_tran,"`tm_tutorid` = '$value[pair_tutor_tranid]'");	
									$memberModel->update("`freepair_time`=`freepair_time`+1","userid = ".$Pairing['info']['tutor_info']['tutor_userid']);
								}elseif($value['pair_sponsor'] == '2' && $value['student_pay'] == '1'){		
									$std_tran['st_state'] = '1';	
									$this->s_tran->update($std_tran,"`st_id` = '$value[pair_student_tranid]'");	
									$memberModel->update("`freepair_time`=`freepair_time`+1","userid = ".$Pairing['info']['stu_info']['student_userid']);
								}
							}
						}
					}
				}
			}
		}
	}


	public function returnEmailData($data,$type){

		if($type == 1){		
			include PHPCMS_PATH.'phpcms/libs/functions/PushPairingMailDataTu.php';
		}else{				
			include PHPCMS_PATH.'phpcms/libs/functions/PushPairingMailDataSt.php';
		}
		return $emailData;
	}

}

?>