<?php defined('IN_ADMIN') or exit('No permission resources.');?>
<?php include $this->admin_tpl('header', 'admin');?>
<script language="javascript" type="text/javascript" src="<?php echo JS_PATH?>member_common.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo JS_PATH?>formvalidator.js" charset="UTF-8"></script>
<script language="javascript" type="text/javascript" src="<?php echo JS_PATH?>formvalidatorregex.js" charset="UTF-8"></script>
<div class="pad-10">
<div class="common-form">

<fieldset>
	<legend>基本信息</legend>
	<table width="100%" class="table_form">
		<tr>
			<td width="80">联系人姓名</td> 
			<td><?php echo $traninfo['username'];?></td>
			<td width="80">学生姓名</td> 
			<td><?php echo $traninfo['student_name'];?></td>
		</tr>
		<tr>
			<td width="80">联系人与学生关系</td> 
			<td><?php echo !empty($traninfo['student_relation']) ? $traninfo['student_relation'] : '未填寫' ?></td>
			<td width="80">學生就读学校</td> 
			<td><?php echo !empty($traninfo['student_school']) ? $traninfo['student_school'] : '未填寫' ?></td>
		</tr>
		<tr>
			<td>联系人手機號碼</td>
			<td><?php echo !empty($traninfo['student_mobile']) ? $traninfo['student_mobile'] : '未填寫' ?></td>
			<td>联系人郵箱</td>
			<td><?php echo !empty($traninfo['student_email']) ? $traninfo['student_email'] : '未填寫' ?></td>
		</tr>
		<tr>
			<td>學生出生年份</td>
			<td><?php echo !empty($traninfo['student_birth_year']) ? $traninfo['student_birth_year'] : '未填寫' ?></td>
			<td width="80">居住地址</td> 
			<td><?php echo !empty($traninfo['student_address']) ? $traninfo['student_address'] : '未填寫' ?></td>
		</tr>
	</table>
</fieldset>
<div class="bk15"></div>
<fieldset>
	<legend>家長學生要求(發佈於<?php echo $traninfo['st_date']; ?>)</legend>
	<table width="100%" class="table_form">
		<tr>
			<td width="50">要求教授時間</td> 
			<td>
				<?php 
					if(!empty($traninfo['sst_weekday']) && !empty($traninfo['sst_fromtime']) && !empty($traninfo['sst_totime'])){
						echo $traninfo['sst_weekday'].'&nbsp;&nbsp;&nbsp;'.substr($traninfo['sst_fromtime'],0,2).':'.substr($traninfo['sst_fromtime'],2,2).'——'.substr($traninfo['sst_totime'],0,2).':'.substr($traninfo['sst_totime'],2,2);
					}else{
						echo '未填寫';
					}
				?>
			</td>
			<td width="80">男學生人數</td> 
			<td><?php echo $traninfo['st_num_join_m']?></td>
		</tr>
		<tr>
			<td width="80">要求教授類別</td> 
			<td><?php echo $traninfo['st_ttid_1']?></td>
			<td width="80">女學生人數</td> 
			<td><?php echo $traninfo['st_num_join_f']?></td>
		</tr>
		<tr>
			<td width="80">要求教授項目</td> 
			<td><?php echo $traninfo['st_ttid_2']?></td>
			<td>要求導師性別</td> 
			<td><?php if($traninfo['st_req_sex'] == 'M'){echo '男';}elseif($traninfo['st_req_sex'] == 'F'){ echo '女';}else{echo '不限';}?></td>
		</tr>
		<tr>
			<td width="80">要求教授級別</td> 
			<td><?php echo $traninfo['st_ttid_3']?></td>
			<td width="80">要求每次上課時間</td> 
			<td><?php echo $traninfo['st_class_hour']?>小時</td>
		</tr>
		<tr>
			<td width="80">可接受最低教育程度</td> 
			<td><?php echo $traninfo['st_min_grade']?></td>
			<td width="80">要求授課語言</td> 
			<td><?php echo !empty($traninfo['st_main_lang']) ? $traninfo['st_main_lang'] : '未填寫' ?></td>
		</tr>
		<tr>
			<td width="80">要求每週上課次數</td> 
			<td><?php echo !empty($traninfo['st_week_class']) ? $traninfo['st_week_class'].'次':'未填寫' ?></td>
			<td width="80">要求每小時學費</td> 
			<td>$<?php echo $traninfo['st_tutor_fee']?></td>
		</tr>
	</table>
</fieldset>
</div>
    <div class="bk15"></div>
    <input type="button" class="dialog" name="dosubmit" id="dosubmit" onclick="window.top.art.dialog({id:'modelinfo'}).close();"/>
</div>
</div>
</body>
</html>