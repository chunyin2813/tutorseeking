<?php defined('IN_ADMIN') or exit('No permission resources.');?>
<?php include $this->admin_tpl('header', 'admin');?>
<div class="pad-lr-10">
<table width="100%" cellspacing="0" class="search-form">
    <tbody>
		<tr>
			<td>
				<div class="explain-col">
					<select name="tor">
						<option value="">導師個案狀態</option>
						<option value='0' <?php if(isset($_GET['tor']) && $_GET['tor']==0){?>selected<?php }?>>未付款</option>
						<option value='1' <?php if(isset($_GET['tor']) && $_GET['tor']==1){?>selected<?php }?>>已付款</option>
						<option value='2' <?php if(isset($_GET['tor']) && $_GET['tor']==2){?>selected<?php }?>>申請退款</option>
						<option value='3' <?php if(isset($_GET['tor']) && $_GET['tor']==3){?>selected<?php }?>>已退款</option>
						<option value='4' <?php if(isset($_GET['tor']) && $_GET['tor']==4){?>selected<?php }?>>金額已使用</option>
					</select>

					<select name="std">
						<option value="">學生個案狀態</option>
						<option value='0' <?php if(isset($_GET['std']) && $_GET['std']==0){?>selected<?php }?>>未付款</option>
						<option value='1' <?php if(isset($_GET['std']) && $_GET['std']==1){?>selected<?php }?>>已付款</option>
						<option value='2' <?php if(isset($_GET['std']) && $_GET['std']==2){?>selected<?php }?>>申請退款</option>
						<option value='3' <?php if(isset($_GET['std']) && $_GET['std']==3){?>selected<?php }?>>已退款</option>
						<option value='4' <?php if(isset($_GET['std']) && $_GET['std']==4){?>selected<?php }?>>金額已使用</option>
					</select>

					<select name="pairtype">
						<option value=''>配對狀態</option>
						<option value='0' <?php if(isset($_GET['pairtype']) && $_GET['pairtype']==0){?>selected<?php }?>>等待回應</option>
						<option value='1' <?php if(isset($_GET['pairtype']) && $_GET['pairtype']==1){?>selected<?php }?>>配對成功</option>
						<option value='2' <?php if(isset($_GET['pairtype']) && $_GET['pairtype']==2){?>selected<?php }?>>配對失敗</option>
						<option value='3' <?php if(isset($_GET['pairtype']) && $_GET['pairtype']==3){?>selected<?php }?>>超時失敗</option>
					</select>

					<select name="type">
						<option value='2' <?php if(isset($_GET['type']) && $_GET['type']==2){?>selected<?php }?>>導師名稱</option>
						<option value='3' <?php if(isset($_GET['type']) && $_GET['type']==3){?>selected<?php }?>>學生姓名</option>
						<option value='4' <?php if(isset($_GET['type']) && $_GET['type']==4){?>selected<?php }?>>學生編號</option>
						<option value='5' <?php if(isset($_GET['type']) && $_GET['type']==5){?>selected<?php }?>>導師編號</option>
					</select>

					<input name="keyword" type="text" value="<?php if(isset($_GET['keyword'])) {echo $_GET['keyword'];}?>" class="input-text" />

					<input type="button" name="search" class="button pair_search" value="筛选" />
					<input type="button" value="刷新" class="button" style="float:right;" onclick="Refresh();">
				</div>
			</td>
		</tr>
    </tbody>
</table>
<script>
	$('.pair_search').click(function(){
		var url = './index.php?m=pair&c=pair_admin&a=pairlist'
		var tor = $("select[name='tor']").val();
		var std = $("select[name='std']").val();
		var type = $("select[name='type']").val();
		var pairtype = $("select[name='pairtype']").val();
		var keyword = $("input[name='keyword']").val();
		if(tor != ''){ url = url+'&tor='+tor; }
		if(std != ''){ url = url+'&std='+std; }
		if(type != ''){ url = url+'&type='+type; }
		if(pairtype != ''){ url = url+'&pairtype='+pairtype; }
		if(keyword != ''){ url = url+'&keyword='+keyword; }
		window.location.href=url;
	});
</script>
<form name="myform" action="?m=pair&c=pair_list&a=search" method="post" onsubmit="checkuid();return false;">
<div class="table-list">
<table width="100%" cellspacing="0">
	<thead>
		<tr>
			<th  align="left" width="20"><input type="checkbox" value="" id="check_box" onclick="selectall('pair_id[]');"></th>
			<th align="left">配對編號</th>
			<th align="left">导师個案編號</th>
			<th align="left">学生個案編號</th>
			<th align="left">匹配状态</th>
			<th align="left">发起者身份</th>
			<th align="left">匹配时间</th>
			<th align="left">操作</th>
		</tr>
	</thead>
<tbody>
<?php
	if(is_array($pairlist['data'])){
	foreach($pairlist['data'] as $k=>$v) {
?>
    <tr>
		<td align="left"><input type="checkbox" value="<?php echo $v['pair_id']?>" name="pair_id[]"></td>
		<td align="left"><?php echo $v['pair_id']?></td>
		<td align="left"><?php echo $v[pair_tutor_tranid];?><a href="javascript:tutor_traninfo('<?php echo $v['tm_id']?>')"><img src="<?php echo IMG_PATH?>admin_img/detail.png"></a><?php if($v['tutor_pay'] == '0'){echo '<span style="color:red;">(未付款)</span>';}elseif($v['tutor_pay'] == '1'){echo '<span style="color:green;">(已付款)</span>';}elseif($v['tutor_pay'] == '2'){echo '<b style="color:purple;">(申請退款)</b>';}elseif($v['tutor_pay'] == '3'){echo '<span style="color:#ccc;">(已退款)</span>';}else{echo '<span style="color:blue;">(金額已使用)</span>';} ?></td>
		<td align="left"><?php echo $v[pair_student_tranid]?><a href="javascript:student_traninfo('<?php echo $v['st_id']?>')"><img src="<?php echo IMG_PATH?>admin_img/detail.png"></a><?php if($v['student_pay'] == '0'){echo '<span style="color:red;">(未付款)</span>';}elseif($v['student_pay'] == '1'){echo '<span style="color:green;">(已付款)</span>';}elseif($v['student_pay'] == '2'){echo '<b style="color:purple;">(申請退款)</b>';}elseif($v['student_pay'] == '3'){echo '<span style="color:#ccc;">(已退款)</span>';}else{echo '<span style="color:blue;">(金額已使用)</span>';} ?></td>
		<td align="left"><?php if($v['pair_state'] == '0'){if($v['pair_sponsor'] == '1'){echo '等待學生回應';}else{echo '等待導師回應';}}elseif($v['pair_state'] == '1'){echo '配對成功';}elseif($v['pair_state'] == '2'){echo '配對失敗';}else{echo '超時失敗';}?></td>
		<td align="left"><?php if($v['pair_sponsor'] == '1'){ echo '導師';}else{echo '學生';}?></td>
		<td align="left"><?php echo $v['pair_modtime']?></td>
		<td align="left">
			<a href="javascript:refund('<?php echo $v['pair_id']?>','<?php echo $v['pair_state']?>')">[查看詳情]</a>
		</td>
    </tr>
<?php
	}
}
?>
</tbody>
</table>

<!-- <div class="btn">
<label for="check_box"><?php echo L('select_all')?>/<?php echo L('cancel')?></label> <input type="submit" class="button" name="dosubmit" value="<?php echo L('delete')?>" onclick="return confirm('<?php echo L('sure_delete')?>')"/>
</div> -->

<div id="pages"><?php echo $pairlist['page_link']?></div>
</div>
</form>
</div>
<script type="text/javascript">
function Refresh(){
	window.location.reload();
}

function refund(pairid,state){
	window.top.art.dialog({id:'edit'}).close();
	window.top.art.dialog({title:'配對詳情',id:'edit',iframe:'?m=pair&c=refund&a=refund&pairid='+pairid+'&state='+state,width:'700',height:'500'}, function(){window.top.art.dialog({id:'edit'}).close()});
}

function edit(id, name) {
	window.top.art.dialog({id:'edit'}).close();
	window.top.art.dialog({title:'編輯導師《'+name+'》',id:'edit',iframe:'?m=member&c=tutor&a=edit&tutor_id='+id,width:'700',height:'500'}, function(){var d = window.top.art.dialog({id:'edit'}).data.iframe;d.document.getElementById('dosubmit').click();return false;}, function(){window.top.art.dialog({id:'edit'}).close()});
}

function checkuid() {
	var ids='';
	$("input[name='tutor_id[]']:checked").each(function(i, n){
		ids += $(n).val() + ',';
	});
	if(ids=='') {
		window.top.art.dialog({content:'<?php echo L('plsease_select').'導師'?>',lock:true,width:'200',height:'50',time:1.5},function(){});
		return false;
	} else {
		myform.submit();
	}
}

function tutor_traninfo(tranid) {
	window.top.art.dialog({id:'modelinfo'}).close();
	window.top.art.dialog({title:'導師匹配信息',id:'modelinfo',iframe:'?m=pair&c=pair_admin&a=tutor_traninfo&tranid='+tranid,width:'700',height:'500'}, function(){var d = window.top.art.dialog({id:'modelinfo'}).data.iframe;d.document.getElementById('dosubmit').click();return false;}, function(){window.top.art.dialog({id:'modelinfo'}).close()});
}

function student_traninfo(tranid) {
	window.top.art.dialog({id:'modelinfo'}).close();
	window.top.art.dialog({title:'學生匹配信息',id:'modelinfo',iframe:'?m=pair&c=pair_admin&a=student_traninfo&tranid='+tranid,width:'700',height:'500'}, function(){var d = window.top.art.dialog({id:'modelinfo'}).data.iframe;d.document.getElementById('dosubmit').click();return false;}, function(){window.top.art.dialog({id:'modelinfo'}).close()});
}

</script>
</body>
</html>