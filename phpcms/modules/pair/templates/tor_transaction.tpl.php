<?php defined('IN_ADMIN') or exit('No permission resources.');?>
<?php include $this->admin_tpl('header', 'admin');?>
<script language="javascript" type="text/javascript" src="<?php echo JS_PATH?>member_common.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo JS_PATH?>formvalidator.js" charset="UTF-8"></script>
<script language="javascript" type="text/javascript" src="<?php echo JS_PATH?>formvalidatorregex.js" charset="UTF-8"></script>
<div class="pad-10">
<div class="common-form">

<fieldset>
	<legend>基本信息</legend>
	<table width="100%" class="table_form">
		<tr>
			<td width="80">姓名</td> 
			<td><?php echo $traninfo['tutor_sname_chi'].$traninfo['tutor_fname_chi'];?></td>
			<td>性別</td> 
			<td><?php if($traninfo['tutor_sex'] == 'M'){echo '男';}else{ echo '女';}?></td>
		</tr>
		<tr>
			<td>手機號碼</td>
			<td><?php echo !empty($traninfo['tutor_mobile']) ? $traninfo['tutor_mobile'] : '未填寫' ?></td>
			<td>Email</td>
			<td><?php echo !empty($traninfo['tutor_email']) ? $traninfo['tutor_email'] : '未填寫' ?></td>
		</tr>
		<tr>
			<td width="80">國籍</td> 
			<td><?php echo !empty($traninfo['tutor_nation']) ? $traninfo['tutor_nation'] : '未填寫' ?></td>
			<td width="80">主要語言</td> 
			<td><?php if($traninfo['tutor_main_lang'] == '0'){echo '廣東話';}elseif($traninfo['tutor_main_lang'] == '1'){echo '普通話';}else{ echo '英文';}?></td>
		</tr>
		<tr>
			<td width="80">出生年份</td> 
			<td><?php echo !empty($traninfo['tutor_birth_year']) ? $traninfo['tutor_birth_year'] : '未填寫' ?></td>
			<td width="80">最高受教育程度</td> 
			<td><?php echo $traninfo['tutor_high_grade']?></td>
		</tr>
		<tr>
			<td width="80">就讀大學</td> 
			<td><?php echo !empty($traninfo['tutor_college']) ? $traninfo['tutor_college'] : '未填寫' ?></td>
			<td width="80">工作經驗</td> 
			<td><?php echo $traninfo['tutor_work_exp']?></td>
		</tr>
	</table>
</fieldset>
<div class="bk15"></div>
<fieldset>
	<legend>匹配信息(發佈於<?php echo $traninfo['tm_modtime'] ?>)</legend>
	<table width="100%" class="table_form">
		<tr>
			<td width="80">可教授時間</td> 
			<td>
				<?php 
					if(!empty($traninfo['tst_weekday']) && !empty($traninfo['tst_fromtime']) && !empty($traninfo['tst_totime'])){
						echo $traninfo['tst_weekday'].'&nbsp;&nbsp;&nbsp;'.substr($traninfo['tst_fromtime'],0,2).':'.substr($traninfo['tst_fromtime'],2,2).'——'.substr($traninfo['tst_totime'],0,2).':'.substr($traninfo['tst_totime'],2,2);
					}else{
						echo '未填寫';
					}
				?>
			</td>
		</tr>
		<tr>
			<td width="80">可教授類別</td> 
			<td><?php echo $traninfo['tstt_ttid_1']?></td>
		</tr>
		<tr>
			<td width="80">可教授項目</td> 
			<td><?php echo $traninfo['tstt_ttid_2']?></td>
		</tr>
		<tr>
			<td width="80">可教授級別</td> 
			<td><?php echo $traninfo['tstt_ttid_3']?></td>
		</tr>
		<tr>
			<td width="80">可教授地区</td> 
			<td><?php echo $traninfo['loc_name']?></td>
		</tr>
	</table>
</fieldset>
</div>
    <div class="bk15"></div>
    <input type="button" class="dialog" name="dosubmit" id="dosubmit" onclick="window.top.art.dialog({id:'modelinfo'}).close();"/>
</div>
</div>
</body>
</html>