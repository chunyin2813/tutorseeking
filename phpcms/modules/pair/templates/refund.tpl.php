<?php defined('IN_ADMIN') or exit('No permission resources.');?>
<?php include $this->admin_tpl('header', 'admin');?>
<script language="javascript" type="text/javascript" src="<?php echo JS_PATH?>member_common.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo JS_PATH?>formvalidator.js" charset="UTF-8"></script>
<script language="javascript" type="text/javascript" src="<?php echo JS_PATH?>formvalidatorregex.js" charset="UTF-8"></script>
<div class="pad-10">
<div class="common-form">

<fieldset>
	<legend>导师信息(<?php if($pair['pair_sponsor'] == 1){echo '發起方';}else{echo '被配對方';}?>) 編號:<?php echo $pair['pair_tutor_tranid'];?></legend>
	<table width="100%" class="table_form">
		<tr>
			<td width="80">姓名：</td> 
			<td><?php echo $tutor['tutor_sname_chi'].$tutor['tutor_fname_chi'];?></td>
			<td>手機號碼：</td>
			<td><?php echo $tutor['tutor_mobile']?></td>
		</tr>
		<tr>
			<td>Email：</td>
			<td><?php echo $tutor['tutor_email']?></td>
			<td>支付方式：</td>
			<td>
				<?php
					if(!empty($torpay)){
						if($pair['tutor_use_points'] == 0){
							echo 'Paypal全額支付';
						}else{
							echo 'Paypal+積分抵扣';
						}
					}else{
						if($pair['tutor_pay'] == 0){
							echo '無';
						}elseif($pair['tutor_use_points'] == 0){
							echo '消耗免費配對次數';
						}else{
							echo '積分抵扣全額';
						}
					}
				?>
			</td>
		</tr>
		<tr>
			<td>Paypal账号：</td>
			<td><?php if(!empty($torpay['payer_email'])){ echo $torpay['payer_email'];}else{ echo '無';}?></td>
			<td>Paypal支付金額：</td>
			<td><?php if(!empty($torpayinfo)){echo $torpayinfo['mc_currency'].' '.$torpayinfo['mc_gross'];}else{echo '無';}?></td>
		</tr>
		<tr>
			<td>使用積分：</td>
			<td><?php if(!empty($pair['tutor_use_points'])){echo $pair['tutor_use_points'];}else{echo '不使用';}?></td>
			<td>付款状态：</td>
			<td><?php if($pair['tutor_pay'] == '0'){ echo '未付款';}elseif($pair['tutor_pay'] == '1'){ echo '已付款';}elseif($pair['tutor_pay'] == '2'){ echo '申請退款';}elseif($pair['tutor_pay'] == '3'){ echo '已退款'; }else{ echo '金額已使用';} ?></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<?php if($pair['tutor_pay'] == '2' && ($pair['pair_state'] == 2 || $pair['pair_state'] == 3)){
				echo '<td>操作：</td><td><input type="button" onclick="refund_operate('.$pair['pair_id'].',\'tor_refund\');" value="確認退款"/></td>';
			}else{
				echo '<td>&nbsp;</td><td>&nbsp;</td>';
			} ?>
		</tr>
	</table>
</fieldset>
<div class="bk15"></div>
<div class="bk15"></div>
<div class="bk15"></div>
<fieldset>
	<legend>学生信息(<?php if($pair['pair_sponsor'] == 2){echo '發起方';}else{echo '被配對方';}?>) 編號:<?php echo $pair['pair_student_tranid'];?></legend>
	<table width="100%" class="table_form">
		<tr>
			<td width="80">姓名：</td> 
			<td><?php echo $student['student_name'];?></td>
			<td>手機號碼：</td>
			<td><?php echo $student['student_mobile']?></td>
		</tr>
		<tr>
			<td>Email：</td>
			<td><?php echo $student['student_email']?></td>
			<td>支付方式：</td>
			<td>
				<?php
					if(!empty($stdpay)){
						if($pair['student_use_points'] == 0){
							echo 'Paypal全額支付';
						}else{
							echo 'Paypal+積分抵扣';
						}
					}else{
						if($pair['student_pay'] == 0){
							echo '無';
						}elseif($pair['student_use_points'] == 0){
							echo '消耗免費配對次數';
						}else{
							echo '積分抵扣全額';
						}
					}
				?>
			</td>
		</tr>
		<tr>
			<td>Paypal账号：</td>
			<td><?php if(!empty($stdpay['payer_email'])){ echo $stdpay['payer_email'];}else{ echo '無';}?></td>
			<td>Paypal支付金額：</td>
			<td><?php if(!empty($stdpayinfo)){echo $stdpayinfo['mc_currency'].' '.$stdpayinfo['mc_gross'];}else{echo '無';}?></td>
		</tr>
		<tr>
			<td>使用積分：</td>
			<td><?php if(!empty($pair['student_use_points'])){echo $pair['student_use_points'];}else{echo '不使用';}?></td>
			<td>付款状态：</td>
			<td><?php if($pair['student_pay'] == '0'){ echo '未付款';}elseif($pair['student_pay'] == '1'){ echo '已付款';}elseif($pair['student_pay'] == '2'){ echo '申請退款';}elseif($pair['student_pay'] == '3'){ echo '已退款'; }else{ echo '金額已使用';} ?></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<?php if($pair['student_pay'] == '2' && ($pair['pair_state'] == 2 || $pair['pair_state'] == 3)){
				echo '<td>操作：</td><td><input type="button" onclick="refund_operate('.$pair['pair_id'].',\'std_refund\');" value="確認退款"/></td>';
			}else{
				echo '<td>&nbsp;</td><td>&nbsp;</td>';
			} ?>
		</tr>
	</table>
</fieldset>
</div>
    <!-- <div class="bk15"></div> -->
    <!-- <input type="button" class="dialog" name="dosubmit" id="dosubmit" onclick="window.top.art.dialog({id:'modelinfo'}).close();"/> -->
</div>
</div>
</body>
</html>
<script>

	function refund_operate(id,item){
		if(confirm('確認退款？')){
			$.post('./index.php?m=pair&c=refund&a=refund_operate',{id:id,item:item},function(data){
				if(data == 'success'){
					window.location.reload();
				}else{
					alert('操作失败,請重試!');
				}
			});
		}
	}

</script>