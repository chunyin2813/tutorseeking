<?php 
defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_app_class('admin','admin',0);
$session_storage = 'session_'.pc_base::load_config('system','session_storage');
pc_base::load_sys_class($session_storage);

class tutor_loc extends admin {
	function __construct() {
        parent::__construct();
        $this->db = pc_base::load_model('tutor_sel_location_model');
        $this->m_db = pc_base::load_model('tutor_master_model');
        $this->s_db = pc_base::load_model('sys_location_model');
        $this->t_db = pc_base::load_model('tutor_transaction_model');
       // $this->member = pc_base::load_model('member_model');
		$this->module_db = pc_base::load_model('module_model');
    }

    public function init(){
        //获取所有导师名称
        $members=$this->m_db->select();
    	include $this->admin_tpl('tutor_loc'); 
    }

    public function ajax_gean(){
        $tutor_id = $_POST['tutor_id'];
        $gean = $_POST['gean'];
        //选择的导师id/個案編號存入session
        $_SESSION['tutor_id'] = $tutor_id;
        $_SESSION['gean'] = $gean;
    
        $tutor_loc_info = $this->db->get_one(array('tsl_tutorid'=>$gean));
        $tsl_locid = explode(',', $tutor_loc_info['tsl_locid']);
        foreach ($tsl_locid as $k => $v) {
            $loc_info = $this->s_db->select(array('loc_id'=>$v));
            $loc[] = $loc_info;
        }     
        echo json_encode($loc); 
        exit;
    }

    public function ajax_addgean(){
         if(isset($_POST['tutor_id'])){
            $transaction=$this->t_db->select(array('master_id'=>$_POST['tutor_id']));
            $transaction=json_encode($transaction);
            echo $transaction;
            exit;
        }
    }

    public function add(){
        $members=$this->m_db->select();
        $infos_nation = $this->s_db->select(array('loc_level'=>1));
        include $this->admin_tpl('tutor_loc_add'); 
    }

    public function ajax_add(){
        if(isset($_POST['loc_id'])){
            $infos_region=$this->s_db->select(array('loc_parentid'=>$_POST['loc_id']));
            $infos_region=json_encode($infos_region);
            echo $infos_region;
            exit;
        }
    }

    public function ajax_sure_add(){
        //获取导师名称
        $members=$this->m_db->select();
        //获取选中导师新增信息
        if(isset($_POST)){
            $tutor_id = $_POST['tutor_id'];
            $loc_id = implode(',',$_POST['tutor_id']);
        }
        //插入要求表
        //个案编号
        $a_sql = "select max(tm_tutorid) from v9_tutor_transaction;";
        $this->tumas->query($a_sql);
        $infos = $this->tumas->fetch_array();
        $tm_tutorid = $infos[0]['max(tm_tutorid)'];
        $t = split('T',$tm_tutorid);
        $tm = $t[1]+1;
        $tm_tutorid = 'T'.$tm;
   
        $res=$this->t_db->insert(array('tm_tutorid'=>$tm_tutorid,'$tm_mdtime'=>$modtime,'master_id'=>$tutor_id));
        if($res){
            //获取导师要求表id
            $tutor_transa=$this->t_db->get_one(array('tm_tutorid'=>$tm_tutorid));
            $tm_id=$tutor_transa['tm_id'];
            //插入
            $data['tsl_tuotrid']= $tm_tutorid;
            $data['tsl_tmid']= $tm_id;
            $data['tsl_locid']=$loc_id;
            $data['tsa_modtime']= $modtime;
            $result = $this->db->insert($data);
            if($result){
                echo "1";
            }else{
                echo "2";
            }
        }
            

    }

    public function ajax_edit(){
        $tutor_id = $_SESSION['tutor_id'];
        $gean_num  = $_SESSION['gean'];
        //新增之前的地區
        $old_locs = $this->db->get_one(array('tsl_tutorid'=>$gean_num));
        $old_loc_str = $old_locs['tsl_locid'];
        $old_loc_arr = explode(',',$old_loc_str);
        //新增的地區數組
        $loc_id = $_POST['loc_id'];
        $new_loc_str = implode(',', $loc_id);
        //地區有重複則數組不為空
        $arr_loc = array_intersect($loc_id,$old_loc_arr); 
        if(!empty($arr_loc)){
            echo "3";
        }else{
            $loc_id = implode(',',$_POST['loc_id']);
            $loc_str = $new_loc_str.','.$old_loc_str;
            $res = $this->db->update(array('tsl_locid'=>$loc_str),array('tsl_tutorid'=>$gean_num));
            if($res){
                echo "1";
            }else{
                echo "2";
            }
        } 
    }

    public function edit(){
        $tutor_id = $_SESSION['tutor_id'];
        $infos_nation = $this->s_db->select(array('loc_level'=>1));
        $id = intval($_GET['id']);
        $info = $this->s_db->get_one(array('loc_id'=>$id));
        if(!$info){
            showmessage('没有找到数据');
        }
        extract($info);
        include $this->admin_tpl('tutor_loc_edit');
    }

    //删除
    public function delete($where) {
        if(isset($_GET)){
            $where=array();
            $where['tsl_locid']=intval($_GET['id']);
            $this->db->delete($where);
            showmessage('删除成功',HTTP_REFERER);
        }else{
            showmessage('删除失败',HTTP_REFERER);
        }
    }

    //全选删除
    public function delete_all(){
        // var_dump($_POST);die;
        if (!isset($_POST['id']) || !is_array($_POST['id'])) {
            showmessage(L('illegal_parameters'), HTTP_REFERER);
        } else {
            array_map(array($this, _del), $_POST['id']);
        }
        showmessage(L('operation_success'), HTTP_REFERER);
    }

    private function _del($id = 0) {
        $id = intval($id);
        if (!$id) return false;
        $this->db->delete(array('tsl_locid'=>$id));
    }

}
?>