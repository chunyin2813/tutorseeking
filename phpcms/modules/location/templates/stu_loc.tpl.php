<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header','admin');
?>
<!--导师可教授地区设置主页面-->
<div class="pad-lr-10">
	<div class="table-list">
		<!-- <a href="?m=location&c=stu_loc&a=add"><button>新增</button></a> -->
		<br /><br />
		选择学生:
		<select name="student_name" id="student_name">
			<option>--请选择学生--</option>
			<?php foreach ($members as $member) {	?>
			<option value="<?php echo $member['student_name'];?>"><?php echo $member['student_name'];?></option>
			<?php }?>
		</select>
		<br /><br />
	    <table width="100%" cellspacing="0" class="contentWrap">
	        <thead>
	            <tr>
	            <th width="30" align="center"><input type="checkbox" value="" id="check_box" onclick="selectall('id[]');"></th>
				<th width="35">ID</th>
				<th width="100" align="center">学生要求教授地区级别</th>
				<th width="100" align="center">学生要求教授地区名称</th>
				<th width="100" align="center">更新时间</th>
				<th width="110" align="center">管理操作</th>
	            </tr>
	        </thead>
	        <tbody id = "tbody"> 
			</tbody>
	    </table>
	    <br />
	    <div id="btn">
	    	<td><label for="check_box">全选/取消</label>&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="submit" name="allsubmit" id="allsubmit" value="删除"></td>&nbsp;&nbsp;
		</div>  
	</div>
	<div id="pages"><?php echo $pages;?></div>
</div>
</body>
</html>
<script type="text/javascript">
	var pc_hash = "<?php echo $_SESSION['pc_hash']?>";
	$("#student_name").change(function(){
		var student_name_v=$("#student_name").val();
		$.post('./index.php?m=location&c=stu_loc&a=ajax_init',{student_name_v:student_name_v,pc_hash:pc_hash},function(data){
				// var data = json_decode(data));
				// console.log(data);
				for(i in data){

				// var forms="<form method='post' action='?m=location&c=stu_loc&a=delete_all' id='myform'><form>";
				// var tr="<tr id='tr_location'><td align='center'><input type='checkbox' name='id[]' value='"+data[i].loc_id+"'></td><td align='center'>"+data[i].loc_id+"</td><td align='center' width='70'>"+data[i].loc_level+"</td><td align='center' width='70'>"+data[i].loc_name+"</td><td align='center' >"+data[i].loc_modtime+"</td><td align='center'><a href='?m=location&c=stu_loc&a=edit&id="+data[i].loc_id+"&pc_hash="+pc_hash+"'>修改</a> | <a href='?m=location&c=stu_loc&a=delete&id="+data[i].loc_id+"&pc_hash="+pc_hash+"'>删除</a></td></tr>";
				// var tbody=$("#tbody");
				// tbody.append(forms);
				// var myform = $("#myform");
				// myform.append(tr);

				var tr="<tr id='tr_region'><td align='center'><input type='checkbox' name='id[]' value='"+data[i].loc_id+"'></td><td align='center'>"+data[i].loc_id+"</td><td align='center' width='70'>"+data[i].loc_level+"</td><td align='center' width='70'>"+data[i].loc_name+"</td><td align='center' >"+data[i].loc_modtime+"</td><td align='center'><a href='?m=location&c=stu_loc&a=edit&id="+data[i].loc_id+"&pc_hash="+pc_hash+"'>修改</a> | <a href='?m=location&c=stu_loc&a=delete&id="+data[i].loc_id+"&pc_hash="+pc_hash+"'>删除</a></td></tr>";
				var btn="<div class='btn' id='btn'><label for='check_box'>全选/取消</label>&nbsp;&nbsp;&nbsp;&nbsp;<input type='submit' name='allsubmit' id='allsubmit' value='删除'>&nbsp;&nbsp;</div>";
				var tbody=$("#tbody");
				tbody.append(tr);
				$("#myform").append(tr);
				$("#myform").append(btn);
				}
		},'json');
	});
</script>