<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header','admin');
if($loc_level==1){
	echo '<script language="javascript">$(document).ready(function(){$("#edit_nation").show();$("#edit_region").hide();$("#edit_area").hide();});</script>';
}else if($loc_level==2){
	echo '<script language="javascript">$(document).ready(function(){$("#edit_nation").hide();$("#edit_region").show();$("#edit_area").hide();});</script>';
}else if($loc_level==3){
	echo '<script language="javascript">$(document).ready(function(){$("#edit_nation").hide();$("#edit_region").hide();$("#edit_area").show();});</script>';
}
?>
<!--修改地区设置主页面-->
<div class="pad-lr-10">
	<div class="table-list">
		<!--修改国籍-->
		<form method="post" action="?m=location&c=sys_loc&a=edit1" name="nationsubmit">
			<input name="loc_id" class="input-text" type="hidden" value="<?php echo $_GET['id']?>">
		<div id="edit_nation">
			<table>
				<tr>
					<td>修改国籍:</td>
					<td><input name="loc_nation_name" class="input-text" type="text" size="25" required value="<?php echo $info['loc_name']?>"></td>
				</tr>
				<tr>
					<td>修改排序:</td>
					<td><input name="loc_seq" class="input-text" type="text" size="25" required value="<?php echo $info['loc_seq']?>"></td>
				</tr>
				<tr>
					<td>更新时间:</td>
					<td><input name="loc_modtime" id="loc_modtime" class="input-text" type="text" size="25" value="<?php echo date("Y-m-d H:i:s")?>" readonly="readonly" >
					</td>
				</tr>
				<tr>
					<td><input type="submit" name="nationsubmit" value="提交"></td>
					<td><INPUT name="pclog" type="button" value="返回" onClick="location.href='index.php?m=location&c=sys_loc&a=init&pc_hash=<?php echo $_SESSION['pc_hash']?>'"></td>
				</tr>
			</table>	
		</div>
		</form>

		<!--修改区域-->
		<div id="edit_region">
			<form method="post" action="?m=location&c=sys_loc&a=edit2" name="regionsubmit">
				<input name="loc_id" class="input-text" type="hidden" value="<?php echo $_GET['id']?>">
			<table>
				<tr>
					<td>国籍名称:</td>
					<td>
						<select name="loc_nation">
							<option value="">--选择国籍--</option>
							<?php foreach ($infos_nation as $infos) {	?>
							<option value="<?php echo $infos['loc_id']?>" <?php if($infos['loc_id']==$info2['loc_parentid']){?>selected<?php }?>><?php echo $infos['loc_name']?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td>修改区域:</td>
					<td><input name="loc_region_name" class="input-text" type="text" size="25" required value="<?php echo $info2['loc_name'];?>"></td>
				</tr>
				<tr>
					<td>修改排序:</td>
					<td><input name="loc_seq" class="input-text" type="text" size="25" required value="<?php echo $info2['loc_seq'];?>"></td>
				</tr>
				<tr>
					<td>更新时间:</td>
					<td>
						<input name="loc_modtime" id="loc_modtime" class="input-text" type="text" size="25" value="<?php echo date("Y-m-d H:i:s")?>" readonly="readonly" >
					</td>
				</tr>
				<tr>
					<td><input type="submit" name="regionsubmit"  value="提交"></td>
					<td><INPUT name="pclog" type="button" value="返回" onClick="location.href='index.php?m=location&c=sys_loc&a=init&pc_hash=<?php echo $_SESSION['pc_hash']?>'"></td>
				</tr>
			</table>	
			</form>
		</div>
		<!--修改地区-->
		<div id="edit_area">
			<form method="post" action="?m=location&c=sys_loc&a=edit3" name="areasubmit">
				<input name="loc_id" class="input-text" type="hidden" value="<?php echo $_GET['id']?>">
			<table>
				<tr>
					<td>国籍名称:</td>
					<td>
						<select name="loc_nation" id="loc_nation">
							<option value="">--选择国籍--</option>
							<?php foreach ($infos_nation as $infos) {	?>
							<option value="<?php echo $infos['loc_id']?>"<?php if($infos['loc_id']==$info3_nation['loc_id']){?>selected<?php }?>><?php echo $infos['loc_name']?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td>区域名称:</td>
					<td>
					<?php ?>
						<select name="loc_region" id="loc_region">
							<option value="">--选择区域--</option>
							<?php foreach ($info3_regions as $info3_region3) {	?>
							<option value="<?php echo $info3_region3['loc_id']?>"<?php if($info3_region3['loc_id']==$info3_region['loc_id']){?>selected<?php }?>><?php echo $info3_region3['loc_name']?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td>修改地区:</td>
					<td>
						<input name="loc_area_name" class="input-text" type="text" size="25" required value="<?php echo $info3['loc_name'];?>">
					</td>
				</tr>
				<tr>
					<td>修改排序:</td>
					<td><input name="loc_seq" class="input-text" type="text" size="25" required value="<?php echo $info3['loc_seq']?>"></td>
				</tr>
				<tr>
					<td>更新时间:</td>
					<td>
						<input name="loc_modtime" id="loc_modtime" class="input-text" type="text" size="25" value="<?php echo date("Y-m-d H:i:s")?>" readonly="readonly" >
					</td>
				</tr>
				<tr>
					<td><input type="submit" name="areasubmit"  value="提交"></td>
					<td><INPUT name="pclog" type="button" value="返回" onClick="location.href='index.php?m=location&c=sys_loc&a=init&pc_hash=<?php echo $_SESSION['pc_hash']?>'"></td>
				</tr>
			</table>	
			</form>
		</div>
	</div>	
</div>
</body>
</html>
<script type="text/javascript">
	var pc_hash = "<?php echo $_SESSION['pc_hash']?>";
	$("#loc_nation").change(function(){
		var v = $("#loc_nation").val();
		if(v.length!=0){
			$.post('./index.php?m=location&c=sys_loc&a=edit_select',{loc_nation:v,pc_hash:pc_hash},function(data){
				$("#loc_region").html(' ');
				$("#loc_region").append('<option value="">--选择区域--</option>');
				for(i in data){
					var course_option="<option class='c_option' value='"+data[i].loc_id+"'>"+data[i].loc_name+"</option>";
					$("#loc_region").append(course_option);
				}
			},'json');
	    	return false;
		}else{
			$("#loc_region").html(' ');
			$("#loc_region").append('<option value="">--选择区域--</option>');
		}
		
	});
</script>
