<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header','admin');
?>
<style type="text/css">
#btn{
	margin-left:45%; 
}
</style>
<!--修改学生要求可教授地区设置主页面-->
<div class="pad-lr-10">
	<div class="table-list">
	<p>修改学生要求可教授地区:</p>
	<br/>
	<form method="post" name="stu_">
	<input type="hidden" value="<?php echo $info['loc_id'];?>" name="student_loc_id">
	<table class="table_form" width="100%" cellspacing="0">
		<tr>
			<td>
				<select name="loc_nation" id="loc_nation">
				<option>-选择国家-</option>
				<?php foreach ($infos_nation as $info) { ?>
				<option value="<?php echo $info['loc_name']?>"><?php echo $info['loc_name']?></option>
				<?php } ?>	
			</select>
			</td>
			<td>
				<input type="text" name="loc_nation_add" id="loc_nation_add" disabled="disabled">
			</td>
		</tr>
		<tr>
			<td>
				<select name="loc_region" id="loc_region">
				<option>-选择区域-</option>
				</select>
			</td>
			<td>
				<input type="text" name="loc_region_add" id="loc_region_add" disabled="disabled">
			</td>
		</tr>
		<tr>
			<td>
				<select name="loc_area" id="loc_area">
				<option>-选择地区-</option>
				</select>
			</td>
			<td>
				<input type="text" name="loc_area_add" id="loc_area_add" disabled="disabled">
			</td>
		</tr>
		<tr>
			<td>
				<select name="loc_house" id="loc_house">
				<option>-选择屋苑-</option>
				</select>
			</td>
			<td>
				<input type="text" name="loc_house_add" id="loc_house_add" disabled="disabled">
			</td>
		</tr>
	</table>
	<br />
	<div id="btn">
		<input type="submit" value="确定" name="edit_submit">&nbsp;&nbsp;
		<INPUT name="pclog" type="button" value="返回" onClick="location.href='index.php?m=location&c=stu_loc&a=init&pc_hash=<?php echo $_SESSION['pc_hash']?>'">
	</div>
	</form>
	<div id="pages"><?php echo $this->db->pages;?></div>
</div>
</body>
</html>
<script type="text/javascript">
$(document).ready(function(){
	// $("tr:eq(0)").nextAll().hide();
	var pc_hash = "<?php echo $_SESSION['pc_hash']?>";
	$("#tutor_name").change(function(){
		var v = $("#tutor_name").val();
		$("#tutor_name_add").attr('value',v);
	})
	//国家联动区域下拉框
	$("#loc_nation").change(function(){
		var v = $("#loc_nation").val();
		$("#loc_nation_add").attr('value',v);
		$.post('./index.php?m=location&c=tutor_loc&a=add',{loc_nation:v,pc_hash:pc_hash},function(data){
			$("#loc_region").html(' ');
			$("#loc_region").append('<option>--请选择区域--</option>');
			if(data.length!==0){
				$("tr:eq(1)").next().show();
				for(i in data){
				//动态添加区域下拉框
				var course_option="<option class='c_option' value='"+data[i].loc_name+"'>"+data[i].loc_name+"</option>"	;	
				$("#loc_region").append(course_option);
				}
			}
		},'json');
    return false;
	});

	//区域联动地区下拉框
	$("#loc_region").change(function(){
		var v = $("#loc_region").val();
		$("#loc_region_add").attr('value',v);
		$.post('./index.php?m=location&c=tutor_loc&a=add',{loc_region:v,pc_hash:pc_hash},function(data){
			$("#loc_area").html(' ');
			$("#loc_area").append('<option>--请选择地区--</option>');
			if(data.length!==0){
				$("tr:eq(2)").next().show();
				for(i in data){
					//动态添加地区下拉框
					var course_option="<option class='c_option' value='"+data[i].loc_name+"'>"+data[i].loc_name+"</option>";
					$("#loc_area").append(course_option);
				}
			}
		},'json');
    return false;
	});

	//地区联动屋苑下拉框
	$("#loc_area").change(function(){
		var v = $("#loc_area").val();
		$("#loc_area_add").attr('value',v);
		$.post('./index.php?m=location&c=tutor_loc&a=add',{loc_area:v,pc_hash:pc_hash},function(data){
			$("#loc_house").html(' ');
			$("#loc_house").append('<option>--请选择屋苑--</option>');
			if(data.length!==0){
				$("tr:eq(3)").next().show();
				for(i in data){
					//动态添加地区下拉框
					var course_option="<option class='c_option' value='"+data[i].loc_name+"'>"+data[i].loc_name+"</option>";
					$("#loc_house").append(course_option);
				}
			}
		},'json');
    return false;
	});

	//下拉框选择屋苑
	$("#loc_house").change(function(){
		var v = $("#loc_house").val();
		$("#loc_house_add").attr('value',v);
	});

});
</script>