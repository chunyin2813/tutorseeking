<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header','admin');
?>
<!--地区设置主页面-->
<div class="pad-lr-10">
	<div class="table-list">
		<a href="?m=location&c=sys_loc&a=add"><button>新增</button></a>
		<br /><br />
		<div class="loc_select">
			选择地区:&nbsp;&nbsp;
			<select name="loc_nation" id="loc_nation">
				<option value="">-选择国家-</option>
				<?php foreach ($infos_nation as $info) {	?>
				<option value="<?php echo $info['loc_id']?>"><?php echo $info['loc_name']?></option>
				<?php } ?>	
			</select>&nbsp;&nbsp;
			<select name="loc_region" id="loc_region">
				<option value="">-选择区域-</option>
			</select>&nbsp;&nbsp;
			<select name="loc_area" id="loc_area">
				<option value="">-选择地区-</option>
			</select>
		</div>
		<br /><br />
	    <table width="100%" cellspacing="0" class="contentWrap" id="table">
	        <thead>
	            <tr>
	            <th width="30" align="center"><input type="checkbox" value="" id="check_box" onclick="selectall('id[]');"></th>
				<th width="35">ID</th>
				<th width="100" align="center">导师可教授地区级别</th>
				<th width="100" align="center">导师可教授地区名称</th>
				<th width="100" align="center">更新时间</th>
				<th width="110" align="center">管理操作</th>
	            </tr>
	        </thead>
	        <tbody id="tbody"> 
	        <form method="post" action="?m=location&c=sys_loc&a=delete_all" id="myform">
	        <?php foreach ($infos as $info) {	?>
				<tr id="tr_nation">
					<td align="center">
					<input type="checkbox" name="id[]" value="<?php echo $info['loc_id']?>">
					</td>
					<td align="center"><?php echo $info['loc_id']?></td>
					<td align="center" width="70"><?php echo $info['loc_level']?></td>
					<td align="center" width="70"><?php echo $info['loc_name']?></td>
					<td align="center" ><?php echo date("Y-m-d H:i:s")?></td>
					<td align="center">
						<a href="?m=location&c=sys_loc&a=edit&id=<?php echo $info['loc_id']?>">修改</a> <!-- | <a href="?m=location&c=sys_loc&a=delete&id=<?php //echo $info['loc_id']?>" onclick="return confirm('<?php //echo new_html_special_chars(new_addslashes(L('confirm', array('message'=> $info['loc_id'].'.'.$info_s['loc_name']))))?>')">删除</a> -->
					</td>
				</tr>
			<?php } ?>	
			</tbody>
	    </table>
	   <!--  <div class="btn" id="btn">
	    	<label for="check_box">全选/取消</label>&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="submit" name="allsubmit" id="allsubmit" value="删除">&nbsp;&nbsp;	
		</div>  -->
		</form>
	</div>
	<div id="pages"><?php echo $pages;?></div>
</div>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){
		$("#loc_region").hide();
		$("#loc_area").hide();
	});
	var pc_hash = "<?php echo $_SESSION['pc_hash']?>";
	//国家联动区域下拉框
	$("#loc_nation").change(function(){
		var v = $("#loc_nation").val();
		if(v.length==0){
			$("#loc_region").hide();
			$("#loc_area").hide();
		}else{
			$.post('./index.php?m=location&c=sys_loc&a=init',{loc_nation:v,pc_hash:pc_hash},function(data){
				$("#loc_region").html(' ');
				$("#loc_region").append('<option value="">--请选择--</option>');
				if ($("#tr_nation")){ 
					$("tr").remove("#tr_nation");
				} 
				if ($("#tr_region")){ 
					$("tr").remove("#tr_region"); 
				} 
				if ($("#tr_area")){ 
					$("tr").remove("#tr_area");
				} 
				for(i in data){
					//动态添加区域下拉框
					if(data.length!==0){
						$("#loc_region").show();
						$("#loc_area").hide();
					}
					var course_option="<option class='c_option' value='"+data[i].loc_id+"'>"+data[i].loc_name+"</option>"	;	
					$("#loc_region").append(course_option);
					//动态生成选定国家的区域表格
					var tr="<tr id='tr_region'><td align='center'><input type='checkbox' name='id[]' value='"+data[i].loc_id+"'></td><td align='center'>"+data[i].loc_id+"</td><td align='center' width='70'>"+data[i].loc_level+"</td><td align='center' width='70'>"+data[i].loc_name+"</td><td align='center' >"+data[i].loc_modtime+"</td><td align='center'><a href='?m=location&c=sys_loc&a=edit&id="+data[i].loc_id+"&pc_hash="+pc_hash+"'>修改</a></td></tr>";
					var tbody=$("#tbody");
					tbody.append(tr);
					$("#myform").append(tr);
				}
			},'json');
	    return false;
	    }
	});

	//区域联动地区下拉框
	$("#loc_region").change(function(){
		var v = $("#loc_region").val();
		if(v.length==0){
			$("#loc_area").hide();
		}else{
			$.post('./index.php?m=location&c=sys_loc&a=init',{loc_region:v,pc_hash:pc_hash},function(data){
				$("#loc_area").html(' ');
				$("#loc_area").append('<option value="">--请选择--</option>');
				if ($("#tr_nation")){ 
					$("tr").remove("#tr_nation");
				} 
				if ($("#tr_region")){ 
					$("tr").remove("#tr_region"); 
				} 
				if ($("#tr_area")){ 
					$("tr").remove("#tr_area");
				} 
				for(i in data){
					if(data.length!==0){
						$("#loc_area").show();
					}
					//动态添加地区下拉框
					var course_option="<option class='c_option' value='"+data[i].loc_id+"'>"+data[i].loc_name+"</option>";
					$("#loc_area").append(course_option);

					//动态生成选定区域的地区表格
					var tr="<tr id='tr_area'><td align='center'><input type='checkbox' name='id[]' value='"+data[i].loc_id+"'></td><td align='center'>"+data[i].loc_id+"</td><td align='center' width='70'>"+data[i].loc_level+"</td><td align='center' width='70'>"+data[i].loc_name+"</td><td align='center' >"+data[i].loc_modtime+"</td><td align='center'><a href='?m=location&c=sys_loc&a=edit&id="+data[i].loc_id+"&pc_hash="+pc_hash+"'>修改</a> </td></tr>";
					var tbody=$("#tbody");
					tbody.append(tr);
					$("#myform").append(tr);
				}
			},'json');
	    return false;
		}
	});

	//选定地区后的表格内容
	$("#loc_area").change(function(){
		var v = $("#loc_area").val();
		if(v.length!=0){
			$.post('./index.php?m=location&c=sys_loc&a=init',{loc_area:v,pc_hash:pc_hash},function(data){
				for(i in data){
					//动态生成选定区域的地区表格
					$("tr").remove("#tr_nation");
					$("tr").remove("#tr_region");
					$("tr").remove("#tr_area");
					$("#btn").remove();
					// console.log(data);
					var tr="<tr id='tr_area'><td align='center'><input type='checkbox' name='id[]' value='"+data.loc_id+"'></td><td align='center'>"+data.loc_id+"</td><td align='center' width='70'>"+data.loc_level+"</td><td align='center' width='70'>"+data.loc_name+"</td><td align='center' >"+data.loc_modtime+"</td><td align='center'><a href='?m=location&c=sys_loc&a=edit&id="+data.loc_id+"&pc_hash="+pc_hash+"'>修改</a></td></tr>";
					var tbody=$("#tbody");
					tbody.append(tr);
					$("#myform").append(tr);
				}
			},'json');
		    return false;
		}
	});
</script>