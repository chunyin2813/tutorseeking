<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header','admin');
?>
<!--新增地区设置主页面-->
<style type="text/css">
	button{
		margin-left:20px; 
	}
	.click{
		background-color: #498CD0;
		color: white;
	}
</style>
<div class="pad-lr-10">
	<div class="table-list">
		<div class="loc_select">
			<button id="btn_add_nation">新增国籍</button>
			<button id="btn_add_region">新增区域</button>
			<button id="btn_add_area">新增地区</button>
		</div>
		<br />
		<!--新增国籍-->
		<div id="add_nation">
			<form method="post" action="?m=location&c=sys_loc&a=add">
			<table>
				<tr>
					<td>新增国籍:</td>
					<td><input name="loc_nation_name" class="input-text" type="text" size="25" required></td>
				</tr>
				<tr>
					<td>新增排序:</td>
					<td><input name="loc_seq" class="input-text" type="text" size="25" required></td>
				</tr>
				<tr>
					<td>更新时间:</td>
					<td><input name="loc_modtime" id="loc_modtime" class="input-text" type="text" size="25" value="<?php echo date("Y-m-d H:i:s")?>" readonly="readonly" >
					</td>
				</tr>
				<tr>
					<td><input type="submit" name="nation_submit" id="dosubmit" value="提交"></td>
					<td><INPUT name="pclog" type="button" value="返回" onClick="location.href='index.php?m=location&c=sys_loc&a=init&pc_hash=<?php echo $_SESSION['pc_hash']?>'"></td>
				</tr>
			</table>	
			</form>
		</div>
		<!--新增区域-->
		<div id="add_region">
			<form method="post" action="?m=location&c=sys_loc&a=add">
			<table>
				<tr>
					<td>国籍名称:</td>
					<td>
						<select name="loc_nation">
							<option value="">--选择国籍--</option>
							<?php foreach ($infos_nation as $infos) {	?>
							<option value="<?php echo $infos['loc_id']?>"><?php echo $infos['loc_name']?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td>新增区域:</td>
					<td><input name="loc_region_name" class="input-text" type="text" size="25" required></td>
				</tr>
				<tr>
					<td>新增排序:</td>
					<td><input name="loc_seq" class="input-text" type="text" size="25" required></td>
				</tr>
				<tr>
					<td>更新时间:</td>
					<td>
						<input name="loc_modtime" id="loc_modtime" class="input-text" type="text" size="25" value="<?php echo date("Y-m-d H:i:s")?>" readonly="readonly" >
					</td>
				</tr>
				<tr>
					<td><input type="submit" name="region_submit" id="dosubmit" value="提交"></td>
					<td><INPUT name="pclog" type="button" value="返回" onClick="location.href='index.php?m=location&c=sys_loc&a=init&pc_hash=<?php echo $_SESSION['pc_hash']?>'"></td>
				</tr>
			</table>	
			</form>
		</div>
		<!--新增地区-->
		<div id="add_area">
			<form method="post" action="?m=location&c=sys_loc&a=add">
			<table>
				<tr>
					<td>国籍名称:</td>
					<td>
						<select name="loc_nation" id="loc_nation">
							<option value="">--选择国籍--</option>
							<?php foreach ($infos_nation as $info) {	?>
							<option value="<?php echo $info['loc_id']?>"><?php echo $info['loc_name']?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td>区域名称:</td>
					<td>
						<select name="loc_region" id="loc_region">
							<option value="">--选择区域--</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>新增地区:</td>
					<td>
						<input name="loc_area_name" class="input-text" type="text" size="25" required>
					</td>
				</tr>
				<tr>
					<td>新增排序:</td>
					<td><input name="loc_seq" class="input-text" type="text" size="25" required></td>
				</tr>
				<tr>
					<td>更新时间:</td>
					<td>
						<input name="loc_modtime" id="loc_modtime" class="input-text" type="text" size="25" value="<?php echo date("Y-m-d H:i:s")?>" readonly="readonly" >
					</td>
				</tr>
				<tr>
					<td><input type="submit" name="area_submit" id="dosubmit" value="提交"></td>
					<td><INPUT name="pclog" type="button" value="返回" onClick="location.href='index.php?m=location&c=sys_loc&a=init&pc_hash=<?php echo $_SESSION['pc_hash']?>'"></td>
				</tr>
			</table>	
			</form>
		</div>
	</div>	
</div>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){
		$("#add_nation").show();
		$("#add_region").hide();
		$("#add_area").hide();
	});
	$("#btn_add_nation").click(function(){
		$("#add_nation").show();
		$("#add_region").hide();
		$("#add_area").hide();
		$('button').attr('class','');
		$(this).attr('class','click');
	});
	$("#btn_add_region").click(function(){
		$("#add_nation").hide();
		$("#add_region").show();
		$("#add_area").hide();
		$('button').attr('class','');
		$(this).attr('class','click');
	});
	$("#btn_add_area").click(function(){
		$("#add_nation").hide();
		$("#add_region").hide();
		$("#add_area").show();
		$('button').attr('class','');
		$(this).attr('class','click');
	});

	var pc_hash = "<?php echo $_SESSION['pc_hash']?>";
	$("#loc_nation").change(function(){
		var v = $("#loc_nation").val();
		if(v){
			$.post('./index.php?m=location&c=sys_loc&a=add2',{loc_nation:v,pc_hash:pc_hash},function(data){
				$("#loc_region").html(' ');
				$("#loc_region").append('<option value="">--选择区域--</option>');
				for(i in data){
					//动态添加区域下拉框
					var course_option="<option class='c_option' value='"+data[i].loc_id+"'>"+data[i].loc_name+"</option>"	;	
					$("#loc_region").append(course_option);
				}
			},'json');
	    	return false;
	    }else{
	    	$("#loc_region").html(' ');
			$("#loc_region").append('<option value="">--选择区域--</option>');
	    }
	});
</script>