<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header','admin');
?>
<!--導師可教授地區设置主页面-->
<div class="pad-lr-10">
	<div class="table-list">
		<a href="?m=location&c=tutor_loc&a=add"><button>新增</button></a>
		<br /><br />
		選擇導師:
		<select name="tutor_name" id="tutor_name">
			<option>--請選擇導師--</option>
			<?php foreach ($members as $member) {	?>
			<option value="<?php echo $member['tutor_id'];?>"><?php echo $member['tutor_gname'];?></option>
			<?php }?>
		</select>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		選擇個案:
		<select name="tutor_gean" id="tutor_gean">
			<option>--請選擇個案--</option>
		</select>
		<br /><br />
	    <table width="100%" cellspacing="0" class="contentWrap">
	        <thead>
	            <tr>
	            <th width="30" align="center"><input type="checkbox" value="" id="check_box" onclick="selectall('id[]');"></th>
				<th width="35">ID</th>
				<th width="100" align="center">導師可教授地區級別</th>
				<th width="100" align="center">導師可教授地區名稱</th>
				<th width="100" align="center">更新時間</th>
				<th width="110" align="center">管理操作</th>
	            </tr>
	        </thead>
	        <tbody id="tbody"> 
			</tbody>
	    </table>
	    <br />
	    <div id="btn">
	    	<td><label for="check_box">全选/取消</label>&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="submit" name="allsubmit" id="allsubmit" value="删除"></td>&nbsp;&nbsp;
		</div>  
	</div>
	<div id="pages"><?php echo $pages;?></div>
</div>
</body>
<style type="text/css">

</style>
</html>
<script type="text/javascript">
	var pc_hash = "<?php echo $_SESSION['pc_hash']?>";
	$("#tutor_name").change(function(){
		var tutor_id=$("#tutor_name").val();
		$.post('./index.php?m=location&c=tutor_loc&a=ajax_addgean',{tutor_id:tutor_id,pc_hash:pc_hash},function(data){
			$("#tutor_gean").html(' ');
			$("#tutor_gean").append('<option>--請選擇個案--</option>');
			if(data.length!==0){
				for(i in data){
				//动态添加区域下拉框
				var course_option="<option class='c_option' value='"+data[i].tm_tutorid+"'>"+data[i].tm_tutorid+"</option>"	;	
				$("#tutor_gean").append(course_option);
				}
			}
		},'json');
    		return false;
    	});

		$("#tutor_gean").change(function(){
			var gean = $("#tutor_gean").val();
			var tutor_id = $("#tutor_name").val();
			$.post('./index.php?m=location&c=tutor_loc&a=ajax_gean',{gean:gean,tutor_id:tutor_id,pc_hash:pc_hash},function(datas){
				for(var j in datas){
					var data = datas[j];
					for(var i in data){
				

				var tr="<tr id='tr_region'><td align='center'><input type='checkbox' name='id[]' value='"+data[i].loc_id+"'></td><td align='center'>"+data[i].loc_id+"</td><td align='center' width='70'>"+data[i].loc_level+"</td><td align='center' width='70'>"+data[i].loc_name+"</td><td align='center' >"+data[i].loc_modtime+"</td><td align='center'><a href='?m=location&c=tutor_loc&a=edit&id="+data[i].loc_id+"&pc_hash="+pc_hash+"'>修改</a> | <a href='?m=location&c=tutor_loc&a=delete&id="+data[i].loc_id+"&pc_hash="+pc_hash+"'>删除</a></td></tr>";
				var btn="<div class='btn' id='btn'><label for='check_box'>全选/取消</label>&nbsp;&nbsp;&nbsp;&nbsp;<input type='submit' name='allsubmit' id='allsubmit' value='删除'>&nbsp;&nbsp;</div>";
				var tbody=$("#tbody");
				tbody.append(tr);
				$("#myform").append(tr);
				$("#myform").append(btn);
					}
				}
			},'json');
		});

		
	
</script>