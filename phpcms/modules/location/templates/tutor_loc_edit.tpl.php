<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header','admin');
?>
<style type="text/css">
#btn{
	margin-left:45%; 
}
</style>



<!--修改导师可教授地区设置主页面-->
<div class="pad-lr-10">
	<div class="table-list">
	<p>修改可教授地区:</p>
	<br/>
	<div>
		<button><a href="javascript:void(0);" name="loc-add" class="loc-add">新增</a></button>&nbsp;&nbsp;
		<button><a href="javascript:void(0);" name="loc-del" class="loc-del">刪除</a></button>&nbsp;&nbsp;
	</div>
	<br />
	<form id="loc_form">
	<input type="hidden" value="<?php echo $info['loc_id'];?>" name="tutor_loc_id">
	<table class="table_form" width="100%" cellspacing="0">
		
		<tr class="old_tr">
			<td>
				請選擇國家:
				<select name="loc_nation" class="loc loc_nation">
				<option>-選擇國家-</option>
				<?php foreach ($infos_nation as $info) { ?>
				<option value="<?php echo $info['loc_id']?>"><?php echo $info['loc_name']?></option>
				<?php } ?>	
				</select>
			</td>
			<td>
				請選擇區域:
				<select name="loc_region" class="loc loc_region">
				<option>-選擇區域-</option>
				</select>
			</td>
			<td>
				請選擇地區:
				<select name="loc_area" class="loc loc_area">
				<option>-選擇地區-</option>
				</select>
			</td>
			<td>
				請選擇分區:
				<select name="loc_house" class="loc loc_house">
				<option>-選擇分區-</option>
				</select>
			</td>
		</tr>
		
	</table>
	<br />
	<div id="btn">
		<button><a href="javascript:void(0);"class="edit_submit">確認</a></button>&nbsp;&nbsp;
		<!-- <button><a href="javascript:void(0);" class="cancel">取消</a></button> --><INPUT name="pclog" type="button" value="返回" onClick="location.href='index.php?m=location&c=tutor_loc&a=init&pc_hash=<?php echo $_SESSION['pc_hash']?>'">&nbsp;&nbsp;
	</div>
	</form>
	<!-- <div id="pages"><?php echo $pages;?></div> -->
</div>
</body>
</html>
<script type="text/javascript">
$(document).ready(function(){
	var pc_hash = "<?php echo $_SESSION['pc_hash']?>";

	//国家联动区域下拉框
	$(".loc_nation").change(function(){
		var v = $(".loc_nation").val();
		// var v = $(this).val();
		$.post('./index.php?m=location&c=tutor_loc&a=ajax_add',{loc_id:v,pc_hash:pc_hash},function(data){
			$(".loc_region").html(' ');
			$(".loc_region").append('<option>--選擇區域--</option>');
			if(data.length!==0){
				for(i in data){
				//动态添加区域下拉框
				var course_option="<option class='c_option' value='"+data[i].loc_id+"'>"+data[i].loc_name+"</option>"	;	
				$(".loc_region").append(course_option);
				}
			}
		},'json');
    return false;
	});

	//区域联动地区下拉框
	$(".loc_region").change(function(){
		var v = $(".loc_region").val();
		$.post('./index.php?m=location&c=tutor_loc&a=ajax_add',{loc_id:v,pc_hash:pc_hash},function(data){
			$(".loc_area").html(' ');
			$(".loc_area").append('<option>--選擇地區--</option>');
			if(data.length!==0){
				for(i in data){
					//动态添加地区下拉框
					var course_option="<option class='c_option' value='"+data[i].loc_id+"'>"+data[i].loc_name+"</option>";
					$(".loc_area").append(course_option);
				}
			}
		},'json');
    return false;
	});

	//地区联动屋苑下拉框
	$(".loc_area").change(function(){
		var v = $(".loc_area").val();
		// var v = $(this).val();
		$.post('./index.php?m=location&c=tutor_loc&a=ajax_add',{loc_id:v,pc_hash:pc_hash},function(data){
			$(".loc_house").html(' ');
			$(".loc_house").append('<option>--選擇分區--</option>');
			if(data.length!==0){
				for(i in data){
					//动态添加地区下拉框
					var course_option="<option class='c_option' value='"+data[i].loc_id+"'>"+data[i].loc_name+"</option>";
					$(".loc_house").append(course_option);
				}
			}
		},'json');
    return false;
	});

	//下拉框选择屋苑
	$(".loc_house").change(function(){
		var v = $(".loc_house").val();
	});

	//新增下拉框
	$(".loc-add").click(function(){
		var tr = $(".table_form tr").eq(0).clone();
		tr.appendTo(".table_form"); 
	});

	//刪除下拉框
	$(".loc-del").click(function(){
		var index = $("tr").index(this);
		$(".table_form tr").eq(index).remove();
	});

	//重新綁定事件
	 $(".table_form").on("change",".loc",function(){       
		var name = $(this).attr('name');
		if(name == "loc_nation"){
			var th = $(this);
			var v = $(this).val();
			var index_th=$(".loc_nation").index(this);
			var index_1=$(".loc_nation").index(this)-1;
			var index = index_th-index_1;
			$.post('./index.php?m=location&c=tutor_loc&a=ajax_add',{loc_id:v,pc_hash:pc_hash},function(data){
				var nextloc = th.parent().parent().children().eq(index).find('select');     
				// console.log(th.parent().parent().children().eq(index+1).html()); 
				nextloc.html(' ');
				nextloc.append('<option>--選擇區域--</option>');
				if(data.length!==0){
					for(i in data){
					//动态添加区域下拉框
					var course_option="<option class='c_option' value='"+data[i].loc_id+"'>"+data[i].loc_name+"</option>"	;	
					nextloc.append(course_option);
					}
				}
			},'json');

		}else if(name == "loc_region"){
			var v = $(this).val();
			var th = $(this);
			var index_th=$(".loc_region").index(this);
			var index_1=$(".loc_region").index(this)-1;
			var index = index_th-index_1;
			$.post('./index.php?m=location&c=tutor_loc&a=ajax_add',{loc_id:v,pc_hash:pc_hash},function(data){
				var nextloc = th.parent().parent().children().eq(index+1).find('select');
				nextloc.html(' ');
				nextloc.append('<option>--選擇地區--</option>');
				if(data.length!==0){
					for(i in data){
					//动态添加区域下拉框
					var course_option="<option class='c_option' value='"+data[i].loc_id+"'>"+data[i].loc_name+"</option>"	;	
					nextloc.append(course_option);
					}
				}
			},'json');
		}else if(name == "loc_area"){
			var v = $(this).val();
			var th = $(this);
			var index_th=$(".loc_area").index(this);
			var index_1=$(".loc_area").index(this)-1;
			var index = index_th-index_1;
			$.post('./index.php?m=location&c=tutor_loc&a=ajax_add',{loc_id:v,pc_hash:pc_hash},function(data){
				var nextloc = th.parent().parent().children().eq(index+2).find('select');
				nextloc.html(' ');
				nextloc.append('<option>--選擇分區--</option>');
				if(data.length!==0){
					for(i in data){
					//动态添加区域下拉框
					var course_option="<option class='c_option' value='"+data[i].loc_id+"'>"+data[i].loc_name+"</option>"	;	
					nextloc.append(course_option);
					}
				}
			},'json');
		}
	});


	$(".edit_submit").click(function(){
		var loc_id=[] ;
	    $(".loc_house option:selected").each(function () {  
	        loc_id.push($(this).val());
	        // console.log(loc_id.length);
	        // // return;  
	        if(loc_id.length>10){
	        	alert('請勿選擇超過十個地區');
	        	setInterval(window.history.back(-1), 10000);
	        }
	    });
	    $.post('./index.php?m=location&c=tutor_loc&a=ajax_edit',{loc_id:loc_id,pc_hash:pc_hash},function(data){
			 switch(data) { 
                case 1: 
                alert("修改成功!"); 
                // setInterval(window.history.back(-1), 10000);
                break;
                case 2:
                alert("修改失败!"); 
                break;
                case 3:
                alert('請勿重複選擇地區');
                break;
            }
		},'json');
	});

	

});
</script>