<?php 
defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_app_class('admin','admin',0);

class sys_loc extends admin {
	function __construct() {
        parent::__construct();
		$this->module_db = pc_base::load_model('module_model');
		$this->db = pc_base::load_model('sys_location_model');
    }

    public function init(){
    	//下拉框国籍内容
    	$infos_nation = $this->db->select(array('loc_level'=>1));
        // var_dump($infos_nation);
    	//表格内容
    	$infos = $infos_nation;
    	//联动地区
        if(isset($_POST['loc_nation'])){
            // $info=$this->db->get_one(array('loc_name'=>$_POST['loc_nation']));
            $info['loc_id'] = $_POST['loc_nation'];
            $infos_nation=$this->db->select(array('loc_parentid'=>$info['loc_id']),'*','','loc_seq asc');
            $infos_nation=json_encode($infos_nation);
            echo $infos_nation;
            exit;
        }
        if(isset($_POST['loc_region'])){
            // $info=$this->db->get_one(array('loc_name'=>$_POST['loc_region']));
            $info['loc_id'] = $_POST['loc_region'];
            $infos_region=$this->db->select(array('loc_parentid'=>$info['loc_id']),'*','','loc_seq asc');
            $infos_region=json_encode($infos_region);
            echo $infos_region;
            exit;
        }
        if(isset($_POST['loc_area'])){
            $info=$this->db->get_one(array('loc_id'=>$_POST['loc_area']),'*','','loc_seq asc');
            $infos_area=json_encode($info);
            echo $infos_area;
            exit;
        }
    	include $this->admin_tpl('sys_loc'); 	
    }

    public function add(){
    	//下拉框国籍内容
    	$infos_nation = $this->db->select(array('loc_level'=>1));
    	if(isset($_POST['nation_submit'])){
    		$data['loc_name'] = $_POST['loc_nation_name'];
    		$data['loc_modtime'] = $_POST['loc_modtime'];
    		$data['loc_parentid']= 0;
    		$data['loc_seq'] = $_POST['loc_seq'];
    		$res=$this->db->insert($data);
    		showmessage(L('add_success'),HTTP_REFERER);
    	}else if(isset($_POST['region_submit'])){
    		//选择国籍
            // var_dump($_POST);
            // exit;
    		$data['loc_level']=2;
    		$data['loc_parentid']= $_POST['loc_nation'];
            if(empty($data['loc_parentid'])){
                showmessage('請選擇國籍',HTTP_REFERER);
            }
    		$data['loc_name'] = $_POST['loc_region_name'];
    		$data['loc_modtime'] = $_POST['loc_modtime'];
    		$data['loc_seq'] = $_POST['loc_seq'];
    		$res=$this->db->insert($data);
    		showmessage(L('add_success'),HTTP_REFERER);
    	}else if(isset($_POST['area_submit'])){
            $data['loc_level']=3;
            $data['loc_parentid']= $_POST['loc_region'];
            if(empty($_POST['loc_region']) || empty($_POST['loc_nation'])){
                showmessage('請選擇國籍或區域',HTTP_REFERER);
            }
    		$data['loc_name'] = $_POST['loc_area_name'];
    		$data['loc_modtime'] = $_POST['loc_modtime'];
    		$data['loc_seq'] = $_POST['loc_seq'];
            $data['loc_google_coord'] = $_POST['loc_google_coord'];
    		$res=$this->db->insert($data);
    		showmessage(L('add_success'),HTTP_REFERER);
    	}
    	include $this->admin_tpl('sys_loc_add'); 
    }

    public function add2(){
    	if(isset($_POST['loc_nation'])){
    		$infos_region=$this->db->select(array('loc_parentid'=>$_POST['loc_nation']));
    		$infos_region=json_encode($infos_region);
    		echo $infos_region;
    		exit;
    	}
    	include $this->admin_tpl('sys_loc_add'); 
    }

    public function edit(){
    	if(isset($_GET['id'])){
    		$info=$this->db->get_one(array('loc_id'=>$_GET['id']));
    		$loc_level=$info['loc_level'];
	    	if($loc_level==2){
	    		$infos_nation=$this->db->select(array('loc_level'=>1));
	    		$id = intval($_GET['id']);
	            $info2 = $this->db->get_one(array('loc_id'=>$id));
	    	}else if($loc_level==3){
                $infos_nation=$this->db->select(array('loc_level'=>1));
                $id = intval($_GET['id']);
                $info3 = $this->db->get_one(array('loc_id'=>$id));
            }else{
                $info = $this->db->get_one(array('loc_id'=>$_GET['id']));
            }
            extract($info);
            extract($info2);
            extract($info3);
            //當前地區的區域
            $info3_region = $this->db->get_one(array('loc_id'=>$info3['loc_parentid']));
            //當前區域的國籍
            $info3_nation = $this->db->get_one(array('loc_id'=>$info3_region['loc_parentid']));
            //當前國籍的所有區域
            $info3_regions = $this->db->select(array('loc_parentid'=>$info3_nation['loc_id']));
            include $this->admin_tpl('sys_loc_edit');
    	}
    }	

    //修改国籍
    public function edit1(){
    	if(isset($_POST['nationsubmit'])){
			$where=array();
            $where['loc_id']=$_POST['loc_id'];
            $data['loc_level'] = 1;
            $data['loc_name'] = $_POST['loc_nation_name'];
            $data['loc_modtime'] = $_POST['loc_modtime'];
            $data['loc_seq'] = $_POST['loc_seq'];
            $result=$this->db->update($data,$where);
            if($result){
                showmessage('修改数据成功',HTTP_REFERER);
            }else{
                showmessage('修改数据失败',HTTP_REFERER);
            }
        }
    }  

    //修改区域
    public function edit2(){
        if(isset($_POST['regionsubmit'])){
        	$data['loc_level'] = 2;
            $data['loc_seq'] = $_POST['loc_seq'];
            $data['loc_name'] = $_POST['loc_region_name'];
            $data['loc_modtime'] = $_POST['loc_modtime'];
            $data['loc_parentid']=$_POST['loc_nation'];
            $where_edit['loc_id']=$_POST['loc_id'];
            if(empty($_POST['loc_nation'])){
                showmessage('請選擇國籍',HTTP_REFERER);
            }
            $result=$this->db->update($data,$where_edit);
            if($result){
                showmessage('修改数据成功',HTTP_REFERER);
            }else{
                showmessage('修改数据失败',HTTP_REFERER);
            }
        }
    }

//修改地区
    public function edit3(){
        if(isset($_POST['areasubmit'])){
            //区域
            $info_region['loc_id'] = $_POST['loc_nation'];
            //地区
            $info_area = $this->db->get_one(array('loc_parentid'=>$info_region['loc_id'],'loc_id'=>$_POST['loc_region']));
            $data['loc_seq'] = $_POST['loc_seq'];  
            $data['loc_name'] = $_POST['loc_area_name'];
            $data['loc_modtime'] = $_POST['loc_modtime'];
            $data['loc_parentid'] = $info_area['loc_id'];
            $data['loc_google_coord'] = $_POST['loc_google_coord'];
            $data['loc_level'] = 3;
            $where_edit['loc_id']=$_POST['loc_id'];
            if(empty($_POST['loc_region']) || empty($_POST['loc_nation'])){
                showmessage('請選擇國籍或區域',HTTP_REFERER);
            }
            $result=$this->db->update($data,$where_edit);
            if($result){
                showmessage('修改数据成功',HTTP_REFERER);
            }else{
                showmessage('修改数据失败',HTTP_REFERER);
            }
        } 
	}
    	
	//联动下拉框
    public function edit_select(){
        if($_POST){
            // $infos=$this->db->get_one(array('loc_name'=>$_POST['loc_nation']));
            $infos['loc_id']=$_POST['loc_nation'];
            $infos_course=$this->db->select(array('loc_parentid'=>$infos['loc_id']));
            $str=json_encode($infos_course);
            echo $str;
            exit;
        }
        include $this->admin_tpl('sys_loc_edit'); 
    }

    //删除
    public function delete($where) {
        // var_dump($_GET);
        // exit;
    	if(isset($_GET)){
    		$where=array();
    		$where['loc_id']=intval($_GET['id']);
            $this->db->delete(array('loc_parentid'=>$where['loc_id']));
			$this->db->delete($where);
			showmessage('删除成功',HTTP_REFERER);
    	}else{
    		showmessage('删除失败',HTTP_REFERER);
    	}
	}

    //全选删除
    public function delete_all(){
        // var_dump($_POST);die;
        if (!isset($_POST['id']) || !is_array($_POST['id'])) {
            showmessage(L('illegal_parameters'), HTTP_REFERER);
        } else {
            array_map(array($this, _del), $_POST['id']);
        }
        showmessage(L('operation_success'), HTTP_REFERER);
    }

    private function _del($id = 0) {
        $id = intval($id);
        // var_dump($id);
        if (!$id) return false;
        $ids=$this->db->select(array('loc_parentid'=>$id));
        //删除三级地区
        foreach ($ids as $k => $v) {
           $this->db->delete(array('loc_parentid'=>$v['loc_id']));
        }
        //删除二级地区
        $this->db->delete(array('loc_parentid'=>$id));
        //删除一级地区
        $this->db->delete(array('loc_id'=>$id));  
    }

}
?>