<?php
error_reporting(E_ALL);
defined('IN_PHPCMS') or exit('Access Denied');
defined('INSTALL') or exit('Access Denied');

$parentid = $menu_db->insert(array('name'=>'sys_location', 'parentid'=>'29', 'm'=>'location', 'c'=>'sys_loc', 'a'=>'init', 'data'=>'', 'listorder'=>0, 'display'=>'1'), true);

$menu_db->insert(array('name'=>'tutor_location', 'parentid'=>$parentid, 'm'=>'location', 'c'=>'tutor_loc', 'a'=>'init', 'data'=>'', 'listorder'=>0, 'display'=>'1'));

$menu_db->insert(array('name'=>'stu_location', 'parentid'=>$parentid, 'm'=>'location', 'c'=>'stu_loc', 'a'=>'init', 'data'=>'', 'listorder'=>0, 'display'=>'1'));

$language = array('sys_location'=>'地区设置','tutor_location'=>'导师要求授课地区','stu_location'=>'学生要求授课地区');
?>