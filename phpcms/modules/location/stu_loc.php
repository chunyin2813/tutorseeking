<?php 
defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_app_class('admin','admin',0);

class stu_loc extends admin {
	function __construct() {
        parent::__construct();
        $this->m_db = pc_base::load_model('student_master_model');
        $this->s_db = pc_base::load_model('sys_location_model');
        $this->module_db = pc_base::load_model('module_model');
    }

    public function init(){
    	$members=$this->m_db->select();
    	include $this->admin_tpl('stu_loc'); 
    }

    public function ajax_init(){
        if(isset($_POST['student_name_v'])){
            //选择的學生名称存入session
            $_SEESSION['student_name_v']=$_POST['student_name_v'];
            $member_info=$this->m_db->get_one(array('student_name'=>$_SEESSION['student_name_v']));
            $living_locid=$member_info['student_locid'];
            $member_master= $this->s_db->select(array('loc_id'=>$living_locid));
            $member_master=json_encode($member_master);
            echo $member_master;
            exit;
        }
    }

    public function add(){
        $infos_nation = $this->s_db->select(array('loc_level'=>1));
        //联动地区
        if(isset($_POST['loc_type'])){
            $info=$this->s_db->get_one(array('loc_name'=>$_POST['loc_type']));
            $infos_region=$this->s_db->select(array('loc_parentid'=>$info['loc_id']));
            $infos_region=json_encode($infos_region);
            echo $infos_region;
            exit;
        }
        include $this->admin_tpl('stu_loc_add'); 
    }

    public function sure_add(){
        //获取导师名称
        $members=$this->m_db->select();
        // var_dump($members);
        //获取选中导师新增信息
        if(isset($_POST['submit'])){
            $student_name=$_POST['student_name_add'];
            $loc_nation = $_POST['loc_nation_add'];
            $loc_region = $_POST['loc_region_add'];
            $loc_area = $_POST['loc_area_add'];
            $loc_house = $_POST['loc_house_add'];
            $modtime=date("Y-m-d H:i:s");
            //获取新增地区id
            if(empty($loc_house)){
                $tutor_loc=$this->s_db->get_one(array('loc_name'=>$loc_area));
            }
            if(empty($loc_house) && empty($loc_area)){
                $tutor_loc=$this->s_db->get_one(array('loc_name'=>$loc_region));
            }
            if(empty($loc_house) && empty($loc_area) && empty($loc_region)){
                $tutor_loc=$this->s_db->get_one(array('loc_name'=>$loc_nation));
            }
            $loc_id=$tutor_loc['loc_id'];
            //获取新增导师id
            $student_info=$this->m_db->get_one(array('student_name'=>$student_name));
            $student_id=$student_info['tutor_id'];
            //新增

            $this->m_db->insert($data);
            showmessage('新增成功',HTTP_REFERER);
        }
    }

   public function edit(){
        if(isset($_POST['edit_submit'])){
            //获取修改地区id
            $loc_old_id=$_POST['student_loc_id'];
            $where['loc_id']=$loc_old_id;
           //获取修改导师名称
            $student_id_name=$_SEESSION['tutor_name_v'];
            //获取修改导师id
            $student_info=$this->m_db->get_one(array('tutor_name'=>$tutor_name));
            $student_id=$student_info['student_id'];
            $where['student_id']=$student_id;
            //提交修改的数据
            $loc_nation = $_POST['loc_nation_add'];
            $loc_region = $_POST['loc_region_add'];
            $loc_area = $_POST['loc_area_add'];
            $loc_house = $_POST['loc_house_add'];
            $modtime=date("Y-m-d H:i:s");
            //获取新增地区id
            if(empty($loc_house)){
                $student_loc=$this->s_db->get_one(array('loc_name'=>$loc_area));

            }
            if(empty($loc_house) && empty($loc_area)){
                $student_loc=$this->s_db->get_one(array('loc_name'=>$loc_region));

            }
            if(empty($loc_house) && empty($loc_area) && empty($loc_region)){
                $student_loc=$this->s_db->get_one(array('loc_name'=>$loc_nation));
            }
            $loc_new_loc=$student_loc['loc_id'];
            $loc_new_id=$student_loc['loc_id'];
            if(!empty($loc_house)){
                $student_loc=$this->s_db->get_one(array('loc_name'=>$loc_house));
                $loc_new_id=$student_loc['loc_id'];
                $loc_new_locid=$student_loc['loc_parentid'];
            }
            $data['student_locid']=$loc_new_locid;
            $data['student_living_locid']=$loc_new_id;
            $result=$this->m_db->update($data,$where);
            if($result){
                showmessage('修改数据成功',HTTP_REFERER,'','edit');
            }else{
                showmessage('修改数据失败',HTTP_REFERER);
            }
        }else{
            $infos_nation = $this->s_db->select(array('loc_level'=>1));
            $id = intval($_GET['id']);
            $info = $this->s_db->get_one(array('loc_id'=>$id));
            if(!$info){
                showmessage('没有找到数据');
            }
            extract($info);
            include $this->admin_tpl('stu_loc_edit');
        } 
    }

    //删除
    public function delete($where) {
        if(isset($_GET)){
            $where=array();
            $where['student_locid']=intval($_GET['id']);
            $this->m_db->delete($where);
            showmessage('删除成功',HTTP_REFERER);
        }else{
            showmessage('删除失败',HTTP_REFERER);
        }
    }

    //全选删除
    public function delete_all(){
         // var_dump($_POST);die;
        if (!isset($_POST['id']) || !is_array($_POST['id'])) {
            showmessage(L('illegal_parameters'), HTTP_REFERER);
        } else {
            array_map(array($this, _del), $_POST['id']);
        }
        showmessage(L('operation_success'), HTTP_REFERER);
    }

    private function _del($id = 0) {
        $id = intval($id);
        if (!$id) return false;
        $this->m_db->delete(array('student_id'=>$id));
    }
} 
?>