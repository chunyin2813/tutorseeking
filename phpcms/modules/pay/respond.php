<?php
defined('IN_PHPCMS') or exit('No permission resources.'); 
class respond {
	
	
	function __construct() {
		pc_base::load_app_func('global');
		$this->pair = pc_base::load_model('pair_model');
		$this->tor = pc_base::load_model('tutor_transaction_model');
		$this->std = pc_base::load_model('student_transaction_model');
		$this->T_Master = pc_base::load_model('tutor_master_model');
		$this->S_Master = pc_base::load_model('student_master_model');
		$this->Member = pc_base::load_model('member_model');
		$this->paypal = pc_base::load_model('paypal_return_model');
	}
	
	public function respond_post() {
		// $data = file_get_contents('pay.log');
		// $_POST = json_decode($data,true);

		// exit;



		// 发送郵件
		// pc_base::load_app_class('tutor','pair');					//實例化導師類
		// $tutor = new tutor();
		// $Pairing = $tutor->getPairingData('85',2);					//獲取配對信息

		// $Pairing = $tutor->structurePairingData($Pairing,2);			//處理配對數據

		// // dump($Pairing);

		// pc_base::load_sys_func('mail');								//實例化郵件類
		// $PairingData = getPairingMailData($Pairing['info']);		//獲取配對郵件數據

		// echo $PairingData;exit;
		// sendmail($Pairing['email']['tutor_email'],'TutorSeekIng配對信息', $PairingData);		//导师
		// sendmail($Pairing['email']['stu_email'],'TutorSeekIng配對信息', $PairingData);			//学生
		// sendmail('451870143@qq.com','TutorSeekIng配對信息', $PairingData);			//管理员
		// exit;
		// $integralOperation = pc_base::load_app_class('integralOperation');
		// $userData = $integralOperation->GetUserData('3','tor_public_pay');
		// $integralOperation->integralOperation($userData['userid'],$userData['Integral'],'-');


		// exit;


		$bool = 'fail';
        $req = 'cmd=_notify-validate';

        // file_put_contents('pay.log', json_encode($_POST));exit;
// $str = '{"mc_gross":"0.01","invoice":"P70","protection_eligibility":"Eligible","payer_id":"QCME7FYM8Y2U6","payment_date":"01:13:02 Dec 08, 2017 PST","payment_status":"Completed","charset":"Big5","first_name":"Jason","mc_fee":"0.01","notify_version":"3.8","custom":"","payer_status":"unverified","business":"jason@indi-house.com.hk","quantity":"1","verify_sign":"AgX7vLQ3UrwYJIBaela6uuPnu7WIA6syHmEU-3FRiTfjr47AGF8rCj25","payer_email":"jason_hkl@mac.com","txn_id":"1HJ57432TE264282J","payment_type":"instant","last_name":"Ho","receiver_email":"jason@indi-house.com.hk","payment_fee":"","shipping_discount":"0.00","receiver_id":"VQBQW6M4QYZEG","insurance_amount":"0.00","txn_type":"web_accept","item_name":"std_agree_pay","discount":"0.00","mc_currency":"HKD","item_number":"","residence_country":"HK","shipping_method":"Default","transaction_subject":"","payment_gross":"","ipn_track_id":"e8b094d617086"}';
//         $_POST = json_decode($str,true);

        // dump($_POST);
    if($_POST){
        foreach ($_POST as $k => $v)
        {
            $v = urlencode(stripslashes($v));
            $req .= "&{$k}={$v}";
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://www.paypal.com/cgi-bin/webscr');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        $res = curl_exec($ch);
        curl_close($ch);
        // dump(strcmp($res, 'VERIFIED') == 0);
        // exit;
		// echo $data;exit;
        $postInvoice = substr($_POST['invoice'],1);//截取实际订单号

		$item_name = $_POST['item_name'];		
		$item_number = $_POST['item_number'];
		$payment_status = $_POST['payment_status'];
		$payment_date = $_POST['payment_date'];
		$payment_amount = $_POST['mc_gross'];
		$payment_currency = $_POST['mc_currency'];
		$txn_id = $_POST['txn_id'];
		$receiver_email = $_POST['receiver_email'];
		$receiver_id = $_POST['business'];
		$payer_email = $_POST['payer_email'];
		$order_sn = $postInvoice;
		$memo = !empty($_POST['memo']) ? $_POST['memo'] : '';
		$action_note = $txn_id . '（' . $GLOBALS['_LANG']['paypal_txn_id'] . '）' . $memo;

		
		$data = array(
				'order_sn'=>$order_sn,
				'payer_email'=>$payer_email,
				'item_name'=>$item_name,
				'currency'=>$payment_currency,
				'gross'=>$payment_amount,
				'status'=>$payment_status,
				'payment_time'=>$payment_date,
				'json_data'=>json_encode($_POST),
			);

		// $_POST['invoice'] = substr($_POST['invoice'],1);			//截取掉訂單號前面的拼接符
		// $order_sn = substr($order_sn,1);			//截取掉訂單號前面的拼接符
		//若支付成功 则更改订单等状态
		if($this->paypal->insert($data)){

			//判斷是否支付成功
 			if ($_POST['payment_status'] != 'Completed' && $_POST['payment_status'] != 'Pending') {
                    exit;
                }
			if($item_name == 'tor_public_pay'){			//导师发布
				$pair_data = array('tutor_pay'=>'1');
				$this->pair->update($pair_data,'pair_id = '.$order_sn);
			}elseif($item_name == 'std_public_pay'){	//学生发布
				$pair_data = array('student_pay'=>'1');
				$this->pair->update($pair_data,'pair_id = '.$order_sn);
			}elseif($item_name == 'tor_agree_pay'){
				//导师同意
				$pair_data = array('tutor_pay'=>'1','pair_state'=>'1');
				$this->pair->update($pair_data,'pair_id = '.$order_sn);//更改配对表导师付款状态
				
				$where = 'pair_id = '.$order_sn;
				$thispair = $this->pair->get_one($where);//查询当前配对表

				$tor_data = array('tm_matched'=>'1');//导师要求表更改字段
				$std_data = array('st_matched'=>'1');//学生要求表更改字段
				$tor_where = 'tm_tutorid = "'.$thispair['pair_tutor_tranid'].'"';
				$std_where = 'st_id = "'.$thispair['pair_student_tranid'].'"';
				$this->tor->update($tor_data,$tor_where);
				$this->std->update($std_data,$std_where);

				$HelpCommon = pc_base::load_app_class('helpcommon','pair');//加載配對輔助類
                //配对成功推荐人积分奖励
                $HelpCommon->activityReward($order_sn);
			}elseif($item_name == 'std_agree_pay'){
				//学生同意
				$pair_data = array('student_pay'=>'1','pair_state'=>'1');
				$this->pair->update($pair_data,'pair_id = '.$order_sn);////更改配对表学生付款状态

				$where = 'pair_id = '.$order_sn;
				$thispair = $this->pair->get_one($where);//查询当前配对表

				$tor_data = array('tm_matched'=>'1');//导师要求表更改字段
				$std_data = array('st_matched'=>'1');//学生要求表更改字段
				$tor_where = 'tm_tutorid = "'.$thispair['pair_tutor_tranid'].'"';
				$std_where = 'st_id = "'.$thispair['pair_student_tranid'].'"';
				$this->tor->update($tor_data,$tor_where);
				$this->std->update($std_data,$std_where);

				$HelpCommon = pc_base::load_app_class('helpcommon','pair');//加載配對輔助類
                //配对成功推荐人积分奖励
                $HelpCommon->activityReward($order_sn);
			}


			//发送郵件
			pc_base::load_app_class('tutor','pair');					//實例化導師類
			$tutor = new tutor();
			$Pairing = $tutor->getPairingData($order_sn);				//獲取配對信息
			$Pairing = $tutor->structurePairingData($Pairing);			//處理配對數據
			pc_base::load_sys_func('mail');								//實例化郵件類
			$PairingData = getPairingMailData($Pairing['info']);		//獲取配對郵件數據
			sendmail($Pairing['email']['tutor_email'],'TutorSeekIng配對信息', $PairingData);		//导师
			sendmail($Pairing['email']['stu_email'],'TutorSeekIng配對信息', $PairingData);			//学生
			sendmail('enquiry@tutorseeking.com','TutorSeekIng配對信息', $PairingData);  //发送邮件通知系统管理员
			}
		}
	}





	public function test() {


// 
		// 发送郵件
		pc_base::load_app_class('tutor','pair');					//實例化導師類
		$tutor = new tutor();
		$Pairing = $tutor->getPairingData('85',2);					//獲取配對信息

		$Pairing = $tutor->structurePairingData($Pairing,2);			//處理配對數據

		// dump($Pairing);

		pc_base::load_sys_func('mail');								//實例化郵件類
		$PairingData = getPairingMailData($Pairing['info']);		//獲取配對郵件數據

		// echo $PairingData;exit;
		// sendmail($Pairing['email']['tutor_email'],'TutorSeekIng配對信息', $PairingData);		//导师
		// sendmail($Pairing['email']['stu_email'],'TutorSeekIng配對信息', $PairingData);			//学生
		sendmail('451870143@qq.com','TutorSeekIng配對信息', $PairingData);			//管理员
		exit;
		
		// $data = file_get_contents('pay.log');
		// $_POST = json_decode($data,true);

		// exit;
		ini_set("display_errors", "On");
		error_reporting(E_ALL | E_STRICT);


		

		// 发送郵件
		pc_base::load_app_class('tutor','pair');					//實例化導師類
		$tutor = new tutor();
		$Pairing = $tutor->getPairingData('87');					//獲取配對信息

		$Pairing = $tutor->structurePairingData($Pairing);			//處理配對數據

		// // dump($Pairing);
// sendmail('451870143@qq.com','TutorSeekIng配對信息', $PairingData)
		pc_base::load_sys_func('mail');								//實例化郵件類
		$PairingData = getPairingMailData($Pairing['info']);		//獲取配對郵件數據

		// echo $PairingData;exit;
		// sendmail($Pairing['email']['tutor_email'],'TutorSeekIng配對信息', $PairingData);		//导师
		// sendmail($Pairing['email']['stu_email'],'TutorSeekIng配對信息', $PairingData);			//学生
		dump(sendmail('451870143@qq.com','TutorSeekIng配對信息', $PairingData));			//管理员
		// exit;
		// $integralOperation = pc_base::load_app_class('integralOperation');
		// $userData = $integralOperation->GetUserData('3','tor_public_pay');
		// $integralOperation->integralOperation($userData['userid'],$userData['Integral'],'-');


	}






}

?>