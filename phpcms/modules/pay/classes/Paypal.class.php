<?php 

defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_app_func('global','pay');
class Paypal{

   
    public function paypal($order){

        $Paypal['cmd'] = '_xclick';
        $Paypal['business'] = 'punkytsoi@tutorseeking.com';  
        $Paypal['item_name'] = $order['item_name'];         
        $Paypal['amount'] = $order['amount'];               
        $Paypal['currency_code'] = $order['currency_code']; 
        $Paypal['return'] = $order['return_url'];      
        $Paypal['invoice'] = $order['invoice'];         
        $Paypal['charset'] = 'utf-8';                   
        $Paypal['no_shipping'] = '1';                       
        $Paypal['no_note'] = $order['no_note'];         
        $Paypal['notify_url'] = APP_PATH.'index.php?m=pay&c=respond&a=respond_post';   
        $Paypal['rm'] = $order['rm'];                      
        $Paypal['cancel_return'] = APP_PATH.'index.php';
        
        $str = '<form id="one_payple" style="text-align:center;" name="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
            <input name="cmd" value="'.$Paypal['cmd'].'" type="hidden">
            <input name="business" value="'.$Paypal['business'].'" type="hidden">
            <input name="item_name" value="'.$Paypal['item_name'].'" type="hidden">
            <input name="amount" value="'.$Paypal['amount'].'" type="hidden">
            <input name="currency_code" value="'.$Paypal['currency_code'].'" type="hidden">
            <input name="return" value="'.$Paypal['return'].'" type="hidden">
            <input name="invoice" value="'.$Paypal['invoice'].'" type="hidden">
            <input name="charset" value="'.$Paypal['charset'].'" type="hidden">
            <input name="no_shipping" value="'.$Paypal['no_shipping'].'" type="hidden">
            <input name="no_note" value="'.$Paypal['no_note'].'" type="hidden">
            <input name="notify_url" value="'.$Paypal['notify_url'].'" type="hidden">
            <input name="rm" value="'.$Paypal['rm'].'" type="hidden">
            <input name="cancel_return" value="'.$Paypal['cancel_return'].'" type="hidden">
            <input id="sub" value="PayPal" type="submit"></form>';
        $str .= '<script>document.paypal.submit();</script>';
        return $str;
    }
}

?>