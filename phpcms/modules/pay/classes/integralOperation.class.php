<?php
defined('IN_PHPCMS') or exit('No permission resources.'); 
class integralOperation {
		
	function __construct() {
		pc_base::load_app_func('global');
		pc_base::load_app_class('inte' ,'integration');
		$this->pair = pc_base::load_model('pair_model');
		$this->tor = pc_base::load_model('tutor_transaction_model');
		$this->std = pc_base::load_model('student_transaction_model');
		$this->T_Master = pc_base::load_model('tutor_master_model');
		$this->S_Master = pc_base::load_model('student_master_model');
		$this->Member = pc_base::load_model('member_model');
		$this->paypal = pc_base::load_model('paypal_return_model');
	}


	public function GetUserData($order_sn,$item_name){
		$pairinfo = $this->pair->get_one("`pair_id` = $order_sn");
		$Integral = 0;
		if($item_name == 'tor_public_pay' || $item_name == 'tor_agree_pay'){			
			$tutorid = $this->tor->get_one("`tm_tutorid` = '$pairinfo[pair_tutor_tranid]'")['master_id'];
			$userid = $this->T_Master->get_one("`tutor_id` = $tutorid")['tutor_userid'];
			$user=$this->Member->get_one("`userid` = $userid");
			$Integral = $pairinfo['tutor_use_points'];

		}elseif($item_name == 'std_public_pay' || $item_name == 'std_agree_pay'){
			$student_id = $this->std->get_one("`st_id` = '$pairinfo[pair_student_tranid]'")['st_studentid'];		
			$userid =	  $this->S_Master->get_one("student_id = $student_id")['student_userid'];
			$user=$this->Member->get_one("`userid` = $userid");
			$Integral = $pairinfo['student_use_points'];
		}
		return array(	
			'userid' => $userid,	
			'Integral' => $Integral, 	
			'personid'=>$user['personid'],	
			'username'=>$user['username']	
			);

	}

	
	public function integralOperation($userid,$integral,$type='+'){	

		if(empty($userid) || empty($integral) || empty($type)){
			return false;
		}

		$sql = "UPDATE v9_member SET integral = integral $type $integral WHERE userid = $userid";
		return $this->Member->query($sql);

	}


	public function freetimeOperation($userid,$freetime,$type='+'){	

		if(empty($userid) || empty($freetime) || empty($type)){
			return false;
		}

		$sql = "UPDATE v9_member SET freepair_time = freepair_time $type $freetime WHERE userid = $userid";
		return $this->Member->query($sql);

	}



	
	public function returnIntegralOperation($order_sn){
		$inte=new inte();
		$pairinfo = $this->pair->get_one("`pair_id` = $order_sn");
		if($pairinfo['tutor_pay'] == 0 && $pairinfo['tutor_use_points'] > 0){
			$userData = $this->GetUserData($order_sn,'tor_agree_pay');
			$this->integralOperation($userData['userid'],$userData['Integral'],'+');
		
            $inte->write_inte_log($userData['personid'],$userData['username'],'12','','','',$userData['Integral']);
			$this->pair->update("`tutor_use_points` = 0","`pair_id` = $order_sn");
		}
		if($pairinfo['student_pay'] == 0 && $pairinfo['student_use_points'] > 0){
			$userData = $this->GetUserData($order_sn,'std_agree_pay');
			$this->integralOperation($userData['userid'],$userData['Integral'],'+');
		
            $inte->write_inte_log($userData['personid'],$userData['username'],'12','','','',$userData['Integral']);
			$this->pair->update("`student_use_points` = 0","`pair_id` = $order_sn");
		}
	}

}

?>