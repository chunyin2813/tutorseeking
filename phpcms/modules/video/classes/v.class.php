<?php


class v {
	
	private $db;
	
	public function __construct(&$db) {
		$this->db = & $db;
	}
	

	public function add($data = array()) {
		if (is_array($data) && !empty($data)) {
			$data['status'] = 1;
			$data['userid'] = defined('IN_ADMIN') ? 0 : intval(param::get_cookie('_userid'));
			$data['vid'] = safe_replace($data['vid']);
			$vid = $this->db->insert($data, true);
			return $vid ? $vid : false; 
		} else {
			return false;
		}
	}
	

	public function edit($data = array(), $vid = 0) {
		if (is_array($data) && !empty($data)) {
			$vid = intval($vid);
			if (!$vid) return false;
			unset($data['vid']);
			$this->db->update($data, "`videoid` = '$vid'");
			return true;
		} else {
			return false;
		}
	}
	

	public function del_video($vid = 0) {
		$vid = intval($vid);
		if (!$vid) return false;
	
		$this->db->delete(array('videoid'=>$vid));
		return true;
	}
}
?>