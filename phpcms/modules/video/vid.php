<?php 
defined('IN_PHPCMS') or exit('No permission resources.');



 class vid {
	
	public function __construct() {
		pc_base::load_app_class('ku6api', 'video', 0);

		$this->setting = getcache('video', 'video');
		$this->ku6api = new ku6api($this->setting['sn'], $this->setting['skey']);
	}

	
	public function check () {
		$vid = $_GET['vid'];
		
		$this->ku6api->check($vid);
	}
 }