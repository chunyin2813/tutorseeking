<?php
defined('IN_PHPCMS') or exit('No permission resources.');

class inte {
	public function __construct() {
		$this->inte_db=pc_base::load_model('inte_setting_model');
		$this->inte_log_db=pc_base::load_model('integration_log_model');
	}

	public function get_inte_setting($inte_id){
        $inte=$this->inte_db->select(array('inte_id'=>$inte_id));
        $inte_level=$inte[0]['inte_level'];
        return $inte_level;
    }

    public function write_inte_log($owner_id,$owner_name,$change_reason,$provider_id,$provider_name,$spread_times,$integration){
    	$this->inte_log_db->insert(array('owner_id'=>$owner_id,'owner_name'=>$owner_name,'change_reason'=>$change_reason,'provider_id'=>$provider_id,'provider_name'=>$provider_name,'spread_times'=>$spread_times,'integration'=>$integration));
    }

    public function get_inte_num($inte_id){
        $inte=$this->inte_db->select(array('inte_id'=>$inte_id));
        $inte_num=$inte[0]['inte_num'];
        return $inte_num;
    }

    public function return_reason($num){
    	
    	$inte_num=$this->get_inte_num(14);
    	switch ($num) {
    		case '1':
    			$status="登記";
    			break;
    		case '2':
    			$status="推薦朋友";
    			break;
			case '3':
				$status="用戶第一次配對成功時，推薦人所得積分";
				break;
			case '4':
				$status="用戶評論導師";
				break;
			case '5':
				$status="導師被評為5星";
				break;
			case '6':
				$status="導師被評為4星";
				break;
			case '7':
				$status="導師被評為3星";
				break;
			case '8':
				$status="導師被評為2星";
				break;
			case '9':
				$status="導師被評為1星";
				break;
			case '10':
				$status="用戶兌換禮品";
				break;
			case '11':
				$status="用户配对扣除";
				break;
			case '12':
				$status="退款到用户";
				break;
			case '13':
				$status="系統後臺手動更改用戶積分";
				break;
			case '14':
				$status="用戶同時推廣".$inte_num."個會員所得積分";
				break;
			case '15':
				$status="推廣碼";
				break;
    		default:
    			$status="未填寫";
    			break;
    	}
    return $status;
    }
}
?>