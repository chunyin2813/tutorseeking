<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header','admin');
?>
<!--积分日志主页面-->
<form name="searchform" action="?m=integration&c=index&a=search_log&menuid=<?php echo $_GET['menuid'];?>" method="get" >
<input type="hidden" value="integration" name="m">
<input type="hidden" value="index" name="c">
<input type="hidden" value="search_log" name="a">
<table width="100%" cellspacing="0" class="search-form">
    <tbody>
		<tr>
		<td><div class="explain-col">用戶編號: <input type="text" class="input-text" name="search[personid]" size='10' value="<?php echo $personid?>"> 用戶姓名： <input type="text" class="input-text" name="search[username]" size='10' value="<?php echo $username?>">  <?php echo L('times')?>  <?php echo form::date('search[start_time]',$start,'0')?> <?php echo L('to')?>   <?php echo form::date('search[end_time]',$end_time,'0')?>    <input type="submit" value="<?php echo L('determine_search')?>" class="button" name="dosubmit"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" class="button" name="del_log_4" value="<?php echo L('removed_data')?>" onclick="location='?m=integration&c=index&a=delete&week=4&menuid=<?php echo $_GET['menuid'];?>&pc_hash=<?php echo $_SESSION['pc_hash'];?>'"  />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<!-- <input type="button" class="button" name="resetting" value="重置" onclick="window.location.reload();"   /> -->
		</div>
		</td>
		</tr>
    </tbody>
</table>
</form>
<div class="pad-lr-10">
	<div class="table-list">
	    <table width="100%" cellspacing="0" class="contentWrap">
	        <thead>
	            <tr>
				<th width="50">ID</th>
				<th width="200">擁有者編號</th>
				<th width="200">擁有者姓名</th>
				<th width="250">加減分原因</th>
				<th width="200">提供積分人編號</th>
				<th width="200">提供積分人姓名</th>
				<th width="100">推廣次數</th>
				<th align="center">積分</th>
				<th align="center">更新時間</th>
	            </tr>
	        </thead>
	        <tbody> 
	        <?php foreach ($infos as $info) {	?>
				<tr class="memList">
				<td align="center"><?php echo $info['id']?></td>
				<td align="center"><?php empty($info['owner_id'])?print "000000":print $info['owner_id'];?></td>
				<td align="center"><?php empty($info['owner_name'])?print "系統":print $info['owner_name'];?></td>
				<td align="center"><?php empty($info['change_reason'])?print "未填寫":print $info['change_reason'];?></td>
				<td align="center"><?php empty($info['provider_id'])?print "000000":print $info['provider_id'];?></td>
				<td align="center"><?php empty($info['provider_name'])?print "系統":print $info['provider_name'];?></td>
				<td align="center"><?php print $info['spread_times'];?></td>
				<td align="center"><?php print $info['integration'];?></td>
				<td align="center"><?php empty($info['change_date'])?print "未填寫":print $info['change_date'];?></td>
				</tr>
			<?php } ?>	
			</tbody>
	    </table> 
	</div>
	<div id="pages"><?php echo $pages;?></div>
</div>
</body>
</html>