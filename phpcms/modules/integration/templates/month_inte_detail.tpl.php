<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header','admin');
?>
<!--每月系統分配給每種類型積分主页面-->
<div class="pad-lr-10">
	<div class="table-list">
		<?php foreach ($time_list as $tkey=>$tlist) {?>
	    <table width="100%" cellspacing="0" class="contentWrap" style="margin-bottom: 20px;">	
	        <thead>
	            <tr>
				<th width="200"><?php echo $tkey?>年</th>
				<?php foreach ($time_arr as $mk =>$mon) {?>
				<th width="100"><?php echo $mon;?>月</th>
				<?php }?>
				<th align="center">總計</th>
	            </tr>
	        </thead>
	        <tbody>
	        	<?php foreach ($type_arr as $inte_type) {?>	
				<tr class="memList">
					<td align="center"><?php echo $inte_type['inte_name'];?></td>
					
					<?php $score_sum=0;
					foreach ($time_arr as $mk =>$mon) {?>
					<td align="center">總分：<?php empty($tlist[$mk][$inte_type['inte_id']])?print '0':print explode("/",$tlist[$mk][$inte_type['inte_id']])[0];?>分</td>
					<?php $score_sum+=explode("/",$tlist[$mk][$inte_type['inte_id']])[0];?>
					<?php }?>
					<td align="center"><?php echo $score_sum;?>分</td>
						
				</tr>
				<tr class="memList">
					<td align="center"></td>
					
					<?php $times_sum=0;
					foreach ($time_arr as $mk =>$mon) {?>
					<td align="center">總次數：<?php empty($tlist[$mk][$inte_type['inte_id']])?print '0':print explode("/",$tlist[$mk][$inte_type['inte_id']])[1];?>次</td>
					<?php $times_sum+=explode("/",$tlist[$mk][$inte_type['inte_id']])[1];?>
					<?php }?>
					<td align="center"><?php echo $times_sum;?>次</td>
						
				</tr>
				<?php } ?>	
			</tbody>	
	    </table>
	    <?php } ?>
	</div>
	<div id="pages" style="width: 100%"> <?php echo $page_link?></div>
	</div>
</body>
</html>