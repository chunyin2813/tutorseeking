<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header','admin');
?>
<!--每月家長評分次數統計主页面-->
<div class="pad-lr-10">
	<div class="table-list">
		<?php foreach ($com_list as $ck=>$clist) {?>
	    <table width="80%" cellspacing="0" class="contentWrap" style="margin-bottom: 20px;">	
	        <thead>
	            <tr>
				<th width="200"><?php echo $ck?>年</th>
				<?php foreach ($time_arr as $mk =>$mon) {?>
				<th width="50"><?php echo $mon;?>月</th>
				<?php }?>
				<th align="center">總計</th>
	            </tr>
	        </thead>
	        <tbody>
	        	<?php foreach ($star_arr as $sk=>$star) {?>	
				<tr class="memList">
					<td align="center"><?php echo $star;?></td>
					
					<?php $sum=0;
					foreach ($time_arr as $mk =>$mon) {?>
					<td align="center"><?php empty($clist[$mk][$sk])?print '0':print $clist[$mk][$sk];?></td>
					<?php $sum+=$clist[$mk][$sk];?>
					<?php }?>
					<td align="center"><?php echo $sum;?></td>
						
				</tr>
				<?php } ?>	
			</tbody>	
	    </table>
	    <?php } ?>
	</div>
	<div id="pages" style="width: 80%"> <?php echo $page_link?></div>
	</div>
</body>
</html>