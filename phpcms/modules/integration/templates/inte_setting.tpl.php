<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header','admin');
?>
<!--积分设置主页面-->
<div class="pad-lr-10">
	<div class="table-list">
		<!-- <a href="javascript:window.top.art.dialog({id:'myform',iframe:'?m=integration&c=inte_setting&a=inte_add', title:'增加积分类型', width:'540', height:'320'}, function(){var d = window.top.art.dialog({id:'myform'}).data.iframe;var form = d.document.getElementById('dosubmit');form.click();return false;}, function(){window.top.art.dialog({id:'myform'}).close()});void(0);"><button>新增</button></a> -->
	    <table width="80%" cellspacing="0" class="contentWrap">
	        <thead>
	            <tr>
				<th width="200">ID</th>
				<th width="300">積分類型</th>
				<th width="200">積分編號</th>
				<th width="200">積分(點擊修改)</th>
				<th width="200">人數設置(點擊修改)</th>
				<th align="center">更新時間</th>
	            </tr>
	        </thead>
	        <tbody> 
	        <?php foreach ($inte_list as $info) {	?>
				<tr class="memList">
				<td align="center"><?php echo $info['id']?></td>
				<td align="center"><?php echo $info['inte_name']?></td>
				<td align="center"><?php echo $info['inte_id']?></td>
				<td align="center" class="editIntegral" onclick="editIntegral(this,'<?php echo $info[id];?>','<?php echo $info[inte_level];?>');"><?php echo $info['inte_level'];?>積分</td>
				<td align="center" <?php if($info[inte_num]!==null){?> class="editIntegral" onclick="editNum(this,'<?php echo $info[id];?>','<?php echo $info[inte_num];?>');"<?php }?>> <?php if($info[inte_num]!==null){?><?php echo $info['inte_num'];?>人<?php }?></td>
				<td align="center"><?php echo $info['inte_settime']?></td>
				</tr>
			<?php } ?>	
			</tbody>
	    </table> 
	</div>
	<div id="pages"><?php echo $pages;?></div>
</div>
</body>
<script>

	//编辑积分
	function editIntegral(th,uid,integral){
		if(!$('.memList').find('.integralVal').val()){
			$(th).html('<input type="text" class="integralVal" value="'+integral+'" style="width:40px;height:20px;"><input type="button" onclick="subEditIntegral('+uid+');" class="button" value="提交"><input type="button" onclick="cancelEdit();" class="button" value="取消">');
			$(th).removeAttr('onclick');
		}
	}

	//提交编辑
	function subEditIntegral(uid){
		var integral = $('.integralVal').val();//修改后的积分
		$.post('./index.php?m=integration&c=inte_setting&a=editIntegral&pc_hash='+pc_hash,{uid:uid,integralVal:integral},function(res){
			if(res){
				window.location.reload();
			}else{
				alert('请刷新重试!');
			}
		},'json');
	}


	//編輯人數
	function editNum(th,uid,num){
		if(!$('.memList').find('.num').val()){
			$(th).html('<input type="text" class="num" value="'+num+'" style="width:40px;height:20px;"><input type="button" onclick="subEditNum('+uid+');" class="button" value="提交"><input type="button" onclick="cancelEdit();" class="button" value="取消">');
			$(th).removeAttr('onclick');
		}
	}

	function subEditNum(uid){
		var num = $('.num').val();//修改后的人數
		$.post('./index.php?m=integration&c=inte_setting&a=editNum&pc_hash='+pc_hash,{uid:uid,num:num},function(res){
			if(res){
				window.location.reload();
			}else{
				alert('请刷新重试!');
			}
		},'json');
	}

	//取消编辑
	function cancelEdit(){
		window.location.reload();
	}

</script>
</html>