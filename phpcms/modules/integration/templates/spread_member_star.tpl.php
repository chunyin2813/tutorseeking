<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header','admin');
?>
<!--每月推廣最多的會員主页面-->
<div class="pad-lr-10">
<div class="table-list">
	    <table width="80%" cellspacing="0" class="contentWrap">
	        <thead>
	            <tr>
				<th width="200">類別</th>
				<th width="200">編號</th>
				<th width="200">姓名</th>
				<th width="200">總推廣次數</th>
				<th align="center">所得積分</th>
	            </tr>
	        </thead>
	        <tbody> 
		        <?php foreach ($max_list as $key=>$mlist) {?>
	        	<tr><td colspan="5"><?php echo  explode('-',$key)[0];?>年<?php echo  explode('-',$key)[1];?>月推廣最多的會員</td></tr>
	        	<?php foreach($mlist as $k=>$m){?>
				<tr class="memList">
					<td align="center"><?php echo $m['role'];?></td>
					<td align="center"><?php echo $m['id'];?></td>
					<td align="center"><?php echo $m['name'];?></td>
					<td align="center"><?php echo $m['num'];?></td>
					<td align="center"><?php echo $m['integration'];?></td>
				</tr>
				<?php }?>
				<?php } ?>	
			</tbody>
	    </table>

	</div>
	<div id="pages" style="width: 80%"> <?php echo $page_link?></div>
	</div>
</body>
</html>