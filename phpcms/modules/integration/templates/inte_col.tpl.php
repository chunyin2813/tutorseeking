<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header','admin');
?>
<!--每月系統操作的積分主页面-->
<div class="pad-lr-10">
	<div class="table-list">
		<?php foreach ($aplus_list as $tkey=>$plist) {?>
	    <table width="80%" cellspacing="0" class="contentWrap" style="margin-bottom: 20px;">	
	        <thead>
	            <tr>
				<th width="200"><?php echo $tkey?>年</th>
				<?php foreach ($time_arr as $mk =>$mon) {?>
				<th width="50"><?php echo $mon;?>月</th>
				<?php }?>
				<th align="center">總計</th>
	            </tr>
	        </thead>
	        <tbody>	
				<tr class="memList">
					<td align="center">系統分配的積分總數</td>
					
					<?php $sum=0;
					foreach ($time_arr as $mk =>$mon) {?>
					<td align="center"><?php empty($plist[$mk])?print '0':print $plist[$mk];?></td>
					<?php $sum+=$plist[$mk];?>
					<?php }?>
					<td align="center"><?php echo $sum;?></td>
						
				</tr>
				<tr class="memList">
					<td align="center">系統收到的積分總數</td>
					
					<?php $sum=0;
					foreach ($time_arr as $mk =>$mon) {?>
					<td align="center"><?php empty($minus_list[$tkey][$mk])?print '0':print abs($minus_list[$tkey][$mk]);?></td>
					<?php $sum+=$minus_list[$tkey][$mk];?>
					<?php }?>
					<td align="center"><?php echo abs($sum);?></td>
						
				</tr>
				<tr class="memList">
					<td align="center">系統剩餘的積分總數</td>
					
					<?php $sum=0;
					foreach ($time_arr as $mk =>$mon) {?>
					<td align="center"><?php empty($left_list[$tkey][$mk])?print '0':print $left_list[$tkey][$mk];?></td>
					<?php $sum+=$left_list[$tkey][$mk];?>
					<?php }?>
					<td align="center"><?php echo $sum;?></td>
						
				</tr>	
			</tbody>	
	    </table>
	    <?php } ?>
	</div>
	<div id="pages" style="width: 80%"> <?php echo $page_link?></div>
	</div>
</body>
</html>