<?php 
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header', 'admin');
?>
<form method="post" id="myform" action="?m=integration&c=inte_setting&a=inte_add">
<table class="table_form" width="100%" cellspacing="0">
<tbody>
	<tr>
    	<th align="right" valign="top">積分類型名稱：</th>
        <td><input name="inte_name" id="inte_name" class="input-text" type="text" size="25" required="required"></td>
    </tr>
    <tr>
        <th align="right" valign="top">積分類型編號：</th>
        <td><input name="inte_id" id="inte_id" class="input-text" type="text" size="25" required="required"></td>
    </tr>
    <tr>
    	<th align="right"  valign="top">積分設置：</th>
        <td><input name="inte_level" id="inte_level" class="input-text" type="text" size="25" required="required" placeholder=""></td>
    </tr>
	<tr>
		<th>更新時間：</th>
		 <td><input name="inte_settime" id="inte_settime" class="input-text" type="text" size="25" value="<?php echo date("Y-m-d H:i:s")?>" readonly="readonly" ></td>
	</tr>
    <input type="submit" name="dosubmit" id="dosubmit" value=" <?php echo L('ok')?> " class='dialog'>&nbsp;<input type="reset" value=" <?php echo L('clear')?> " class='dialog'>
	</tbody>
</table>	
</form>
</body>
</html>
