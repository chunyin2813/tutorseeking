<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header','admin');
?>
<!--每月會員推廣所得積分主页面-->
<div class="pad-lr-10">
<div class="table-list">
	    <table width="80%" cellspacing="0" class="contentWrap">
	        <thead>
	            <tr>
				<th width="200">類別</th>
				<th width="200">編號</th>
				<th width="200">姓名</th>
				<th width="200">總推廣次數</th>
				<th align="center">所得積分</th>
	            </tr>
	        </thead>
	        <tbody> 
		        <?php foreach ($total_list as $key=>$list) {	
		        	$sum=0;
		        	$inte_sum=0;
		        ?>
	        	<tr><td colspan="5"><?php echo  explode('-',$key)[0];?>年<?php echo  explode('-',$key)[1];?>月會員推廣及得分情況</td></tr>
	        	<?php foreach ($list as $k=>$one_list) { ?>
				<tr class="memList">
					<td align="center"><?php echo $one_list['role'];?></td>
					<td align="center"><?php echo $k;?></td>
					<td align="center"><?php echo $one_list['name'];?></td>
					<td align="center"><?php echo $one_list['num'];?></td>
					<td align="center"><?php echo $one_list['integration'];?></td>
					<?php $sum+=$one_list['num'];$inte_sum+=$one_list['integration'];?>
				</tr>
				<?php } ?>
				<tr>
					<td align="center"></td>
					<td align="center"></td>
					<td align="center">總數</td>
					<td align="center"><?php echo $sum;?></td>
					<td align="center"><?php echo $inte_sum;?></td>
				</tr>
				<?php } ?>	
			</tbody>
	    </table>

	</div>
	<div id="pages" style="width: 80%"> <?php echo $page_link?></div>
	
	</div>
</body>
</html>