<?php

defined('IN_PHPCMS') or exit('No permission resources.');

pc_base::load_app_class('admin', 'admin', 0);
pc_base::load_sys_class('form', '', 0);

class inte_collection extends admin {
	function __construct() {
        $this->inte_log=pc_base::load_model('integration_log_model'); 
        $this->inte_setting=pc_base::load_model('inte_setting_model'); 
        $this->pair_comment= pc_base::load_model('pair_comment_model'); 
    }

  
	public function index(){
		$time_arr=array('01'=>1,'02'=>2,'03'=>3,'04'=>4,'05'=>5,'06'=>6,'07'=>7,'08'=>8,'09'=>9,'10'=>10,'11'=>11,'12'=>12);
		$page=$_GET['page']?$_GET['page']:'1'; 
		$limit=2; 
		
		$plus_sql="SELECT integration,change_date FROM `v9_integration_log` where integration>0 order by id";
		$this->inte_log->query($plus_sql);
		$plus_res=$this->inte_log->fetch_array();
		$plus_list=$this->groupVisit($plus_res,3);
		$start = ($page-1)*$limit;
		$aplus_list=array_slice($plus_list,$start,$limit,true);	//true表示保留原鍵名
		$page_link = pages($num = count($plus_list), $page, $limit, '', array(), 3);//分页
		
		$minus_sql="SELECT integration,change_date FROM `v9_integration_log` where integration<0 order by id";
		$this->inte_log->query($minus_sql);
		$minus_res=$this->inte_log->fetch_array();
		$minus_list=$this->groupVisit($minus_res,3);
		
		$left_sql="SELECT integration,change_date FROM `v9_integration_log` order by id";
		$this->inte_log->query($left_sql);
		$left_res=$this->inte_log->fetch_array();
		$left_list=$this->groupVisit($left_res,3);
		//var_dump($times_list);
		include $this->admin_tpl('inte_col');
	}
	
	public function spread_member_star(){
		$page=$_GET['page']?$_GET['page']:'1'; 
		$limit=10;
		$mem_sql="SELECT owner_id,owner_name,spread_times,integration,change_date FROM `v9_integration_log` WHERE change_reason=2 order by id";
		$this->inte_log->query($mem_sql);
		$mem_res=$this->inte_log->fetch_array();
		$total_list=$this->groupVisit($mem_res,1);
		$flag = array();
		foreach ($total_list as $key => $v) {
			foreach ($v as $k => $value) {
				$flag[] = $value['num'];
				$role=substr($k,0,1);
				if($role=='S'){
					$value['role']='學生';
				}
				if($role=='A'){
					$value['role']='導師';
				}
				$v[$k]=$value;
			}
			
			array_multisort($flag, SORT_DESC, $v); 
	
			$first=current($v);
			
			foreach ($v as $k => $value) {
				if($value['num']==$first['num']){
					$one_list[$k]=$value;
				}
			}
	
			$most_list[$key]=$one_list;
		}
		$start = ($page-1)*$limit;
		$max_list=array_slice($most_list,$start,$limit,true);	
		$page_link = pages($num = count($most_list), $page, $limit, '', array(), 3);
		include $this->admin_tpl('spread_member_star');
	}

	
	public function spread_member_inte(){
		$page=$_GET['page']?$_GET['page']:'1'; 
		$limit=1;
		$mem_sql="SELECT owner_id,owner_name,spread_times,integration,change_date FROM `v9_integration_log` WHERE change_reason=2 order by id";
		$this->inte_log->query($mem_sql);
		$mem_res=$this->inte_log->fetch_array();
		$to_list=$this->groupVisit($mem_res,1);
		foreach ($to_list as $key => $v) {	
			foreach ($v as $k => $value) {				
				$role=substr($k,0,1);
				if($role=='S'){
					$value['role']='學生';
				}
				if($role=='A'){
					$value['role']='導師';
				}
				$v[$k]=$value;
			}
			$to_list[$key]=$v;
		}
		$start = ($page-1)*$limit;
		$total_list=array_slice($to_list,$start,$limit,true);	
		$page_link = pages($num = count($to_list), $page, $limit, '', array(), 3);
		include $this->admin_tpl('spread_member_inte');
	}


	public function month_inte_detail(){
		$page=$_GET['page']?$_GET['page']:'1'; 
		$limit=3;

		$time_arr=array('01'=>1,'02'=>2,'03'=>3,'04'=>4,'05'=>5,'06'=>6,'07'=>7,'08'=>8,'09'=>9,'10'=>10,'11'=>11,'12'=>12);
		$type_arr=$this->inte_setting->select('','inte_id,inte_name');
		$times_sql="SELECT change_reason,integration,change_date FROM `v9_integration_log` order by id";
		$this->inte_log->query($times_sql);
		$times_res=$this->inte_log->fetch_array();
		$times_list=$this->groupVisit($times_res,2);
	
		$start = ($page-1)*$limit;
		$time_list=array_slice($times_list,$start,$limit,true);

		$page_link= pages($num = count($times_list), $page, $limit, '', array(), 3);//分页
		include $this->admin_tpl('month_inte_detail');
	}

	
	public function teacher_comment_inte(){
		$page=$_GET['page']?$_GET['page']:'1'; 
		$limit=3;
		$time_arr=array('01'=>1,'02'=>2,'03'=>3,'04'=>4,'05'=>5,'06'=>6,'07'=>7,'08'=>8,'09'=>9,'10'=>10,'11'=>11,'12'=>12);
		$star_arr=array('1'=>'1星','2'=>'2星','3'=>'3星','4'=>'4星','5'=>'5星');
		$comment_res=$this->pair_comment->select('','pc_star,pc_modtime');
		$comment_list=$this->groupVisit($comment_res,4);
	
		$start = ($page-1)*$limit;
		$com_list=array_slice($comment_list,$start,$limit,true);

		$page_link= pages($num = count($comment_list), $page, $limit, '', array(), 3);//分页

		include $this->admin_tpl('teacher_comment_inte');
	}


	public function groupVisit($array,$status){
		$result =   array();
		if($status==1){
			foreach ($array as $key => $value) {
			
				$timestrap=strtotime($value['change_date']);
			
				$date= date('Y-m',$timestrap);
			
				$result[$date][$value['owner_id']] ['num']  += $value['spread_times'] ;
				$result[$date][$value['owner_id']] ['name']  =  $value['owner_name'];
				$result[$date][$value['owner_id']] ['id']  =  $value['owner_id'];
				$result[$date][$value['owner_id']] ['integration']  +=  $value['integration'];
			}
		}
		if($status==2){
			foreach ($array as $key => $value) {
		
				$timestrap=strtotime($value['change_date']);
			
				$year= date('Y',$timestrap);
		
				$month= date('m',$timestrap);
		
				$result[$year][$month][$value['change_reason']] []  =$value['integration'] ;
			}

			foreach ($result as $key => $v) {
				foreach ($v as $i => $vi) {
					foreach ($vi as $j=> $vj) {
						$result[$key][$i][$j]=abs(array_sum($vj))."/".count($vj);
					}
					
				}
			}
		}

		if($status==3){
			foreach ($array as $key => $value) {
				
				$timestrap=strtotime($value['change_date']);
		
				$year= date('Y',$timestrap);
		
				$month= date('m',$timestrap);
			
				$result[$year][$month] []  =$value['integration'] ;
			}
			foreach ($result as $key => $v) {
				foreach ($v as $i => $vi) {
					$sum=0;
					foreach ($vi as $j=> $vj) {
						$sum+=$vj;
						$result[$key][$i]=$sum;
					}
					
				}
			}
		}

		if($status==4){
			foreach ($array as $key => $va) {
			
				$timestrap=strtotime($va['pc_modtime']);

				$year= date('Y',$timestrap);

				$month= date('m',$timestrap);
			
				$result[$year][$month][$this->get_star($va['pc_star'])] []  = $this->get_star($va['pc_star']);
				
				
			}
			foreach ($result as $key => $v) {
				foreach ($v as $i => $vi) {
					foreach ($vi as $j=> $vj) {
						$result[$key][$i][$j]=count($vj);	
					}
					
				}
			}
		}
		return $result;
	}

	public function get_star($score){
		if($score>16){
			$star=5;
		}
		if(12<$score and $score<=16){
			$star=4;
		}
		if(8<$score and $score<=12){
			$star=3;
		}
		if(4<$score and $score<=8){
			$star=2;
		}
		if($score<=4){
			$star=1;
		}
		return $star;
	}
}
?>