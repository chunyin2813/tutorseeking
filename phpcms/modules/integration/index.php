<?php

defined('IN_PHPCMS') or exit('No permission resources.');

pc_base::load_app_class('admin', 'admin', 0);
pc_base::load_sys_class('form', '', 0);

class index extends admin {
	function __construct(){
		parent::__construct();
		$this->inte_log=pc_base::load_model('integration_log_model');
		$this->inte=pc_base::load_app_class('inte');
	}
	public function init(){
		$page = isset($_GET['page']) && intval($_GET['page']) ? intval($_GET['page']) : 1;
        $infos = $this->inte_log->listinfo($where = '',$order = 'change_date desc',$page, $pages = '20');
        foreach ($infos as $key => $v) {
            $infos[$key]['change_reason'] =$this->inte->return_reason($v['change_reason']);  
        }
        $pages = $this->inte_log->pages;
		include $this->admin_tpl('inte_log');
	}


	public function delete(){
		$week = intval($_GET['week']);
		if($week){
			$where = '';
			$start = SYS_TIME - $week*7*24*3600;
			$d = date("Y-m-d",$start); 
			$where .= "`change_date` <= '$d'";
			$this->inte_log->delete($where);
			showmessage(L('operation_success'),'?m=integration&c=index&a=init');
		} else {
			return false;
		}
	}
	
	public function search_log() {
 		$where = '';
		extract($_GET['search'],EXTR_SKIP);
		if($personid){
			$where .= $where ?  " AND (owner_id='$personid') or (provider_id='$personid')" : " owner_id='$personid' or (provider_id='$personid')";
		}
		if($username){
			$where .= $where ?  " AND (owner_name='$username') or (provider_name='$username')" : " owner_name='$username' or (provider_name='$username')";
		}
		if($start_time) {
			$start = $start_time;
			
			$where .= $where ?  "AND `change_date` >= '$start' ": "`change_date` >= '$start'";
		}
 		if($end_time) {
			$end =  date("Y-m-d",strtotime("+1 day",strtotime($end_time)));
			$where .= $where ?  "AND `change_date` <= '$end' ": "`change_date` <= '$end' ";
		}

		$page = isset($_GET['page']) && intval($_GET['page']) ? intval($_GET['page']) : 1;
        $infos = $this->inte_log->listinfo($where,$order = 'change_date desc',$page, $pages = '20');
        foreach ($infos as $key => $v) {
            $infos[$key]['change_reason'] =$this->inte->return_reason($v['change_reason']);  
        }
        $pages = $this->inte_log->pages;

		
		
 		include $this->admin_tpl('inte_log');
	} 
}
?>