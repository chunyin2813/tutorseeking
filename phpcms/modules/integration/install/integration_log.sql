DROP TABLE IF EXISTS `phpcms_integration_log`;
CREATE TABLE IF NOT EXISTS `v9_integration_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` varchar(40) NOT NULL COMMENT '拥有者编号',
  `owner_name` varchar(100) NOT NULL COMMENT '拥有者姓名',
  `change_reason` int(5) NOT NULL COMMENT '加减分原因（1-登記，2-推廣朋友，3-用戶第一次配對成功時，推薦人所得積分，4-用戶評論導師，5-導師被評為5星，6-導師被評為4星，7-導師被評為3星，8-導師被評為2星，9-導師被評為1星，10-用戶兌換禮品，，11-用戶配對所用積分，12-用戶配對失敗返回積分）',
  `provider_id` varchar(40) NOT NULL DEFAULT '000000' COMMENT '提供积分者编号',
  `provider_name` varchar(100) NOT NULL DEFAULT '系統' COMMENT '提供积分者姓名',
  `spread_times` int(10) NOT NULL DEFAULT '0' COMMENT '推广次数',
  `integration` int(10) NOT NULL DEFAULT '0' COMMENT '积分',
  `change_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '积分变动时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='积分日志表' AUTO_INCREMENT=1 ;