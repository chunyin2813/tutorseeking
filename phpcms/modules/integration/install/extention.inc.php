<?php
defined('IN_PHPCMS') or exit('Access Denied');
defined('INSTALL') or exit('Access Denied');
$parentid = $menu_db->insert(array(
	'name'=>'integration',
	'parentid'=>2,
	'm'=>'integration',
	'c'=>'index',
	'a'=>'init',
	'data'=>'',
	'listorder'=>0,
	'display'=>'1'), true);
$menu_db->insert(array('name'=>'inte_log', 'parentid'=>$parentid, 'm'=>'integration', 'c'=>'index', 'a'=>'init', 'data'=>'', 'listorder'=>0, 'display'=>'1'));
$menu_db->insert(array('name'=>'inte_setting', 'parentid'=>$parentid, 'm'=>'integration', 'c'=>'inte_setting', 'a'=>'index', 'data'=>'', 'listorder'=>0, 'display'=>'1'));
$two_parentid=$menu_db->insert(array('name'=>'inte_collection', 'parentid'=>$parentid, 'm'=>'integration', 'c'=>'inte_collection', 'a'=>'index', 'data'=>'', 'listorder'=>0, 'display'=>'1'),true);
$menu_db->insert(array('name'=>'spread_member_star', 'parentid'=>$two_parentid, 'm'=>'integration', 'c'=>'inte_collection', 'a'=>'spread_member_star', 'data'=>'', 'listorder'=>1, 'display'=>'1'));
$menu_db->insert(array('name'=>'spread_member_inte', 'parentid'=>$two_parentid, 'm'=>'integration', 'c'=>'inte_collection', 'a'=>'spread_member_inte', 'data'=>'', 'listorder'=>2, 'display'=>'1'));
$menu_db->insert(array('name'=>'month_inte_detail', 'parentid'=>$two_parentid, 'm'=>'integration', 'c'=>'inte_collection', 'a'=>'month_inte_detail', 'data'=>'', 'listorder'=>3, 'display'=>'1'));
$menu_db->insert(array('name'=>'teachers_comment_inte', 'parentid'=>$two_parentid, 'm'=>'integration', 'c'=>'inte_collection', 'a'=>'teacher_comment_inte', 'data'=>'', 'listorder'=>4, 'display'=>'1'));

$language = array('integration'=>'用户积分管理','spread_member_star'=>'每月推薦之星','spread_member_inte'=>'會員推薦所得積分','month_inte_detail'=>'每月系統積分分配','teachers_comment_inte'=>'導師評分統計');
?>