<?php

defined('IN_PHPCMS') or exit('No permission resources.');

pc_base::load_app_class('admin', 'admin', 0);
pc_base::load_sys_class('form', '', 0);

class inte_setting extends admin {
	function __construct(){
		parent::__construct();
		$this->inte_setdb=pc_base::load_model('inte_setting_model');
	}

	public function index(){
		$inte_list=$this->inte_setdb->listinfo($where = '',$order = 'inte_id');
		include $this->admin_tpl('inte_setting');
	}
	
	public function inte_add(){
		if (isset($_POST['dosubmit'])) {
			$inte_name=$_POST['inte_name'];
			$inte_id=(int)($_POST['inte_id']);
			$inte_level=$_POST['inte_level'];
			$inte_settime=$_POST['inte_settime'];
			$res=$this->inte_setdb->insert(array('inte_name'=>$inte_name,'inte_id'=>$inte_id,'inte_level'=>$inte_level,'inte_settime'=>$inte_settime));
			if ($res) {
				showmessage("添加成功");
			}
		} else {
			include $this->admin_tpl('inte_add');
		}
		
	}


    public function editIntegral(){
       
        if(!empty($_POST['uid']) && isset($_POST['integralVal'])){
            $updateInfo = array('inte_level'=>$_POST['integralVal']);
            $res = $this->inte_setdb->update($updateInfo,"id = $_POST[uid]");
            if($res){
                echo 1;
            }else{
                echo 0;
            }
        }
    }


    public function editNum(){
  
        if(!empty($_POST['uid']) && isset($_POST['num'])){
            $updateInfo = array('inte_num'=>$_POST['num']);
            $res = $this->inte_setdb->update($updateInfo,"id = $_POST[uid]");
            if($res){
                echo 1;
            }else{
                echo 0;
            }
        }
    }

	
}
?>