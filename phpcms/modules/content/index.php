﻿<?php
defined('IN_PHPCMS') or exit('No permission resources.');
//模型缓存路径
define('CACHE_MODEL_PATH',CACHE_PATH.'caches_model'.DIRECTORY_SEPARATOR.'caches_data'.DIRECTORY_SEPARATOR);
pc_base::load_app_func('util','content');
header("Content-Type:text/html;charset:utf8");
class index {
	private $db;
	function __construct() {
		pc_base::load_sys_class('form');
		$this->db = pc_base::load_model('content_model');
		$this->member=pc_base::load_model('member_model');
		$this->studb=pc_base::load_model('student_transaction_model');
		$this->tutordb=pc_base::load_model('tutor_transaction_model');
		$this->tutor_type=pc_base::load_model('sys_tutor_type_model');//导师可教授类型
		$this->pair=pc_base::load_model('pair_model');
		$this->syl=pc_base::load_model('sys_location_model');
		$this->popup=pc_base::load_model('popup_model');
		$this->_userid = param::get_cookie('_userid');
		$this->_username = param::get_cookie('_username');
		$this->_groupid = param::get_cookie('_groupid');


		$this->tutor = pc_base::load_app_class('tutor','pair');//初始化导师操作类
		$this->std = pc_base::load_app_class('student','pair');//初始化学生操作类
		$this->inte=pc_base::load_app_class('inte','integration');
	}


	private function _session_start() {
	  	$session_storage = 'session_'.pc_base::load_config('system','session_storage');
		pc_base::load_sys_class($session_storage);
	}

	//首页
	public function init() {
		//首頁彈窗
		$popup=$this->popup->listinfo(array('status'=>1), '');
		foreach ($popup as $k=> $v) {
            if(!in_array('0', explode(',', $v['seat']))){
                unset($popup[$k]);
            }
        }
        foreach ($popup as $k=> $v) {
            if($v['position']=='0'){        //手機版  
               $ph_src=$v['setting']; 
            }
            if($v['position']=='1'){       //ipad版
               $ip_src=$v['setting'];
            }
            if($v['position']=='2'){      //電腦版
               $com_src=$v['setting'];
            }
        }

		$this->_session_start();
		
		$userid = $_SESSION['id'];

		if(isset($_GET['siteid'])) {
			$siteid = intval($_GET['siteid']);
		} else {
			$siteid = 1;
		}
		$siteid = $GLOBALS['siteid'] = max($siteid,1);
		define('SITEID', $siteid);
		$_userid = $this->_userid;
		$_username = $this->_username;
		$_groupid = $this->_groupid;

	
		if($_SESSION['role_verify']=='0'){
			$sql="select userid,personid,st_id,loc_name,tt_name,st_num_join,st_tutor_fee,st_matched,username,userimage,role_verify,sst_weekday,st_studentid from `v9_student_transaction` as tran 
			left join `v9_student_master` as master on master.student_id=tran.st_studentid 
			left join `v9_sys_location` as loc on loc.loc_id = tran.st_area 
			left join `v9_sys_tutor_type` as type on type.tt_id = tran.st_ttid_2
			left join `v9_member` as mem on mem.userid = master.student_userid 
			left join `v9_student_sel_time` as time on time.sst_stid = tran.st_id 
			where role_verify='1' and st_matched='0' order by st_date desc limit 4";

			$this->studb->query($sql);
			$exlist = $this->studb->fetch_array();
		}elseif ($_SESSION['role_verify']=='1') {
			$sql="select userid,personid,tm_tutorid,role_verify,tsl_locid,tstt_ttid_2,tutor_main_lang,tstt_fee,tm_matched,userimage,tst_weekday,master_id from `v9_tutor_transaction` as tran
			left join `v9_tutor_master` as master on master.tutor_id = tran.master_id
			left join `v9_member` as member on member.userid = master.tutor_userid
			left join `v9_tutor_sel_tutor_type` as type on type.tstt_tmid = tran.tm_id
			left join `v9_tutor_sel_location` as loc on loc.tsl_tmid = tran.tm_id
			left join `v9_sys_location` as syl on syl.loc_id = loc.tsl_locid
			left join `v9_tutor_sel_time` as time on time.tst_tmid = tran.tm_id 
			where role_verify='0' and tm_matched='0' order by tutor_modtime desc limit 4";
			
			$this->tutordb->query($sql);
			$exlist = $this->tutordb->fetch_array();
		}elseif (!isset($_SESSION['role_verify'])) {
			$sql1="select userid,personid,st_id,loc_name,tt_name,st_num_join,st_tutor_fee,st_matched,username,userimage,role_verify,sst_weekday,st_studentid from `v9_student_transaction` as tran 
			left join `v9_student_master` as master on master.student_id=tran.st_studentid 
			left join `v9_sys_location` as loc on loc.loc_id = tran.st_area 
			left join `v9_sys_tutor_type` as type on type.tt_id = tran.st_ttid_2
			left join `v9_member` as mem on mem.userid = master.student_userid 
			left join `v9_student_sel_time` as time on time.sst_stid = tran.st_id 
			where role_verify='1' and st_matched='0' order by st_date desc limit 2";
			$this->studb->query($sql1);
			$stulist = $this->studb->fetch_array();
			// _dd($stulist);
			$sql2="select userid,personid,tm_tutorid,role_verify,tsl_locid,tstt_ttid_2,tutor_main_lang,tstt_fee,tm_matched,userimage,tst_weekday,master_id from `v9_tutor_transaction` as tran
			left join `v9_tutor_master` as master on master.tutor_id = tran.master_id
			left join `v9_member` as member on member.userid = master.tutor_userid
			left join `v9_tutor_sel_tutor_type` as type on type.tstt_tmid = tran.tm_id
			left join `v9_tutor_sel_location` as loc on loc.tsl_tmid = tran.tm_id
			left join `v9_sys_location` as syl on syl.loc_id = loc.tsl_locid
			left join `v9_tutor_sel_time` as time on time.tst_tmid = tran.tm_id
			where role_verify='0' and tm_matched='0' order by tutor_modtime desc limit 2";	

			$this->tutordb->query($sql2);
			$tutorlist = $this->tutordb->fetch_array();
			$exlist=array_merge($stulist,$tutorlist);
		
		}

		$sql1="SELECT count(*) as count from v9_member where role_verify='0'";
		$this->db=pc_base::load_model('member_model');
		$this->db->query($sql1);
		$num1 = $this->db->fetch_array();

		$sql2="SELECT count(*) as count from v9_member where role_verify='1'";
		$this->db=pc_base::load_model('member_model');
		$this->db->query($sql2);
		$num2 = $this->db->fetch_array();

		$sql3="SELECT count(*) as count from v9_pair where pair_state='1'";
		$this->db=pc_base::load_model('pair_model');
		$this->db->query($sql3);
		$num3 = $this->db->fetch_array();

		
		$this->es_db = pc_base::load_model('extend_setting_model');
		$baseday = strtotime($this->es_db->get_one(array('key'=>"Cardinal_Day"))['data']);
		
		$today =  strtotime(now);
		$times = $today - $baseday;
		$days = floor($times/(3600*24));
		$days = intval($days)+1;
		$Cardinality = 108+$days*6;
		$sum = $num3[0][count] + $Cardinality;

		$sql4="SELECT count(*) as count from  v9_pair where pair_state='1' and to_days(pair_modtime) = to_days(now())";
		$this->db=pc_base::load_model('pair_model');
		$this->db->query($sql4);
		$num4 = $this->db->fetch_array();

		//SEO
		$SEO = seo($siteid);
		$sitelist  = getcache('sitelist','commons');
		$default_style = $sitelist[$siteid]['default_style'];

		$CATEGORYS = getcache('category_content_'.$siteid,'commons');
		include template('content','index',$default_style);
	}

	public function PairingTimes($times) 
	{
		
		$baseday = strtotime('2018-2-9');
		
		$today =  strtotime(now);
		$times = $today - $baseday;
		$days = floor($times/(3600*24));
		$days = intval($days);
		return $days;
	}



	public function show() {
		$this->_session_start();
		$catid = intval($_GET['catid']);
		$id = intval($_GET['id']);

		if(!$catid || !$id) showmessage(L('information_does_not_exist'),'blank');
		$_userid = $this->_userid;
		$_username = $this->_username;
		$_groupid = $this->_groupid;

		$page = intval($_GET['page']);
		$page = max($page,1);
		$siteids = getcache('category_content','commons');
		$siteid = $siteids[$catid];
		$CATEGORYS = getcache('category_content_'.$siteid,'commons');
		
		if(!isset($CATEGORYS[$catid]) || $CATEGORYS[$catid]['type']!=0) showmessage(L('information_does_not_exist'),'blank');
		$this->category = $CAT = $CATEGORYS[$catid];
		$this->category_setting = $CAT['setting'] = string2array($this->category['setting']);
		$siteid = $GLOBALS['siteid'] = $CAT['siteid'];
		
		$MODEL = getcache('model','commons');
		$modelid = $CAT['modelid'];
		
		$tablename = $this->db->table_name = $this->db->db_tablepre.$MODEL[$modelid]['tablename'];
		$r = $this->db->get_one(array('id'=>$id));
		if(!$r || $r['status'] != 99) showmessage(L('info_does_not_exists'),'blank');
		
		$this->db->table_name = $tablename.'_data';
		$r2 = $this->db->get_one(array('id'=>$id));
		$rs = $r2 ? array_merge($r,$r2) : $r;

		
		$catid = $CATEGORYS[$r['catid']]['catid'];
		$modelid = $CATEGORYS[$catid]['modelid'];
		
		require_once CACHE_MODEL_PATH.'content_output.class.php';
		$content_output = new content_output($modelid,$catid,$CATEGORYS);
		$data = $content_output->get($rs);
		extract($data);
		
		
		if($groupids_view && is_array($groupids_view)) {
			$_groupid = param::get_cookie('_groupid');
			$_groupid = intval($_groupid);
			if(!$_groupid) {
				$forward = urlencode(get_url());
				showmessage(L('login_website'),APP_PATH.'index.php?m=member&c=index&a=login&forward='.$forward);
			}
			if(!in_array($_groupid,$groupids_view)) showmessage(L('no_priv'));
		} else {
		
			$_priv_data = $this->_category_priv($catid);
			if($_priv_data=='-1') {
				$forward = urlencode(get_url());
				showmessage(L('login_website'),APP_PATH.'index.php?m=member&c=index&a=login&forward='.$forward);
			} elseif($_priv_data=='-2') {
				showmessage(L('no_priv'));
			}
		}
		if(module_exists('comment')) {
			$allow_comment = isset($allow_comment) ? $allow_comment : 1;
		} else {
			$allow_comment = 0;
		}
	
		$paytype = $rs['paytype'];
		$readpoint = $rs['readpoint'];
		$allow_visitor = 1;
		if($readpoint || $this->category_setting['defaultchargepoint']) {
			if(!$readpoint) {
				$readpoint = $this->category_setting['defaultchargepoint'];
				$paytype = $this->category_setting['paytype'];
			}
			
			//检查是否支付过
			$allow_visitor = self::_check_payment($catid.'_'.$id,$paytype);
			if(!$allow_visitor) {
				$http_referer = urlencode(get_url());
				$allow_visitor = sys_auth($catid.'_'.$id.'|'.$readpoint.'|'.$paytype).'&http_referer='.$http_referer;
			} else {
				$allow_visitor = 1;
			}
		}
	
		$arrparentid = explode(',', $CAT['arrparentid']);
		$top_parentid = $arrparentid[1] ? $arrparentid[1] : $catid;
		
		$template = $template ? $template : $CAT['setting']['show_template'];
		if(!$template) $template = 'show';
		//SEO
		$seo_keywords = '';
		if(!empty($keywords)) $seo_keywords = implode(',',$keywords);
		$SEO = seo($siteid, $catid, $title, $description, $seo_keywords);
		
		define('STYLE',$CAT['setting']['template_list']);
		if(isset($rs['paginationtype'])) {
			$paginationtype = $rs['paginationtype'];
			$maxcharperpage = $rs['maxcharperpage'];
		}
		$pages = $titles = '';
		if($rs['paginationtype']==1) {
			//自动分页
			if($maxcharperpage < 10) $maxcharperpage = 500;
			$contentpage = pc_base::load_app_class('contentpage');
			$content = $contentpage->get_data($content,$maxcharperpage);
		}
		if($rs['paginationtype']!=0) {
			//手动分页
			$CONTENT_POS = strpos($content, '[page]');
			if($CONTENT_POS !== false) {
				$this->url = pc_base::load_app_class('url', 'content');
				$contents = array_filter(explode('[page]', $content));
				$pagenumber = count($contents);
				if (strpos($content, '[/page]')!==false && ($CONTENT_POS<7)) {
					$pagenumber--;
				}
				for($i=1; $i<=$pagenumber; $i++) {
					$pageurls[$i] = $this->url->show($id, $i, $catid, $rs['inputtime']);
				}
				$END_POS = strpos($content, '[/page]');
				if($END_POS !== false) {
					if($CONTENT_POS>7) {
						$content = '[page]'.$title.'[/page]'.$content;
					}
					if(preg_match_all("|\[page\](.*)\[/page\]|U", $content, $m, PREG_PATTERN_ORDER)) {
						foreach($m[1] as $k=>$v) {
							$p = $k+1;
							$titles[$p]['title'] = strip_tags($v);
							$titles[$p]['url'] = $pageurls[$p][0];
						}
					}
				}
			
				$pages = content_pages($pagenumber,$page, $pageurls);
			
				if($CONTENT_POS<7) {
					$content = $contents[$page];
				} else {
					if ($page==1 && !empty($titles)) {
						$content = $title.'[/page]'.$contents[$page-1];
					} else {
						$content = $contents[$page-1];
					}
				}
				if($titles) {
					list($title, $content) = explode('[/page]', $content);
					$content = trim($content);
					if(strpos($content,'</p>')===0) {
						$content = '<p>'.$content;
					}
					if(stripos($content,'<p>')===0) {
						$content = $content.'</p>';
					}
				}
			}
		}
		$this->db->table_name = $tablename;
		//上一页
		$previous_page = $this->db->get_one("`catid` = '$catid' AND `id`<'$id' AND `status`=99",'*','id DESC');
		//下一页
		$next_page = $this->db->get_one("`catid`= '$catid' AND `id`>'$id' AND `status`=99",'*','id ASC');

		if(empty($previous_page)) {
			$previous_page = array('title'=>L('first_page'), 'thumb'=>IMG_PATH.'nopic_small.gif', 'url'=>'javascript:alert(\''.L('first_page').'\');');
		}

		if(empty($next_page)) {
			$next_page = array('title'=>L('last_page'), 'thumb'=>IMG_PATH.'nopic_small.gif', 'url'=>'javascript:alert(\''.L('last_page').'\');');
		}
		include template('content',$template);
	}

	public function lists() {
				
		$this->_session_start();
		$catid = $_GET['catid'] = intval($_GET['catid']);
		$_priv_data = $this->_category_priv($catid);
		if($_priv_data=='-1') {
			$forward = urlencode(get_url());
			showmessage(L('login_website'),APP_PATH.'index.php?m=member&c=index&a=login&forward='.$forward);
		} elseif($_priv_data=='-2') {
			showmessage(L('no_priv'));
		}
		$_userid = $this->_userid;
		$_username = $this->_username;
		$_groupid = $this->_groupid;

		if(!$catid) showmessage(L('category_not_exists'),'blank');
		$siteids = getcache('category_content','commons');
		$siteid = $siteids[$catid];
		$CATEGORYS = getcache('category_content_'.$siteid,'commons');
		if(!isset($CATEGORYS[$catid])) showmessage(L('category_not_exists'),'blank');
		$CAT = $CATEGORYS[$catid];
		$siteid = $GLOBALS['siteid'] = $CAT['siteid'];
		extract($CAT);
		$setting = string2array($setting);
		//SEO
		if(!$setting['meta_title']) $setting['meta_title'] = $catname;
		$SEO = seo($siteid, '',$setting['meta_title'],$setting['meta_description'],$setting['meta_keywords']);
		define('STYLE',$setting['template_list']);
		$page = intval($_GET['page']);

		$template = $setting['category_template'] ? $setting['category_template'] : 'category';
		$template_list = $setting['list_template'] ? $setting['list_template'] : 'list';
		
		if($type==0) {

			$page = isset($_GET['page']) ? intval($_GET['page']) : 1;  //分页 
	      	$this->db=pc_base::load_model('news_model'); 
	      	$today=time();
	      	$where="inputtime <".$today."";
	        $newslist=$this->db->listinfo($where, 'inputtime desc,listorder asc', $page, 10);  
	        $wz_pages = $this->db->wz_pages;  //分页条 
		

			$template = $child ? $template : $template_list;
			$arrparentid = explode(',', $arrparentid);
			$top_parentid = $arrparentid[1] ? $arrparentid[1] : $catid;
			$array_child = array();
			$self_array = explode(',', $arrchildid);
	
			foreach ($self_array as $arr) {
				if($arr!=$catid && $CATEGORYS[$arr][parentid]==$catid) {
					$array_child[] = $arr;
				}
			}
			$arrchildid = implode(',', $array_child);
		
			$urlrules = getcache('urlrules','commons');
			$urlrules = str_replace('|', '~',$urlrules[$category_ruleid]);
			$tmp_urls = explode('~',$urlrules);
			$tmp_urls = isset($tmp_urls[1]) ?  $tmp_urls[1] : $tmp_urls[0];
			preg_match_all('/{\$([a-z0-9_]+)}/i',$tmp_urls,$_urls);
			if(!empty($_urls[1])) {
				foreach($_urls[1] as $_v) {
					$GLOBALS['URL_ARRAY'][$_v] = $_GET[$_v];
				}
			}
			define('URLRULE', $urlrules);
			$GLOBALS['URL_ARRAY']['categorydir'] = $categorydir;
			$GLOBALS['URL_ARRAY']['catdir'] = $catdir;
			$GLOBALS['URL_ARRAY']['catid'] = $catid;
			// echo $template;exit;
			include template('content',$template);
		} else {

			$this->page_db = pc_base::load_model('page_model');
			$r = $this->page_db->get_one(array('catid'=>$catid));
			if($r) extract($r);
			$template = $setting['page_template'] ? $setting['page_template'] : 'page';
			$arrchild_arr = $CATEGORYS[$parentid]['arrchildid'];
			if($arrchild_arr=='') $arrchild_arr = $CATEGORYS[$catid]['arrchildid'];
			$arrchild_arr = explode(',',$arrchild_arr);
			array_shift($arrchild_arr);
			$keywords = $keywords ? $keywords : $setting['meta_keywords'];
			$SEO = seo($siteid, 0, $title,$setting['meta_description'],$keywords);
			// echo $template;exit;
			include template('content',$template);	
			
			
		}
	}
	

	public function json_list() {
		if($_GET['type']=='keyword' && $_GET['modelid'] && $_GET['keywords']) {

			$modelid = intval($_GET['modelid']);
			$id = intval($_GET['id']);

			$MODEL = getcache('model','commons');
			if(isset($MODEL[$modelid])) {
				$keywords = safe_replace(new_html_special_chars($_GET['keywords']));
				$keywords = addslashes(iconv('utf-8','gbk',$keywords));
				$this->db->set_model($modelid);
				$result = $this->db->select("keywords LIKE '%$keywords%'",'id,title,url',10);
				if(!empty($result)) {
					$data = array();
					foreach($result as $rs) {
						if($rs['id']==$id) continue;
						if(CHARSET=='gbk') {
							foreach($rs as $key=>$r) {
								$rs[$key] = iconv('gbk','utf-8',$r);
							}
						}
						$data[] = $rs;
					}
					if(count($data)==0) exit('0');
					echo json_encode($data);
				} else {
					//没有数据
					exit('0');
				}
			}
		}

	}
	
	
	protected function _check_payment($flag,$paytype) {
		$_userid = $this->_userid;
		$_username = $this->_username;
		if(!$_userid) return false;
		pc_base::load_app_class('spend','pay',0);
		$setting = $this->category_setting;
		$repeatchargedays = intval($setting['repeatchargedays']);
		if($repeatchargedays) {
			$fromtime = SYS_TIME - 86400 * $repeatchargedays;
			$r = spend::spend_time($_userid,$fromtime,$flag);
			if($r['id']) return true;
		}
		return false;
	}
	
	
	protected function _category_priv($catid) {
		$catid = intval($catid);
		if(!$catid) return '-2';
		$_groupid = $this->_groupid;
		$_groupid = intval($_groupid);
		if($_groupid==0) $_groupid = 8;
		$this->category_priv_db = pc_base::load_model('category_priv_model');
		$result = $this->category_priv_db->select(array('catid'=>$catid,'is_admin'=>0,'action'=>'visit'));
		if($result) {
			if(!$_groupid) return '-1';
			foreach($result as $r) {
				if($r['roleid'] == $_groupid) return '1';
			}
			return '-2';
		} else {
			return '1';
		}
	 }


	public function tutor_login(){
		include template('content','tutor_login');
	}

	
	public function std_login(){
		include template('content','std_login');
	}

	
	public function log(){
		$this->_session_start();
    	if(isset($_POST['role'])){
			$session_storage = 'session_'.pc_base::load_config('system','session_storage');
        	pc_base::load_sys_class($session_storage);
            $email=$_POST['email'];
            $pwd=md5($_POST['pwd']);
            $role_verify=$_POST['role'];
    		$info=$this->member->get_one(array('email'=>$email,'role_verify'=>$role_verify));
    		$pinfo=$this->member->get_one(array('phone'=>$email,'role_verify'=>$role_verify));


    		$code = isset($_POST['code']) && trim($_POST['code']) ? trim($_POST['code']) : "";
    		if(strtolower($_SESSION['code'])!= strtolower($code)) {
				echo "驗證碼錯誤，請重新輸入";
				exit();
			}
	
			if(empty($info)&& empty($pinfo)){
				echo "此用戶未註冊，請先進行註冊再登陸";
				exit();
			}


			$infoData = array();
			if(empty($info)&& !empty($pinfo)){
				$infoData = $pinfo;
			}

			if(!empty($info)&& empty($pinfo)){
				$infoData = $info;
			}

			if(($email!=$infoData['phone'] && $email!=$infoData['email']) || $pwd!==$infoData['password']){
				echo "密碼不正確，請重新輸入";
					exit();
				}
			
            $_SESSION['id']=$infoData['userid'];
            $_SESSION['username']=$infoData['username'];
            $_SESSION['role_verify']=$infoData['role_verify'];
            $_SESSION['login_time']=time();

          
			$member=$this->member->get_one("`userid` =". $infoData['userid']);
			$gift_inte=$this->inte->get_inte_setting(10);
			if($member['integral']>=$gift_inte){
				echo '1';		
			}else{
				echo '0';
			}
    	}
    }

	
	public function contact_us(){
	 	if(isset($_POST['dosubmit'])){
	 		$data['userid']=$_SESSION['id'];
	 		$data['name']=$_POST['nickname'];
	 		$data['phone']=$_POST['phone'];
	 		$data['title']=$_POST['title'];
	 		$data['content']=$_POST['content'];
	 		$data['addtime']=date("Y-m-d H:i:s"); 
	 		$this->db = pc_base::load_model('user_suggestion_model');
	 		$consult=$this->db->insert($data);
	 	}
	 	
	
		if(isset($_POST['dosubmit'])){
			$nickname = $_POST['nickname'];
		 	$phone = $_POST['phone'];
		 	$email = $_POST['email'];
		 	$title = $_POST['title'];
		 	$content = $_POST['content'];
		 	pc_base::load_sys_func('mail');
		 	include "./phpcms/libs/functions/PushContactMailData.php";
		 	
		 	sendmail('enquiry@tutorseeking.com',"$email", $emailData); 
		 	echo 1;die;
		}
	
	 	
	 }



	 public function search(){
	
	 		$keyword=$_POST['keyword'];
	 		$today=time();
	 		$catid=$_GET['catid'];
	 		$where="inputtime < ".$today." and title LIKE '%".$keyword."%'";

	 	$page = isset($_GET['page']) ? intval($_GET['page']) : 1;  //分页 
      	$this->db=pc_base::load_model('news_model'); 
        $newslist=$this->db->listinfo($where, 'listorder', $page, 2);  
        $wz_pages = $this->db->wz_pages;  //分页条
        include template('content','list_share');
	 }

	public function subscribe(){
		if(isset($_POST)){
			$data['email']=$_POST['newlleter-email'];
			$this->db=pc_base::load_model('email_model');
			$where['email']=$data['email'];
			$consult=$this->db->select($where);
			if(empty($consult)){
				$this->db->insert($data);
				showmessage('訂閲成功',HTTP_REFERER);
			}else{
				showmessage('此郵箱已訂閲,請重新填寫',HTTP_REFERER);
			}
		}
	}


	public function logout(){
		$this->_session_start();
		unset($_SESSION['id']);
		unset($_SESSION['username']);
		unset($_SESSION['role_verify']);
		showmessage("登出成功", HTTP_REFERER);
	}



	public function morecase(){
		$this->_session_start();			

		$SEO['title'] = "配對中心 - Tutorseeking";

		$session_userid=$_SESSION['id'];
		$session_role=$_SESSION['role_verify'];	
		$PageType = 0;
		$isShow = 1;
		$page=$_GET['page']?$_GET['page']:'1'; 
		$limit=12; 
		
		if($_GET){
			$_SESSION['type']=$_GET['typeid'];		
			$_SESSION['item']=$_GET['subid'];		
			$_SESSION['level']=$_GET['gradeid'];	
			$_SESSION['place']=$_GET['place'];		
			$_SESSION['hourlyfee']=$_GET['fee'];	
			$_SESSION['miniEduLevel']=$_GET['eduid'];		
			$_SESSION['gender']=$_GET['sexid'];		
			$_SESSION['LIKE_collage']=$_GET['colid'];		
			$_SESSION['classlang']=$_GET['classlang'];		
			$_SESSION['personid']=$_GET['personid'];	
			$_SESSION['tutor_hschool2']=$_GET['tutor_hschool2'];	
			$_SESSION['weeknum']=$_GET['weeknum'];	
			$_SESSION['teachstar']=$_GET['teachstar'];	
		}

		$type=isset($_SESSION['type'])?$_SESSION['type']:'';			
		$item=isset($_SESSION['item'])?$_SESSION['item']:'';			
		$level=isset($_SESSION['level'])?$_SESSION['level']:'';			
		$place=isset($_SESSION['place'])?$_SESSION['place']:'';		
		$hourlyfee=isset($_SESSION['hourlyfee'])?$_SESSION['hourlyfee']:'';			
		$miniEduLevel=isset($_SESSION['miniEduLevel'])?$_SESSION['miniEduLevel']:'';			
		$gender=isset($_SESSION['gender'])?$_SESSION['gender']:'';	
		$classlang=isset($_SESSION['classlang'])?$_SESSION['classlang']:'';	
		$personid=isset($_SESSION['personid'])?$_SESSION['personid']:'';	
		$tutor_hschool2=isset($_SESSION['tutor_hschool2'])?$_SESSION['tutor_hschool2']:'';	
		$weeknum=isset($_SESSION['weeknum'])?$_SESSION['weeknum']:'';	
		$teachstar=isset($_SESSION['teachstar'])?$_SESSION['teachstar']:'';	

		
		$ScreeningData['type'] = $type;
		$ScreeningData['item'] = $item;
		$ScreeningData['level'] = $level;
		$ScreeningData['place'] = $place;
		$ScreeningData['hourlyfee'] = $hourlyfee;
		$ScreeningData['classlang'] = $classlang;
		$ScreeningData['gender'] = $gender;
		$ScreeningData['miniEduLevel'] = $miniEduLevel;
		$ScreeningData['personid'] = $personid;
		$ScreeningData['tutor_hschool2'] =$tutor_hschool2;
		$ScreeningData['weeknum'] =$weeknum;
		$ScreeningData['teachstar'] =$teachstar;
		
		if($_SESSION['role_verify']=="1"){		

			
			if(!empty($_GET['Project']) && !empty($_GET['Level']) && !empty($_GET['price']) && !empty($_GET['type'])) {
				
				$type = $this->tutor_type->get_one("tt_name = '$_GET[type]' AND tt_parentid = 0",'tt_id')['tt_id'];		

				$_SESSION['type']=$type;		
				$ScreeningData['type'] = $type;
				if(!empty($type)){	
					$item = $this->tutor_type->get_one("tt_name = '$_GET[Project]' AND tt_parentid = $type",'tt_id')['tt_id'];		
					$_SESSION['item']=$item;
					$ScreeningData['item'] = $item;		
					if(!empty($item)){		
						$level = $this->tutor_type->get_one("tt_name = '$_GET[Level]' AND tt_parentid = $item",'tt_id')['tt_id'];			
						
						$_SESSION['level']=$level;	
						$ScreeningData['level'] = $level;
					}								
				}
				$hourlyfee = $_GET['price'];			
				$_SESSION['hourlyfee']=$hourlyfee;	
				$ScreeningData['hourlyfee'] = $hourlyfee;
			
			}

			if(!empty($hourlyfee)){
				$hourlyfee = array($hourlyfee-50,$hourlyfee+50);
			}
		
			

			pc_base::load_app_class('tutor' ,'pair');
			$tutor=new tutor();
			$caselist=$tutor->getSuitedTeacher($type,$item,$level,$place,$classtime,$hourlyfee,$miniEduLevel,$gender,$classlang,$like_collage,$page,$limit='12',$order,$personid,$tutor_hschool2,$weeknum,3,$teachstar);
			
		}elseif ($_SESSION['role_verify']=="0") {	
			if(!empty($hourlyfee)){
				$hourlyfee = array($hourlyfee-50,$hourlyfee+50);
			}
			pc_base::load_app_class('student' ,'pair');
			$tutor=new student();
			$caselist=$tutor->getSuitedStudent($type,$item,$level,$place,$classtime,$hourlyfee,$miniEduLevel,$gender,$classlang,$like_collage,$page,$limit='12',$order,$personid,$weeknum,3);	
		}else{
	
			if(!empty($hourlyfee)){
				$hourlyfee = array($hourlyfee-50,$hourlyfee+50);
			}
			pc_base::load_app_class('tutor' ,'pair');
			$tutor=new tutor();
		
			$tutorlist=$tutor->getSuitedTeacher($type,$item,$level,$place,$classtime,$hourlyfee,$miniEduLevel,$gender,$classlang,$like_collage,'','',$order,$personid,$tutor_hschool2,$weeknum,'',$teachstar);

			pc_base::load_app_class('student' ,'pair');
			$student=new student();
			$stulist=$student->getSuitedStudent($type,$item,$level,$place,$classtime,$hourlyfee,$miniEduLevel,$gender,$classlang,$like_collage,'','',$order,$personid,$weeknum);

			$total_caselist=array_merge($tutorlist['data'],$stulist['data']);
			$start = ($page-1)*$limit;
			$caselist['data']=array_slice($total_caselist,$start,$limit);
			$caselist['page_link'] = pages($num = count($total_caselist), $page, $limit, '', array(), 3);
		}
		include template('content','morecase');	
	}


	public function parent_act(){
		$this->_session_start();
		if($_SESSION['role_verify']=='1'){

			$SEO['title'] = "家長活動 - Tutorseeking";
			$td = date("Y-m-d");
			$this->act_db = pc_base::load_model('parent_act_model');
			$sql = "select m.personid,p.act_number,p.act_name,p.act_area,p.act_date,p.act_address,p.act_closing_date,p.act_content,p.act_aim,p.act_ischarge,p.act_adult_limit,p.act_child_limit,p.act_charge_mode,p.act_charge,p.act_time,p.act_cut_date from `v9_parent_act` as p 
			left join `v9_member` as m 
			on p.act_userid = m.userid 
			left join `v9_parent_act_rev` as r
			on r.par_act_num = p.act_number
			where r.par_status = '1' and p.act_cut_date>= '".$td."' and p.act_closing_date>= '".$td."'";
           	
           	if(isset($_GET['closing_date']) || isset($_GET['start_date']) || isset($_GET['act_number']) || isset($_GET['act_area'])){
           		$start_date = $_GET['start_date']; 
				$closing_date = $_GET['closing_date'];
				$act_number = $_GET['act_number'];
				$act_area = $_GET['act_area'];
				if(!empty($act_number)){
					$sql = $sql."and p.act_number = "."'".$act_number."'";
				}
				if(!empty($act_area)){
					$sql = $sql."and p.act_area = "."'".$act_area."'";
				}
				if(!empty($start_date) ){
					$sql = $sql."and p.act_date>="."'".$start_date."'";
				}
				if(!empty($closing_date) ){
					$sql = $sql."and p.act_closing_date<="."'".$closing_date."'";
				}
			}
            $sql= $sql." order by p.act_id desc";

	        $get_page = pc_base::load_model('get_model');
	        $page = intval($_GET['page'])?intval($_GET['page']) :'1';
	        $infos = $get_page->multi_listinfo($sql,$page,8);
        	$pages = $get_page->pages;
			include template('content','parent_act');
		}else{
			showmessage('請登錄家長賬號', APP_PATH."index.php?m=content&c=index&a=std_login");
		}
	}

	
	public function send_url(){
		$this->_session_start();

		$act_id=$_GET['act_id'];
		$act_personid=$_GET['act_personid'];
		$act_userid = $_GET['act_userid'];
		if(isset($_SESSION['id']) && !empty($_SESSION['id'])){
			header("location:".APP_PATH."index.php?m=content&c=index&a=parent_act_detail&act_id=".$act_id."&act_personid=".$act_personid."");	
		}else{
			showmessage('请先登録!',APP_PATH."index.php?m=content&c=index&a=std_login&act_id=".$act_id."&act_personid=".$act_personid."");
		}
	}


	public function parent_act_detail(){
		$this->_session_start();
		$this->act_db = pc_base::load_model('parent_act_model');
		$this->act_rev_db = pc_base::load_model('parent_act_rev_model');

		$_SESSION['act_personid'] = $_GET['act_personid'];
		$act_number = $_GET['act_id'];
		if(isset($_SESSION['id']) && $_SESSION['role_verify']=="1"){
		
			$user_id = $_SESSION['id'];
		
			if(isset($_GET)){
				
				$act_parent_id = $this->member->get_one(array('personid'=>$_GET['act_personid']),'userid')['userid'];
			
				$act_join_id = $this->act_db->get_one(array('act_number'=>$act_number),'act_join_parentid')['act_join_parentid'];
				
				$is_inactid = in_array($user_id, explode(',', $act_join_id));

				$act_detail = $this->act_db->select(array('act_number'=>$act_number));
				$act_rev_detail = $this->act_rev_db->select(array('par_act_num'=>$act_number));
			}
		
			if (isset($_POST['adult_num']) && isset($_POST['child_num']) && isset($_POST['act_num'])) {
				$act_num = $_POST['act_num'];
				$child_num = $_POST['child_num'];
				$adult_num = $_POST['adult_num'];
				$act_personid = $_POST['parent_id'];

				$act_detail = $this->act_db->get_one(array('act_number'=>$act_num),'act_date,act_closing_date,act_time,act_area,act_address,act_child,act_adult,act_child_limit,act_adult_limit,act_join_parentid');
			
				$act_area_name = $this->syl->get_one(array('loc_id'=>$act_detail['act_area']))['loc_name'];
			
				$Time_s_arr = explode(",", $act_detail['act_time']);
				$fromTime = substr_replace($Time_s_arr[0],':','2','0');
				$toTime = substr_replace($Time_s_arr[1],':','2','0');
				$act_time = $act_detail['act_date']." ".$fromTime."  至  ".$act_detail['act_closing_date']." ".$toTime;
				
				$act_child = $act_detail['act_child'];
				$act_adult = $act_detail['act_adult'];
				if(!preg_match("/^[0-9][0-9]*$/",$adult_num)){
					echo "error_adult_num";
				}elseif(!preg_match("/^[0-9][0-9]*$/",$child_num)){
					echo "error_child_num";	
				}elseif ($act_detail['act_child_limit']<$child_num){
					echo "child_error";
				}elseif ($act_detail['act_adult_limit']<$adult_num) {
					echo "adult_error";
				}elseif($act_detail['act_adult_limit']<(string)($adult_num+$act_adult)){
					echo "adult_full";
				}elseif($act_detail['act_child_limit']<(string)($child_num+$act_child)){
					echo "child_full";
				}else{
					
					$act_child+=$child_num;
					$act_adult+=$adult_num;
				
					$act_join_parentid = $act_detail['act_join_parentid'].",".$user_id;
					$res = $this->act_db->update(array('act_join_parentid'=>$act_join_parentid,'act_child'=>$act_child,'act_adult'=>$act_adult),array('act_number'=>$act_num));
					if($res){
						$this->m_db = pc_base::load_model('member_model');
					
						$starter_email = $this->m_db->select(array('personid'=>$act_personid),$data='email',$limit='1',$order='')[0]['email'];
					
						$joiner_email = $this->m_db->select(array('userid'=>$user_id),$data='email',$limit='1',$order='')[0]['email'];
						$joiner_username = $this->m_db->select(array('userid'=>$user_id),$data='username',$limit='1',$order='')[0]['username'];
						$email_data="<!DOCTYPE html>
	                <html>
	                <head>
	                    <meta charset='utf-8'>
	                    <meta name='viewport' content='width=device-width,initial-scale=1.0, maximum-scale=1.0,minimum-scale=1.0,user-scalable=no'>
	                    <title>tutorseeking</title>
	                    <style>
	                        /*.match-table td{padding: 6px;}*/
	                    </style>
	                </head>
	                <body>
	                    <div style='max-width: 880px; margin:0 auto; font-size: 14px; color: #333; text-align: justify'>
	                        <form action=''>
	                            <div style='text-align: center'>
	                                <br>
	                                <br>
	                                <img width='250px' src='https://www.tutorseeking.com/statics/images/add_image/logo-sticky.png' alt=''>
	                            </div>
	                            <h2 style='text-align: center; font-size: 24px;'>尊敬的用戶：您好！您于 ". date('Y-m-d H:i')." 參與了TutorSeeking的家長活動！以下是您的活動信息！</h2>
	                            <br>
	                            <div style='text-align: center;margin-left:8%;'>
	                                <div style='display:inline-block; margin-right: 40px; margin-bottom: 10px'>
	                                    <table border='1px' class='match-table' cellpadding='0' cellspacing='0' style='width: 650px; font-size: 20px; text-align: center;border-collapse:collapse' >
	                                        <tbody>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;'  colspan='2' >用戶信息</td>
	                                        </tr>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;' >參與者姓名</td>
	                                            <td style='padding: 6px;font-size: 20px;' >".$joiner_username."</td>
	                                        </tr>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;' >活動編號</td>
	                                            <td style='padding: 6px;font-size: 20px;' >".$act_num."</td>
	                                        </tr>

	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;' >活動時間</td>
	                                            <td style='padding: 6px;font-size: 20px;' >".$act_time."</td>
	                                        </tr>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;' >活動地址</td>
	                                            <td style='padding: 6px;font-size: 20px;' >".$act_area_name." ".$act_detail['act_address']."</td>
	                                        </tr>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;'  colspan='2'><a href='https://www.tutorseeking.com/index.php?m=content&c=index&a=send_url&act_id=".$act_num."&act_personid=".$act_personid."&act_userid=".$user_id."' style='text-decoration: none; color: #e39916;'>查看詳情</a></td>
	                                        </tr>
	                                        </tbody>
	                                    </table>
	                                </div>
	                            </div>
	                            <br>";
	                $stu_email_data .="        </form>
	                    </div>
	                </body>
	                </html>";

	                $email_start_data="<!DOCTYPE html>
	                <html>
	                <head>
	                    <meta charset='utf-8'>
	                    <meta name='viewport' content='width=device-width,initial-scale=1.0, maximum-scale=1.0,minimum-scale=1.0,user-scalable=no'>
	                    <title>tutorseeking</title>
	                    <style>
	                        /*.match-table td{padding: 6px;}*/
	                    </style>
	                </head>
	                <body>
	                    <div style='max-width: 880px; margin:0 auto; font-size: 14px; color: #333; text-align: justify'>
	                        <form action=''>
	                            <div style='text-align: center'>
	                                <br>
	                                <br>
	                                <img width='250px' src='https://www.tutorseeking.com/statics/images/add_image/logo-sticky.png' alt=''>
	                            </div>
	                            <h2 style='text-align: center; font-size: 24px;'>尊敬的用戶：您好！家長".$joiner_username."于 ". date('Y-m-d H:i')." 參與了您在TutorSeeking發起的家長活動！以下是您的活動信息！</h2>
	                            <br>
	                            <div style='text-align: center;margin-left:8%;'>
	                                <div style='display:inline-block; margin-right: 40px; margin-bottom: 10px'>
	                                    <table border='1px' class='match-table' cellpadding='0' cellspacing='0' style='width: 650px; font-size: 20px; text-align: center;border-collapse:collapse' >
	                                        <tbody>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;'  colspan='2' >用戶信息</td>
	                                        </tr>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;' >參與者姓名</td>
	                                            <td style='padding: 6px;font-size: 20px;' >".$joiner_username."</td>
	                                        </tr>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;' >活動編號</td>
	                                            <td style='padding: 6px;font-size: 20px;' >".$act_num."</td>
	                                        </tr>

	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;' >活動時間</td>
	                                            <td style='padding: 6px;font-size: 20px;' >".$act_time."</td>
	                                        </tr>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;' >活動地址</td>
	                                            <td style='padding: 6px;font-size: 20px;' >".$act_area_name." ".$act_detail['act_address']."</td>
	                                        </tr>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;'  colspan='2'><a href='https://www.tutorseeking.com/index.php?m=content&c=index&a=send_url&act_id=".$act_num."&act_personid=".$act_personid."&act_userid=".$userid."' style='text-decoration: none; color: #e39916;'>查看詳情</a></td>
	                                        </tr>
	                                        </tbody>
	                                    </table>
	                                </div>
	                            </div>
	                            <br>";
	                $email_start_data .="        </form>
	                    </div>
	                </body>
	                </html>";

	                pc_base::load_sys_func('mail'); 
	                sendmail($joiner_email,'TutorSeeking家長活動信息', $email_data);
	                sendmail($starter_email,'TutorSeeking家長活動信息', $email_start_data);
						echo "success";
					}else{
						echo "fail";
					}
				}
				exit;
			}
		}else{
			showmessage('請登錄家長賬號', APP_PATH."index.php?m=content&c=index&a=std_login&act_id=".$_GET['act_id']."&act_personid=".$_GET['act_personid']."");
		}
		include template('content','parent_act_detail');
	}



	public function parent_act_add(){
		$this->_session_start();
		$timelist = array(
        '0700' =>"7:00" ,'0715' =>"7:15" ,'0730' =>"7:30",'0745' =>"7:45",
        '0800' =>"8:00" ,'0815' =>"8:15" ,'0830' =>"8:30" ,'0845' =>"8:45",
        '0900' =>"9:00" ,'0915' =>"9:15" ,'0930' =>"9:30" ,'0945' =>"9:45",
        '1000' =>"10:00" ,'1015' =>"10:15" ,'1030' =>"10:30" ,'1045' =>"10:45",
        '1100' =>"11:00" ,'1115' =>"11:15" ,'1130' =>"11:30" ,'1145' =>"11:45",
        '1200' =>"12:00" ,'1215' =>"12:15" ,'1230' =>"12:30" ,'1245' =>"12:45",
        '1300' =>"13:00" ,'1315' =>"13:15" ,'1330' =>"13:30" ,'1345' =>"13:45",
        '1400' =>"14:00" ,'1415' =>"14:15" ,'1430' =>"14:30" ,'1445' =>"14:45",
        '1500' =>"15:00" ,'1515' =>"15:15" ,'1530' =>"15:30" ,'1545' =>"15:45",
        '1600' =>"16:00" ,'1615' =>"16:15" ,'1630' =>"16:30" ,'1645' =>"16:45",
        '1700' =>"17:00" ,'1715' =>"17:15" ,'1730' =>"17:30" ,'1745' =>"17:45",
        '1800' =>"18:00" ,'1815' =>"18:15" ,'1830' =>"18:30" ,'1845' =>"18:45",
        '1900' =>"19:00" ,'1915' =>"19:15" ,'1930' =>"19:30" ,'1945' =>"19:45",
        '2000' =>"20:00" ,'2015' =>"20:15" ,'2030' =>"20:30" ,'2045' =>"20:45",
        '2100' =>"21:00" ,'2115' =>"21:15" ,'2130' =>"21:30" ,'2145' =>"21:45",
        '2200' =>"22:00" ,'2215' =>"22:15" ,'2230' =>"22:30" ,'2245' =>"22:45",
        '2300' =>"23:00");

		if(isset($_SESSION['id']) && $_SESSION['role_verify']=="1"){
			if(isset($_POST['parent_act_info'])){
				$parent_act_info = $_POST['parent_act_info'];
				
				$id = $_SESSION['id'];
			
				$person_id = $this->member->get_one(array('userid'=>$id),'personid')['personid'];
				$_SESSION['act_personid'] = $person_id;
			
				$fromTimes = substr_replace($parent_act_info['from_time'],':','2','0');
				$toTimes = substr_replace($parent_act_info['to_time'],':','2','0');

				$fromTime = $parent_act_info['from_time'];
				$toTime = $parent_act_info['to_time'];

				$username = $_SESSION['username'];
				$act_time = $parent_act_info['act_date']." ".$fromTimes."  至  ".$parent_act_info['act_closing_date']." ".$toTimes;
				$act_area = $parent_act_info['act_area']." ".$parent_act_info['act_adress'];
				$act_cut_time = $parent_act_info['act_cut_date'];
			
				if(!empty($fromTime) && !empty($toTime)){
					$parent_act_info['act_time'] = $fromTime .",".$toTime;
				}
				unset($parent_act_info['from_time']);
				unset($parent_act_info['to_time']);
				$parent_act_info['act_userid'] = $id;
				$parent_act_info['act_join_parentid'] = $id;

		
				$this->act_add = pc_base::load_model('parent_act_model');
				$act_number_infos = $this->act_add->select("","act_number","",$order="act_id desc");
				if(empty($act_number_infos)){
					$parent_act_info['act_number'] = "H0000168";
				}else{
			
					$act_number_arr = str_split($act_number_infos[0]['act_number'],1);
		
					for($i=0;$i<strlen($act_number_infos[0]['act_number']);$i++){
						if(preg_match('/[1-9]/',$act_number_arr[$i])){
							$N[] = substr($act_number_infos[0]['act_number'],$i);
						}
					}
			
					$all_len = strlen($act_number_infos[0]['act_number']);
				
					$all_lens = $all_len-1;

					$len = strlen($N[0]); 
				
					$len_2 = strlen($N[0]+1);
					if($len!=$len_2){
					
						if($len == $all_lens){
							$left = "H".substr($act_number_infos[0]['act_number'],0,$all_len-$len-1);
						}else{
							$left = substr($act_number_infos[0]['act_number'],0,$all_len-$len-1);
						}						
					}else{
						$left = substr($act_number_infos[0]['act_number'],0,$all_len-$len);
					}
					$right = substr($act_number_infos[0]['act_number'],$all_len-$len,$len);
					$parent_act_info['act_number'] = $left.($right+1);
				}

				$_SESSION['act_number'] = $parent_act_info['act_number'];
				

				$act_add_res=$this->act_add->insert($parent_act_info);
				
		
				$parent_act_rev['par_parent_id'] = $id;
				$parent_act_rev['par_act_num'] = $_SESSION['act_number'];
				$parent_act_rev['par_remark'] ="";
				$parent_act_rev['par_status'] ="0";
				$this->act_add_rev = pc_base::load_model('parent_act_rev_model');
				$act_rev_res = $this->act_add_rev->insert($parent_act_rev);

				
				if($act_rev_res && $act_add_res){
		
					$this->m_db = pc_base::load_model('member_model');
					$to_stumail_info = $this->m_db->select(array('userid'=>$id),$data='email',$limit='1',$order='');
					$to_stumail = $to_stumail_info[0]['email'];
			
					$act_area_name = $this->syl->get_one(array('loc_id'=>$act_area))['loc_name'];
				
					$admin_email = "enquiry@tutorseeking.com";
			
					$stu_email_data="<!DOCTYPE html>
	                <html>
	                <head>
	                    <meta charset='utf-8'>
	                    <meta name='viewport' content='width=device-width,initial-scale=1.0, maximum-scale=1.0,minimum-scale=1.0,user-scalable=no'>
	                    <title>tutorseeking</title>
	                    <style>
	                        /*.match-table td{padding: 6px;}*/
	                    </style>
	                </head>
	                <body>
	                    <div style='max-width: 880px; margin:0 auto; font-size: 14px; color: #333; text-align: justify'>
	                        <form action=''>
	                            <div style='text-align: center'>
	                                <br>
	                                <br>
	                                <img width='250px' src='https://www.tutorseeking.com/statics/images/add_image/logo-sticky.png' alt=''>
	                            </div>
	                            <h2 style='text-align: center; font-size: 24px;'>尊敬的用戶：您好！您于 ". date('Y-m-d H:i')." 發起了TutorSeeking的家長活動！以下是您的活動信息！</h2>
	                            <br>
	                            <div style='text-align: center;margin-left:8%;'>
	                                <div style='display:inline-block; margin-right: 40px; margin-bottom: 10px'>
	                                    <table border='1px' class='match-table' cellpadding='0' cellspacing='0' style='width: 650px; font-size: 20px; text-align: center;border-collapse:collapse' >
	                                        <tbody>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;'  colspan='2' >用戶信息</td>
	                                        </tr>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;' >姓名</td>
	                                            <td style='padding: 6px;font-size: 20px;' >".$username."</td>
	                                        </tr>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;' >活動編號</td>
	                                            <td style='padding: 6px;font-size: 20px;' >".$parent_act_rev['par_act_num']."</td>
	                                        </tr>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;' >活動時間</td>
	                                            <td style='padding: 6px;font-size: 20px;' >".$act_time."</td>
	                                        </tr>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;' >活動報名截止時間</td>
	                                            <td style='padding: 6px;font-size: 20px;' >".$act_cut_time."</td>
	                                        </tr>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;' >活動地址</td>
	                                            <td style='padding: 6px;font-size: 20px;' >".$act_area_name."</td>
	                                        </tr>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;'  colspan='2'><a href='https://www.tutorseeking.com/index.php?m=content&c=index&a=send_url&act_id=".$_SESSION['act_number']."&act_personid=".$person_id."&act_userid=".$id."' style='text-decoration: none; color: #e39916;'>查看詳情</a></td>
	                                        </tr>
	                                        </tbody>
	                                    </table>
	                                </div>
	                            </div>
	                            <br>";
	                $stu_email_data .="        </form>
	                    </div>
	                </body>
	                </html>";

	                $admin_email_data="<!DOCTYPE html>
	                <html>
	                <head>
	                    <meta charset='utf-8'>
	                    <meta name='viewport' content='width=device-width,initial-scale=1.0, maximum-scale=1.0,minimum-scale=1.0,user-scalable=no'>
	                    <title>tutorseeking</title>
	                    <style>
	                        /*.match-table td{padding: 6px;}*/
	                    </style>
	                </head>
	                <body>
	                    <div style='max-width: 880px; margin:0 auto; font-size: 14px; color: #333; text-align: justify'>
	                        <form action=''>
	                            <div style='text-align: center'>
	                                <br>
	                                <br>
	                                <img width='250px' src='https://www.tutorseeking.com/statics/images/add_image/logo-sticky.png' alt=''>
	                            </div>
	                            <h2 style='text-align: center; font-size: 24px;'>您好!  用戶".$username."于 ". date('Y-m-d H:i')." 發起了TutorSeeking家長活動！以下是活動信息,請盡快前往審批！</h2>
	                            <br>
	                            <div style='text-align: center;margin-left:8%;'>
	                                <div style='display:inline-block; margin-right: 40px; margin-bottom: 10px'>
	                                    <table border='1px' class='match-table' cellpadding='0' cellspacing='0' style='width: 650px; font-size: 20px; text-align: center;border-collapse:collapse' >
	                                        <tbody>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;'  colspan='2' >用戶信息</td>
	                                        </tr>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;' >姓名</td>
	                                            <td style='padding: 6px;font-size: 20px;' >".$username."</td>
	                                        </tr>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;' >活動編號</td>
	                                            <td style='padding: 6px;font-size: 20px;' >".$parent_act_rev['par_act_num']."</td>
	                                        </tr>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;' >活動時間</td>
	                                            <td style='padding: 6px;font-size: 20px;' >".$act_time."</td>
	                                        </tr>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;' >活動報名截止時間</td>
	                                            <td style='padding: 6px;font-size: 20px;' >".$act_cut_time."</td>
	                                        </tr>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;' >活動地址</td>
	                                            <td style='padding: 6px;font-size: 20px;' >".$act_area_name."</td>
	                                        </tr>
	                                        <tr>
	                                            <td style='padding: 6px;font-size: 20px;'  colspan='2'><a href='https://www.tutorseeking.com/admin.php' style='text-decoration: none; color: #e39916;'>前往登録</a></td>
	                                        </tr>
	                                        </tbody>
	                                    </table>
	                                </div>
	                            </div>
	                            <br>";
	                $admin_email_data .="        </form>
	                    </div>
	                </body>
	                </html>";
	                pc_base::load_sys_func('mail'); 
	                sendmail($to_stumail,'TutorSeeking家長活動信息', $stu_email_data);
	                sendmail($admin_email,'TutorSeeking家長活動信息', $admin_email_data);
				}else{
				
				}  
				exit;
			}
		}else{
		
			showmessage('請登録!',APP_PATH.'index.php');
	
		}
		include template('content','parent_act_add');
	}


	public function map(){
		$this->_session_start();			

		$session_userid=$_SESSION['id'];
		$session_role=$_SESSION['role_verify'];	

		if($_POST){
			$type=$_POST['typeid'];		
			$item=$_POST['subid'];		
			$level=$_POST['gradeid'];	
			$place=$_POST['place'];		
			$hourlyfee=$_POST['fee'];	
			$miniEduLevel=$_POST['eduid'];		
			$gender=$_POST['sexid'];		
			$like_collage=$_POST['colid'];		
			$classlang=$_POST['classlang'];		
			$personid=$_POST['personid'];	
			$tutor_hschool2=$_POST['tutor_hschool2'];	
			$weeknum=$_POST['weeknum'];	

		}
		$ScreeningData['type'] = $type;
		$ScreeningData['item'] = $item;
		$ScreeningData['level'] = $level;
		$ScreeningData['place'] = $place;
		$ScreeningData['hourlyfee'] = $hourlyfee;
		$ScreeningData['classlang'] = $classlang;
		$ScreeningData['gender'] = $gender;
		$ScreeningData['miniEduLevel'] = $miniEduLevel;
		$ScreeningData['personid'] = $personid;
		$ScreeningData['tutor_hschool2'] =$tutor_hschool2;
		$ScreeningData['weeknum'] =$weeknum;
		
		
		if($_SESSION['role_verify']=="1"){		

			if(!empty($hourlyfee)){
				$hourlyfee = array($hourlyfee-50,$hourlyfee+50);
			}
			
			pc_base::load_app_class('tutor' ,'pair');
			$tutor=new tutor();
		
			$alllist=$tutor->getSuitedTeacher($type,$item,$level,$place,$classtime,$hourlyfee,$miniEduLevel,$gender,$classlang,$like_collage,$page,$limit,$order,$personid,$tutor_hschool2,$weeknum);
			$caselist=$alllist['data'];
		}elseif ($_SESSION['role_verify']=="0") {	
			if(!empty($hourlyfee)){
				$hourlyfee = array($hourlyfee-50,$hourlyfee+50);
			}
			pc_base::load_app_class('student' ,'pair');
			$tutor=new student();
			$alllist=$tutor->getSuitedStudent($type,$item,$level,$place,$classtime,$hourlyfee,$miniEduLevel,$gender,$classlang,$like_collage,$page,$limit,$order,$personid,$weeknum);	
			$caselist=$alllist['data'];
		}else{
			//var_dump($weeknum);
			if(!empty($hourlyfee)){
				$hourlyfee = array($hourlyfee-50,$hourlyfee+50);
			}
			pc_base::load_app_class('tutor' ,'pair');
			$tutor=new tutor();
			// echo $hourlyfee;exit;
			$tutorlist=$tutor->getSuitedTeacher($type,$item,$level,$place,$classtime,$hourlyfee,$miniEduLevel,$gender,$classlang,$like_collage,'','',$order,$personid,$tutor_hschool2,$weeknum);

			pc_base::load_app_class('student' ,'pair');
			$student=new student();
			$stulist=$student->getSuitedStudent($type,$item,$level,$place,$classtime,$hourlyfee,$miniEduLevel,$gender,$classlang,$like_collage,'','',$order,$personid,$weeknum);

			$caselist=array_merge($tutorlist['data'],$stulist['data']);
		}
	
		$newlist=array();
		$weeknums = array(6=> 'S',0=> 'M',1=> 'T',2=> 'W',3=> 'T',4=> 'F',5=> 'S');
		foreach ($caselist as $key => $case) {
			if($case['role_verify']=='0'){
			
                pc_base::load_app_class('helpcommon' ,'pair');
                $helpcommon=new helpcommon();
                $place=$helpcommon->create_location($case['tsl_locid']);
                if(strpos($place,',') !== false){
                    $place=substr($place,0,strpos($place,',')).'...';
                }
              
                $subject=$helpcommon->create_subject($case['tstt_ttid_2']);
                if(strpos($subject,',') !== false){
                    $subject=substr($subject,0,strpos($subject,',')).'...';
                }
              
                $weeknum="";
                if($case['tst_weekday']==''){
                   foreach($weeknums as $num){
                   	$weeknum.="<span>".$num."<span>";
                   }
               	}else{
                    $week=explode(',',$case['tst_weekday']);
	                foreach($weeknums as $k=>$num){
	                	$str="";
	                	if(strpos($case['tst_weekday'],strval($k)) !== false ){
	                		$str = "style='color: black'";
	                	}
	                	$weeknum.="<span ".$str.">".$num."</span>";
	                }
                }
                $newlist[$key]['role']=$case['role_verify'];
	            $newlist[$key]['userid']="<h5>導師編號：".$case['personid']."</h5>";
                $newlist[$key]['caseid']=$case['tm_tutorid'];
                $newlist[$key]['place']="<dd>授課地區</dd><dt>".$place."</dt>";
                $newlist[$key]['subject']="<dd>可教授科目</dd><dt>".$subject."</dt>";
                $newlist[$key]['studentnum']="<dd>主要教学语言</dd><dt>".$case['tutor_main_lang']."</dt>";
                $newlist[$key]['weeknum']="<dd>可教授日期</dd><dt class='class_time'>".$weeknum."</dt>";
               	$newlist[$key]['url']="/index.php?m=teachers&c=index&a=tutor_detail&userid=".$case['userid']."&tutor_id=".$case['tm_tutorid']; 
            }else{
     
                $weeknum="";
                if($case['sst_weekday']==''){
                   foreach($weeknums as $num){
                   	$weeknum.="<span>".$num."<span>";
                   }
               	}else{
                    $week=explode(',',$case['sst_weekday']);
	                foreach($weeknums as $k=>$num){
	                	$str="";
	                	if(strpos($case['sst_weekday'],strval($k)) !== false ){
	                		$str = "style='color: black'";
	                	}
	                	$weeknum.="<span ".$str.">".$num."</span>";
	                }
                }
                $newlist[$key]['role']=$case['role_verify'];
                $newlist[$key]['address']=$case['loc_name'].$case['student_address'];
	            $newlist[$key]['userid']="<h5>學生編號：".$case['personid']."</h5>";
            	$newlist[$key]['caseid']=$case['st_id'];
            	$newlist[$key]['place']="<dd>上課地區</dd><dt>".$case['loc_name']."</dt>";
            	$newlist[$key]['subject']="<dd>補習科目</dd><dt>".$case['tt_name']."</dt>";
            	$newlist[$key]['studentnum']="<dd>學生人數</dd><dt>".$case['st_num_join']."</dt>";
	            $newlist[$key]['weeknum']="<dd>要求上課日期</dd><dt class='class_time'>".$weeknum."</dt>";
	            $newlist[$key]['url']="/index.php?m=tparents&c=index&a=stu_detail&stuid=".$case['userid']."&tran_id=".$case['st_id']; 
            }
            $newlist[$key]['userimage']=$case['userimage'];
            $totallist['case']=$newlist;

		}
		$loc=$this->syl->get_one(array("loc_id"=>$_POST['place']));
		$totallist['lng_lat']=$loc['loc_google_coord'];
		echo json_encode($totallist);

	}

	public function password_find(){
		$this->_session_start();
		if(isset($_POST['mobile']) && isset($_POST['userType']) && isset($_POST['msg_code']) && isset($_POST['newpasswd'])){
		
            if(strtolower($_SESSION['msg_code'])!= strtolower($_POST['msg_code'])) {
                $return['code']='error_code';
                echo json_encode($return);exit;
            }
    
            if(strtolower($_SESSION['msg_phone'])!= strtolower($_POST['mobile'])) {
                $return['code']='error_mobile';
                echo json_encode($return);exit;
            }
            $this->db=pc_base::load_model('member_model');
            $data['password'] = md5($_POST['newpasswd']);
 			$update = $this->db->update($data,array('phone'=>$_POST['mobile'],'role_verify'=>$_POST['userType']));
 			if($update){
 				$return['code']='success';
 				echo json_encode($return);exit;
 			}
		}

		include template('content','password_find');
	}


	public function checkMobIdentity(){
		if(!empty($_POST['mobile']) && isset($_POST['userType'])){
			$this->db=pc_base::load_model('member_model');
			$is_exist = $this->db->get_one(array('phone'=>$_POST['mobile'],'role_verify'=>$_POST['userType']));
			if($is_exist){
				$return['code'] = 'existent';
			}else{
				$return['code'] = 'non-existent';
			}
			echo json_encode($return);
		}
	}



	public function st_matching(){		
		$SEO['title'] = "個案中心 - Tutorseeking";

		$id = $_GET['id'];
		$catid = 6;
		$tutor = pc_base::load_app_class('tutor');
			
		$sql = "select * from `v9_student_transaction` as tran 
				left join `v9_student_master` as std on std.`student_id` = tran.`st_studentid`
				left join `v9_student_sel_time` as time on time.`sst_stid` = tran.`st_id` 
				where `st_id` = '$id'";
		$this->db->query($sql);
		$data = $this->db->fetch_array();
		$value = $data[0];
		$pair_tutor = $this->tutor->getSuitedTeacher($value['st_ttid_1'],$value['st_ttid_2'],$value['st_ttid_3'],$value['student_locid'],$value['st_class_hour'],$value['st_tutor_fee'],$value['st_min_grade'],$value['st_req_sex'],$value['st_main_lang'],$value['like_collage']);
		


		$PageType = 1;
		$isShow = 0;
		$til="暫無合適的導師可以推薦";
		if(count($pair_tutor['data']) > 0){
			if(isset($_GET['yid'])){		
				foreach ($pair_tutor['data'] as $key => $value) {		
					if($value['tm_tutorid'] != $_GET['yid']){
						$caselist['data'][0] = $value;
						 break;
					}
				}
			}else{
				$caselist['data'][0] = $pair_tutor['data'][0];
			}
			if(count($caselist['data'][0]) > 0){		
				$isShow = 1;
				$til="最適合你的一位導師";
			}else{
				$caselist['data'][0] = '';
			}
		}else{
			$caselist['data'][0] = '';
		}
	
		$bottomName = '查看更多導師';
		$usetType = 1;
		include template('content','morecase');	
	}


	public function tut_matching(){		
		$SEO['title'] = "個案中心 - Tutorseeking";
		$catid = 6;
		$id = $_GET['id'];
		
		$std = pc_base::load_app_class('student');
	
		$sql = "select * from `v9_tutor_transaction` as tran 
		left join `v9_tutor_master` as ttr on ttr.`tutor_id` = tran.`master_id`
		left join `v9_tutor_sel_location` as loc on loc.`tsl_tmid` = tran.`tm_id` 
		left join `v9_tutor_sel_time` as time on time.`tst_tmid` = tran.`tm_id` 
		left join `v9_tutor_sel_tutor_type` as type on type.`tstt_tmid` = tran.`tm_id` 
		where `tm_tutorid` = '$id'";

		$this->db->query($sql);
		$data = $this->db->fetch_array();
		$value = $data[0];
		
		$pair_student = $this->std->getSuitedStudent($value['tstt_ttid_1'],$value['tstt_ttid_2'],$value['tstt_ttid_3'],$value['tsl_locid'],$value['tst_class_hour'],$value['tstt_fee'],$value['tutor_high_grade'],$value['tutor_sex'],$value['tutor_main_lang'],$value['tutor_college']);
		
		$PageType = 1;
		$isShow = 0;
		$til="暫無合適的學生可以推薦";
		if(count($pair_student['data']) > 0){
			if(isset($_GET['yid'])){		
				foreach ($pair_student['data'] as $key => $value) {		
					if($value['st_id'] != $_GET['yid']){
						$caselist['data'][0] = $value;
						 break;
					}
				}
			}else{
				$caselist['data'][0] = $pair_student['data'][0];
			}
			if(count($caselist['data'][0]) > 0){		
				$isShow = 1;
				$til="最適合你的一位學生";
			}else{
				$caselist['data'][0] = '';
			}
		}else{
			$caselist['data'][0] = '';
		}
		
		$bottomName = '查看更多學生';
		$usetType = 0;
		include template('content','morecase');	
	}

	
	public function set_uert_type(){
		$this->_session_start();
		$_SESSION['role_verify'] = $_POST['type'];
		echo 1;
	}


	
}
?>