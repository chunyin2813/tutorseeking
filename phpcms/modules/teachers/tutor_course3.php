<?php 
//导师可教授类型设置:级别
//获得点击的level级数

defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_app_class('admin','admin',0);
    class tutor_course3 extends admin{
        public function __construct() {
        parent::__construct();
        header("content-type:text/html;charset=utf-8");
        $this->db = pc_base::load_model('sys_tutor_type_model');
        $this->module_db = pc_base::load_model('module_model');
       }

       //主页
       public function init(){
            $tttypes = $this->db->select(array("tt_level"=>1),'*','','tt_seq asc');
       		$where['tt_level']=3;
            $page = isset($_GET['page']) && intval($_GET['page']) ? intval($_GET['page']) : 1;
            $infos = $this->db->listinfo($where,$order = 'tt_parentid asc,tt_seq asc',$page, $pages = '20');
            $pages = $this->db->pages;
            foreach ($infos as $key => $value) {
            $p = $this->db->select(array('tt_id'=>$value['tt_parentid']));
            foreach ($p as $kp => $vp) {                
               $a[] = $vp;
               } 
           }
           // var_dump($a);
            include $this->admin_tpl('courselevel');
       }


       //增加
       public function add($where){
            $infos_type=$this->db->select(array('tt_level'=>1),'*','','tt_seq asc');
            if(isset($_POST['dosubmit'])){
               
                if(empty($_POST['coursetype']) || empty($_POST['course'])){
                    showmessage('請選擇可教授類別或項目',HTTP_REFERER);
                }
                $data['tt_seq'] = $_POST['tt_seq'];  
                $data['tt_name'] = $_POST['tt_name'];
                $data['tt_modtime'] = $_POST['tt_modtime'];
                $data['tt_parentid'] = $_POST['course'];
                $data['tt_level'] = 3;
                $table='sys_tutor_type';
                if(!is_array( $data ) || $table == '') {
                    return false;
                }
                $this->db->insert($data,$table);
                showmessage(L('add_success'),HTTP_REFERER);
            }
            include $this->admin_tpl('courselevel_add');
       
       }

       //联动下拉框
        public function add2(){
            if($_POST){
                // $infos=$this->db->get_one(array('tt_name'=>$_POST['intnew']));
                $infos['tt_id'] = $_POST['intnew'];
                $infos_course=$this->db->select(array('tt_parentid'=>$infos['tt_id']),'*','','tt_seq asc');
                $str=json_encode($infos_course);
                echo $str;
                exit;
            }
        }

       //删除单条数据
       public function delete($where){
        if(isset($_GET)){
            $table='sys_tutor_type';
            $where['tt_id']=intval($_GET['id']);
            $data['tt_parentid']=intval($_GET['id']);
            if ($table == '' || $where == '') {
                return false;
            }
            $this->db->delete($where,$table);
            $this->db->delete($data,$table);
            showmessage('删除成功',HTTP_REFERER);
        }else{
            showmessage('删除失败',HTTP_REFERER);
            }
       }

       //全选删除
        public function delete_all() {
            if (!isset($_POST['id']) || !is_array($_POST['id'])) {
                showmessage(L('illegal_parameters'), HTTP_REFERER);
            } else {
                array_map(array($this, _del), $_POST['id']);
            }
            showmessage(L('operation_success'), HTTP_REFERER);
        }

        private function _del($id = 0) {
            $id = intval($id);
            if (!$id) return false;
            // $this->db->delete(array('tt_parentid'=>$id));
            $this->db->delete(array('tt_id'=>$id));
        }

        //联动下拉框
        public function edit2(){
            if($_POST){
                // $infos=$this->db->get_one(array('tt_name'=>$_POST['intnew']));
                $infos['tt_id'] = $_POST['intnew'];
                $infos_course=$this->db->select(array('tt_parentid'=>$infos['tt_id']),'*','','tt_seq asc');
                $str=json_encode($infos_course);
                echo $str;
                exit;
            }
        }

       //修改可教授类别
        public function edit($where){
            $infos_type=$this->db->select(array('tt_level'=>1),'*','','tt_seq asc');//类别
            $infos_course=$this->db->select(array('tt_level'=>2),'*','','tt_seq asc');//项目
            // var_dump($infos_type);exit;
            if(isset($_POST['dosubmit'])){
                // var_dump($_POST);
                // exit();
                if(empty($_POST['coursetype']) || empty($_POST['course'])){
                    showmessage('請選擇可教授類別或項目',HTTP_REFERER);
                }
                $data['tt_seq'] = $_POST['tt_seq'];  
                $data['tt_name'] = $_POST['tt_name'];
                $data['tt_modtime'] = $_POST['tt_modtime'];
                $data['tt_parentid'] = $_POST['course'];
                $data['tt_level'] = 3;
                $where['tt_id'] = $_POST['tt_id'];
                $result=$this->db->update($data,$where);
                if($result){
                    showmessage('修改数据成功',HTTP_REFERER,'','edit');
                }else{
                    showmessage('修改数据失败',HTTP_REFERER);
                }
            }else{
                //未提交编辑，展示编辑页面
                $id = intval($_GET['id']);
                $inf = $this->db->get_one(array('tt_id'=>$id));
                foreach ($infos_course as $v)
                {
                    if($v['tt_id']==$inf['tt_parentid'])
                    {
                        $coursename = $v['tt_parentid'];//科目的id
                    }
                }
                $infos_course2=$this->db->select(array('tt_parentid'=>$coursename),'*','','tt_seq asc');//项目
                // var_dump($infos_course2);exit;
                if(!$inf){
                    showmessage('没有找到数据');
                }
                extract($inf);
                extract($coursename);
                 include $this->admin_tpl('courselevel_edit');
               } 
        } 

    }  
?>  
