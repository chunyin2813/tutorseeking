<?php
//老师注册资料
 defined('IN_PHPCMS') or exit('No permission resources.');
$session_storage = 'session_'.pc_base::load_config('system','session_storage');//输出值session_mysql
pc_base::load_sys_class($session_storage);//载入文件路径phpcms\libs\classes\session_mysql.class.php
class index {
    function __construct() {
        header("Content-type: text/html; charset=utf-8");
    	$this->db = pc_base::load_model('member_model');
		$this->o_db = pc_base::load_model('tutor_master_model');
        $this->e_db = pc_base::load_model('sys_tutor_achieve_model');
        $this->trandb= pc_base::load_model('tutor_transaction_model');
        $this->tsttdb= pc_base::load_model('tutor_sel_tutor_type_model');
        $this->tsldb=pc_base::load_model('tutor_sel_location_model');
        $this->tsadb=pc_base::load_model('tutor_sel_achievement_model');
        $this->p_db=pc_base::load_model('pair_model');
        $this->inte_db=pc_base::load_model('integration_log_model');
        $this->popup=pc_base::load_model('popup_model');
        pc_base::load_app_class('inte' ,'integration');
        
        
    }

    private function _session_start() {
        $session_storage = 'session_'.pc_base::load_config('system','session_storage');
        pc_base::load_sys_class($session_storage);
    }

    //网站首页页面
    public function init(){
        $timelist = array(
        '0700' =>"7:00" ,'0715' =>"7:15" ,'0730' =>"7:30",'0745' =>"7:45",
        '0800' =>"8:00" ,'0815' =>"8:15" ,'0830' =>"8:30" ,'0845' =>"8:45",
        '0900' =>"9:00" ,'0915' =>"9:15" ,'0930' =>"9:30" ,'0945' =>"9:45",
        '1000' =>"10:00" ,'1015' =>"10:15" ,'1030' =>"10:30" ,'1045' =>"10:45",
        '1100' =>"11:00" ,'1115' =>"11:15" ,'1130' =>"11:30" ,'1145' =>"11:45",
        '1200' =>"12:00" ,'1215' =>"12:15" ,'1230' =>"12:30" ,'1245' =>"12:45",
        '1300' =>"13:00" ,'1315' =>"13:15" ,'1330' =>"13:30" ,'1345' =>"13:45",
        '1400' =>"14:00" ,'1415' =>"14:15" ,'1430' =>"14:30" ,'1445' =>"14:45",
        '1500' =>"15:00" ,'1515' =>"15:15" ,'1530' =>"15:30" ,'1545' =>"15:45",
        '1600' =>"16:00" ,'1615' =>"16:15" ,'1630' =>"16:30" ,'1645' =>"16:45",
        '1700' =>"17:00" ,'1715' =>"17:15" ,'1730' =>"17:30" ,'1745' =>"17:45",
        '1800' =>"18:00" ,'1815' =>"18:15" ,'1830' =>"18:30" ,'1845' =>"18:45",
        '1900' =>"19:00" ,'1915' =>"19:15" ,'1930' =>"19:30" ,'1945' =>"19:45",
        '2000' =>"20:00" ,'2015' =>"20:15" ,'2030' =>"20:30" ,'2045' =>"20:45",
        '2100' =>"21:00" ,'2115' =>"21:15" ,'2130' =>"21:30" ,'2145' =>"21:45",
        '2200' =>"22:00" ,'2215' =>"22:15" ,'2230' =>"22:30" ,'2245' =>"22:45",
        '2300' =>"23:00");

        //彈窗
        $popup=$this->popup->listinfo(array('status'=>1), '');
        foreach ($popup as $k=> $v) {
            if(!in_array('1', explode(',', $v['seat']))){
                unset($popup[$k]);
            }
        }
        foreach ($popup as $k=> $v) {
            if($v['position']=='0'){        //手機版  
               $ph_src=$v['setting']; 
            }
            if($v['position']=='1'){       //ipad版
               $ip_src=$v['setting'];
            }
            if($v['position']=='2'){      //電腦版
               $com_src=$v['setting'];
            }
        }

        //導師類別
        $sql="SELECT * FROM v9_sys_tutor_type where tt_level=1 order by tt_seq ASC";

        $data1 = $this->db->fetch_array($this->db->query($sql));
        $res=$this->dataScreen($data1);
        include template('teachers', 'tutor_reg');
     }





    public function dataScreen($data){
        foreach ($data as $key => $v) {
            if(!($this->dataIsShow($v[tt_id]))){
                unset($data[$key]);
            }
        }
        return $data;
    }

    public function dataIsShow($id,$data){
         $sql="SELECT tt_id FROM v9_sys_tutor_type where tt_level=2 and tt_parentid=$id";
         $two_level=$this->db->fetch_array($this->db->query($sql));
         if(empty($two_level)){
            return false;
         }else{
            return true;
         }
    }




    //注册完成发送短信操作时进行黑名单验证
    public function black_ip_list($reg_verify){
        //获得当前ip
        // $user_IP = ($_SERVER["HTTP_VIA"]) ? $_SERVER["HTTP_X_FORWARDED_FOR"] : $_SERVER["REMOTE_ADDR"];
        // $regip = ($user_IP) ? $user_IP : $_SERVER["REMOTE_ADDR"];
        pc_base::load_app_func('global'); 
        $regip=ip();
        $master_info=$_POST['master'];
        //member表需要的数据
        $data['username']=$master_info['tutor_gname'];
        $data['password']=md5($master_info['tutor_pwd']);
        $data['role_verify']=$_POST['role_verify'];
        $data['regip']=$regip;
        $data['phone']=$master_info['tutor_mobile'];
        $where['regip']=$regip;
        $infos=$this->db->select($where);
        //默认verify=0,发送短信改为1,短信验证成功后改为2;
		//首个注册提交,直接插入数据
		if(empty($infos)){
            $this->db->insert($data);
            $where_info['role_verify']=2;
            $mas_info = $this->db->select($where_info, $data = '*', $limit = '1', $order = 'userid desc');
            $userid=$mas_info[0]['userid'];
            $where1['userid']=$userid;
            //发送短信并且操作成功
            if($this->reg_verify==true){
                $data1['verify']=2;
                $this->db->update($data1, $where1);
                echo "認證成功.插入一條數據1,註冊成功，發送短信";
                //发送短信但未操作成功
            }else if($this->reg_verify==false){
                $data1['verify']=1;
                $this->db->update($data1, $where1);
                echo "認證失敗，插入一條數據1";
            }
		}else{
//如果有ip,且统一ip下verify=0/1的个数小于三条,插入数据(verify=0 未验证 /verify=1验证失败 /verify=2 验证成功),否则加入黑名单
			$verify_num=count($this->db->select(array('regip'=>$regip,'verify'=>1)));
			if($verify_num<3){
                $this->db->insert($data);
                $where_info['role_verify']=2;
                $mas_info = $this->db->select($where_info, $data = '*', $limit = '1', $order = 'userid desc');
                $userid=$mas_info[0]['userid'];
                $where2['userid']=$userid;
                if($this->reg_verify==true){
                    $data2['verify']=2;
                    $this->db->update($data2, $where2);
                    echo "認證成功.插入一條數據2,註冊成功，發送短信";
                }else if($this->reg_verify==false){
                    $data2['verify']=1;
                    $this->db->update($data2, $where2);
                    echo "認證失敗，插入一條數據2";
                }
			}else{
                //黑名单也插入一次
                //$this->db->insert($data);
				echo '您註冊次數過多，已加入黑名單';
			}
		}
    }
    //无三级时不显示二级
    public function TchItem_change(){
        if(isset($_POST['tt_id'])){
            $this->tt_db= pc_base::load_model('sys_tutor_type_model');
            $tt_infos=$this->tt_db->select(array('tt_parentid'=>$_POST['tt_id']),'*','','`tt_seq` ASC','','');
            foreach ($tt_infos as $key =>$tt_info) {
                $res=$this->tt_db->select(array('tt_parentid'=>$tt_info['tt_id']),'*','','`tt_seq` ASC','','');
                if(empty($res)){
                    unset($tt_infos[$key]);
                }
            }
            $tt_infos=json_encode($tt_infos);
            echo $tt_infos;
            exit;
        }
    }

    //验证用户邮箱地址是否存在
    public function ajax_checkusermail(){
        if(!empty($_POST['usermail'])){
            $where['email']=$_POST['usermail'];
            $where['role_verify']='0';//导师
            $res=$this->db->select($where);
            //print_r($res);
            if(empty($res)){
                echo 1;
            }else{
                echo 0;
            }
        }
    }

    //验证用户手机号码是否存在
    public function ajax_checkusertel(){
        if(!empty($_POST['usertel'])){
            $role_verify = !empty($_POST['role']) ? $_POST['role'] : '0';
            $where['phone']=$_POST['usertel'];
            $where['role_verify']=$role_verify;//默认0:导师 1:学生
            $res=$this->db->select($where);
            //print_r($res);
            if(empty($res)){
                echo 1;
            }else{
                echo 0;
            }
        }
    }
    public function ajax_checkReferee(){
        if(!empty($_POST['Referee'])){
            $where['phone']=$_POST['Referee'];
            $res=$this->db->select($where,"role_verify");
            if(empty($res)){
                echo 0;
            }else{
                echo 1;
            }
        }
    }
    
     //老师注册信息提交
    public function tutor_reg(){   
        $inte=new inte();
        if(isset($_POST['useremail'])){
            //授课地区若为空
            if(empty($_POST['area'])){
                $mas_arr = 'fail_error';
                echo json_encode($mas_arr);exit;
            }

            //验证码不能为空
            if(empty($_SESSION['msg_code']) || empty($_POST['msg_code'])) {
                $mas_arr = 'msg_empty_error';
                echo json_encode($mas_arr);
                return;
            }

            //验证短信验证码
            if(strtolower($_SESSION['msg_code'])!= strtolower($_POST['msg_code'])) {
                $mas_arr = 'msg_code_error';
                echo json_encode($mas_arr);
                return;
            }

            //验证注册手机号与接收短信手机号是否一致
            if(strtolower($_SESSION['msg_phone'])!= strtolower($_POST['usertel'])) {
                $mas_arr = 'msg_phone_error';
                echo json_encode($mas_arr);
                return;
            }

            if(!empty($_POST['useremail']) && !empty($_POST['usertel'])){
            $query=$this->db->query('select userid from v9_member where role_verify="0" and email="'.$_POST['useremail'] .'"');
            $res = $this->db->fetch_array($query);
            $query1=$this->db->query('select userid from v9_member where role_verify="0" and phone="'.$_POST['usertel'].'"');
            $res1 = $this->db->fetch_array($query1);
            if(empty($res) && empty($res1)){
                //处理可教授项目数据，去掉空数据
                $types=explode(',', $_POST['type']);
                $subs=explode(',', $_POST['sub']);
                $levels=explode(',', $_POST['level']);
                foreach ($types as $key => $type) {
                    if($type==""){
                        unset($types[$key]);
                        unset($subs[$key]);
                        unset($levels[$key]);
                    }
                }
                $type_string=implode(',', $types);
                $sub_string=implode(',', $subs);
                $level_string=implode(',', $levels);

                //member表
                $m['username']=$_POST['userNickName'];
                $m['password']=md5($_POST['userpwd']);
                //$m['nickname']=$_POST['useremail'];
                $m['regdate']=time();
                $m['regip']=$_SERVER["REMOTE_ADDR"];
                $m['email']=$_POST['useremail'];
                $m['mobile']=$_POST['mobile'];
                $m['phone']=$_POST['usertel'];
                $m['nickname']=$_POST['userNickName'];
                $m['Referee']=$_POST['Referee'];
                $m['userimage']=$_POST['userimage'];
                $m['role_verify']="0";
                $where['email']=$_POST['useremail'];
                $where['role_verify']='0';
                $mem_res=$this->db->insert($m);
                $userid=$this->db->select($where);

                $this->_session_start();
                $_SESSION['id']=$userid[0]['userid'];
                $_SESSION['username']=$userid[0]['username'];
                $_SESSION['role_verify']='0';

                $hou = substr(strval("263"+$userid[0]['userid']+1000000),1,6);
                $personid="A".$hou;
                $this->db->update(array('personid'=>$personid),$where);

                if($mem_res){
                    //tutor_master表
                    $tm['tutor_userid']=$userid[0]['userid'];
                    $tm['tutor_mobile']=$_POST['usertel'];
                    $tm['tutor_email']=$_POST['useremail'];
                    $tm['tutor_fname_eng']=$_POST['userIdEn'];
                    $tm['tutor_fname_chi']=$_POST['userIdCh'];
                    $tm['tutor_gname']=empty($_POST['userNickName'])?$_POST['userIdEn']:$_POST['userNickName'];
                    $tm['tutor_address_type']=$_POST['userHome'];
                    $tm['tutor_dor_address']=$_POST['userDormitoryAddr'];
                    $tm['tutor_address']=$_POST['userHomeAddr'];
                    $tm['tutor_address_locid']=$_POST['userHomeArea'];
                    $tm['tutor_sex']=$_POST['userSex'];
                    $tm['tutor_birth_year']=$_POST['userBirth'];
                    $tm['tutor_id_card']=$_POST['id_card'];
                    $tm['tutor_nation']=$_POST['nation'];
                    $tm['tutor_main_lang']=$_POST['userMainLan'];
                    $tm['tutor_sexually']=$_POST['sexOffenses'];
                    $tm['tutor_high_grade']=$_POST['userHighEd'];
                    $tm['tutor_hschool1']=$_POST['tutor_hschool1'];
                    $tm['tutor_hschool2']=$_POST['tutor_hschool2'];
                    $tm['tutor_hschool_lang']=$_POST['tutor_hschool_lang'];
                    $tm['tutor_hmainclass']=$_POST['tutor_hmainclass'];
                    $tm['tutor_hkcee_score']=$_POST['tutor_hkcee_score'];
                    $tm['tutor_hkcee_lang']=$_POST['tutor_hkcee_lang'];
                    $tm['tutor_college']=$_POST['tutor_college'];
                    $tm['tutor_college_other']=$_POST['tutor_college_other'];
                    $tm['tutor_cmainclass']=$_POST['tutor_cmainclass'];
                    $tm['tutor_otherclass']=$_POST['tutor_otherclass'];
                    $tm['tutor_other_major']=$_POST['tutor_other_major'];
                    $tm['tutor_now_edcuation']=$_POST['tutor_now_edcuation'];
                    $tm['tutor_now_career']=$_POST['userCurOct'];
                    $tm['tutor_work_exp']=$_POST['userWorkExp'];
                    $tm['tutor_working1']=$_POST['tutor_working1'];
                    $tm['tutor_working2']=$_POST['tutor_working2'];
                    $tm['tutor_high_teach']=$_POST['userTutorClass'];
                    $tm['tutor_teach_exp']=$_POST['userTutorExp'];
                    $tm['tutor_proivde_note']=$_POST['userProNote'];
                    $tm['tutor_edu_exp']=$_POST['userEdiExp'];
                    $tm['tutor_self_describe']=$_POST['self_introduction'];
                    $tm['tutor_modtime']=date("Y-m-d H:i:s");
                    $tm['tutor_know_way']=$_POST['know_way'];
                    // $tm['tutor_userTchEn']=$_POST['userTchEn'];
                    $tm['tutor_userTchPt']=$_POST['userTchPt'];
                    $tm['tutor_believe']=$_POST['believe'];
                    $tm_res=$this->o_db->insert($tm);

                    if($tm_res){
                        //tutor_transaction表
                        $a_sql = "select max(tm_id) from v9_tutor_transaction;";
                        $this->trandb->query($a_sql);
                        $infos = $this->trandb->fetch_array();
                        $tutor_id = $infos[0]['max(tm_id)'];
                        $tutor_id+=1;

                        $where1['tutor_userid']=$userid[0]['userid'];
                        $tm_tutorid=$this->o_db->select($where1);

                        $year=substr(date("Y"),-2);
                        $newNumber = substr(strval("263"+$tutor_id+100000),1,5);
                        $tran['tm_tutorid'] = "T".$year.$newNumber;
                        $tran['master_id']=$tm_tutorid[0]['tutor_id'];
                        $tran['tm_modtime']=date("Y-m-d H:i:s");
                        $tran_res=$this->trandb->insert($tran);

                        if($tran_res){
                            //tutor_sel_tutor_type表                       
                            $where2['tm_tutorid']=$tran['tm_tutorid'];
                            $tm_id= $this->trandb->select($where2); 
                            
                            $tstt['tstt_ttid_1']=$type_string;
                            $tstt['tstt_ttid_2']=$sub_string;
                            $tstt['tstt_ttid_3']=$level_string;
                            $tstt['tstt_tmid']=$tm_id[0]['tm_id'];
                            $tstt['tstt_fee']=$_POST['tutor_class_price'];
                            $tstt['tstt_tutorid']=$tran['tm_tutorid'];
                            $tstt['tstt_modtime']=date("Y-m-d H:i:s");
                            $tstt_res=$this->tsttdb->insert($tstt);

                            //tutor_sel_location表
                            $tsl['tsl_tutorid']=$tran['tm_tutorid'];
                            $tsl['tsl_tmid']=$tm_id[0]['tm_id'];
                            $tsl['tsl_locid']=$_POST['area'];
                            $tsl['tsl_modtime']=date("Y-m-d H:i:s");
                            $tsl_res=$this->tsldb->insert($tsl);

                            //sys_achieve_score表
                            $scores=explode(",", $_POST['scores']);
                            foreach ($scores as $key => $v) {
                                $one_score=explode("-", $v);
                                $tsa['tsa_taid']=$one_score[0];
                                $tsa['tsa_asscore']=$one_score[1];
                                $tsa['tsa_userid']=$tm['tutor_userid'];
                                $tsa['tsa_modtime']=date("Y-m-d H:i:s");
                                $tsa_res= $this->tsadb->insert($tsa);
                            }


                            //处理上课时间数据
                            $time=$_POST['teachertime'];
                            if(empty($time)){return '';}
                            $time = explode("],", $time); 

                            //取出数字
                            $reg="/\d+/";
                            for($i=0;$i<count($time);$i++){
                                preg_match_all($reg,$time[$i],$time_arr[$i]);
                            }

                            //删除星期值都为空的值
                            foreach ($time_arr as $key =>$v){  
                                $new_arr[]=$v[0]; 
                            }
                            foreach($new_arr as $k=>$v){
                                //删除星期值都为空的值
                                if(!$v){//判断是否为空（false）
                                    unset($new_arr[$k]);//删除
                                }
                                //删除星期不完整的值
                                //js判断必须填完整
                            }
                            //所有星期为空
                            if(!$new_arr){
                                unset($new_arr);
                            }
                            $from = '';
                            $to = '';
                            foreach($new_arr as $key =>$value){
                                foreach ($value as $ke => $val) {
                                    if($ke%2 == 0){
                                        $from .= $val.',';
                                    }else{
                                        $to .= $val.',';
                                    }
                                }
                                $from = rtrim($from,',');
                                $to = rtrim($to,',');
                                $from .= ';';
                                $to .= ';';
                            }
                            $time_re['sst_weekday']=array_keys($new_arr);


                            //tutor_sel_time表
                            $time_re_arr['tst_tutorid']=$tran['tm_tutorid'];
                            $time_re_arr['tst_tmid']=$tm_id[0]['tm_id'];
                            $time_re_arr['tst_class_hour']=$_POST['tst_class_hour'];
                            $time_re_arr['tst_fromtime']=$from;
                            $time_re_arr['tst_totime']=$to;
                            $time_re_arr['tst_weekday']=implode(',', $time_re['sst_weekday']);
                            $this->time_db = pc_base::load_model('tutor_sel_time_model');
                            $time_res=$this->time_db->insert($time_re_arr);

                            $integration=$inte->get_inte_setting(2);
                            $sp_inte=$inte->get_inte_setting(14); 
                            $spread_num=$inte->get_inte_num(14);
                            //為推薦人賬戶加10積分
                            if($_POST['Referee']!=""){
                                //推荐人号码与导师注册号码一致时，将积分给学生用户
                                if($_POST['usertel']==$_POST['Referee']){
                                    $spread_man=$this->db->get_one(array('phone'=>$_POST['Referee'],'role_verify'=>1),"role_verify,integral,personid,username");
                                    //推荐人号码不存在或者填的推荐码时
                                    if(empty($spread_man)){
                                         $inte->write_inte_log($personid,$_SESSION['username'],'15',$_POST['Referee'],'推廣碼','1','0'); 
                                       
                                    }else{
                                        //推薦人同時推薦多少會員之後額外奬勵積分
                                        $num=$this->db->count(array('Referee'=>$_POST['Referee']));
                                        if(!empty($spread_num) && !empty($sp_inte)){
                                            if(($num%$spread_num)==0){
                                                $spread_inte=$inte->get_inte_setting(14);
                                                $inte->write_inte_log($spread_man['personid'],$spread_man['username'],'14','','','',$spread_inte);
                                            }
                                        }
                                        $integral=$spread_man['integral']+$integration+$spread_inte;
                                        $this->db->update(array('integral'=>$integral),array('phone'=>$_POST['Referee'],'role_verify'=>1)); 
                                        //将积分记录在日志表
                                        $inte->write_inte_log($spread_man['personid'],$spread_man['username'],'2',$personid,$_SESSION['username'],'1',$integration);
                                    }
                                }else{
                                    $condition['phone']=$_POST['Referee'];
                                    $role=$this->db->select($condition,"role_verify,integral,personid,username");
                                    //推荐人号码不存在或者填的推荐码时
                                    if(empty($role)){
                                         $inte->write_inte_log($personid,$_SESSION['username'],'15',$_POST['Referee'],'推廣碼','1','0'); 
                                        
                                    }else{
                                        foreach ($role as $v) {
                                            if(count($role)>1){
                                                if($v['role_verify']=='0'){
                                                    $condition['role_verify']="0";
                                                    //得到推荐号码的导师账号信息
                                                    $person=$this->db->select($condition,"role_verify,integral,personid,username");
                                                    //推薦人同時推薦多少會員之後額外奬勵積分 
                                                    $num=$this->db->count(array('Referee'=>$condition['phone']));
                                                    if(!empty($spread_num) && !empty($sp_inte)){
                                                        if(($num%$spread_num)==0){
                                                            $spread_inte=$inte->get_inte_setting(14); 
                                                            $inte->write_inte_log($person[0]['personid'],$person[0]['username'],'14','','','',$spread_inte);
                                                        }
                                                    }
                                                    $integral=$v['integral']+$integration+$spread_inte;
                                                    
                                                    //为推荐人账户添加积分
                                                    $this->db->update(array('integral'=>$integral),$condition); 
                                                    //将积分记录在日志表
                                                    $inte->write_inte_log($person[0]['personid'],$person[0]['username'],'2',$personid,$_SESSION['username'],'1',$integration);
                                                     
                                                }
                                            }else{
                                                //推薦人同時推薦多少會員之後額外奬勵積分
                                                $num=$this->db->count(array('Referee'=>$condition['phone']));
                                                if(!empty($spread_num) && !empty($sp_inte)){
                                                    if(($num%$spread_num)==0){
                                                        $spread_inte=$inte->get_inte_setting(14);
                                                        $inte->write_inte_log($v['personid'],$v['username'],'14','','','',$spread_inte);
                                                    }
                                                }
                                                $integral=$v['integral']+$integration+$spread_inte;
                                                $this->db->update(array('integral'=>$integral),$condition); 
                                                //将积分记录在日志表
                                                $inte->write_inte_log($v['personid'],$v['username'],'2',$personid,$_SESSION['username'],'1',$integration);   
                                            }
                                        }
                                    } 
                                }  
                            }

                            
                            if($tstt_res && $tsl_res && $tsa_res && $time_res){
                                $reg_inte=$inte->get_inte_setting(1);
                               //为注册用户加积分
                                $this->db->update(array('integral'=>$reg_inte),array('userid'=>$_SESSION['id']));
                                //将积分记录在日志表
                                if(!empty($reg_inte)){
                                    $inte->write_inte_log($personid,$_SESSION['username'],'1','','','',$reg_inte);
                                }
                                //根据填写的资料为条件查找合适的学生
                                if(!empty($_POST['type'])){$type=$type_string;}
                                if(!empty($_POST['sub'])){$item=$sub_string;}
                                if(!empty($_POST['level'])){$level=$level_string;}
                                if(!empty($_POST['area'])){$place=$_POST['area'];}
                                if(!empty($_POST['tutor_class_price'])){$hourlyfee=$_POST['tutor_class_price'];}
                                if(!empty($_POST['tst_class_hour'])){$classtime=$_POST['tst_class_hour'];}
                                if(!empty($_POST['userHighEd'])){$miniEduLevel=$_POST['userHighEd'];}
                                if(!empty($_POST['userSex'])){$gender=$_POST['userSex'];}
                                if(!empty($_POST['userMainLan'])){$classlang=$_POST['userMainLan'];}
                                if(!empty($_POST['tutor_college'])){$like_collage=$_POST['tutor_college'];}
                                $week=$time_re_arr['tst_weekday'];
                                pc_base::load_app_class('student' ,'pair');
                                $stu=new student();
                                $caselist=$stu->getSuitedStudent($type,$item,$level,$place,$classtime,$hourlyfee,$miniEduLevel,$gender,$classlang,$like_collage,1,1,'','',$week);
                                $data=$caselist['data'];
                                pc_base::load_app_class('tutor' ,'pair');
                                $tutor=new tutor();
                                foreach ($data as $student) {
                                    $star=$tutor->return_star($student['st_studentid'],$student['role_verify']);
                                    $student['star']=$star;

                                    $weeknums = array('6'=> "S",'0'=> "M",'1'=> "T",'2'=> "W",'3'=> "T",'4'=> "F",'5'=> "S");
                                    //得到可教授時間
                                    if($student['sst_weekday']==""){
                                        foreach($weeknums as $num){
                                            $student['stu_time'].="<span>".$num."</sapn>";
                                        }
                                    }else{
                                        
                                        $week=explode(",",$student['sst_weekday']);
                                        foreach($weeknums as $k=>$num){
                                        $str="";
                                        if(strpos($student['sst_weekday'],strval($k)) !== false ){
                                            $str = "style='color: black'";
                                        }
                                       $student['stu_time'].="<span ".$str.">".$num."</span>";
                                        }
                                    }
                                    //確定此個案未配對還是在配對中
                                    $pairid=$this->p_db->get_one("pair_student_tranid = '$student[st_id]'");
                                    if(!empty($pairid)){
                                        $student['match_area']="<div class='course-match'>配對中</div>";
                                    }else{
                                        $student['match_area']="";
                                    }
                                    $data=$student;
                                }
                                echo json_encode($data);
                                //成功后发送邮件
                                $teach_email_data="<!DOCTYPE html>
                                <html>
                                <head>
                                    <meta charset='utf-8'>
                                    <meta name='viewport' content='width=device-width,initial-scale=1.0, maximum-scale=1.0,minimum-scale=1.0,user-scalable=no'>
                                    <title>tutorseeking</title>
                                    <style>
                                        /*.match-table td{padding: 6px;}*/
                                    </style>
                                </head>
                                <body>
                                    <div style='max-width: 880px; margin:0 auto; font-size: 14px; color: #333; text-align: justify'>
                                        <form action=''>
                                            <div style='text-align: center'>
                                                <br>
                                                <br>
                                                <img width='250px' src='".APP_PATH."statics/images/add_image/logo-sticky.png' alt=''>
                                            </div>
                                            <h2 style='text-align: center; font-size: 24px;'>尊敬的用戶：您好！您于 ". date('Y-m-d H:i')." 註冊了TutorSeeking的導師帳號！以下是您的帳戶信息！</h2>
                                            <br>
                                            <div style='text-align: center;margin-left:8%;'>
                                                <div style='display:inline-block; margin-right: 60px; margin-bottom: 10px'>
                                                    <table border='1px' class='match-table' cellpadding='0' cellspacing='0' style='width: 500px; font-size: 20px; text-align: center;border-collapse:collapse' >
                                                        <tbody>
                                                        <tr>
                                                            <td style='padding: 6px;font-size: 20px;'  colspan='2' >用戶信息</td>
                                                        </tr>
                                                        <tr>
                                                            <td style='padding: 6px;font-size: 20px;' >姓名</td>
                                                            <td style='padding: 6px;font-size: 20px;' >".$_POST['userNickName']."</td>
                                                        </tr>
                                                        <tr>
                                                            <td style='padding: 6px;font-size: 20px;' >註冊電話</td>
                                                            <td style='padding: 6px;font-size: 20px;' >".$_POST['usertel']."</td>
                                                        </tr>
                                                        <tr>
                                                            <td style='padding: 6px;font-size: 20px;' >註冊電郵</td>
                                                            <td style='padding: 6px;font-size: 20px;' >".$_POST['useremail']."</td>
                                                        </tr>
                                                        <tr>
                                                            <td style='padding: 6px;font-size: 20px;' >登録密碼</td>
                                                            <td style='padding: 6px;font-size: 20px;' >".$_POST['userpwd']."</td>
                                                        </tr>
                                                        <tr>
                                                            <td style='padding: 6px;font-size: 20px;'  colspan='2'><a href='".APP_PATH."index.php?m=content&c=index&a=tutor_login' style='text-decoration: none; color: #e39916;'>前往登録</a></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <p style='text-align: center; font-size: 15px;'>多謝您登記成為<b>Tutorseeking</b>導師，請您在收到郵件後<b>48小時內</b>將有關證書電郵給<a href='mailto:registration@tutorseeking.com'>registration@tutorseeking.com</a> 所有資料只供本網站確認閣下提供資料是否正確之用，不會用作其他商業用途。</p>
                                            </div>
                                            <br>";
                                $teach_email_data .="        </form>
                                    </div>
                                </body>
                                </html>";
                                pc_base::load_sys_func('mail'); //實例化郵件類
                                sendmail($_POST['useremail'],'TutorSeeking用戶註冊信息', $teach_email_data);//發送郵件給註冊用戶

                            }
                        }
                    }
                }
            }else{
                $mas_arr = 'duplicate_error';
                echo json_encode($mas_arr);
                return;
            }
            }   
        }  	
    }


    //登陆判断
    public function log(){
    	if(isset($_POST['role_log'])){
            $username=$_POST['username'];
            $pwd=$_POST['userpwd'];
            $pwd=md5($pwd);
            $mail=$_POST['mail'];
    		$info=$this->db->get_one(array('username'=>$username));
    		$regip=$info['regip'];
    		//1,验证密码 2,验证是否ip黑名单 3,验证认证状态 4,验证角色 5,成功跳转点击页面 
    		if($username!==$info['username'] && $pwd!==$info['password']){
				showmessage('用戶名或密碼不正確請重新輸入',HTTP_REFERER);   
    		}
            $verify_num=count($this->db->select(array('regip'=>$regip,'verify'=>0)));
            if($verify_num<3){
                $verify=$info['verify'];
                if($verify==0){
                    showmessage('該用戶未認證，請先認證',HTTP_REFERER);  
                }else if($verify==1){
                    $_SESSION['id']=$info['userid'];
                    $_SESSION['username']=$info['username'];
                    $_SESSION['role_verify']=$info['role_verify'];
                    return $_SESSION['username'];
                }
            }else{
               showmessage('您註冊次數過多，已被加入黑名單',HTTP_REFERER); 
            }
    	}
    }

     //登陆
    public function role_log(){
        $this->log();
        if(isset($_SESSION['username'])){
            include template('teachers', 'fakeindex');
        }else{
            $thisurl='?'.$_SERVER['QUERY_STRING'];
            include template('teachers', 'fakelog');
        }
    }

    //需登陆链接1
    public function role_center(){
        $this->log();
        if(isset($_SESSION['username'])){
            $role_verify=$_SESSION['role_verify'];
            $infos=$this->db->select(array('username'=>$_SESSION['username']));
            if($role_verify==1){
               include template('teachers', 'tutor_center'); 
           }else if($role_verify==2){
                include template('tparents', 'stu_center');
           }
        }else{
            $thisurl='?'.$_SERVER['QUERY_STRING'];
            include template('teachers', 'fakelog');
        }
    }

    //需登陆链接2
    public function role_buy(){
        $this->log();
        if(isset($_SESSION['username'])){
            echo "您已經登録";
        }else{
            $thisurl='?'.$_SERVER['QUERY_STRING'];
            include template('teachers', 'fakelog');
        }
        
    }

    //登出
    public function logout(){
        $_SESSION=array();
        showmessage('已退出登録','?index.php&m=teachers&c=index&a=init');
    }

    //个人中心资料修改
    public function tutor_edit(){
        if(isset($_POST['dosubmit'])){
            $where['userid']=$_POST['userid'];
            $data['username']=$_POST['username'];
            $data['mobile']=$_POST['mobile'];
            $result=$this->db->update($data,$where);
            if($result){
                //修改成功以后更新session中的用户名
                $_SESSION['username']=$_POST['username'];
                showmessage('修改數據成功','?m=teachers&c=index&a=tutor_center');
            }else{
                showmessage('修改數據失敗',HTTP_REFERER);
            }
        }else{
            $id = intval($_GET['id']);
            $info = $this->db->get_one(array('userid'=>$id));
            if(!$info){
                showmessage('沒有找到數據');
            }
            extract($info);
            include template('teachers', 'tutorinfo_edit');
        }  
    }

    //个人中心资料增加
    public function tutor_add(){
        include template('teachers', 'tutorinfo_add');
    }

    //个人中心资料删除
    public function tutor_delete(){
        if(isset($_GET['id'])){
            $id=$_GET['id'];
            // var_dump($id);die;
            $res=$this->db->delete(array('userid'=>$id));
            if($res){
                showmessage('刪除成功',HTTP_REFERER);  
            }else{
                showmessage('刪除失敗',HTTP_REFERER);
            }
        }
        include template('teachers', 'person_center');
    }
    public function tutor_detail(){
        if(isset($_GET['userid'])){
            $userid=$_GET['userid'];
            $tutor_id=$_GET['tutor_id'];
            //查詢個案詳情
            $sql="select * from `v9_member` as member 
            left join `v9_tutor_master` as master on master.tutor_userid=member.userid 
            left join `v9_tutor_transaction` as tran on tran.master_id = master.tutor_id 
            left join `v9_tutor_sel_location` as loc on loc.tsl_tutorid = tran.tm_tutorid 
            left join `v9_tutor_sel_tutor_type` as tstt on tstt.tstt_tutorid =tran.tm_tutorid 
            left join `v9_tutor_sel_time` as tst on tst.tst_tutorid =tran.tm_tutorid 
            left join `v9_student_type` as stype on stype.st_id = master.tutor_high_teach
            left join `v9_sys_location` as place on place.loc_id = master.tutor_address_locid 
            left join `v9_grade` as grade on grade.grade_id = master.tutor_high_grade
            left join `v9_collage` as collage on collage.id = master.tutor_college
            where member.userid='$userid' and tran.tm_tutorid='$tutor_id'";
            $this->db->query($sql);
            $tutor= $this->db->fetch_array();

            //得到导师可教授地区
            pc_base::load_app_class('helpcommon' ,'pair');
            $helpcommon=new helpcommon();
            $place=$helpcommon->create_location($tutor[0]['tsl_locid']);

            //得到导师的考试成绩，组成数组
            $score_arr=$this->tsadb->select(array("tsa_userid"=>$userid),'tsa_taid,tsa_asscore','','','');
            foreach ($score_arr as $key => $score) {
                if(empty($score['tsa_asscore'])){
                    unset($score_arr[$key]);
                }
            }
            //查詢pair表中是否存在當前登陸人與此個案的配對
            if(!empty($_SESSION['id'])){
                $userid=$_SESSION['id'];
            $sql_case="select st_id from `v9_student_transaction` as tran 
            left join `v9_student_master` as master on tran.st_studentid = master.student_id
            left join `v9_member` as member on master.student_userid=member.userid
            where member.userid=$userid
            ";
            $this->db=pc_base::load_model('student_transaction_model');
            $this->db->query($sql_case);
            $caselist= $this->db->fetch_array();
                foreach ($caselist as  $value) {
                    $pair_student_tranid=$value['st_id'];
                    $sql_isset="select * from v9_pair where pair_student_tranid='$pair_student_tranid' and pair_tutor_tranid='$tutor_id' ";
                    $this->db=pc_base::load_model('pair_model');
                    $pair= $this->db->fetch_array($this->db->query($sql_isset));
                    // var_dump($pair);
                    // var_dump(empty($pair));
                    //登陸者有個案與此個案匹配  
                    if(!empty($pair)){
                        $current_pair = $pair[0];//當前用戶的個案與當前個案配對信息
                        $tm_tutorid=$tutor_id;
                        $stid=$value['st_id'];
                    }   
                }
            } 
            $sql_stu="select pair_id,pair_state from v9_pair where pair_tutor_tranid='$tutor_id'";
            $pair_stu= $this->db->fetch_array($this->db->query($sql_stu))[0];
           include template('teachers', 'tutor_detail');  
        }
    }

    //學生發起匹配申請
    public function std_request(){
        if(!empty($_POST['tmid']) && !empty($_POST['stid'])){
            $this->std_class = pc_base::load_app_class('student','pair');//加載學生公共操作類
            $res = $this->std_class->std_send_request($_POST['tmid'],$_POST['stid'],$_POST['url'],$_POST['amount'],$_POST['points']);//調用學生匹配導師方法
            // var_dump($res);exit();
            echo json_encode($res);//返回支付JS或失敗狀態
        }
    }

    //學生審核配對申請
    public function std_respond(){
        if(!empty($_POST['tmid']) && !empty($_POST['stid']) && !empty($_POST['respond'])){
            $this->std_class = pc_base::load_app_class('student','pair');//加載學生公共操作類
            $res = $this->std_class->std_respond($_POST['tmid'],$_POST['stid'],$_POST['respond'],$_POST['url'],$_POST['amount'],$_POST['points']);//調用學生審核配對方法
            echo json_encode($res);//返回支付JS或失敗狀態 
        }
    }

    //學生免費發起匹配申請
    public function std_request_free(){
        $PairModel = pc_base::load_model('pair_model');//加載配對表模型
        $FailModel = pc_base::load_model('pair_fail_model');//加載配對副表模型
        $this->tran = pc_base::load_model('tutor_transaction_model');//加載導師執行表模型
        $this->S_tran = pc_base::load_model('student_transaction_model');//加載學生要求表模型
        if(!empty($_POST['tmid']) && !empty($_POST['stid'])){
            $tmid = $_POST['tmid'];
            $stid = $_POST['stid'];
            $where = " `pair_tutor_tranid` = '$tmid' && `pair_student_tranid` = '$stid' && `pair_state` = '0'";
            //查看是否曾經配對過
            // $fail_where = " `pair_tutor_tranid` = '$tmid' && `pair_student_tranid` = '$stid'";
            // $is_paired = $FailModel->get_one($fail_where);
            // if(!empty($is_paired)){
            //     $return['msg'] = '曾經配對過的個案不可再次配對!';
            //     echo json_encode($return);exit;
            // }
            $is_exist = $PairModel->get_one($where);
            if(!$is_exist){ //不存在配對信息時 新增配對信息

                //防止已經在配對的個案再次配對
                $tutor_is_pair = $PairModel->get_one("`pair_tutor_tranid` = '$tmid'");
                $student_is_pair = $PairModel->get_one("`pair_student_tranid` = '$stid'");
                if(!empty($tutor_is_pair) || !empty($student_is_pair)){
                    $return = array('msg'=>'fail');
                    return $return;exit;
                }

                $this->tran->update("`tm_state` = 0","`tm_tutorid` = '$tmid'");//导师执行表是否被拒绝状态改为0:否
                $this->S_tran->update("`st_state` = 0","`st_id` = '$stid'");//学生要求表是否被拒绝状态改为0:否
                $data = array(
                    'pair_tutor_tranid'=>$tmid,
                    'pair_student_tranid'=>$stid,
                    'pair_sponsor'=>2,
                    'student_pay'=>1,
                );
                if($PairModel->insert($data)){
                    //發起成功 扣除免費配對次數
                    $pairid = $PairModel->insert_id();
                    $this->deductStdPairfreetime($pairid);

                    //发送郵件
                    pc_base::load_app_class('tutor','pair');//實例化導師類
                    $tutor = new tutor();
                    $Pairing = $tutor->order_send_email($pairid);
                    $return = array('msg'=>'success');
                }else{
                    $return = array('msg'=>'fail');
                }
            }else{ //已存在配對信息時 直接更改付款狀態

                //異常情況返回系統繁忙：導師或學生已付款
                if($is_exist['tutor_pay'] == '1' || $is_exist['student_pay'] == '1'){
                    $return = array('msg'=>'fail');
                    return $return;exit;
                }
                
                if($PairModel->update("`student_pay` = 1","`pair_id` = $is_exist[pair_id]")){
                    //發起成功 扣除免費配對次數
                    $this->deductStdPairfreetime($is_exist['pair_id']);

                    //发送郵件
                    pc_base::load_app_class('tutor','pair');//實例化導師類
                    $tutor = new tutor();
                    $Pairing = $tutor->order_send_email($is_exist['pair_id']);
                    $return = array('msg'=>'success');
                }else{
                    $return = array('msg'=>'fail');
                }
            }
            echo json_encode($return);
        }
    }

    //學生免費同意配對申請
    public function std_respond_free(){
        $PairModel = pc_base::load_model('pair_model');//加載配對表模型
        if(!empty($_POST['tmid']) && !empty($_POST['stid']) && !empty($_POST['respond'])){
            $tmid = $_POST['tmid'];
            $stid = $_POST['stid'];
            $where = " `pair_tutor_tranid` = '$tmid' && `pair_student_tranid` = '$stid' && `pair_state` = '0'";
            $is_exist = $PairModel->get_one($where);
            $success = array('student_pay'=>'1','pair_state'=>'1');//配對成功狀態
            if($PairModel->update($success,"`pair_id` = $is_exist[pair_id]")){
                //發起成功 扣除免費配對次數
                $this->deductStdPairfreetime($is_exist['pair_id']);

                //更改配對狀態和執行表狀態
                $stdTranModel = pc_base::load_model('student_transaction_model');       //加載學生要求表模型
                $this->trandb->update("`tm_matched` = '1'","`tm_tutorid` = '$is_exist[pair_tutor_tranid]'");
                $stdTranModel->update("`st_matched` = '1'","`st_id` = '$is_exist[pair_student_tranid]'");

                //发送郵件
                pc_base::load_app_class('tutor','pair');//實例化導師類
                $tutor = new tutor();
                $Pairing = $tutor->order_send_email($is_exist['pair_id']);

                $HelpCommon = pc_base::load_app_class('helpcommon','pair');//加載配對輔助類
                //配对成功推荐人积分奖励
                $HelpCommon->activityReward($is_exist['pair_id']);
                $return = array('msg'=>'success');
            }else{
                $return = array('msg'=>'fail');
            }
            echo json_encode($return);
        }
    }

    //扣除學生免費配對次數並將被拒絕的其中一個配對訂單的學生付款狀態改為金額已使用
    private function deductStdPairfreetime($pairid){
        $returnPoint = pc_base::load_app_class('integralOperation','pay');
        $userData= $returnPoint->GetUserData($pairid,'std_agree_pay');             //获取学生userid
        $deduct = $returnPoint->freetimeOperation($userData['userid'],1,$type='-');//扣除學生一次免費配對次數
        
        $stdModel = pc_base::load_model('student_master_model');                //加載學生主资料表模型
        $stdTranModel = pc_base::load_model('student_transaction_model');       //加載學生要求表模型
        $pairFail = pc_base::load_model('pair_fail_model');                     //加載配對副表模型
        $alluser = $stdModel->select("`student_userid` = $userData[userid]");   //獲取所有學生
        $alluserid = '';//所有學生ID
        foreach ($alluser as $key => $value) {
            $alluserid .= $value['student_id'].',';
        }
        $alluserid = rtrim($alluserid,',');//移除右邊的逗號
        $allTran = $stdTranModel->select("`st_studentid` in ($alluserid)");   //獲取所有要求表
        $allTranid = '';//所有要求表ID
        foreach ($allTran as $key => $value) {
            $allTranid .= "'".$value['st_id']."',";
        }
        $allTranid = rtrim($allTranid,',');//移除右邊的逗號
        $unrefundPair = $pairFail->select("`pair_student_tranid` in ($allTranid) and `student_pay` = '1'")[0];//獲取其中一個配對信息
        if(!empty($unrefundPair)){
            $pairFail->update("`student_pay` = '4'","`pair_id` = $unrefundPair[pair_id]");//更改狀態為4(金額已被使用)
            //将退款金额被使用的配对个案ID存入使用免费配对次数的配对个案
            if(!empty($unrefundPair['std_freepair_id'])){
                $this->p_db->update("`std_freepair_id` = '$unrefundPair[std_freepair_id]'","`pair_id` = $pairid");
            }else{
                $this->p_db->update("`std_freepair_id` = '$unrefundPair[pair_id]'","`pair_id` = $pairid");
            }
        }
    }



    
}
?>