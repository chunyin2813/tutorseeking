<?php 
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header', 'admin');
?>
<form method="post" action="?m=teachers&c=tpinfo1&a=add" id="myform">
<table class="table_form" width="100%" cellspacing="0">
<tbody>
<input name="ta_level" id="ta_level" class="input-text" type="hidden" size="25" value="1">
	<tr>
    	<th align="right" valign="top">考试类别名称：</th>
        <td><input name="ta_name" id="ta_name" class="input-text" type="text" size="25" required="required"></td>
    </tr>
    <tr>
    	<th align="right"  valign="top">分数显示类型：</th>
        <td><input name="ta_score_type" id="ta_score_type" class="input-text" type="text" size="25" required="required" placeholder="按照A,B,C,D,E的形式填写"></td>
    </tr>
     <tr>
    	<th align="right"  valign="top">考试类别排序：</th>
        <td>
        	<input name="ta_seq" id="ta_seq" class="input-text" type="text" size="25" required="required">
        </td>
    </tr>
	<tr>
		<th>更新时间：</th>
		 <td><input name="ta_modtime" id="ta_modtime" class="input-text" type="text" size="25" value="<?php echo date("Y-m-d H:i:s")?>" readonly="readonly" ></td>
	</tr>
	<tr>
		<td></td>
		<td><input type="submit" name="dosubmit" id="dosubmit" value="提交">
			<INPUT name="pclog" type="button" value="返回" onClick="location.href='index.php?m=teachers&c=tpinfo1&a=init&pc_hash=<?php echo $_SESSION['pc_hash']?>'"></td>
	</tr>
	</tbody>
</table>	
</form>
</body>
</html>
