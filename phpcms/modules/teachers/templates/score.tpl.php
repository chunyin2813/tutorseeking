<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header','admin');
?>
<!--导师考试分数主页面-->
<div class="pad-lr-10">
	<div class="table-list">
		<!-- <a href="?m=teachers&c=score&a=add"><button>新增</button></a> -->
		<br /><br />
	    <table width="100%" cellspacing="0" class="contentWrap">
	        <thead>
	            <tr>
	            <th width="30" align="center"><input type="checkbox" value="" id="check_box" onclick="selectall('id[]');"></th>
				<th width="35">ID</th>
				<th width="100" align="center">考试成绩类别ID(ta_level=1)</th>
				<th width="100" align="center">考试成绩类别名稱</th>
				<th width="100" align="center">考试分数</th>
				<th width="100" align="center">分数排序</th>
				<th width="100" align="center">更新时间</th>
				<th width="110" align="center">管理操作</th>
	            </tr>
	        </thead>
	        <tbody> 
	        <?php foreach ($infos as $k=>$info) {	?>
				<tr>
				<td align="center">
				<form method="post" action="?m=teachers&c=score&a=delete_all" id="myform">
				<input type="checkbox" name="id[]" value="<?php echo $info['as_id']?>">
				</td>
				<td align="center"><?php echo $info['as_id']?></td>
				<td align="center"><?php echo $info['as_ta_id']?></td>

				<?php if($a[$k]['ta_id']==$info['as_ta_id']){?>
				<td align="center"><?php echo $a[$k]['ta_name'];?></td>
				<?php }?>

				<td align="center"><?php echo $info['as_score']?></td>
				<td align="center"><?php echo $info['as_seq']?></td>
				<td align="center"><?php echo $info['as_modtime']?></td>
				<td align="center">
					<a href="?m=teachers&c=score&a=edit&id=<?php echo $info['as_id']?>">修改</a> | <a href="?m=teachers&c=score&a=delete&id=<?php echo $info['as_id']?>" onclick="return confirm('<?php echo new_html_special_chars(new_addslashes(L('confirm', array('message'=> $info['as_id'].'.'.$info['as_score']))))?>')">删除</a></td>
				</tr>
			<?php } ?>	
			</tbody>
	    </table>
	    <div class="btn">
	    	<td><label for="check_box">全选/取消</label>&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="submit" name="allsubmit" id="allsubmit" value="删除"></td>&nbsp;&nbsp;
			</form>
		</div>  
	</div>
	<div id="pages"><?php echo $pages;?></div>
</div>
</body>
</html>
<script type="text/javascript">
function edit(id, name) {
	window.top.art.dialog({title:'《'+name+'》',id:'edit',iframe:'?m=teachers&c=score&a=edit&id='+id,width:'700',height:'500'}, function(){var d = window.top.art.dialog({id:'edit'}).data.iframe;d.document.getElementById('dosubmit').click();return false;}, function(){window.top.art.dialog({id:'edit'}).close()});
}
</script>