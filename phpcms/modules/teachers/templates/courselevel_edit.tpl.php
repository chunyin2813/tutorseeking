<?php 
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header', 'admin');
?>
<form method="post" action="?m=teachers&c=tutor_course3&a=edit" id="myform">
<input  type="hidden" value="<?php echo $info['tt_id'];?>" name="tt_id">
<table class="table_form" width="100%" cellspacing="0">
<tbody>
	<tr>
		<th width="100">请选择导师可教授类别：</th>
		<td>
			<select name="coursetype" id="coursetype">
			<option value="">--请选择--</option>
			<?php foreach ($infos_type as $info) {	?>
				<option value="<?php echo $info['tt_id']?>" <?php if($info['tt_id']==$coursename){ ?>selected<?php }?>><?php echo $info['tt_name']?></option>
			<?php } ?>	
			</select>
		</td>
	</tr>
	<tr id="course_tr">
		<th width="100">请选择导师可教授项目：</th>
		<td>
			<select name="course" id="course">
				<option value="">--请选择--</option>
				<?php foreach ($infos_course2 as $info) {	?>
					<option value="<?php echo $info['tt_id']?>" <?php if($info['tt_id']==$inf['tt_parentid']){ ?>selected<?php }?>><?php echo $info['tt_name']?></option>
				<?php } ?>
			</select>
		</td>
	</tr>
	<tr>
		<th width="100">导师可教授类型级别名称：</th>
		<input type="hidden" value="<?php echo $inf['tt_id'];?>" name="tt_id">
		<td>
			<input name="tt_name" id="tt_name" class="input-text" type="text" size="25" required="required" value="<?php echo $inf['tt_name'];?>">
		</td>
	</tr>
	<tr>
    	<th align="right"  valign="top">导师可教授类型级别排序：</th>
        <td>
        	<input name="tt_seq" id="tt_seq" class="input-text" type="text" size="25"  required="required" value="<?php echo $inf['tt_seq'];?>">
        </td>
    </tr>
	<tr>
		<th>更新时间：</th>
		 <td><input name="tt_modtime" id="tt_modtime" class="input-text" type="text" size="25" value="<?php echo date("Y-m-d H:i:s")?>" readonly="readonly" ></td>
	</tr>
	<tr>
		<td><input type="submit" name="dosubmit" id="dosubmit" value="提交"></td>
		<td><INPUT name="pclog" type="button" value="返回" onClick="location.href='index.php?m=teachers&c=tutor_course3&a=init&pc_hash=<?php echo $_SESSION['pc_hash']?>'"></td>
	</tr>
	</tbody>
</table>	
</form>
</body>
</html>
<script type="text/javascript">
var pc_hash = "<?php echo $_SESSION['pc_hash']?>";
$(document).ready(function(){	
	// $("#course_tr").hide();
	// $("#course").hide();
	$("#coursetype").change(function(){
		// $("#course_tr").show();
		// $("#course").show();
		var v = $("#coursetype").val();
		if(v){
			$.post('./index.php?m=teachers&c=tutor_course3&a=edit2',{intnew:v,pc_hash:pc_hash},function(data){
				$("#course").html(' ');
				$("#course").append('<option value="">--请选择--</option>');
				for(i in data){
					var course_option="<option class='c_option' value='"+data[i].tt_id+"'>"+data[i].tt_name+"</option>";
					$("#course").append(course_option);
				}
			},'json');
	    	return false;
	    }else{
	    	$("#course").html(' ');
			$("#course").append('<option value="">--请选择--</option>');
	    }
	});
});
</script>
