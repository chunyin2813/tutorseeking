<?php 
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header', 'admin');
?>
<form method="post" action="?m=teachers&c=score&a=edit" id="myform">
<input  type="hidden" value="<?php echo $info_re['as_id'];?>" name="as_id">
<table class="table_form" width="100%" cellspacing="0">
<tbody>
	<tr>
		<th width="100">考試類別：</th>
		<td>
			<select name="test_coursetype" id="test_coursetype">
				<option value="">--請選擇考試類別--</option>
				<?php foreach ($infos as $info) {	?>
					<option value="<?php echo $info['ta_id']?>" <?php if($info['ta_id']==$info_re['as_ta_id']){ ?>selected<?php }?>><?php echo $info['ta_name']?></option>
				<?php } ?>
			</select>
		</td>
	</tr>
	<tr>
    	<th align="right"  valign="top">考試分數：</th>
        <td>
	        <select id="score" name="as_score">
	        	<option value="">請選擇考試分數</option>
	        	<?php for ($i=0; $i < count($newarr); $i++) { ?>
	        		<option value="<?php echo $newarr[$i]?>" <?php if($newarr[$i]==$info_re['as_score']){ ?>selected<?php }?>><?php echo $newarr[$i]?></option>
	        	<?php } ?>
	        </select>
        </td>
    </tr>
    <tr>
    	<th align="right"  valign="top">考試排序：</th>
        <td><input name="as_seq" id="as_seq" class="input-text" type="text" size="25" required value="<?php echo $info_re['as_seq'];?>"></td>
    </tr>
	<tr>
		<th>更新时间：</th>
		 <td><input name="as_modtime" id="as_modtime" class="input-text" type="text" size="25" value="<?php echo date("Y-m-d H:i:s")?>" readonly="readonly" ></td>
	</tr>
	<tr>
		<td><input type="submit" name="dosubmit" id="dosubmit" value="提交"></td>
		<td><input type="button" name="back" id="back" value="返回"></td>
	</tr>
	</tbody>
</table>	
</form>
</body>
</html>
<script type="text/javascript">
	$("#back").click(function(){
		window.history.back(-1); 
		window.location.href = document.referrer; 
	});

	$("#test_coursetype").change(function(){
		var ta_id = $("#test_coursetype").val();
		// console.log(ta_id);
		$.post('./index.php?m=teachers&c=score&a=ajax_edit',{ta_id:ta_id,pc_hash:pc_hash},function(data){
			var arr = data.ta_score_type.split(',');
			$("#score").html(' ');
			$("#score").append('<option>--请选择--</option>');
			for(var i in arr){
			  var course_option="<option class='c_option' value='"+arr[i]+"'>"+arr[i]+"</option>";
			  $("#score").append(course_option);
			}
		},'json');
    return false;
	});
</script>
