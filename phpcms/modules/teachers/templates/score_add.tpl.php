<?php 
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header', 'admin');
?>
<form method="post" action="?m=teachers&c=score&a=add" id="myform">
<table class="table_form" width="100%" cellspacing="0">
<tbody>
	<tr>
		<th width="100">考试类别：</th>
		<td>
			<select name="test_coursetype" id="test_coursetype">
				<option>--请选择--</option>
				<?php foreach ($infos as $info) {	?>
				<option value="<?php echo $info['ta_id']?>"><?php echo $info['ta_name']?></option>
				<?php } ?>	
			</select>
		</td>
	</tr>
	<tr>
    	<th align="right"  valign="top">考试分数：</th>
        <td><input name="as_score" id="as_score" class="input-text" type="text" size="25" required="required"></td>
    </tr>
    <tr>
    	<th align="right"  valign="top">考试排序：</th>
        <td><input name="as_seq" id="as_seq" class="input-text" type="text" size="25" required="required"></td>
    </tr>
	<tr>
		<th>更新时间：</th>
		 <td><input name="as_modtime" id="as_modtime" class="input-text" type="text" size="25" value="<?php echo date("Y-m-d H:i:s")?>" readonly="readonly" ></td>
	</tr>
	<tr>
		<td><input type="submit" name="dosubmit" id="dosubmit" value="提交"></td>
		<td><input type="button" name="back" id="back" value="返回"></td>
	</tr>
	</tbody>
</table>	
</form>
</body>
</html>
<script type="text/javascript">
	$("#back").click(function(){
		window.history.back(-1);
		window.location.href = document.referrer; 
	});
</script>
