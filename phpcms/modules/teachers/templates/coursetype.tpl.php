<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header','admin');
?>
<!--导师可教授类型设置:类别主页面-->
<div class="pad-lr-10">
	<div class="table-list">
		<a href="?m=teachers&c=tutor_course1&a=add"><button>新增</button></a>
		<br /><br />
		<form method="post" action="?m=teachers&c=tutor_course1&a=delete_all" id="myform">
	    <table width="100%" cellspacing="0" class="contentWrap">
	        <thead>
	            <tr>
	            <th width="30" align="center"><input type="checkbox" value="" id="check_box" onclick="selectall('id[]');"></th>
				<th width="35">ID</th>
				<!-- <th width="100" align="center">导师可教授类型级别</th> -->
				<th width="100" align="center">导师可教授类别名称</th>
				<th width="100" align="center">更新时间</th>
				<th width="110" align="center">管理操作</th>
	            </tr>
	        </thead>
	        <tbody> 
	        <?php foreach ($infos as $info) {	?>
				<tr>
				<td align="center">
				<input type="checkbox" name="id[]" value="<?php echo $info['tt_id']?>">
				</td>
				<td align="center" ><?php echo $info['tt_id']?></td>
				<!-- <td align="center" width="70"><?php echo $info['tt_level']?></td> -->
				<td align="center" width="70"><?php echo $info['tt_name']?></td>
				<td align="center"><?php echo $info['tt_modtime']?></td>
				<td align="center">
					<a href="?m=teachers&c=tutor_course1&a=edit&id=<?php echo $info['tt_id']?>">修改</a> <!-- | <a href="?m=teachers&c=tutor_course1&a=delete&id=<?php //echo $info['tt_id']?>" onclick="return confirm('<?php //echo new_html_special_chars(new_addslashes(L('confirm', array('message'=> $info['tt_id'].'.'.$info['tt_name']))))?>')">删除</a> --></td>
				</tr>
			<?php } ?>	
			</tbody>
	    </table>
	    <!-- <div class="btn">
	    	<td><label for="check_box">全选/取消</label>&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="submit" name="allsubmit" id="allsubmit" value="删除"></td>&nbsp;&nbsp;	
		</div> -->
		</form> 
	</div>
	<div id="pages"><?php echo $pages;?></div>
</div>
</body>
</html>
<script type="text/javascript">
function edit(id, name) {
	window.top.art.dialog({title:'《'+name+'》',id:'edit',iframe:'?m=teachers&c=tpinfo1&a=edit&id='+id,width:'700',height:'500'}, function(){var d = window.top.art.dialog({id:'edit'}).data.iframe;d.document.getElementById('dosubmit').click();return false;}, function(){window.top.art.dialog({id:'edit'}).close()});
}
</script>