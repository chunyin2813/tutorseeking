<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header','admin');
?>
<!--导师可教授类型设置:项目主页面-->
<div class="pad-lr-10">
	<div class="table-list">
		<a href="?m=teachers&c=tutor_course2&a=add"><button>新增</button></a>
		<br /><br />
		<div class="tttypes">
			选择導師可教授類別:&nbsp;&nbsp;
			<select name="tttype" id="tttype">
				<option value="">-选择可教授類別-</option>
				<?php foreach ($tttypes as $tttype) {	?>
				<option value="<?php echo $tttype['tt_id']?>"><?php echo $tttype['tt_name']?></option>
				<?php } ?>	
			</select>
		</div>
		<br /><br />
		<form method="post" action="?m=teachers&c=tutor_course2&a=delete_all" id="myform">
	    <table width="100%" cellspacing="0" class="contentWrap">
	        <thead>
	            <tr>
	            <th width="30" align="center"><input type="checkbox" value="" id="check_box" onclick="selectall('id[]');"></th>
				<th width="35">ID</th>
				<th width="100" align="center">导师可教授类別ID</th>
				<th width="100" align="center">导师可教授类別名称</th>
				<th width="100" align="center">导师可教授项目名称</th>
				<th width="100" align="center">更新时间</th>
				<th width="110" align="center">管理操作</th>
	            </tr>
	        </thead>
	        <tbody id="tbody"> 
	        <?php foreach ($infos as $k=>$info) {	?>
				<tr>
				<td align="center">
				
				<input type="checkbox" name="id[]" value="<?php echo $info['tt_id']?>">
				</td>
				<td align="center" ><?php echo $info['tt_id']?></td>
				<td align="center" width="70"><?php echo $info['tt_parentid']?></td>

				<?php if($a[$k]['tt_id']==$info['tt_parentid']){?>
				<td align="center"><?php echo $a[$k]['tt_name'];?></td>
				<?php }?>

				<td align="center" width="70"><?php echo $info['tt_name']?></td>
				<td align="center"><?php echo $info['tt_modtime']?></td>
				<td align="center">
					<a href="?m=teachers&c=tutor_course2&a=edit&id=<?php echo $info['tt_id']?>">修改</a> <!-- | <a href="?m=teachers&c=tutor_course2&a=delete&id=<?php //echo $info['tt_id']?>" onclick="return confirm('<?php //echo new_html_special_chars(new_addslashes(L('confirm', array('message'=> $info['tt_id'].'.'.$info['tt_name']))))?>')">删除</a>--></td> 
				</tr>
			<?php } ?>	
			</tbody>
	    </table>
	    <!-- <div class="btn">
	    	<td><label for="check_box">全选/取消</label>&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="submit" name="allsubmit" id="allsubmit" value="删除"></td>&nbsp;&nbsp;
			
		</div> -->  
		</form>
	</div>
	<div id="pages"><?php echo $pages;?></div>
</div>
</body>
</html>
<script type="text/javascript">
	var pc_hash = "<?php echo $_SESSION['pc_hash']?>";
	$("#tttype").change(function(){
		var tttype_id = $(this).val();
		// alert(tttype_id);
		var tt_name = $("select option:selected").text();
		// alert(ta_name);
		// return;
		if(tttype_id.length!=0){
			$.post('./index.php?m=teachers&c=tutor_course2&a=ajax_init',{tttype_id:tttype_id,pc_hash:pc_hash},function(data){
				$("#pages").hide();
				var tbody=$("#tbody");
				tbody.html("");
				for(i in data){
					var tr="<tr id='tr_region'><td align='center'><input type='checkbox' name='id[]' value='"+data[i].tt_id+"'></td><td align='center'>"+data[i].tt_id+"</td><td align='center' width='70'>"+tttype_id+"</td><td align='center' width='70'>"+tt_name+"</td><td align='center' width='70'>"+data[i].tt_name+"</td><td align='center' >"+data[i].tt_modtime+"</td><td align='center'><a href='?m=teachers&c=tutor_course2&a=edit&id="+data[i].tt_id+"&pc_hash="+pc_hash+"'>修改</a> </td></tr>";
					
					tbody.append(tr);
					// $("#myform").append(tr);
					// $("#myform").append(btn);
				}
			},'json');
	    	return false;
	    }
	});
</script>