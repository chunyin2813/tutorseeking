<?php 
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header', 'admin');
?>
<form method="post" action="?m=teachers&c=tpinfo1&a=edit" id="myform">
<input  type="hidden" value="<?php echo $info['ta_id'];?>" name="ta_id">
<table class="table_form" width="100%" cellspacing="0">
<tbody>
	<!-- <tr>
		<th width="100">考试类别：</th>
		<td>
			<input name="ta_level" id="ta_level" class="input-text" type="text" size="25" placeholder="<?php echo $info['ta_level']?>" value="<?php echo $info['ta_level']?>" required="required">
		</td>
	</tr> -->
	<input type="hidden" value="1" name="ta_level" id="ta_level">
	<tr>
    	<th align="right"  valign="top">考试类别名称：</th>
        <td>
        	<input name="ta_name" id="ta_name" class="input-text" type="text" size="25" placeholder="<?php echo $info['ta_name']?>" value="<?php echo $info['ta_name']?>" required="required">
        </td>
    </tr>
    <tr>
    	<th align="right"  valign="top">考试类别排序：</th>
        <td>
        	<input name="ta_seq" id="ta_seq" class="input-text" type="text" size="25" placeholder="<?php echo $info['ta_seq']?>"  value="<?php echo $info['ta_seq']?>" required="required">
        </td>
    </tr>
    <tr>
    	<th align="right"  valign="top">分数显示类型：</th>
        <td>
        	<input name="ta_score_type" id="ta_score_type" class="input-text" type="text" size="25" value="<?php empty($info['ta_score_type'])? print '按照A,B,C,D,E的形式填写':print $info['ta_score_type']?>"  required="required">
        </td>
    </tr>
	<tr>
		<th>更新时间：</th>
		 <td><input name="ta_modtime" id="ta_modtime" class="input-text" type="text" size="25" value="<?php echo date("Y-m-d H:i:s")?>" readonly="readonly" ></td>
	</tr>
	<tr>
		<td><input type="submit" name="dosubmit" id="dosubmit" value="提交"></td>
		<td><INPUT name="pclog" type="button" value="返回" onClick="location.href='index.php?m=teachers&c=tpinfo1&a=init&pc_hash=<?php echo $_SESSION['pc_hash']?>'"></td>
	</tr>
	</tbody>
</table>	
</form>
</body>
</html>
