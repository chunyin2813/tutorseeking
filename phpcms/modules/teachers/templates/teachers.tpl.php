<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header','admin');
?>
<!--导师考试类别主页面-->
<div class="pad-lr-10">
	<div class="table-list">
		<a href="?m=teachers&c=tpinfo1&a=add"><button>新增</button></a>
		<br /><br />
	    <table width="100%" cellspacing="0" class="contentWrap">
	        <thead>
	            <tr>
	            <th width="30" align="center"><input type="checkbox" value="" id="check_box" onclick="selectall('id[]');"></th>
				<th width="35">ID</th>
				<!-- <th width="100" align="center">考试类别</th> -->
				<th width="100" align="center">考试类别名称</th>
				<th width="100" align="center">更新时间</th>
				<th width="110" align="center">管理操作</th>
	            </tr>
	        </thead>
	        <tbody> 
	        <?php foreach ($infos as $info) {	?>
				<tr>
				<td align="center">
				<form method="post" action="?m=teachers&c=tpinfo1&a=delete_all" id="myform">
				<input type="checkbox" name="id[]" value="<?php echo $info['ta_id']?>">
				</td>
				<td align="center"><?php echo $info['ta_id']?></td>
				<!-- <td align="center"><?php echo $info['ta_level']?></td> -->
				<td align="center"><?php echo $info['ta_name']?></td>
				<td align="center"><?php echo $info['ta_modtime']?></td>
				<td align="center">
					<a href="?m=teachers&c=tpinfo1&a=edit&id=<?php echo $info['ta_id']?>">修改</a> | <a href="?m=teachers&c=tpinfo1&a=delete&id=<?php echo $info['ta_id']?>" onclick="return confirm('<?php echo new_html_special_chars(new_addslashes(L('confirm', array('message'=> $info['ta_id'].'.'.$info['ta_name']))))?>')">删除</a></td>
				</tr>
			<?php } ?>	
			</tbody>
	    </table>
	    <div class="btn">
	    	<td><label for="check_box">全选/取消</label>&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="submit" name="allsubmit" id="allsubmit" value="删除"></td>&nbsp;&nbsp;
			</form>
		</div>  
	</div>
	<div id="pages"><?php echo $pages;?></div>
</div>
</body>
</html>
