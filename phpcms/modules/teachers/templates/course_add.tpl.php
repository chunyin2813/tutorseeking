<?php 
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header', 'admin');
?>
<form method="post" action="?m=teachers&c=tutor_course2&a=add" id="myform">
<table class="table_form" width="100%" cellspacing="0">
<tbody>
	<tr>
		<th width="100">请选择导师可教授类别：</th>
		<td>
			<select name="coursetype">
			<option value="">--请选择--</option>
			<?php foreach ($infos as $info) {	?>
				<option value="<?php echo $info['tt_id']?>"><?php echo $info['tt_name']?></option>
			<?php } ?>	
			</select>
		</td>
	</tr>
	<tr>
		<th width="100">导师可教授类型项目名称：</th>
		<td><input name="tt_name" id="tt_name" class="input-text" type="text" size="25" required="required"></td>
	</tr>
	<tr>
    	<th align="right"  valign="top">导师可教授类型项目排序：</th>
        <td><input name="tt_seq" id="tt_seq" class="input-text" type="text" size="25" required="required"></td>
    </tr>
	<tr>
		<th>更新时间：</th>
		 <td><input name="tt_modtime" id="tt_modtime" class="input-text" type="text" size="25" value="<?php echo date("Y-m-d H:i:s")?>" readonly="readonly" ></td>
	</tr>
	<tr>
		<td><input type="submit" name="dosubmit" id="dosubmit" value="提交"></td>
		<td><INPUT name="pclog" type="button" value="返回" onClick="location.href='index.php?m=teachers&c=tutor_course2&a=init&pc_hash=<?php echo $_SESSION['pc_hash']?>'"></td>
	</tr>
	</tbody>
</table>	
</form>
</body>
</html>
