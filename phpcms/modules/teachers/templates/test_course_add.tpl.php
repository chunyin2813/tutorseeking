<?php 
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header', 'admin');
?>
<form method="post" action="?m=teachers&c=tpinfo3&a=add" id="myform">
<table class="table_form" width="100%" cellspacing="0">
<tbody>
	<tr>
		<th width="100">请选择导师考试类别：</th>
		<td>
			<select name="test_coursetype" id="test_coursetype">
				<option value="">--请选择--</option>
				<?php foreach ($infos_type as $info) {	?>
				<option value="<?php echo $info['ta_id']?>"><?php echo $info['ta_name']?></option>
				<?php } ?>	
			</select>
		</td>
	</tr>
	<tr id="course_tr">
		<th width="100">请选择导师考试科目类别：</th>
		<td>
			<select name="test_course" id="test_course">
				<option value="">--请选择--</option>
			</select>
		</td>
	</tr>
	<tr>
    	<th align="right"  valign="top">考试科目名称：</th>
        <td><input name="ta_name" id="ta_name" class="input-text" type="text" size="25" required="required"></td>
    </tr>
    <tr>
    	<th align="right"  valign="top">考试科目类别排序：</th>
        <td><input name="ta_seq" id="ta_seq" class="input-text" type="text" size="25" required="required"></td>
    </tr>
	<tr>
		<th>更新时间：</th>
		 <td><input name="ta_modtime" id="ta_modtime" class="input-text" type="text" size="25" value="<?php echo date("Y-m-d H:i:s")?>" readonly="readonly" ></td>
	</tr>
	<tr>
		<td><input type="submit" name="dosubmit" id="dosubmit" value="提交"></td>
		<td><INPUT name="pclog" type="button" value="返回" onClick="location.href='index.php?m=teachers&c=tpinfo3&a=init&pc_hash=<?php echo $_SESSION['pc_hash']?>'"></td>
	</tr>
	</tbody>
</table>	
</form>
</body>
</html>
<script type="text/javascript">
var pc_hash = "<?php echo $_SESSION['pc_hash']?>";
$(document).ready(function(){
	$("#course_tr").hide();
	$("#test_course").hide();
	$("#test_coursetype").change(function(){
		$("#course_tr").show();
		$("#test_course").show();
		var v = $("#test_coursetype").val();
		if(v){
			$.post('./index.php?m=teachers&c=tpinfo3&a=add2',{intnew:v,pc_hash:pc_hash},function(data){
				$("#test_course").html(' ');
				$("#test_course").append('<option value="">--请选择--</option>');
				for(i in data){
					var course_option="<option class='c_option' value='"+data[i].ta_id+"'>"+data[i].ta_name+"</option>";
					$("#test_course").append(course_option);
				}
			},'json');
	    	return false;
	    }else{
	    	$("#test_course").html(' ');
			$("#test_course").append('<option value="">--请选择--</option>');
	    }
	});
});
</script>
