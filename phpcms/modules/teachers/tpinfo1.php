<?php
defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_app_class('admin','admin',0);
//导师考试类别
class tpinfo1 extends admin {
    function __construct() {
        parent::__construct();
        $this->db = pc_base::load_model('sys_tutor_achieve_model');
		$this->module_db = pc_base::load_model('module_model');
    }
     public function init() {
        $where['ta_level']=1; 
        $page = isset($_GET['page']) && intval($_GET['page']) ? intval($_GET['page']) : 1;
        $infos = $this->db->listinfo($where,$order = 'ta_seq asc',$page, $pages = '20');
        $pages = $this->db->pages;
        include $this->admin_tpl('teachers');  
     }

     //新增考试类别数据
    public function add(){
    	if(isset($_POST['dosubmit'])){
    		$data=array();
    		$data['ta_level'] = $_POST['ta_level'];
    		$data['ta_name'] = $_POST['ta_name'];
            $data['ta_score_type']=$_POST['ta_score_type'];
    		$data['ta_modtime'] = $_POST['ta_modtime'];
            $data['ta_seq'] = $_POST['ta_seq'];
    		// var_dump($data);die;
    		$table='sys_tutor_achieve';
    		if(!is_array( $data ) || $table == '') {
				return false;
			}
    		$this->db->insert($data,$table);
    		showmessage(L('add_success'),HTTP_REFERER);
    	}
    	include $this->admin_tpl('teachers_add');
    }

    //删除考试类别单条数据
    public function delete($table, $where) {
        // var_dump($_GET);
        // exit;
    	if(isset($_GET)){
    		$table='sys_tutor_achieve';
    		$where=array();
    		$where['ta_id']=intval($_GET['id']);
    		if ($table == '' || $where == '') {
				return false;
			}
            $ids=$this->db->select(array('ta_parentid'=>$where['ta_id']));
            //删除三级
            foreach ($ids as $k => $v) {
               $this->db->delete(array('ta_parentid'=>$v['ta_id']));
            }
            $this->db->delete(array('ta_parentid'=>$where['ta_id']));
			$this->db->delete($where,$table);
			showmessage('刪除成功',HTTP_REFERER);
        }else{
            showmessage('刪除失敗',HTTP_REFERER);
    	}
	}

    //全选删除考试类别数据
    public function delete_all() {
        if (!isset($_POST['id']) || !is_array($_POST['id'])) {
            showmessage(L('illegal_parameters'), HTTP_REFERER);
        } else {
            array_map(array($this, _del), $_POST['id']);
        }
        showmessage(L('operation_success'), HTTP_REFERER);
    }

    private function _del($id = 0) {
        $id = intval($id);
        if (!$id) return false;
        $ids=$this->db->select(array('ta_parentid'=>$id));
        //删除三级
        foreach ($ids as $k => $v) {
           $this->db->delete(array('ta_parentid'=>$v['ta_id']));
        }
        $this->db->delete(array('ta_parentid'=>$id));
        $this->db->delete(array('ta_id'=>$id));
    }


    //修改考试类别单条数据
    public function edit(){
		if(isset($_POST['dosubmit'])){
			//提交编辑
    		$where['ta_id']=$_POST['ta_id'];
    		$data['ta_level'] = $_POST['ta_level'];
    		$data['ta_name'] = $_POST['ta_name'];
            $data['ta_score_type']=$_POST['ta_score_type'];
    		$data['ta_modtime'] = $_POST['ta_modtime'];
            $data['ta_seq'] = $_POST['ta_seq'];
			$result=$this->db->update($data,$where);
			if($result){
				showmessage('修改數據成功',HTTP_REFERER,'','edit');
            }else{
                showmessage('修改數據失敗',HTTP_REFERER);
			}
		}else{
			//未提交编辑，展示编辑页面
			$id = intval($_GET['id']);
		    $info = $this->db->get_one(array('ta_id'=>$id));
		    if(!$info){
		    	showmessage('沒有找到數據');
		    }
		    extract($info);
		    include $this->admin_tpl('teachers_edit');
		}	
	}
}

?>