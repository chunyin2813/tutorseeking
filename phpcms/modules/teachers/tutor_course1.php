<?php 
//导师可教授类型设置:类别
//获得点击的level级数

defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_app_class('admin','admin',0);
    class tutor_course1 extends admin{
        public function __construct() {
        parent::__construct();
        header("content-type:text/html;charset=utf-8");
        $this->db = pc_base::load_model('sys_tutor_type_model');
        $this->module_db = pc_base::load_model('module_model');
        $where['tt_level']=1;
       }

       //主页
       public function init($where){
        //$infos = $this->db->select($where);
        $where['tt_level']=1;   
        $page = isset($_GET['page']) && intval($_GET['page']) ? intval($_GET['page']) : 1;
        $infos = $this->db->listinfo($where,$order = 'tt_seq asc',$page, $pages = '20');
        $pages = $this->db->pages;
        include $this->admin_tpl('coursetype');
          
       }

       //增加
       public function add($where){
        if(isset($_POST['dosubmit'])){
            $data=array();
            $data['tt_level'] = 1;
            $data['tt_name'] = $_POST['tt_name'];
            $data['tt_modtime'] = $_POST['tt_modtime'];
            $data['tt_seq'] = $_POST['tt_seq'];
            $table='sys_tutor_type';
            if(!is_array( $data ) || $table == '') {
                return false;
                }
            $this->db->insert($data,$table);
            showmessage(L('add_success'),HTTP_REFERER);
            }
        include $this->admin_tpl('coursetype_add');
       }

       //联动下拉框
        public function add2(){
            if($_POST){
                $infos=$this->db->get_one(array('tt_name'=>$_POST['intnew']));
                $infos_course=$this->db->select(array('tt_parentid'=>$infos['tt_id']));
                $str=json_encode($infos_course);
                echo $str;
                exit;
            }
        }

       //删除单条数据
       public function delete($where){
        if(isset($_GET)){
            $table='sys_tutor_type';
            $where['tt_id']=intval($_GET['id']);
            if ($table == '' || $where == '') {
                return false;
            }
            // $this->db->delete($where,$table);
            $ids=$this->db->select(array('tt_parentid'=>$where['tt_id']));
            //删除三级
            foreach ($ids as $k => $v) {
               $this->db->delete(array('tt_parentid'=>$v['tt_id']));
            }
            $this->db->delete(array('tt_parentid'=>$where['tt_id']));
            $this->db->delete($where,$table);
            showmessage('删除成功',HTTP_REFERER);
        }else{
            showmessage('删除失败',HTTP_REFERER);
            }
       }

       //全选删除
        public function delete_all() {
            if (!isset($_POST['id']) || !is_array($_POST['id'])) {
                showmessage(L('illegal_parameters'), HTTP_REFERER);
            } else {
                array_map(array($this, _del), $_POST['id']);
            }
            showmessage(L('operation_success'), HTTP_REFERER);
        }

        private function _del($id = 0) {
            $id = intval($id);
            if (!$id) return false;
            $ids=$this->db->select(array('tt_parentid'=>$id));
            //删除三级
            foreach ($ids as $k => $v) {
               $this->db->delete(array('tt_parentid'=>$v['tt_id']));
            }
            $this->db->delete(array('tt_parentid'=>$id));
            $this->db->delete(array('tt_id'=>$id));
        }

        //联动下拉框
        public function edit2(){
            if($_POST){
                $infos=$this->db->get_one(array('tt_name'=>$_POST['intnew']));
                $infos_course=$this->db->select(array('tt_parentid'=>$infos['tt_id']));
                $str=json_encode($infos_course);
                echo $str;
                exit;
            }
        }

       //修改可教授类别
        public function edit($where){
            
            if(isset($_POST['dosubmit'])){
                $where=array();
                $where['tt_id']=$_POST['tt_id'];
                $data['tt_level'] = 1;
                $data['tt_name'] = $_POST['tt_name'];
                $data['tt_modtime'] = $_POST['tt_modtime'];
                $data['tt_seq'] = $_POST['tt_seq'];
                $result=$this->db->update($data,$where);
                if($result){
                    showmessage('修改数据成功',HTTP_REFERER,'','edit');
                }else{
                    showmessage('修改数据失败',HTTP_REFERER);
                }
               }else{
                //未提交编辑，展示编辑页面
                $id = intval($_GET['id']);
                $info = $this->db->get_one(array('tt_id'=>$id));
                if(!$info){
                    showmessage('没有找到数据');
                }
            extract($info);
            include $this->admin_tpl('coursetype_edit');
           } 
        
    	}
    }  
?>  
