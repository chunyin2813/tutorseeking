<?php
defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_app_class('admin','admin',0);

class score extends admin {
    function __construct() {
        parent::__construct();
        $this->s_db = pc_base::load_model('sys_tutor_achieve_model');
        $this->db = pc_base::load_model('sys_achieve_score_model');
		$this->module_db = pc_base::load_model('module_model');
    }
    public function init() {
     	//$infos = $this->db->select();
        $page = isset($_GET['page']) && intval($_GET['page']) ? intval($_GET['page']) : 1;
        $infos = $this->db->listinfo($where = '',$order = 'as_seq asc',$page, $pages = '20');
        $pages = $this->db->pages;
        foreach ($infos as $key => $value) {
            $p = $this->s_db->select(array('ta_id'=>$value['as_ta_id']));
            foreach ($p as $kp => $vp) {                
               $a[] = $vp;
           } 
       }
        include $this->admin_tpl('score');  
    }
    
    //新增可教授项目数据
    public function add(){
        $infos=$this->s_db->select(array('ta_level'=>1));
    	if(isset($_POST['dosubmit'])){
    		$data=array();
            // $info=$this->s_db->get_one(array('tt_name'=>$_POST['test_coursetype']));
            // $data['as_ta_id'] = $info['tt_id'];
            if(empty($_POST['test_coursetype'])){
                    showmessage('請選擇考試類別',HTTP_REFERER);
            }
            $data['as_ta_id'] = $_POST['test_coursetype'];
    		$data['as_score'] = $_POST['as_score'];
            $data['as_seq'] = $_POST['as_seq'];
    		$data['as_modtime'] = $_POST['as_modtime'];
    		$this->db->insert($data);
    		showmessage(L('add_success'),HTTP_REFERER);
    	}
    	include $this->admin_tpl('score_add');
    }

    //删除可教授项目单条数据
    public function delete($table, $where) {
    	if(isset($_GET)){
    		$where=array();
    		$where['as_id']=intval($_GET['id']);
			$this->db->delete($where);
			showmessage('删除成功',HTTP_REFERER);
    	}else{
    		showmessage('删除失败',HTTP_REFERER);
    	}
	}
	
    //全选删除
    public function delete_all() {
        if (!isset($_POST['id']) || !is_array($_POST['id'])) {
            showmessage(L('illegal_parameters'), HTTP_REFERER);
        } else {
            array_map(array($this, _del), $_POST['id']);
        }
        showmessage(L('operation_success'), HTTP_REFERER);
    }

    private function _del($id = 0) {
        $id = intval($id);
        if (!$id) return false;
        $this->db->delete(array('as_id'=>$id));
    }

    public function ajax_edit(){
        $ajax_ta_id = $_POST['ta_id'];
        $ajax_info = $this->s_db->get_one(array('ta_id'=>$ajax_ta_id));
        $ajax_info=json_encode($ajax_info);
        echo $ajax_info;
        exit;
    }

	public function edit(){
        $infos=$this->s_db->select(array('ta_level'=>1));
		if(isset($_POST['dosubmit'])){
			//提交编辑
            // var_dump($_POST);
            // exit;
            if(empty($_POST['test_coursetype'])){
                    showmessage('請選擇考試類別',HTTP_REFERER);
            }
    		$where=array();
            $where['as_id'] = $_POST['as_id']; 
    		$data['as_score'] = $_POST['as_score'];
            $data['as_seq'] = $_POST['as_seq'];
            $data['as_ta_id'] = $_POST['test_coursetype'];
			$result=$this->db->update($data,$where);
			if($result){
				showmessage('修改数据成功',HTTP_REFERER,'','edit');
			}else{
				showmessage('修改数据失败',HTTP_REFERER);
			}
		}else{
			//未提交编辑，展示编辑页面
			$id = intval($_GET['id']);
		    $info_re = $this->db->get_one(array('as_id'=>$id));
            // var_dump($info_re);
		    if(!$info_re){
		    	showmessage('没有找到数据');
		    }
		    extract($info_re);
		    include $this->admin_tpl('score_edit');
		}	
	}
}     
