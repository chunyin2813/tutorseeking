<?php
defined('IN_PHPCMS') or exit('Access Denied');
defined('INSTALL') or exit('Access Denied');
$parentid = $menu_db->insert(array('name'=>'teachers', 'parentid'=>'29', 'm'=>'teachers', 'c'=>'tpinfo1', 'a'=>'init', 'data'=>'', 'listorder'=>0, 'display'=>'1'), true);

$menu_db->insert(array('name'=>'teachers_coursetype', 'parentid'=>$parentid, 'm'=>'teachers', 'c'=>'tpinfo2', 'a'=>'init', 'data'=>'', 'listorder'=>1, 'display'=>'1'));

$menu_db->insert(array('name'=>'teachers_course', 'parentid'=>$parentid, 'm'=>'teachers', 'c'=>'tpinfo3', 'a'=>'init', 'data'=>'', 'listorder'=>2, 'display'=>'1'));

$menu_db->insert(array('name'=>'teachers_score', 'parentid'=>$parentid, 'm'=>'teachers', 'c'=>'score', 'a'=>'init', 'data'=>'', 'listorder'=>3, 'display'=>'1'));

$menu_db->insert(array('name'=>'teachers_coursetypename', 'parentid'=>$parentid, 'm'=>'teachers', 'c'=>'tutor_course1', 'a'=>'init', 'data'=>'', 'listorder'=>4, 'display'=>'1'));

$menu_db->insert(array('name'=>'teachers_trainableitem', 'parentid'=>$parentid, 'm'=>'teachers', 'c'=>'tutor_course2', 'a'=>'init', 'data'=>'', 'listorder'=>5, 'display'=>'1'));

$menu_db->insert(array('name'=>'teachers_courselevel', 'parentid'=>$parentid, 'm'=>'teachers', 'c'=>'tutor_course3', 'a'=>'init', 'data'=>'', 'listorder'=>6, 'display'=>'1'));

$language = array('teachers'=>'导师考试类别','teachers_coursetype'=>'导师考试科目类别','teachers_course'=>'导师考试科目','teachers_score'=>'导师考试分数','teachers_coursetypename'=>'导师可教授类别','teachers_trainableitem'=>'导师可教授项目','teachers_courselevel'=>'导师可教授级别');
?>