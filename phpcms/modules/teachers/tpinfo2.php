<?php

defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_app_class('admin','admin',0);
class tpinfo2 extends admin {
	public function __construct() {
        parent::__construct();
        header("content-type:text/html;charset=utf-8");
        $this->db = pc_base::load_model('sys_tutor_achieve_model');
		$this->module_db = pc_base::load_model('module_model');
    }

	//首页
    public function init($where) {
        $tttypes = $this->db->select(array("ta_level"=>1),'*','','ta_seq asc');
		$where['ta_level']=2;
        $page = isset($_GET['page']) && intval($_GET['page']) ? intval($_GET['page']) : 1;
        $infos = $this->db->listinfo($where,$order = 'ta_parentid asc,ta_seq asc',$page, $pages = '20');
        $pages = $this->db->pages;
        foreach ($infos as $ki=> $vi) {
           $p=$vi['ta_parentid'];
           $pinfos = $this->db->select(array('ta_id'=>$p),'*','','ta_seq asc');
           foreach ($pinfos as $kp => $vp) {
               $a[] = $vp;
           } 
        }
 		include $this->admin_tpl('test_coursetype');
     }

     public function ajax_init(){
        $where['ta_parentid']=$_POST['tttype_id'];
        $infos = $this->db->select($where,'*','','ta_seq asc');
        $infos=json_encode($infos);
        echo $infos;
        exit;
     }

     //新增
    public function add($where){
            $infos=$this->db->select(array('ta_level'=>1),'*','','ta_seq asc');
            if(isset($_POST['dosubmit'])){
                $data['ta_level'] = 2;
                $data['ta_seq'] = $_POST['ta_seq'];
                $data['ta_name'] = $_POST['ta_name'];
                $data['ta_modtime'] = $_POST['ta_modtime'];
                $data['ta_parentid']=$_POST['test_coursetype'];
                if(empty($_POST['test_coursetype'])){
                    showmessage('請選擇考試科目類別',HTTP_REFERER);
                }
                // var_dump($data);die;
                $table='sys_tutor_achieve';
                if(!is_array( $data ) || $table == '') {
                    return false;
                }
                $this->db->insert($data,$table);
                showmessage(L('add_success'),HTTP_REFERER);
            }
            include $this->admin_tpl('test_coursetype_add'); 
    } 

    // //联动下拉框
    // public function add2(){
    //     if($_POST){
    //         // $infos=$this->db->get_one(array('ta_name'=>$_POST['intnew']));
    //         $infos['ta_id'] = $_POST['intnew'];
    //         $infos_course=$this->db->select(array('ta_parentid'=>$infos['ta_id']));
    //         $str=json_encode($infos_course);
    //         echo $str;
    //         exit;
    //     }
    // }

    //删除
    public function delete($table, $where) {
    	if(isset($_GET)){
    		$table='sys_tutor_achieve';
    		$where=array();
    		$where['ta_id']=intval($_GET['id']);
    		if ($table == '' || $where == '') {
				return false;
			}
			$this->db->delete($where,$table);
            $this->db->delete(array('ta_parentid'=>$where['ta_id']));
			showmessage('删除成功',HTTP_REFERER);
    	}else{
    		showmessage('删除失败',HTTP_REFERER);
    	}
	}

    //全选删除
    public function delete_all() {
        if (!isset($_POST['id']) || !is_array($_POST['id'])) {
            showmessage(L('illegal_parameters'), HTTP_REFERER);
        } else {
            array_map(array($this, _del), $_POST['id']);
        }
        showmessage(L('operation_success'), HTTP_REFERER);
    }

    private function _del($id = 0) {
        $id = intval($id);
        if (!$id) return false;
        $this->db->delete(array('ta_parentid'=>$id));
        $this->db->delete(array('ta_id'=>$id));
        
    }

    //  //联动下拉框
    // public function edit2(){
    //     if($_POST){
    //         // $infos=$this->db->get_one(array('ta_name'=>$_POST['intnew']));
    //         $infos['ta_id'] = $_POST['intnew'];
    //         $infos_course=$this->db->select(array('ta_parentid'=>$infos['ta_id']));
    //         $str=json_encode($infos_course);
    //         echo $str;
    //         exit;
    //     }
    // }

    //修改
    public function edit($where){
        
	    $infos=$this->db->select(array('ta_level'=>1),'*','','ta_seq asc');
	    if(isset($_POST['dosubmit'])){
            // var_dump($_POST);
            // exit;
            if(empty($_POST['test_coursetype'])){
                showmessage('請選擇考試科目類別',HTTP_REFERER);
            }
	        $data['ta_level'] = 2;
	        $data['ta_seq'] = $_POST['ta_seq'];
	        $data['ta_name'] = $_POST['ta_name'];
	        $data['ta_modtime'] = $_POST['ta_modtime'];
	        $data['ta_parentid'] = $_POST['test_coursetype'];
            $where['ta_id'] = $_POST['ta_id'];
	        $result=$this->db->update($data,$where);
	        if($result){
	            showmessage('修改数据成功',HTTP_REFERER,'','edit');
	        }else{
	            showmessage('修改数据失败',HTTP_REFERER);
	        }
	    }else{
	        //未提交编辑，展示编辑页面
	        $id = intval($_GET['id']);
	        $inf = $this->db->get_one(array('ta_id'=>$id));
	        if(!$inf){
	            showmessage('没有找到数据');
	        }
	        extract($inf);
	        include $this->admin_tpl('test_coursetype_edit');
	       } 
	}
}
?>