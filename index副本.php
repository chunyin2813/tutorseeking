<?php
/**
 *  index.php PHPCMS 入口
 *
 * @copyright			(C) 2005-2010 PHPCMS
 * @license				http://www.phpcms.cn/license/
 * @lastmodify			2010-6-1
 */
 //PHPCMS根目录
// ini_set("display_errors", "On");
// error_reporting(E_ALL | E_STRICT);


//强制不带www的网站跳转到带www的网址 Start
$the_host = $_SERVER['HTTP_HOST'];//取得当前域名
$the_url = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';//判断地址后面部分
$the_url = strtolower($the_url);//将英文字母转成小写
if($the_url=="/index.php"){//如果是首页,赋值为空
	$the_url="";
}
if($the_host == 'tutorseeking.com'){//如果域名是不带www的网址那么进行下面的301跳转
header('HTTP/1.1 301 Moved Permanently');//发出301头部
header('Location:http://www.tutorseeking.com'.$the_url);//跳转到带www的网址
}
//强制不带www的网站跳转到带www的网址 End

define('PHPCMS_PATH', dirname(__FILE__).DIRECTORY_SEPARATOR);

include PHPCMS_PATH.'/phpcms/base.php';

pc_base::creat_app();

?>