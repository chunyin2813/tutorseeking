<?php
return array(

'web_path' => '/',

'session_storage' => 'files',
'session_ttl' => 1800,
'session_savepath' => CACHE_PATH.'sessions/',
'session_n' => 0,

'cookie_domain' => '',
'cookie_path' => '', 
'cookie_pre' => 'grLqa_',
'cookie_ttl' => 1800, 

'tpl_root' => 'templates/', 
'tpl_name' => 'default', 
'tpl_css' => 'default', 
'tpl_referesh' => 1,
'tpl_edit'=> 0,

'upload_path' => PHPCMS_PATH.'uploadfile/',
'upload_url' => 'https://localhost/uploadfile/', 
'attachment_stat' => '1',

'js_path' => 'https://localhost/statics/js/', 
'css_path' => 'https://localhost/statics/css/', 
'img_path' => 'https://localhost/statics/images/',
'app_path' => 'https://localhost/',

'charset' => 'utf-8', 
'timezone' => 'Etc/GMT-8', 
'debug' => 0,
'admin_log' => 1, 
'errorlog' => 1, 
'gzip' => 1, 
'auth_key' => 'X11oRMK7UMLHFwQ6aVHe', 
'lang' => 'zh-cn',  
'lock_ex' => '1',  

'admin_founders' => '1',
'execution_sql' => 0,

'phpsso' => '1',	
'phpsso_appid' => '1',	
'phpsso_api_url' => 'https://localhost/phpsso_server',	
'phpsso_auth_key' => 'Ra9x8RZ5fXkeIihQgomStNkWubGADqHw', 
'phpsso_version' => '1', 

'html_root' => '/html',
'safe_card'=>'1',

'connect_enable' => '0',	
'sina_akey' => '',	
'sina_skey' => '',	

'snda_akey' => '',	
'snda_skey' => '',	

'qq_akey' => '',	
'qq_skey' => '',	

'qq_appkey' => '',	
'qq_appid' => '',	
'qq_callback' => '',	

'admin_url' => '',	
);
?>