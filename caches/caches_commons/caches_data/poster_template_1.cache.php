<?php
return array (
  'banner' => 
  array (
    'name' => '矩形横幅',
    'select' => '0',
    'padding' => '0',
    'size' => '1',
    'option' => '0',
    'num' => '1',
    'iscore' => '1',
    'type' => 
    array (
      'images' => '图片',
      'flash' => '动画',
    ),
  ),
  'fixure' => 
  array (
    'name' => '固定位置',
    'align' => 'align',
    'select' => '1',
    'padding' => '1',
    'size' => '1',
    'option' => '0',
    'num' => '1',
    'iscore' => '1',
    'type' => 
    array (
      'images' => '图片',
      'flash' => '动画',
    ),
  ),
  'float' => 
  array (
    'name' => '漂浮移动',
    'select' => '0',
    'padding' => '1',
    'size' => '1',
    'option' => '0',
    'num' => '1',
    'iscore' => '1',
    'type' => 
    array (
      'images' => '图片',
      'flash' => '动画',
    ),
  ),
  'couplet' => 
  array (
    'name' => '对联广告',
    'align' => 'scroll',
    'select' => '0',
    'padding' => '1',
    'size' => '1',
    'option' => '0',
    'num' => '2',
    'iscore' => '1',
    'type' => 
    array (
      'images' => '图片',
      'flash' => '动画',
    ),
  ),
  'imagelist' => 
  array (
    'name' => '图片列表广告',
    'select' => '0',
    'padding' => '0',
    'size' => '1',
    'option' => '1',
    'num' => '1',
    'iscore' => '1',
    'type' => 
    array (
      'images' => '图片',
    ),
  ),
  'text' => 
  array (
    'name' => '文字广告',
    'select' => '0',
    'padding' => '0',
    'size' => '0',
    'option' => '1',
    'num' => '1',
    'iscore' => '1',
    'type' => 
    array (
      'text' => '文字',
    ),
  ),
  'code' => 
  array (
    'name' => '代码广告',
    'type' => 
    array (
      'text' => '代码',
    ),
    'num' => 1,
    'iscore' => 1,
    'option' => 0,
  ),
  'imagechange' => 
  array (
    'name' => '首頁banner图片轮播',
    'select' => '0',
    'padding' => '0',
    'size' => '1',
    'option' => '0',
    'num' => '3',
    'type' => 
    array (
      'images' => '图片',
    ),
  ),
  'test' => 
  array (
    'name' => '',
    'select' => '0',
    'padding' => '0',
    'size' => '0',
    'option' => '0',
    'num' => '4',
    'type' => 
    array (
      'images' => '图片',
    ),
  ),
);
?>