<?php
return array (
  1 => 
  array (
    'siteid' => '1',
    'name' => '默认站点',
    'dirname' => '',
    'domain' => 'https://www.tutorseeking.com/',
    'site_title' => 'Tutorseeking,網上自動配對，私人補習,上門補習,補習介紹,免費補習導師,尋找導師,免費尋找補習導師',
    'keywords' => '上門補習,補習,english tutor,補習agent,tutor,private tutor,中學補習,補習介紹,補習中介,私人補習,小學補習,補習老師,補習網,補習導師,兼職補習,教練,鋼琴導師,朗誦老師，補習老師配對',
    'description' => '本網站利用大數據及高科技電腦配對系統進行配對,完全不經人手操作',
    'release_point' => '',
    'default_style' => 'default',
    'template' => 'default',
    'setting' => '{"upload_maxsize":"2048","upload_allowext":"jpg|jpeg|gif|bmp|png|doc|docx|xls|xlsx|ppt|pptx|pdf|txt|rar|zip|swf","watermark_enable":"0","watermark_minwidth":"150","watermark_minheight":"120","watermark_img":"statics\\/images\\/water\\/\\/logo-water.png","watermark_pct":"75","watermark_quality":"80","watermark_pos":"9"}',
    'uuid' => '520fc695-96c5-11e7-81b0-f0761cbe21b8',
    'url' => 'https://www.tutorseeking.com/',
  ),
);
?>