# phpcms bakfile
# version:PHPCMS V9
# time:2019-02-06 01:04:48
# type:phpcms
# phpcms:http://www.phpcms.cn
# --------------------------------------------------------


DROP TABLE IF EXISTS `v9_student_master`;
CREATE TABLE `v9_student_master` (
  `student_id` int(8) NOT NULL AUTO_INCREMENT COMMENT '家長學生主键編號',
  `student_contactname` varchar(100) NOT NULL COMMENT '聯絡人姓名 (家長暱稱）',
  `student_name` varchar(100) NOT NULL COMMENT '首名學生名稱',
  `student_contactno` varchar(20) DEFAULT NULL COMMENT '聯絡電話號碼',
  `student_relation` varchar(20) DEFAULT NULL COMMENT '聯絡人與學生的關係',
  `student_locid` int(8) NOT NULL COMMENT '居住地區 ID (loc_level = 3 的 loc_id)',
  `student_address` varchar(250) DEFAULT NULL COMMENT '居住地址',
  `tutor_res` text NOT NULL COMMENT '期望補習成果',
  `student_school` varchar(100) DEFAULT NULL COMMENT '首位學生就讀學校名稱',
  `student_modtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新時間',
  `student_housemobile` varchar(11) NOT NULL COMMENT '住宅电话',
  `st_how_know` text NOT NULL COMMENT '得知公司方式',
  `student_mobile` varchar(11) NOT NULL COMMENT '手机号',
  `student_email` varchar(50) NOT NULL COMMENT '邮箱',
  `student_userid` int(11) NOT NULL COMMENT 'member表注册后的userid',
  PRIMARY KEY (`student_id`),
  KEY `student_contactname` (`student_contactname`),
  KEY `student_name` (`student_name`),
  KEY `student_relation` (`student_relation`),
  KEY `student_locid` (`student_locid`),
  KEY `student_school` (`student_school`),
  KEY `student_mobile` (`student_mobile`),
  KEY `student_email` (`student_email`),
  KEY `student_userid` (`student_userid`)
) ENGINE=MyISAM AUTO_INCREMENT=110 DEFAULT CHARSET=utf8 COMMENT='家長學生主資料表';

INSERT INTO `v9_student_master` VALUES('1','劉芳','劉乾坤','','父親','12','中半山','','中半山中學','2018-01-25 18:29:42','15802583945','1','15802583945','1009356248@qq.com','1');
INSERT INTO `v9_student_master` VALUES('2','張羽凡','張小路','','母親','49','牛頭角','期望補習成果','牛頭角小學','2018-01-25 18:33:33','18684753042','1','18684753042','zal_apink@163.com','3');
INSERT INTO `v9_student_master` VALUES('7','蔡先生','蔡碩文','','父親','28','西沙路530號凱弦居1座3E','','呂明才中學','2018-02-01 23:40:05','','1','94312155','punkytsoi@hotmail.com','11');
INSERT INTO `v9_student_master` VALUES('5','吳陳建銘','吳陳建銘','','監護人','19','藍天小區座306','','香港科技大學','2018-01-27 02:15:08','15017890764','6','15017890764','562395350@qq.com','9');
INSERT INTO `v9_student_master` VALUES('12','Calvin','Calvin','','監護人','29','Garden','','','2018-02-23 06:28:11','','','65899756','calvinyip6@gmail.com','38');
INSERT INTO `v9_student_master` VALUES('11','Zoe Ip','Cheng Sze Yu','','母親','80','Flora Plaza','','HKBUK','2018-02-16 19:09:57','','1','98205821','zoting@hotmail.com','34');
INSERT INTO `v9_student_master` VALUES('10','小风','张一风','','','19','藍天小區座306','','小风学院','2018-02-12 11:23:16','','','15017890764','562395350@qq.com','9');
INSERT INTO `v9_student_master` VALUES('13','','Calvin','','','29','Garden','','','2018-03-05 21:35:20','','','65899756','calvinyip6@gmail.com','38');
INSERT INTO `v9_student_master` VALUES('14','Li Kwai Sang','CY Ko','','母親','35','沙田','','','2018-03-06 19:29:32','Li26486680','','91796848','Sandy_ksli@yahoo.com.hk','49');
INSERT INTO `v9_student_master` VALUES('15','CHOI Kok Hang ','Moy chun kiu','','母親','82','新圍村','','基督教宣道會徐澤林紀念小學','2018-03-07 12:50:18','','1','97104421','Choi_0919@yahoo.com.hk','50');
INSERT INTO `v9_student_master` VALUES('16','Kara Ng','Anakin Lo','','母親','35','Flat 6, 6/F, Blk K, Yu Chui Court, Shatin ','','','2018-03-08 15:19:26','94943514','1','94943514','karang1213@gmail.com','51');
INSERT INTO `v9_student_master` VALUES('17','Yan Wong','Lok Tin','','父親','82','元朗','','','2018-03-13 14:46:48','','3','62000283','wanyan11@yahoo.com.hk','56');
INSERT INTO `v9_student_master` VALUES('18','sean','him','','父親','81','州頭村','','','2018-03-15 21:06:10','','1','67296978','seanman0880@gmail.com','64');
INSERT INTO `v9_student_master` VALUES('19','Ethan Mak','Marcus Mak','','父親','82','又新街','','','2018-03-15 21:22:18','','','93086947','ethanmak612@gmail.com','65');
INSERT INTO `v9_student_master` VALUES('20','Bird','Dolly','','母親','83','天瑞邨','','','2018-03-16 13:19:56','','1','57226516','yin9006@yahoo.com','66');
INSERT INTO `v9_student_master` VALUES('21','Tyler Wong','Selina Wong','','父親','86','Ting Hong HSE , Siu On Crt','','呂祥光小學','2018-03-19 13:56:23','26188204','','69739858','yugiohwkf@gmail.com','70');
INSERT INTO `v9_student_master` VALUES('22','','黄雅','','','22','牛頭角','','','2018-03-19 14:16:24','','','18684753042','zal_apink@163.com','3');
INSERT INTO `v9_student_master` VALUES('23','','张欣','','','22','牛頭角','','','2018-03-19 14:17:59','','','18684753042','zal_apink@163.com','3');
INSERT INTO `v9_student_master` VALUES('24','','晓明','','','22','牛頭角','','','2018-03-19 15:04:14','','','18684753042','zal_apink@163.com','3');
INSERT INTO `v9_student_master` VALUES('25','grace chan','katy','','監護人','21','翠屏邨','','','2018-05-30 11:19:49','','4','68557997','gracechan0424@gmail.com','114');
INSERT INTO `v9_student_master` VALUES('26','coco','patrick','','母親','25','牛池灣','','','2018-05-30 15:00:51','','4','68457974','coco7878@gmail.com','115');
INSERT INTO `v9_student_master` VALUES('27','Peter Yeung','Haley','','監護人','21','123','','','2018-05-31 13:11:38','','','98608148','peteryeung222@gmail.com','116');
INSERT INTO `v9_student_master` VALUES('39','Celia','Chio','','母親','38','Symphony Bay','','Renaissance College','2018-08-15 11:34:45','','1','96621239','celia.chio@yahoo.com','145');
INSERT INTO `v9_student_master` VALUES('38','Candy Ho','Tse Ming Hang','','母親','38','Flat F, 7/F, Block 1, Villa Concerto, Symphony Bay, 530 Sai Sha Road','','Wah Yan College Kowloon','2018-07-22 10:32:58','26309014','1','96012128','candyho323@yahoo.com.hk','137');
INSERT INTO `v9_student_master` VALUES('37','Alice','何馨悦','','','70','The Austin T3 17A','','方方乐趣幼稚园','2018-06-12 13:24:50','','','52068891','ruby19881024@126.com','128');
INSERT INTO `v9_student_master` VALUES('36','何倩','何馨悦','','母親','70','The Austin T3 17A','','方方乐趣国际幼儿园','2018-06-12 13:20:09','','1','52068891','ruby19881024@126.com','128');
INSERT INTO `v9_student_master` VALUES('45','Yvonne Yip','Ashley Yiu','','母親','50','西灣河','','Bradbury School','2018-08-20 15:16:40','','1','61922704','yvonne@btc.com.hk','153');
INSERT INTO `v9_student_master` VALUES('46','Sunny','Sunny','','監護人','21','觀塘開源道','','','2018-08-22 13:01:21','','','95870981','sunsfcjade@gmail.com','155');
INSERT INTO `v9_student_master` VALUES('42','Grace Cheng','Gillian Lee','','母親','12','9/F, Flat B, 55 Conduit Road','','HKIS','2018-08-15 15:19:30','60786023','1','60786023','gracecheng1604@gmail.com','148');
INSERT INTO `v9_student_master` VALUES('43','Gladys','Luk','','母親','18','Flat 2 1/F Blk A Evergreen Villa, 43 Stubbs Road','','Chinese International School ','2018-08-15 15:41:06','+8529022088','1','90220888','gladys.luk@gmail.com','149');
INSERT INTO `v9_student_master` VALUES('44','CHRISTINE NG','LAU','','母親','35','沙田大南寮村41號1樓','','','2018-08-17 16:40:15','','','93457472','christineng@asianfencinghk.com','150');
INSERT INTO `v9_student_master` VALUES('47','','Branson','','','28','西沙路530號凱弦居1座3E','','','2018-08-22 14:25:40','','','94312155','punkytsoi@hotmail.com','11');
INSERT INTO `v9_student_master` VALUES('48','','Branson','','','28','西沙路530號凱弦居1座3E','','','2018-08-22 14:26:37','','','94312155','punkytsoi@hotmail.com','11');
INSERT INTO `v9_student_master` VALUES('49','','Branson','','','28','西沙路530號凱弦居1座3E','','','2018-08-22 14:27:18','','','94312155','punkytsoi@hotmail.com','11');
INSERT INTO `v9_student_master` VALUES('50','Leung Pui Ki','Cheung King Hin','','母親','33','浩景台','','聖芳齊各英文小學','2018-08-31 10:52:49','26682558','','51311431','leungpuiki2002@yahoo.com.hk','163');
INSERT INTO `v9_student_master` VALUES('51','銘','吳陳建銘','','','19','藍天小區座306','','','2018-09-01 12:50:11','','','15017890764','562395350@qq.com','9');
INSERT INTO `v9_student_master` VALUES('52','Sylvia Siu','Sylvia Siu','','母親','18','渣甸山金雲閣','','銅鑼灣維多利亞','2018-09-05 22:48:15','36769590','','63833906','silverstar325@gmail.com','166');
INSERT INTO `v9_student_master` VALUES('53','Raymond Cheung','Kelly Cheung','','父親','30','青华苑A座2111室','','東華三院黃芴南中學','2018-09-12 15:00:15','91949955','1','91949955','raymond420@outlook.com','191');
INSERT INTO `v9_student_master` VALUES('54','Karmen chiu','Winnie wong','','母親','29','大窩口','','','2018-09-12 16:47:02','','','96555860','Karmen4750@yahoo.com.hk','192');
INSERT INTO `v9_student_master` VALUES('56','Karen Chow (or 媽媽Angel)','Annabelle','','母親','47','北角明園西街','','真光（大坑）','2018-09-13 12:18:10','','1','96232721','angel@pointersgemcraft.com','195');
INSERT INTO `v9_student_master` VALUES('57','Apple Tsang','Jason Chau','','母親','86','Tuen Mun','','啟思','2018-09-14 13:46:20','97070551','1','97070551','popoapple@gmail.com','201');
INSERT INTO `v9_student_master` VALUES('58','Bonnie','Amos','','母親','30','Mayfair Gardens','','Alliance Primary School Kowloon Tong','2018-09-15 11:58:45','92599159','2','92599159','bonniepnc128@gmail.com','202');
INSERT INTO `v9_student_master` VALUES('59','Cecilia Chim','Erika Lin','','母親','64','何文田','','聖羅撒書院','2018-09-19 20:58:32','26830860','1','92442062','linerika125@gmail.com','216');
INSERT INTO `v9_student_master` VALUES('60','Stephen Lau','Hilfred Lau','','父親','12','Robinson Road','提升校內成績','蘇浙公學','2018-09-28 08:24:53','25457678','1','97178899','hongkongtrader@gmail.com','239');
INSERT INTO `v9_student_master` VALUES('61','Jenny','Kayden','','母親','16','Wan chaj','','Ycis','2018-09-30 13:49:45','68866689 ','1','68866689','Jennymyy@yahoo.com.hk','258');
INSERT INTO `v9_student_master` VALUES('63','Kennis Yip','Marcus Lam','','母親','30','藍澄灣','每科也進步，尢其是中文','王錦輝','2018-10-12 08:29:43','94223440','3','94223440','kenniscobe@yahoo.com.hk','298');
INSERT INTO `v9_student_master` VALUES('65','James Ho','Ho Lok Yin','','','82','元朗','','','2018-10-15 11:43:44','','','62000283','wanyan11@yahoo.com.hk','56');
INSERT INTO `v9_student_master` VALUES('66','Mrs.Wong','Lucia Wong','','母親','77','Flat H, The Pacifica , Shsm Shing Rd Kowloon','','Good Hope School ','2018-10-16 16:02:56','','1','92778897','macy9277@gmail.com','305');
INSERT INTO `v9_student_master` VALUES('67','Amy','Race and chase','','母親','82','Park yoho','完成功課及明白點解','Pooi Kei primary school ','2018-10-16 20:21:13','96993194','1','96993194','amy@style50s.com','306');
INSERT INTO `v9_student_master` VALUES('68','Ruby  Lee ','Rachel Liu','','母親','81','上水圍圍內村','數等 A grade','李志達紀念學校','2018-10-20 13:59:29','95230001 ','3','95230001','ruby-cy.lee@iwc.com','314');
INSERT INTO `v9_student_master` VALUES('69','Koni Suen','Molly Ho','','母親','86','Leung King Est','jolly phonics','','2018-10-21 20:22:56','91895282','1','91895282','babeekon@gmail.com','318');
INSERT INTO `v9_student_master` VALUES('70','LAM','SUN','','監護人','21','Kwun Tong','','','2018-10-30 14:48:16','','1','61043981','lamyicksun@gmail.com','329');
INSERT INTO `v9_student_master` VALUES('71','Doris','Tung Wilshere ','','母親','35','17C, Delite Court, Pictorial Garden, Phase 2','Level up on the reading level and spelling as well ','Sha Tin Junior ','2018-11-05 08:28:23','','1','62555095','dordor71@gmail.com','340');
INSERT INTO `v9_student_master` VALUES('72','Suka Lau','Chan Ying Yue ','','母親','19','Hong Tin Court','加强數理能力','Good Hope School','2018-11-12 21:36:30','','1','93075476','Lausuka@gmail.com','343');
INSERT INTO `v9_student_master` VALUES('73','Chak Yuet Ching','Lam Chun Hin ','','母親','39','Tai Po','','','2018-11-28 09:03:57','','3','63902311','bigbird129@hotmail.com','345');
INSERT INTO `v9_student_master` VALUES('74','Mrs. Ho','Abbie','','母親','73','Prince Edward','','Heep Woh ','2018-11-28 19:28:49','94298343','','94298343','heidi_pong@yahoo.com.hk','346');
INSERT INTO `v9_student_master` VALUES('75','Miss Cheung','Carlos ','','母親','66','土瓜灣道','提高對數學的信心，強化文字提理解力，\r\n成績希望70分以上','獻主會聖馬善樂小學','2018-11-29 00:04:55','98062283','3','98062283','rachelccl2000@yahoo.com.hk','347');
INSERT INTO `v9_student_master` VALUES('76','Bat Or','Teresa ','','母親','12','羅便臣道','學業成績進歩，啟發小朋友，有趣學習','嘉諾撒聖心小學','2018-12-01 01:05:00','','3','98281630','batortobe@yahoo.com','348');
INSERT INTO `v9_student_master` VALUES('77','Pinki Chu','Anson Poon','','母親','81','海禧華庭','','','2018-12-04 19:55:47','','3','96648839','Pinki.chu@takbo.hk','350');
INSERT INTO `v9_student_master` VALUES('78','ADA','Nam Wing','','母親','28','將軍澳','','馮晴','2018-12-05 17:23:59','','3','98112741','adatsoiou@yahoo.com.hk','351');
INSERT INTO `v9_student_master` VALUES('79','Chow','On Kei','','母親','35','豐禾邨','','','2018-12-06 13:26:39','','3','60804881','chowmary2003@yahoo.com.hk','352');
INSERT INTO `v9_student_master` VALUES('80','Abbie','Abbie Ho','','','73','Prince Edward','打好基礎\r\n培養學習動機和興趣','Heep Woh','2018-12-06 14:53:08','','','94298343','heidi_pong@yahoo.com.hk','346');
INSERT INTO `v9_student_master` VALUES('81','','On Kei','','','35','豐禾邨','星期二，四或 星期三，五','','2018-12-06 17:21:18','','','60804881','chowmary2003@yahoo.com.hk','352');
INSERT INTO `v9_student_master` VALUES('82','','On Kei','','','35','豐禾邨','星期二，四或 星期三，五','','2018-12-06 17:23:26','','','60804881','chowmary2003@yahoo.com.hk','352');
INSERT INTO `v9_student_master` VALUES('83','Connie Au Yeung','Leung Nga Yin','','母親','38','錦泰苑','DSE','東華三院馮黃鳳亭中學','2018-12-07 00:17:34','','3','66223123','ay.connie@yahoo.com.hk','353');
INSERT INTO `v9_student_master` VALUES('84','D LEUNG','C LEUNG','','父親','67','Harbour Place','','','2018-12-08 23:27:05','','3','92634887','domleungok@gmail.com','354');
INSERT INTO `v9_student_master` VALUES('85','Mrs Lee','Katie Lee','','母親','23','Richland gardens','Get the skills on study','Good Hope School','2018-12-09 20:12:21','97709289','3','97709289','Huipui711@hotmail.com','355');
INSERT INTO `v9_student_master` VALUES('86','Ophie Fu','Adrian Wong','','母親','27','清水湾 碧沙别墅','全级top 10','creative secondary school','2018-12-16 12:20:52','96029648','1','96029648','adrian940499@gmail.com','360');
INSERT INTO `v9_student_master` VALUES('87','Maximus','Maximus Wong','','','27','清水湾 碧沙别墅','全级top 5','creative secondary school','2018-12-16 13:29:40','','','96029648','adrian940499@gmail.com','360');
INSERT INTO `v9_student_master` VALUES('88','Cassy','Cassandra Wong','','','27','清水湾 碧沙别墅','全级top 15','Good hope primary school','2018-12-16 13:34:31','','','96029648','adrian940499@gmail.com','360');
INSERT INTO `v9_student_master` VALUES('89','Cassey','Cassandra Wong','','','27','清水湾 碧沙别墅','全级top 15','Good hope primary school','2018-12-16 13:36:35','','','96029648','adrian940499@gmail.com','360');
INSERT INTO `v9_student_master` VALUES('90','Yeung','Sim','','母親','80','牽晴間','','','2018-12-16 19:25:37','98461430','3','98461430','wingsimy@yahoo.com.hk','362');
INSERT INTO `v9_student_master` VALUES('91','Ms Yip','Law hui mong hera','','母親','38','帝琴灣','Physics與數學有進步','浸信會呂明才中學','2018-12-19 22:07:10','23452555','','92608040','shukyiyip@gmail.com','363');
INSERT INTO `v9_student_master` VALUES('92','文偉龍','文逸澧','','父親','27','將軍澳景林邨景楠樓1407室','','東華三院王余家潔小學','2018-12-24 19:23:29','27039329','3','92161025','a48manlung@yahoo.com','364');
INSERT INTO `v9_student_master` VALUES('93','Yuki Hung','Elsa Leung','','母親','77','泓景臺','普通話拼音','聖安德烈小學','2018-12-29 22:32:31','97028899','3','97028899','yuki_hung2819@yahoo.com.hk','365');
INSERT INTO `v9_student_master` VALUES('94','Lee','Dora Lee','','母親','71','Kln','','','2019-01-01 17:26:02','','3','60838436','rosang3888@yahoo.com.hk','366');
INSERT INTO `v9_student_master` VALUES('95','Mrs lam ','Lam Elise','','母親','62','廣播道','預備小一','Cckg','2019-01-02 16:04:15','','3','90655152','Celinetsang@gmail.com','367');
INSERT INTO `v9_student_master` VALUES('96','JANET CHENG ','Alvin Ng','','母親','54','黃竹坑警察學院','完成每天功課/預習默書/溫習','天主教總堂區學校','2019-01-03 07:54:32','','3','61514755','janet75113@yahoo.com.hk','368');
INSERT INTO `v9_student_master` VALUES('97','Cherie','Bianca','','母親','35','水泉澳邨','','沙田官立小學','2019-01-03 15:27:50','61229663','3','61229663','ksstksst@yahoo.com.hk','369');
INSERT INTO `v9_student_master` VALUES('98','戴靖家','文逸澧','','父親','27','將軍澳景林邨','一步步進步','東華三院王余潔紀念小學','2019-01-05 13:08:22','31583287','3','61843812','Angel61843812@gmail.com','370');
INSERT INTO `v9_student_master` VALUES('99','Mrs. So','Winky ','','母親','67','Hung hom','','','2019-01-06 22:59:26','','3','92249713','Kenniskan@hotmail.com','371');
INSERT INTO `v9_student_master` VALUES('100','Chu pui ching','Lai Yan Tung Larine','','母親','33','荔景','','嘉諾撒聖心','2019-01-09 20:44:51','95339980','3','95339980','angelchu989@gmail.com','374');
INSERT INTO `v9_student_master` VALUES('101','Joey Fong','Stephanie Chung','','母親','31','國瑞路海壩新村','有進步','香港浸會大學附屬學校王錦輝中小學','2019-01-10 02:44:57','91094991','3','91094991','joeyfong_2001@yahoo.com.hk','375');
INSERT INTO `v9_student_master` VALUES('102','Stephany Tam','Archie Leung','','母親','38',' Ma on shan','','','2019-01-11 17:04:25','','','51211757','steptam2003@yahoo.com.hk','377');
INSERT INTO `v9_student_master` VALUES('103','Pony lee','Matilda Seung','','母親','48','Kornhill','','','2019-01-11 23:36:02','90152268','3','90152268','Ponylee@ymail.com','378');
INSERT INTO `v9_student_master` VALUES('104','Joy','王恩琳','','母親','32','葵芳邨葵歡樓3604室','各科成績有進步','聖公會青衣主恩小學','2019-01-12 15:53:00','24100520','1','93016300','hello_joyjoy2008@hotmail.com','379');
INSERT INTO `v9_student_master` VALUES('105','Winnie','Aiden','','母親','21','功樂道',' Grade C or over','SFA','2019-01-16 00:01:45','','3','92373514','winnie425@gmail.com','381');
INSERT INTO `v9_student_master` VALUES('106','Mr.To','To Chun Hei','','父親','22','淘大花園','','','2019-01-21 21:55:34','','2','92251428','Jackie_to_2008@yahoo.com.hk','384');
INSERT INTO `v9_student_master` VALUES('107','Lo','Yeung','','母親','29','荃灣','','','2019-01-21 23:24:02','98623395','3','98623395','Cherryloo1230@yahoo.com.hk','385');
INSERT INTO `v9_student_master` VALUES('109','Mrs Ng','Evan','','母親','58','翠竹花園','小三中,英,英數\r\n功課和温習','聖三一堂小學','2019-01-30 12:32:12','64701511','2','64701511','btgigi@gmail.com','390');

