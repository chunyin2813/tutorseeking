<?php defined('IN_PHPCMS') or exit('No permission resources.'); ?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="<?php echo $SEO['keyword'];?>">
    <meta name="description" content="<?php echo $SEO['description'];?>">
    <link rel="stylesheet" href="<?php echo CSS_PATH;?>add/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo CSS_PATH;?>add/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo CSS_PATH;?>add/base.css">
    <title><?php if(isset($SEO['title']) && !empty($SEO['title'])) { ?><?php echo $SEO['title'];?><?php } ?><?php echo $SEO['site_title'];?></title>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-62236188-14"></script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-808866381"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'AW-808866381');
    </script>
    <script>
      gtag('event', 'page_view', {
        'send_to': 'AW-808866381',
        'user_id': 'replace with value'
      });
    </script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-62236188-14');
    </script>
    <!--Google Tag Manager -->

    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

    })(window,document,'script','dataLayer','GTM-W775LCQ');</script>

    <!-- End Google Tag Manager -->

</head>
<body>

    <?php 
        //自动更改未配对个案的配对状态为0
        $this->thisdb = pc_base::load_model('content_model');
        $this->thisdb->query("update `v9_student_transaction` set `st_matched` = '0' where `st_id` in(SELECT `st_id` from (SELECT `st_id` FROM `v9_student_transaction` where `st_matched` = '1' and `st_id` not in(select `pair_student_tranid` from `v9_pair`)) as temp)");

        $this->thisdb->query("update `v9_tutor_transaction` set `tm_matched` = '0' where `tm_tutorid` in(SELECT `tm_tutorid` from (SELECT `tm_tutorid` FROM `v9_tutor_transaction` where `tm_matched` = '1' and `tm_tutorid` not in(select `pair_tutor_tranid` from `v9_pair`)) as temp)");

        //判断当前账号session是否过期
        if(!empty($_SESSION['userid'])){
            if(time() - $_SESSION['login_time'] > 1800){
                unset($_SESSION['id']);
                unset($_SESSION['username']);
                unset($_SESSION['role_verify']);
            }else{
                $_SESSION['login_time'] = time();
            }
        }
    ?>
    <div class="content">
        <!--头部-->
        <header class="tutor-header">
            <div class="fixed-header">
                <div class="container">
                    <div class="row">
                        <div class="header-content">
                            <div class="logo">
                                <a href="<?php echo APP_PATH;?>"><img src="<?php echo IMG_PATH;?>add_image/logo-sticky.png" alt=""></a>
                            </div>
                            <?php if(defined('IN_ADMIN')  && !defined('HTML')) {echo "<div class=\"admin_piao\" pc_action=\"get\" data=\"op=get&tag_md5=75710831cddeb9a00bc798c8225eff6e&sql=SELECT+%2A+from+v9_category+where+ismenu+%3D+%271%27+order+by+listorder&cache=3600&return=data\"><a href=\"javascript:void(0)\" class=\"admin_piao_edit\">编辑</a>";}$tag_cache_name = md5(implode('&',array('sql'=>'SELECT * from v9_category where ismenu = \'1\' order by listorder',)).'75710831cddeb9a00bc798c8225eff6e');if(!$data = tpl_cache($tag_cache_name,3600)){pc_base::load_sys_class("get_model", "model", 0);$get_db = new get_model();$r = $get_db->sql_query("SELECT * from v9_category where ismenu = '1' order by listorder LIMIT 20");while(($s = $get_db->fetch_next()) != false) {$a[] = $s;}$data = $a;unset($a);if(!empty($data)){setcache($tag_cache_name, $data, 'tpl_data');}}?>
                            <nav class="header-menu">
                                <ul class="nav-menu">
                                    <li <?php if(!$_GET['catid'] && strpos($_SERVER["QUERY_STRING"],$str)=== false){?> class="menu-item active"<?php }?>><a href="<?php echo APP_PATH;?>">首頁</a></li>
                                    <?php $n=1; if(is_array($data)) foreach($data AS $k => $v) { ?> 
                                        <?php if($_GET['catid']==$v[catid]) { ?>
                                        <!--判断显示的catid，如果是显示的catid--> 
                                            <li class="menu-item active"><a href="<?php echo $v['url'];?>"><?php echo $v['catname'];?></a></li>
                                        <?php } else { ?>
                                            <li class="menu-item"><a href="<?php echo $v['url'];?>"><?php echo $v['catname'];?></a></li><!--如果显示的不是，内容正常显示--> 
                                        <?php } ?>   
                                    <?php $n++;}unset($n); ?>
                                    <li class="menu-item inline-item"><a target="_blank" href="https://www.facebook.com/TutorSeeking/" class="fb_href"><i class="fa fa-facebook"></i></a></li>
                                   <!--  <li class="menu-item  inline-item"><a href=""><i class="fa fa-youtube"></i></a></li> -->
                                    <li class="menu-item  inline-item"><a target="_blank" href="https://www.instagram.com/tutorseeking/" class="ig_href_contact"><i class="fa fa-instagram"></i></a></li>
                                    <li class="menu-item  inline-item search-item">
                                        <a href=""><i class="fa fa-search"></i></a>
                                        <div class="header-search">
                                            <div class="search-input">
                                                <form action="<?php echo APP_PATH;?>index.php?m=content&c=index&a=search&catid=3" method='post'>
                                                <input type="text" name="keyword">
                                                <button type="submit" class="icon-search"><i class="fa fa-search"></i></button>
                                                </form>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </nav>
                            <?php if(defined('IN_ADMIN') && !defined('HTML')) {echo '</div>';}?>
                            <div class="menu-mobile">
                                <i class="fa fa-bars"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </header>
        <!--头部结束-->
        <script src="<?php echo JS_PATH;?>add/jquery-1.11.2.min.js"></script>
        <script>
  
      

            // Fb、Ig打开APP 未安装则跳转链接
            // $('.fb_href').click(function() {

            //     if(navigator.userAgent.match(/(iPhone|iPod|iPad);?/i)) {
            //         var loadDateTime = new Date();
            //         window.setTimeout(function() {
            //             var timeOutDateTime = new Date();
            //             if(timeOutDateTime - loadDateTime < 5000) {
            //                 window.location = "https://www.facebook.com/TutorSeeking/";
            //             }else{
            //                 window.close();
            //             }
            //         },
            //     25);
            //     window.location = "fb:page/<span style='color:#ff0000;'>341983292945136</span>";
            //     }else if(navigator.userAgent.match(/android/i)) {
            //         var state = null;
            //         try{
            //            state = window.open("fb:page/<span style='color:#ff0000;'>341983292945136</span>", '_blank');
            //         }catch(e){}
            //             if(state){
            //             window.close();
            //         }else{
            //             window.location = "https://www.facebook.com/TutorSeeking/";
            //         }
            //     }

            // });

            // $('.ig_href_contact').click(function() {
                
            //     if(navigator.userAgent.match(/(iPhone|iPod|iPad);?/i)) {
            //         var loadDateTime = new Date();
            //         window.setTimeout(function() {
            //             var timeOutDateTime = new Date();
            //             if(timeOutDateTime - loadDateTime < 5000) {
            //                 window.location = "https://www.instagram.com/tutorSeeking/";
            //             }else{
            //                 window.location = 'instagram:open';
            //             }
            //         },
            //     25);
            //     window.location = "instagram:open";
            //     }else if(navigator.userAgent.match(/android/i)) {
            //         var state = null;
            //         try{
            //            state = window.open("instagram:open ", '_blank');
            //         }catch(e){}
            //             if(state){
            //             window.close();
            //         }else{
            //             window.location = "https://www.instagram.com/tutorSeeking/";
            //         }
            //     }
                
            // });
        </script>

