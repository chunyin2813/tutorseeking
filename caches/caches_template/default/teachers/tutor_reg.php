<?php defined('IN_PHPCMS') or exit('No permission resources.'); ?><title>導師登記</title>
<?php include template("content","header_new"); ?>
<!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">-->
<link rel="stylesheet" href="<?php echo CSS_PATH;?>add/bootstrapValidator.css">
<link rel="stylesheet" href="<?php echo CSS_PATH;?>add/tutor-reg.css">
<link rel="stylesheet" href="<?php echo CSS_PATH;?>add/index.css">
<style type="text/css">
    .error-sex{
        color: red;
    }
</style>
    <?php if(defined('IN_ADMIN')  && !defined('HTML')) {echo "<div class=\"admin_piao\" pc_action=\"get\" data=\"op=get&tag_md5=f6d701f225f20b414cae6110b1edfbcf&sql=select+thumb+from+v9_position+where+posid%3D1+order+by+listorder+desc&cache=%273600%27+return%3D\"><a href=\"javascript:void(0)\" class=\"admin_piao_edit\">编辑</a>";}pc_base::load_sys_class("get_model", "model", 0);$get_db = new get_model();$r = $get_db->sql_query("select thumb from v9_position where posid=1 order by listorder desc LIMIT 20");while(($s = $get_db->fetch_next()) != false) {$a[] = $s;}$data = $a;unset($a);?>
        <?php $n=1;if(is_array($data)) foreach($data AS $v) { ?>    
        <div class="top-tutor-main" style="background-image:url(<?php echo $v['thumb'];?>)">
            <span class="overlay-top-main"></span>
            <div class="page-title-content">
                <div class="container">
                    <h1 class="page-title">導師登記</h1>
                </div>
            </div>
        </div>
        <?php $n++;}unset($n); ?>
    <?php if(defined('IN_ADMIN') && !defined('HTML')) {echo '</div>';}?>
        <div class="section-content">
            <div class="container">
                <div class="row">
                    <div class="breadcrumbs">
                        <ul>
                            <li><a href="<?php echo APP_PATH;?>"><span>首頁</span></a></li>
                            <li><span>導師登記</span></li>
                        </ul>
                    </div>
                    <div class="entry-content tutor-horizontal">
                        <div class="panel-tab">
                            <div class="panel-tab-item panel-tab-rule">導師條款</div>
                            <div class="panel-tab-item">導師類別</div>
                            <div class="panel-tab-item">授課區域/時間</div>
                            <div class="panel-tab-item">個人資料</div>
                            <div class="panel-tab-item">其他技能</div>
                            <div class="panel-tab-item">自我簡介</div>
                            <div class="panel-tab-item">個案配對</div>
                        </div>
                        <div class="tab-panel-content">
                            <!--導師條款及規則-->
                            <div class="tab-panel-item">
                                <div class="tab-panel-title">導師條款及規則</div>
                                <div class="rule-box">
                                    <div class="rule-content">
                                        <?php include template("teachers","rule"); ?>
                                    </div>
                                </div>
                                <div class="rule-checkbox">
                                    <div class="checkbox checkbox-primary">
                                        <input  type="checkbox" class="agree-check">
                                        <label for="">我同意以上條款，並開始進行登記</label>
                                    </div>
                                    <p class="error-rule">請確認上述條款</p>
                                </div>
                            </div>
                            <!--導師條款及規則結束-->
                            <!--導師類別-->
                            <div class="tab-panel-item">
                                <div class="tab-panel-title">導師類別</div>
                                <p>請選擇所有可教之項目，我們只根據你所選項目進行工作配對。</p>
                                <div class="tutor-cate-box">
                                    <div class="tutor-cate-content">
                                        <div class="tutor-cate-item cate-item-1">
                                            <div class="tutor-cate-title">可教授類別</div>
                                            <div class="cate-item-list">
                                                
                                                <select   class="form-control TchCate" name="TchCate" >
                                                    <option disabled="disabled" selected="selected"></option>
                                                   
                                                    <?php foreach ($res as $key => $r1) {?>
                                                        <option value="<?php echo $r1['tt_id'];?>"><?php echo $r1['tt_name'];?></option>
                                                    <?php }?>
                                                </select>
                                               
                                            </div>
                                        </div>

                                        <div class="tutor-cate-item  cate-item-2">
                                            <div class="tutor-cate-title">可教授項目</div>
                                            <div class="cate-item-list">
                                                <select   class="form-control TchItem" name="TchItem">
                                                    <option disabled="disabled" selected="selected" value=""></option>
                                                </select>
                                            </div> 
                                        </div>
                                        <div class="tutor-cate-item  cate-item-3">
                                            <div class="tutor-cate-title">可教授級別</div>
                                            <div class="cate-item-list">
                                                <select   class="form-control TchLevel" name="TchLevel">
                                                    <option disabled="disabled" selected="selected" value=""></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tutor-add">
                                        <i class="fa fa-plus"></i>&nbsp;增加可教授項目
                                    </div>                                    
									<div class="tutor-Remove">                                    
									<i class="fa fa-minus"></i>&nbsp;減少可教授項目
                                    </div>
                                    <p class="error-area"></p>
                                </div>
                            </div>
                            <!--導師類別結束-->
                            <!--授課區域-->
                            <div class="tab-panel-item area-panel">
                                <div class="tab-panel-title">授課區域</div>
                                <!-- <p>請選擇不超過10個地區作為補習區域</p> -->
                                <div class="area-box">
                                    <?php if(defined('IN_ADMIN')  && !defined('HTML')) {echo "<div class=\"admin_piao\" pc_action=\"get\" data=\"op=get&tag_md5=d0ed962092e1acf7a2a99f28fff4a2da&sql=SELECT+loc_level%2Cloc_name%2Cloc_parentid%2Cloc_id+FROM+v9_sys_location+where+loc_level%3D2+order+by+loc_seq+asc--&return=location\"><a href=\"javascript:void(0)\" class=\"admin_piao_edit\">编辑</a>";}pc_base::load_sys_class("get_model", "model", 0);$get_db = new get_model();$r = $get_db->sql_query("SELECT loc_level,loc_name,loc_parentid,loc_id FROM v9_sys_location where loc_level=2 order by loc_seq asc-- LIMIT 20");while(($s = $get_db->fetch_next()) != false) {$a[] = $s;}$location = $a;unset($a);?>
                                    <?php $n=1;if(is_array($location)) foreach($location AS $loc) { ?>
                                    <div class="area-item" id="<?php echo $loc['loc_id'];?>">
                                        <div class="area-item-panel"><?php echo $loc['loc_name'];?></div>
                                        <ul class="item-panel-list">
                                            <?php $loc_id=$loc[loc_id]?>
                                            <?php if(defined('IN_ADMIN')  && !defined('HTML')) {echo "<div class=\"admin_piao\" pc_action=\"get\" data=\"op=get&tag_md5=584caef8fdf02d9221fe62be67ad7130&sql=SELECT+loc_name%2Cloc_id+FROM+v9_sys_location+where+loc_level%3D3+and+loc_parentid%3D%24loc_id+order+by+loc_seq+asc--&return=place\"><a href=\"javascript:void(0)\" class=\"admin_piao_edit\">编辑</a>";}pc_base::load_sys_class("get_model", "model", 0);$get_db = new get_model();$r = $get_db->sql_query("SELECT loc_name,loc_id FROM v9_sys_location where loc_level=3 and loc_parentid=$loc_id order by loc_seq asc-- LIMIT 20");while(($s = $get_db->fetch_next()) != false) {$a[] = $s;}$place = $a;unset($a);?>
                                            <?php if(empty($place)){
                                                echo "<script>
                                                    $('#".$loc_id."').css('display','none');
                                                    </script>";
                                            }?>
                                            <li>
                                                <div class="checkbox checkbox-primary">
                                                    <input type="checkbox" onclick="all_checkbox(this);" name="TchArea" class="all" value="<?php echo $loc['loc_id'];?>">
                                                    <label for="">全<?php echo $loc['loc_name'];?></label>
                                                </div>
                                            </li>
                                            <?php $n=1;if(is_array($place)) foreach($place AS $p) { ?>                           
                                                <li>
                                                    <div class="checkbox checkbox-primary">
                                                        <input type="checkbox" value="<?php echo $p['loc_id'];?>" name="TchArea" class="agree-check TchArea" onclick="single_click(this);">
                                                        <label for=""><?php echo $p['loc_name'];?></label>
                                                    </div>
                                                </li>
                                            <?php $n++;}unset($n); ?>
                                            <?php if(defined('IN_ADMIN') && !defined('HTML')) {echo '</div>';}?>
                                        </ul>
                                    </div>
                                    <?php $n++;}unset($n); ?>
                                    <?php if(defined('IN_ADMIN') && !defined('HTML')) {echo '</div>';}?>                                   
                                </div>
                                <script>

                                    //全選與全不選
                                    function all_checkbox(option){
                                        var allselect = $(option).is(':checked');//判斷全選或是全不選
                                        $(option).parent().parent().parent().find(".agree-check").each(function(){
                                            // alert($(this).is(":checked"));
                                            if(allselect){                      //全選 
                                                if(!$(this).is(":checked")){    //click未選中的選項
                                                    // $(this).click();
                                                    $(this).prop("checked",true);
                                                }
                                            }else{                              //全不選
                                                if($(this).is(":checked")){     //click已選中的選項
                                                    // $this.click();
                                                    $(this).prop("checked",false);
                                                }
                                            }
                                        });
                                    }

                                    //點擊地區時
                                    function single_click(option){
                                        var allselect = 0;
                                        if(!$(option).is(':checked')){ //取消選擇時 全選按鈕去除勾選
                                            $(option).parent().parent().parent().find('.all').removeAttr('checked');
                                        }else{
                                            //點擊選擇時 判斷若全部選擇 則全選按鈕打勾
                                            $(option).parent().parent().parent().find('.agree-check').each(function(){
                                                if(!$(this).is(':checked')){
                                                    allselect = 1;
                                                }
                                            });
                                            if(allselect == 0){
                                                $(option).parent().parent().parent().find('.all').click();
                                            }
                                        }
                                    }

                                </script>
                                <p class="error-area"></p>
                                <div class="tab-panel-title">授課時間</div>
                                <div class="info-list"> 
                                   <table class="listpanel-table" id="time"> 
                                    <thead> 
                                     <tr>
                                      <th colspan="2" class="listpanel-title">可教授時間</th> 
                                     </tr>
                                    </thead> 
                                    <tbody> 
                                     <tr class="mon">
                                        <td>星期一</td>
                                        <td>
                                            <div class="class-list">
                                                <div class="add-class-group"><i class="fa fa-plus"></i></div>
                                                <div class="class-group">
                                                    <div class="class-inline">
                                                        <label for="">由</label>
                                                        <select name="time[mon][from_hour]"  class="from_hour class-control">
                                                            <option value=""></option>
                                                            <?php foreach($timelist as $k=>$v){?>
                                                            <option value="<?php echo $k;?>"><?php echo $v;?></option>
                                                           <?php }?> 
                                                        </select>
                                                    </div>
                                                    <div class="class-inline">
                                                        <label for="">至</label>
                                                        <select name="time[mon][to_hour]"  class="to_hour class-control">
                                                            <option value=""></option>
                                                            <?php foreach($timelist as $k=>$v){?>
                                                            <option value="<?php echo $k;?>"><?php echo $v;?></option>
                                                           <?php }?> 
                                                        </select>
                                                    </div>
                                               </div>
                                            </div>  
                                        </td>
                                    </tr>
                                    <tr class="tues">
                                        <td>星期二</td>
                                        <td>
                                            <div class="class-list">
                                                <div class="add-class-group"><i class="fa fa-plus"></i></div>
                                                <div class="class-group">
                                                    <div class="class-inline">
                                                        <label for="">由</label>
                                                        <select name="time[tues][from_hour]"  class="from_hour class-control">
                                                            <option value=""></option>
                                                            <?php foreach($timelist as $k=>$v){?>
                                                            <option value="<?php echo $k;?>"><?php echo $v;?></option>
                                                           <?php }?> 
                                                        </select>
                                                    </div>
                                                    <div class="class-inline">
                                                        <label for="">至</label>
                                                        <select name="time[tues][to_hour]"  class="to_hour class-control">
                                                            <option value=""></option>
                                                            <?php foreach($timelist as $k=>$v){?>
                                                            <option value="<?php echo $k;?>"><?php echo $v;?></option>
                                                           <?php }?> 
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>  
                                        </td>
                                    </tr>
                                    <tr class="wed">
                                        <td>星期三</td>
                                        <td>
                                            <div class="class-list">
                                                <div class="add-class-group"><i class="fa fa-plus"></i></div>
                                                <div class="class-group">
                                                    <div class="class-inline">
                                                        <label for="">由</label>
                                                        <select name="time[wed][from_hour]"  class="from_hour class-control">
                                                            <option value=""></option>
                                                          <?php foreach($timelist as $k=>$v){?>
                                                            <option value="<?php echo $k;?>"><?php echo $v;?></option>
                                                           <?php }?> 
                                                        </select>
                                                    </div>
                                                    <div class="class-inline">
                                                        <label for="">至</label>
                                                        <select name="time[wed][to_hour]" class="to_hour class-control">
                                                            <option value=""></option>
                                                            <?php foreach($timelist as $k=>$v){?>
                                                            <option value="<?php echo $k;?>"><?php echo $v;?></option>
                                                           <?php }?> 
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>  
                                        </td>
                                    </tr>
                                    <tr class="thur">
                                        <td>星期四</td>
                                        <td>
                                            <div class="class-list">
                                                <div class="add-class-group"><i class="fa fa-plus"></i></div>
                                                <div class="class-group">
                                                    <div class="class-inline">
                                                        <label for="">由</label>
                                                        <select name="time[thur][from_hour]" class="from_hour class-control">
                                                            <option value=""></option>
                                                            <?php foreach($timelist as $k=>$v){?>
                                                            <option value="<?php echo $k;?>"><?php echo $v;?></option>
                                                           <?php }?> 
                                                        </select>
                                                    </div>
                                                    <div class="class-inline">
                                                        <label for="">至</label>
                                                        <select name="time[thur][to_hour]" class="to_hour class-control">
                                                            <option value=""></option>
                                                            <?php foreach($timelist as $k=>$v){?>
                                                            <option value="<?php echo $k;?>"><?php echo $v;?></option>
                                                           <?php }?> 
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>  
                                        </td>
                                    </tr>
                                    <tr class="fri">
                                        <td>星期五</td>
                                        <td>
                                            <div class="class-list">
                                                <div class="add-class-group"><i class="fa fa-plus"></i></div>
                                                <div class="class-group">
                                                    <div class="class-inline">
                                                        <label for="">由</label>
                                                        <select name="time[fri][from_hour]" class="from_hour class-control">
                                                            <option value=""></option>
                                                            <?php foreach($timelist as $k=>$v){?>
                                                            <option value="<?php echo $k;?>"><?php echo $v;?></option>
                                                           <?php }?> 
                                                        </select>
                                                    </div>
                                                    <div class="class-inline">
                                                        <label for="">至</label>
                                                        <select name="time[fri][to_hour]" class="to_hour class-control">
                                                            <option value=""></option>
                                                           <?php foreach($timelist as $k=>$v){?>
                                                            <option value="<?php echo $k;?>"><?php echo $v;?></option>
                                                           <?php }?> 
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>  
                                        </td>
                                    </tr>
                                    <tr class="sat">
                                        <td>星期六</td>
                                        <td>
                                            <div class="class-list">
                                                <div class="add-class-group"><i class="fa fa-plus"></i></div>
                                                <div class="class-group">
                                                    <div class="class-inline">
                                                        <label for="">由</label>
                                                        <select name="time[sat][from_hour]" class="from_hour class-control">
                                                            <option value=""></option>
                                                            <?php foreach($timelist as $k=>$v){?>
                                                            <option value="<?php echo $k;?>"><?php echo $v;?></option>
                                                           <?php }?> 
                                                        </select>
                                                    </div>
                                                    <div class="class-inline">
                                                        <label for="">至</label>
                                                        <select name="time[sat][to_hour]" class="to_hour class-control">
                                                            <option value=""></option>
                                                           <?php foreach($timelist as $k=>$v){?>
                                                            <option value="<?php echo $k;?>"><?php echo $v;?></option>
                                                           <?php }?> 
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>  
                                        </td>
                                    </tr>
                                    <tr class="sun">
                                        <td>星期日</td>
                                        <td>
                                            <div class="class-list">
                                                <div class="add-class-group"><i class="fa fa-plus"></i></div>
                                                <div class="class-group">
                                                    <div class="class-inline">
                                                        <label for="">由</label>
                                                        <select name="time[sun][from_hour]" class="from_hour class-control">
                                                            <option value=""></option>
                                                            <?php foreach($timelist as $k=>$v){?>
                                                            <option value="<?php echo $k;?>"><?php echo $v;?></option>
                                                           <?php }?> 
                                                        </select>
                                                    </div>
                                                    <div class="class-inline">
                                                        <label for="">至</label>
                                                        <select name="time[sun][to_hour]" class="to_hour class-control">
                                                            <option value=""></option>
                                                            <?php foreach($timelist as $k=>$v){?>
                                                            <option value="<?php echo $k;?>"><?php echo $v;?></option>
                                                           <?php }?> 
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>  
                                        </td>
                                    </tr> 
                                    </tbody> 
                                   </table> 
                                  </div>
                            </div>
                            <!--授課區域結束-->
                            <!--個人資料-->
                            <div class="tab-panel-item">
                                <form action="<?php echo APP_PATH;?>index.php?m=teachers&c=index&a=tutor_reg" id="personForm" method="post">
                                    <!--賬號註冊-->
                                    <div class="panel-list-item borb">
                                        <div class="tab-panel-title">賬號註冊</div>
                                        <div class="">
                                            <!-- <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4"><span class="req-color">*</span>用户名</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="text" class="form-control require" name="username">
                                                    <span class="error-tip"></span>
                                                </div>
                                            </div> -->
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4"><span class="req-color">*</span>手提電話</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="text" class="form-control require" name="usertel" oninput="checkusertel();">
                                                     <span class="error-tip"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4"><span class="req-color">*</span>登記電郵</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="text" class="form-control require" name="useremail" oninput="checkusermail();">
                                                    <span class="error-tip"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4"><span class="req-color">*</span>確認登記電郵</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="text" class="form-control require" name="useremailAgain" autocomplete="off">
                                                </div>
                                            </div>    
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">住宅電話</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="text" class="form-control" name="mobile">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4"><span class="req-color">*</span>登入密碼</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="password" class="form-control require" name="userpwd" placeholder="請輸入六位數以上密碼，密碼由字母+數字組成">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4"><span class="req-color">*</span>確認密碼</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="password" class="form-control require" name="userpwdAgain" placeholder="請再次確認您的密碼">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">推薦碼 / 推薦人手提號碼</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="text" class="form-control"  name="Referee" id="Referee">
                                                    <span class="error-tip"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--賬號註冊結束-->
                                    <!--個人資料-->
                                    <div class="panel-list-item borb">
                                        <div class="tab-panel-title">個人資料</div>
                                        <div class="">
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4"><span class="req-color">*</span>身份證上的英文名</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="text" class="form-control require" name="userIdEn">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4"><span class="req-color">*</span>身份證上的中文名</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="text" class="form-control require" name="userIdCh">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">居住地點</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <div class="radio-list">
                                                        <div class="radio-inline pl-0">
                                                        <span class="radio radio-primary ">
                                                            <input type="radio" name="userHome" value="0">
                                                        <label for="">住宅</label>
                                                        </span>
                                                        </div>
                                                        <div class="radio-inline">
                                                        <span class="radio radio-primary ">
                                                            <input type="radio" name="userHome" value="1">
                                                        <label for="">宿舍</label>
                                                        </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">宿舍地址(如有)</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="text" class="form-control" name="userDormitoryAddr">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4"><span class="req-color">*</span>固定住宅地址區域</label>
                                                <div class="col-md-7 col-sm-8">
                                                <?php if(defined('IN_ADMIN')  && !defined('HTML')) {echo "<div class=\"admin_piao\" pc_action=\"get\" data=\"op=get&tag_md5=df62c0388f3def7f3f55f1b3921a281a&sql=SELECT+loc_name%2Cloc_id+FROM+v9_sys_location+where+loc_level%3D2+order+by+loc_seq+asc--&return=location\"><a href=\"javascript:void(0)\" class=\"admin_piao_edit\">编辑</a>";}pc_base::load_sys_class("get_model", "model", 0);$get_db = new get_model();$r = $get_db->sql_query("SELECT loc_name,loc_id FROM v9_sys_location where loc_level=2 order by loc_seq asc-- LIMIT 20");while(($s = $get_db->fetch_next()) != false) {$a[] = $s;}$location = $a;unset($a);?>
                                                
                                                    <select class="form-control require" name="userHomeArea">
                                                        <option disabled="disabled" selected="selected"></option>
                                                        <?php $n=1;if(is_array($location)) foreach($location AS $loc) { ?>
                                                        <optgroup label="<?php echo $loc['loc_name'];?>">
                                                        <?php $loc_id=$loc[loc_id]?>
                                                        <?php if(defined('IN_ADMIN')  && !defined('HTML')) {echo "<div class=\"admin_piao\" pc_action=\"get\" data=\"op=get&tag_md5=584caef8fdf02d9221fe62be67ad7130&sql=SELECT+loc_name%2Cloc_id+FROM+v9_sys_location+where+loc_level%3D3+and+loc_parentid%3D%24loc_id+order+by+loc_seq+asc--&return=place\"><a href=\"javascript:void(0)\" class=\"admin_piao_edit\">编辑</a>";}pc_base::load_sys_class("get_model", "model", 0);$get_db = new get_model();$r = $get_db->sql_query("SELECT loc_name,loc_id FROM v9_sys_location where loc_level=3 and loc_parentid=$loc_id order by loc_seq asc-- LIMIT 20");while(($s = $get_db->fetch_next()) != false) {$a[] = $s;}$place = $a;unset($a);?>
                                                        <?php $n=1;if(is_array($place)) foreach($place AS $p) { ?>
                                                            <option value="<?php echo $p['loc_id'];?>"><?php echo $p['loc_name'];?></option>
                                                        <?php $n++;}unset($n); ?>
                                                        <?php if(defined('IN_ADMIN') && !defined('HTML')) {echo '</div>';}?>    
                                                        </optgroup>
                                                        <?php $n++;}unset($n); ?>
                                                    </select>   
                                                <?php if(defined('IN_ADMIN') && !defined('HTML')) {echo '</div>';}?> 
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4"><span class="req-color">*</span>固定住宅完整地址</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="text" class="form-control require" name="userHomeAddr">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4"><span class="req-color">*</span>暱稱或英文名</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="text" class="form-control" name="userNickName">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4"><span class="req-color">*</span>性别</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <div class="radio-list">
                                                        <div class="radio-inline pl-0">
                                                        <span class="radio radio-primary ">
                                                            <input type="radio" name="userSex" value="M">
                                                        <label for="">男</label>
                                                        </span>
                                                        </div>
                                                        <div class="radio-inline">
                                                        <span class="radio radio-primary ">
                                                            <input type="radio" name="userSex" value="F">
                                                        <label for="">女</label>
                                                        </span>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="userimage" class="userimage" value="">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4"></label>
                                                <div class="col-md-7 col-sm-8">
                                                    <div style="width: 20%;height: 10%;display:none;" class="Man">
                                                        <img src="<?php echo APP_PATH;?>statics/images/add_image/nantimg.jpg" width="100%" height="100%">
                                                    </div>
                                                    <div style="width: 20%;height: 10%;display:none;" class="Female">
                                                        <img src="<?php echo APP_PATH;?>statics/images/add_image/Nvtimg.jpg" width="100%" height="100%">
                                                    </div>
                                                </div>
                                            </div>
                                            <script>
                                                $('input[name="userSex"]').click(function(){
                                                    var gender = $('input[name="userSex"]:checked').val();
                                                    if(gender == 'M'){
                                                        $('.Man').show();
                                                        $('.userimage').val('/statics/images/add_image/nantimg.jpg');
                                                        $('.Female').hide();
                                                    }else{
                                                        $('.Female').show();
                                                        $('.userimage').val('/statics/images/add_image/Nvtimg.jpg');
                                                        $('.Man').hide();
                                                    }
                                                });
                                            </script>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4"><span class="req-color">*</span>出生年份</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <?php 
                                                    for ($i=date("Y")-17; $i >=date("Y")-97 ; $i--) {
                                                        $time[$i]=$i;      
                                                    }?>
                                                    <select  class="form-control" name="userBirth">
                                                        <option disabled="disabled" selected="selected"></option>
                                                        <?php foreach($time as $k=>$v){?>
                                                        <option value="<?php echo $k;?>"><?php echo $v;?></option>
                                                        <?php }?>
                                                    </select>
                                                </div>
                                            </div>
                                            <!-- <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4"><span class="req-color">*</span>身份證首4位</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="text" class="form-control" name="id_card">
                                                </div>
                                            </div> -->
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">國籍</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="text" class="form-control" name="nation">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">宗教信仰</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="text" class="form-control" name="believe">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4"><span class="req-color">*</span>主要語言</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <select class="form-control" name="userMainLan">
                                                        <option disabled="disabled" selected="selected"></option>
                                                        <option value="廣東話">廣東話</option>
                                                        <option value="普通話">普通話</option>
                                                        <option value="英語">英語</option>
                                                        <option value="其他語言">其他語言</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <!-- <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">宗教信仰</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div> -->
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4"><span class="req-color">*</span>每小時學費</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <div class="" data-info="每小時學費">
                                                        <input type="text" class="form-control price-input" name="tutor_class_price">
                                                       <!--  <span>至</span>
                                                        <input type="text" class="form-control price-input" name="tutor_class_price_to"> -->
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">每次上课時間</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <select name="tst_class_hour" id="coaching_time" class="form-control">
                                                        <option value="">請選擇</option>
                                                        <option value="0.75">45分鐘</option>
                                                        <option value="1">1小時</option>
                                                        <option value="1.5">1.5小時</option>
                                                        <option value="2">2小時</option>
                                                        <option value="2.5">2.5小時</option>
                                                        <option value="3">3小時</option>
                                                        <option value="3.5">3.5小時</option>
                                                        <option value="4">4小時</option>
                                                        <option value="4.5">4.5小時</option>
                                                        <option value="5">5小時</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">性罪行犯罪記録</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <div class="radio-list">
                                                        <div class="radio-inline pl-0">
                                                        <span class="radio radio-primary ">
                                                            <input type="radio" name="sexOffenses" value="0">
                                                        <label for="">沒有</label>
                                                        </span>
                                                        </div>
                                                        <div class="radio-inline">
                                                        <span class="radio radio-primary ">
                                                            <input type="radio" name="sexOffenses" value="1">
                                                        <label for="">有</label>
                                                        </span>
                                                        </div>
                                                        <span class='error-sex'>【我們會將你現時的 IP 地址 <?php echo $ip = $_SERVER["REMOTE_ADDR"];?>記錄下來】</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--個人資料結束-->
                                    <!--學歷履歷資料-->
                                    <div class="panel-list-item borb">
                                        <div class="tab-panel-title">學歷履歷資料</div>
                                        <div class="">
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4"><span class="req-color">*</span>最高教育程度</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <?php if(defined('IN_ADMIN')  && !defined('HTML')) {echo "<div class=\"admin_piao\" pc_action=\"get\" data=\"op=get&tag_md5=6064da4edc2ed8108672da670fcd12ae&sql=SELECT+%2A+FROM+v9_grade+order+by+grade_id+asc&return=data_grade\"><a href=\"javascript:void(0)\" class=\"admin_piao_edit\">编辑</a>";}pc_base::load_sys_class("get_model", "model", 0);$get_db = new get_model();$r = $get_db->sql_query("SELECT * FROM v9_grade order by grade_id asc LIMIT 20");while(($s = $get_db->fetch_next()) != false) {$a[] = $s;}$data_grade = $a;unset($a);?>
                                                    <select class="form-control" name="userHighEd">
                                                        <option disabled="disabled" selected="selected"></option>
                                                        <?php $n=1;if(is_array($data_grade)) foreach($data_grade AS $r_grade) { ?>
                                                        <option value="<?php echo $r_grade['grade_id'];?>"><?php echo $r_grade['grade_name'];?></option>
                                                        <?php $n++;}unset($n); ?>
                                                    </select>
                                                    <?php if(defined('IN_ADMIN') && !defined('HTML')) {echo '</div>';}?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">就讀小學</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="text" class="form-control" name="tutor_hschool1">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4"><span class="req-color">*</span>就讀中學</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="text" class="form-control" name="tutor_hschool2">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">中學主要教學語言</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <select class="form-control" name="tutor_hschool_lang">
                                                        <option disabled="disabled" selected="selected"></option>
                                                        <option value="廣東話">廣東話</option>
                                                        <option value="普通話">普通話</option>
                                                        <option value="英語">英語</option>
                                                        <option value="其他語言">其他語言</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">就讀大學</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <?php if(defined('IN_ADMIN')  && !defined('HTML')) {echo "<div class=\"admin_piao\" pc_action=\"get\" data=\"op=get&tag_md5=959e775cf08b38580334b2907f4aaaaa&sql=SELECT+%2A+FROM+v9_collage+order+by+id+desc&return=data_collage\"><a href=\"javascript:void(0)\" class=\"admin_piao_edit\">编辑</a>";}pc_base::load_sys_class("get_model", "model", 0);$get_db = new get_model();$r = $get_db->sql_query("SELECT * FROM v9_collage order by id desc LIMIT 20");while(($s = $get_db->fetch_next()) != false) {$a[] = $s;}$data_collage = $a;unset($a);?>
                                                    <select class="form-control" name="tutor_college">
                                                        <option disabled="disabled" selected="selected"></option>
                                                        <?php $n=1;if(is_array($data_collage)) foreach($data_collage AS $r_collage) { ?>
                                                        <option value="<?php echo $r_collage['id'];?>"><?php echo $r_collage['c_name'];?></option>
                                                        <?php $n++;}unset($n); ?>
                                                    </select>
                                                    <?php if(defined('IN_ADMIN') && !defined('HTML')) {echo '</div>';}?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">其他大學</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="text" class="form-control" name="tutor_college_other">
                                                </div>
                                            </div>
                                            <!-- <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">高中修讀科目類別</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <select class="form-control" name="tutor_hmainclass">
                                                        <option disabled="disabled" selected="selected"></option>
                                                        <option value="文科">文科</option>
                                                        <option value="理科">理科</option>
                                                        <option value="商科">商科</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">大學主修科目</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="text" class="form-control" name="tutor_cmainclass">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">其他修讀課程</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="text" class="form-control" name="tutor_otherclass">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">其他專業應可課程</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="text" class="form-control" name="tutor_other_major">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">現時就讀年級</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="text" class="form-control" name="tutor_now_edcuation">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">香港中學會考分數(六科成績總分)</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="text" class="form-control" name="tutor_hkcee_score">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">香港中學會考主要應考主要語言</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="text" class="form-control" name="tutor_hkcee_lang">
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>
                                    <!--香港中學會考成績（中五）-->
                                    <?php if(defined('IN_ADMIN')  && !defined('HTML')) {echo "<div class=\"admin_piao\" pc_action=\"get\" data=\"op=get&tag_md5=31943b2ce2cf6ff51debc6f74544c339&sql=SELECT+%2A+FROM+v9_sys_tutor_achieve+where+ta_level%3D1+order+by+ta_seq+asc--&return=levels\"><a href=\"javascript:void(0)\" class=\"admin_piao_edit\">编辑</a>";}pc_base::load_sys_class("get_model", "model", 0);$get_db = new get_model();$r = $get_db->sql_query("SELECT * FROM v9_sys_tutor_achieve where ta_level=1 order by ta_seq asc-- LIMIT 20");while(($s = $get_db->fetch_next()) != false) {$a[] = $s;}$levels = $a;unset($a);?>
                                    <?php $n=1;if(is_array($levels)) foreach($levels AS $level) { ?>
                                        
                                    <div class="panel-list-item">
                                        <div class="panel-bread">
                                            <div class="panel-bread-title">
                                            <?php echo $level['ta_name'];?></div><button class="Extend">顯示</button>
                                            <?php if($level[ta_id] == 1 || $level[ta_id] == 2){?>
                                            <div class="panel-bread-content">
                                                <?php $id=$level[ta_id]?>
                                                <?php if(defined('IN_ADMIN')  && !defined('HTML')) {echo "<div class=\"admin_piao\" pc_action=\"get\" data=\"op=get&tag_md5=ddd189992a5c3d77105734c8d02bd0f1&sql=SELECT+%2A+FROM+v9_sys_tutor_achieve+where+ta_level%3D2+and+ta_parentid%3D%24id+order+by+ta_seq+asc+limit+0%2C4--&return=one_levels\"><a href=\"javascript:void(0)\" class=\"admin_piao_edit\">编辑</a>";}pc_base::load_sys_class("get_model", "model", 0);$get_db = new get_model();$r = $get_db->sql_query("SELECT * FROM v9_sys_tutor_achieve where ta_level=2 and ta_parentid=$id order by ta_seq asc limit 0,4-- LIMIT 20");while(($s = $get_db->fetch_next()) != false) {$a[] = $s;}$one_levels = $a;unset($a);?>
                                                 <?php $n=1;if(is_array($one_levels)) foreach($one_levels AS $one_levels) { ?>
                                                <div class="panel-bread-item">
                                                    <div class="bread-item-title"><?php echo $one_levels['ta_name'];?></div>
                                                    <?php $one_levelid=$one_levels[ta_id]?>
                                                     <?php if(defined('IN_ADMIN')  && !defined('HTML')) {echo "<div class=\"admin_piao\" pc_action=\"get\" data=\"op=get&tag_md5=9513d35f993cd71a5a66744fc8bcb87f&sql=SELECT+%2A+FROM+v9_sys_tutor_achieve+where+ta_level%3D3+and+ta_parentid%3D%24one_levelid+order+by+ta_seq+asc--&return=two_levels\"><a href=\"javascript:void(0)\" class=\"admin_piao_edit\">编辑</a>";}pc_base::load_sys_class("get_model", "model", 0);$get_db = new get_model();$r = $get_db->sql_query("SELECT * FROM v9_sys_tutor_achieve where ta_level=3 and ta_parentid=$one_levelid order by ta_seq asc-- LIMIT 20");while(($s = $get_db->fetch_next()) != false) {$a[] = $s;}$two_levels = $a;unset($a);?>
                                                 
                                                    <ul class="bread-item-list">
                                                        <?php $n=1;if(is_array($two_levels)) foreach($two_levels AS $two_levels) { ?>
                                                        <li>
                                                            <div class="bread-left"><span><?php echo $two_levels['ta_name'];?></span></div>
                                                            <div class="bread-right">
                                                                <select class="scores" data-info="<?php echo $two_levels['ta_id'];?>">
                                                                    <option value=""></option>
                                                                    <?php $type=explode(",",$level[ta_score_type]);?>
                                                                    <?php foreach ($type as $v) {?>
                                                                    <option value="<?php print $v;?>"><?php print $v;?></option>
                                                                    <?php }?>   
                                                                </select>
                                                            </div>
                                                        </li>
                                                        <?php $n++;}unset($n); ?>
                                                    </ul>
                                                    <?php if(defined('IN_ADMIN') && !defined('HTML')) {echo '</div>';}?>
                                                </div>
                                                <?php $n++;}unset($n); ?>
                                                <?php if(defined('IN_ADMIN') && !defined('HTML')) {echo '</div>';}?>
                                            </div>
                                            <?php }else{?>
                                                <div class="panel-bread-grid">
                                                    <?php $id=$level[ta_id]?>
                                                    <?php if(defined('IN_ADMIN')  && !defined('HTML')) {echo "<div class=\"admin_piao\" pc_action=\"get\" data=\"op=get&tag_md5=f091883f4812b125c9eaa61a0b836db6&sql=SELECT+%2A+FROM+v9_sys_tutor_achieve+where+ta_level%3D2+and+ta_parentid%3D%24id+order+by+ta_seq+asc--&return=one_level\"><a href=\"javascript:void(0)\" class=\"admin_piao_edit\">编辑</a>";}pc_base::load_sys_class("get_model", "model", 0);$get_db = new get_model();$r = $get_db->sql_query("SELECT * FROM v9_sys_tutor_achieve where ta_level=2 and ta_parentid=$id order by ta_seq asc-- LIMIT 20");while(($s = $get_db->fetch_next()) != false) {$a[] = $s;}$one_level = $a;unset($a);?>
                                                     <?php $n=1;if(is_array($one_level)) foreach($one_level AS $one_levels) { ?>
                                                    <div class="bread-grid-list">
                                                        <div class="bread-grid-title"><?php echo $one_levels['ta_name'];?></div>
                                                        <?php $one_levelid=$one_levels[ta_id]?>
                                                         <?php if(defined('IN_ADMIN')  && !defined('HTML')) {echo "<div class=\"admin_piao\" pc_action=\"get\" data=\"op=get&tag_md5=a5607bed72a9d9862d407afe2163a649&sql=SELECT+%2A+FROM+v9_sys_tutor_achieve+where+ta_level%3D3+and+ta_parentid%3D%24one_levelid+order+by+ta_seq+asc--&return=two_level\"><a href=\"javascript:void(0)\" class=\"admin_piao_edit\">编辑</a>";}pc_base::load_sys_class("get_model", "model", 0);$get_db = new get_model();$r = $get_db->sql_query("SELECT * FROM v9_sys_tutor_achieve where ta_level=3 and ta_parentid=$one_levelid order by ta_seq asc-- LIMIT 20");while(($s = $get_db->fetch_next()) != false) {$a[] = $s;}$two_level = $a;unset($a);?>

                                                        <?php
                                                            //《添加》数组重构部分 By Ming
                                                            $linenum  = ceil(count($two_level)/4);//行数
                                                            $remainder  = count($two_level)%4;//余数
                                                            $new_two_level = array();
                                                            for($i = 1;$i <= $linenum;$i++){//循环每一行
                                                                $start = ($i-1)*4;//每一行第一个键值
                                                                $newlevel = array_slice($two_level,$start,4);//每一行的数据
                                                                foreach($newlevel as $key => $value){
                                                                    $new_two_level[$i][] = $value;
                                                                }
                                                            }
                                                        ?>
                                                        
                                                        <?php $n=1; if(is_array($new_two_level)) foreach($new_two_level AS $two_key => $news) { ?>
                                                            <div class="bread-grid-content">
                                                                <?php $n=1;if(is_array($news)) foreach($news AS $new) { ?>
                                                                    <div class="bread-grid-item">
                                                                        <div class="grid-item-list">
                                                                            <div class="bread-grid-left"><?php echo $new['ta_name'];?></div>
                                                                            <div class="bread-grid-right">
                                                                                <select class="scores" data-info="<?php echo $new['ta_id'];?>">
                                                                                    <option value=""></option>
                                                                                     <?php $type=explode(",",$level[ta_score_type]);?>
                                                                    <?php foreach ($type as $v) {?>
                                                                    <option value="<?php print $v;?>"><?php print $v;?></option>
                                                                    <?php }?> 
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php $n++;}unset($n); ?>
                                                                <?php if($remainder!=0 && $two_key==$linenum){?>
                                                                    <?php for($y=1;$y<=(4-$remainder);$y++){?>
                                                                        <?php echo '<div class="bread-grid-item"><div class="grid-item-list"><div class="bread-grid-left"></div><div class="bread-grid-right"></div></div></div>';?>
                                                                    <?php }?>
                                                                <?php }?>
                                                            </div>
                                                        <?php $n++;}unset($n); ?>
                                                         <?php if(defined('IN_ADMIN') && !defined('HTML')) {echo '</div>';}?>
                                                    </div>
                                                    <?php $n++;}unset($n); ?>
                                                    <?php if(defined('IN_ADMIN') && !defined('HTML')) {echo '</div>';}?>
                                                </div>
                                            <?php }?>
                                        </div>
                                    </div> 
                                    <?php $n++;}unset($n); ?>
                                    <?php if(defined('IN_ADMIN') && !defined('HTML')) {echo '</div>';}?>
                                    <!--職業履歷資料-->
                                    <div class="panel-list-item">
                                        <div class="tab-panel-title">職業履歷資料</div>
                                        <div class="">
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4"><span class="req-color">*</span>現時職業</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <select  class="form-control require" name="userCurOct">
                                                        <option disabled="disabled" selected="selected"></option>
                                                        <option value="學生">學生</option>
                                                        <option value="現職小學教師">現職小學教師</option>
                                                        <option value="現職中學教師">現職中學教師</option>
                                                        <option value="全職補習導師">全職補習導師</option>
                                                        <option value="幼稚園導師">幼稚園導師</option>
                                                        <option value="外國回流導師">外國回流導師</option>
                                                        <option value="退休老師">退休老師</option>
                                                        <option value="現職大學教授">現職大學教授</option>
                                                        <option value="教育機構老師">教育機構老師</option>
                                                        <option value="其他職業">其他職業</option>
                                                        <option value="無業">無業</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4"><span class="req-color">*</span>工作經驗</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <select  class="form-control require" name="userWorkExp">
                                                        <option disabled="disabled" selected="selected"></option>
                                                        <option value="一年">一年</option>
                                                        <option value="二年">二年</option>
                                                        <option value="三年">三年</option>
                                                        <option value="四年">四年</option>
                                                        <option value="五年或以上">五年或以上</option>
                                                        <option value="只做過暑假工">只做過暑假工</option>
                                                        <option value="無工作經驗">無工作經驗</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">過去工作資料(1)</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <textarea cols="30" rows="2" class="form-control" name="tutor_working1"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">過去工作資料(2)</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <textarea cols="30" rows="2" class="form-control" name="tutor_working2"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4"><span class="req-color">*</span>最高可補年級</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <?php if(defined('IN_ADMIN')  && !defined('HTML')) {echo "<div class=\"admin_piao\" pc_action=\"get\" data=\"op=get&tag_md5=b1184bd456120723ffb810ef7f9a2c05&sql=SELECT+st_id%2Cst_name+FROM+v9_student_type+where+st_ttid%3D19+order+by+st_order&return=student_type\"><a href=\"javascript:void(0)\" class=\"admin_piao_edit\">编辑</a>";}pc_base::load_sys_class("get_model", "model", 0);$get_db = new get_model();$r = $get_db->sql_query("SELECT st_id,st_name FROM v9_student_type where st_ttid=19 order by st_order LIMIT 20");while(($s = $get_db->fetch_next()) != false) {$a[] = $s;}$student_type = $a;unset($a);?>
                                                    <select  class="form-control require" name="userTutorClass">

                                                        <option disabled="disabled" selected="selected"></option>
                                                        <?php $n=1;if(is_array($student_type)) foreach($student_type AS $stut) { ?>
                                                        <option value="<?php echo $stut['st_id'];?>"><?php echo $stut['st_name'];?></option>
                                                        <?php $n++;}unset($n); ?>
                                                    </select>
                                                    <?php if(defined('IN_ADMIN') && !defined('HTML')) {echo '</div>';}?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4"><span class="req-color">*</span>補習經驗及年資</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <select  class="form-control require" name="userTutorExp">
                                                        <option disabled="disabled" selected="selected"></option>
                                                       <option value="一年">一年</option>
                                                        <option value="二年">二年</option>
                                                        <option value="三年">三年</option>
                                                        <option value="四年">四年</option>
                                                        <option value="五年或以上">五年或以上</option>
                                                        <option value="只做過暑假工">只做過暑假工</option>
                                                        <option value="無工作經驗">無工作經驗</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4"><span class="req-color">*</span>教育機構經驗</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <select  class="form-control require" name="userEdiExp">
                                                        <option disabled="disabled" selected="selected"></option>
                                                        <option value="一年">一年</option>
                                                        <option value="二年">二年</option>
                                                        <option value="三年">三年</option>
                                                        <option value="四年">四年</option>
                                                        <option value="五年或以上">五年或以上</option>
                                                        <option value="只做過暑假工">只做過暑假工</option>
                                                        <option value="無工作經驗">無工作經驗</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">如何得知本公司？</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <select name="know_way" id="know_way" class="form-control" >
                                                        <option value="">請選擇</option>
                                                        <option value="1">朋友介紹</option>
                                                        <option value="2">宣傳廣告</option>
                                                        <option value="3">Facebook</option>
                                                        <option value="4">Google</option>
                                                        <option value="5">百度</option>
                                                        <option value="6">優惠劵</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--職業履歷資料結束-->
                                </form>

                            </div>
                            <!--個人資料結束-->
                            <!--音樂及其他技能-->
                            <div class="tab-panel-item">
                                <form action="<?php echo APP_PATH;?>index.php?m=teachers&c=index&a=tutor_reg" id="otherSkiiForm" method="post">
                                    <!--其他學術及語言技能-->
                                    <div class="panel-list-item">
                                        <div class="tab-panel-title">其他技能</div>
                                        <div class="">
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4"><span class="req-color">*</span>課程方面，可否提供筆記</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <select class="form-control require" name="userProNote">
                                                        <option disabled="disabled" selected="selected"></option>
                                                        <option value="0">不可以</option>
                                                        <option value="1">可以</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <!-- <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4"><span class="req-color">*</span>可否教授英文拼音</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <select class="form-control require" name="userTchEn">
                                                        <option disabled="disabled" selected="selected"></option>
                                                        <option value="0">不可以</option>
                                                        <option value="1">可以</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4"><span class="req-color">*</span>可否教授普通話</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <select class="form-control require" name="userTchPt">
                                                        <option disabled="disabled" selected="selected"></option>
                                                        <option value="0">不可以</option>
                                                        <option value="1">可以</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">可否教授日本語 / 日文</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <select  id="" class="form-control">
                                                        <option disabled="disabled" selected="selected"></option>
                                                        <option value="不可以">不可以</option>
                                                        <option value="可以">可以</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">可否教授法語 / 法文</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <select  id="" class="form-control">
                                                        <option disabled="disabled" selected="selected"></option>
                                                        <option value="不可以">不可以</option>
                                                        <option value="可以">可以</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">可否教授意大利語 / 意大利文</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <select  id="" class="form-control">
                                                        <option disabled="disabled" selected="selected"></option>
                                                        <option value="不可以">不可以</option>
                                                        <option value="可以">可以</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">可否教授德語 / 德文</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <select  id="" class="form-control">
                                                        <option disabled="disabled" selected="selected"></option>
                                                        <option value="不可以">不可以</option>
                                                        <option value="可以">可以</option>
                                                    </select>
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>
                                    <!--其他學術及語言技能結束-->
                                    <!--音樂技能-->
                                    <!-- <div class="panel-list-item">
                                        <div class="tab-panel-title">音樂及其他技能</div>
                                        <div class="">
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4"><span class="req-color">*</span>可教授之樂器</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <select  id="" class="form-control require" name="userTchMui">
                                                        <option disabled="disabled" selected="selected"></option>
                                                        <option disabled="disabled" selected="selected"></option>
                                                        <option value="大提琴">大提琴</option>
                                                        <option value="小提琴">小提琴</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">可教授樂器等級</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <select  id="" class="form-control">
                                                        <option disabled="disabled" selected="selected"></option>
                                                        <option disabled="disabled" selected="selected"></option>
                                                        <option value="五級">五級</option>
                                                        <option value="四級">四級</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">可教授之樂器之考取年份</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">其他樂器、級別及考取年份（如有）</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">已考獲資格(學院名稱)</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">已考樂理級別</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <select  id="" class="form-control">
                                                        <option disabled="disabled" selected="selected"></option>
                                                        <option value="0">五級</option>
                                                        <option value="1">四級</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">可教授之電腦程式/軟件</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">可教授之游泳技能/詠式</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label text-right col-sm-4">曾參與之課外活動或校內外組織</label>
                                                <div class="col-md-7 col-sm-8">
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->
                                    <!--音樂技能結束-->
                                </form>

                            </div>
                            <!--音樂及其他技能結束-->
                            <!--自我介紹-->
                            <div class="tab-panel-item">
                                <div class="tab-panel-title">自我介紹<span style="color: #555;">【</span><span style="color: red;">* 請勿填寫電話、郵箱等聯繫方式</span><span style="color: #555;">】</span></div>
                                <p>請用50字以上介紹自己</p>
                                <div class="produce-box">
                                    <textarea cols="30" rows="10" class="form-control pr-text" name="pr-text"></textarea>
                                    <p class="error-area"></p>
                                </div>
                            </div>
                            <!--自我介紹結束-->
                            <!--完成-->
                            <div class="tab-panel-item">
                                <div class="reg-finish">
                                    <div class="finish-title">
                                        最適合你的學生
                                    </div>
                                    <div class="row student"></div>

                                    <div class="finish-dec">
                                       
                                    </div>
                                    
                                    <div class="show_index" style="text-align: center;">
                                        <a style="width:150px;background-color: #b7b755;padding: 4px 20px;" href="<?php echo APP_PATH;?>">回到首頁</a>
                                   </div>
                                </div>
                            </div>
                            <!--完成結束-->
                        </div>

                        <!-- 短信验证码部分Start -->
                            <div class="form-group msg_code" style="display:none;">
                                <label for="" class="control-label text-right col-sm-4"><span class="main-color">*</span>短信驗證碼：</label>
                                <div class="col-md-3 col-sm-8">
                                    <input type="text" class="form-control" placeholder="短信驗證碼" data-info="短信驗證碼" data-type="msg_code" name="msg_code" style="width:55%;float: left;">
                                    <input type="button" value="發送驗證碼" onclick="send_msg_code();" style="height: 33px;width: 45%;" class="send_code">
                                </div>
                                
                            </div>
                        <!-- 短信验证码部分End -->

                        <!--步驟-->
                        <div class="step-box">
                            <div class="step-btn">
                                <a href="javascript:; " class="step-prev">上一步</a>
                                <a href="javascript:; " class="step-next">下一步</a>
                            </div>
                            <p class="betip">如在過程中有任何疑問，請<a href="<?php echo APP_PATH;?>index.php?m=content&c=index&a=lists&catid=8" target="_blank">聯繫我們</a>並嘗試描述問題的詳情。</p>
                        </div>
                        <script>
                            //发送短信验证码
                            function send_msg_code(){
                                var phone = $('input[name="usertel"]').val();
                                if(phone == ''){
                                    $('input[name="usertel"]').focus();return false;
                                }
                                $.post("<?php echo APP_PATH;?>index.php?m=tparents&c=index&a=send_msg_code",{phone:phone},function(data){
                                    if(data.error_code != '2'){
                                        alert(data.msg);
                                    }else{
                                        $('.send_code').attr("disabled",true);
                                        countdown(60);//短信发送成功倒计时
                                    }
                                },'json');
                            }

                            //发送按钮倒计时
                            function countdown(v){
                             if(v>0){
                                $('.send_code').val((--v)+'秒後重發');
                                    setTimeout(function(){
                                        countdown(v);
                                    },1000);
                                }
                                else{
                                 $('.send_code').val('重發驗證碼');
                                 $('.send_code').attr("disabled",false);
                                }
                            }
                        </script>
                        <!--步驟結束-->
                    </div>
                </div>
            </div>
        </div>
<?php include template("content","footer_new"); ?>

        <!--回到顶部-->
        <a href="javascript: ;" id="back-to-top">
            <i class="fa fa-chevron-up"></i>
        </a>

    </div>
    <script src="<?php echo JS_PATH;?>add/jquery-1.11.2.min.js"></script>
    <script src="<?php echo JS_PATH;?>add/base.js"></script>
    <script src="<?php echo JS_PATH;?>add/bootstrapValidator.min.js"></script>
    <script src="<?php echo JS_PATH;?>add/zh_CN.js"></script>
    <script language='javascript' src='<?php echo JS_PATH;?>add/newPopup.js' charset="UTF-8"></script> 
    <script>
        if(parseInt($(window).width()) > 768 && parseInt($(window).width()) < 1024){    //ipad版
            <?php if(isset($ip_src)){?>
                window.onload=ShowHtmlString('<?php echo $ip_src?>');
            <?php }?>
        }else if(parseInt($(window).width()) < 768){        //手機版
            <?php if(isset($ph_src)){?>
                window.onload=ShowHtmlString('<?php echo $ph_src?>');
            <?php }?>
        }else{          //電腦版
            <?php if(isset($com_src)){?>
                window.onload=ShowHtmlString('<?php echo $com_src?>');
            <?php }?>
        }
        // 进入页面显示弹窗
        function ShowHtmlString(src) //显示html 
        { 
            var strHtml = "<img src='"+src+"'>"; 
            var pop=new Popup({ contentType:2,isReloadOnClose:false,isHaveTitle:false,width:350,height:300}); 
            pop.setContent("contentHtml",strHtml); 
            pop.setContent("title","字符串示例html"); 
            pop.build(); 
            pop.show(); 
        }



        $(function(){
            var telstatus;
            var tuijian;
            var emailstatus;
        })
        //最佳上課時間的添加
        $('.add-class-group').click(function () {
            var str = '<div class="class-group"><div class="del-class-group"><i class="fa fa-minus"></i></div></div>';
            var text=$(".class-group").html();
            $(this).parent().append(str);
            $(this).parent().children("div:last-child").append(text);
        });
        $('.class-list').on('click','.del-class-group',function () {
            $(this).parent().remove();
        })

        //教授类别，项目，级别三级联动
        $(".TchCate").change(function(){
            var v = $(".TchCate").val();
            var index=$(".TchCate").index(this);
            var th = $(this);
            $.post('./index.php?m=teachers&c=index&a=TchItem_change',{tt_id:v},function(data){
                                // $(".TchLevel").html(' ');
                var ThisSelectObj = th.parent().parent().next().children().eq(index+1).find('select');
                var nextselectobj=th.parent().parent().next().next().children().eq(index+1).find('select');
                ThisSelectObj.html("");
                nextselectobj.html("");
                var text=" <option disabled='disabled' selected='selected'></option>";
                ThisSelectObj.append(text);
                nextselectobj.append(text);
                if(data.length!==0){
                    for(i in data){
                    var course_option="<option value='"+data[i].tt_id+"'>"+data[i].tt_name+"</option>" ;   
                    ThisSelectObj.append(course_option);
                    }
                    
                }
            },'json');
        return false;
        });

        $(".TchItem").change(function(){
            var v = $(".TchItem").val();
            var index=$(".TchItem").index(this);
            var th = $(this);
            $.post('./index.php?m=tparents&c=index&a=stu_reg',{tt_id:v},function(data){
                                // $(".TchLevel").html(' ');
                 var ThisSelectObj = th.parent().parent().next().children().eq(index+1).find('select');
                ThisSelectObj.html(' ');
                var text=" <option disabled='disabled' selected='selected'></option>";
                // $(".TchLevel").eq(index).append(text);
                ThisSelectObj.append(text);
                if(data.length!==0){
                    for(i in data){
                    var course_option="<option value='"+data[i].tt_id+"'>"+data[i].tt_name+"</option>" ;   
                    ThisSelectObj.append(course_option);
                    }
                    
                }
            },'json');
        return false;
        });
        //验证邮箱是否已注册
    function checkusermail(){
        var usermail = $('input[name="useremail"]').val();
        $.post("<?php echo APP_PATH;?>index.php?m=teachers&c=index&a=ajax_checkusermail",{usermail:usermail},function(data){
            if(data == '1'){
               $('input[name="useremail"]').parent().find(".error-tip").html("");
               emailstatus=true;
            }else if(data=="0"){
                $('input[name="useremail"]').parent().find(".error-tip").html("郵箱已註冊，請重新填寫");
                emailstatus=false;
            }
        });
    }
    //验证手机号是否已注册
    function checkusertel(){
        var usertel = $('input[name="usertel"]').val();
        $.post("<?php echo APP_PATH;?>index.php?m=teachers&c=index&a=ajax_checkusertel",{usertel:usertel},function(data){
            if(data == '1'){
               $('input[name="usertel"]').parent().find(".error-tip").html("");
               telstatus = true;

            }else if(data=="0"){
                $('input[name="usertel"]').parent().find(".error-tip").html("手機號已註冊，請重新填寫");
                telstatus = false;
            }
        });
    }
    //驗證推薦人是否存在
    function checkReferee(){
       var  Referee= $('input[name="Referee"]').val();
       tuijian = true;
       if(Referee!=""){
        $.post("<?php echo APP_PATH;?>index.php?m=teachers&c=index&a=ajax_checkReferee",{Referee:Referee},function(data){
            if(data == '1'){
               $('input[name="Referee"]').parent().find(".error-tip").html("");
               tuijian = true;
            }else if(data=='0'){
                $('input[name="Referee"]').parent().find(".error-tip").html("此推薦人不存在，請重新填寫");
                tuijian = false;
            }
        }); 
        }else{
           $('input[name="Referee"]').parent().find(".error-tip").html(""); 
        }
    }
        //個人資料表单初始配置
        $('#personForm').bootstrapValidator({
            message: '這個欄位必須填寫。',
            fields: {
                useremail: {
                    validators: {
                        notEmpty: {
                            message: '請填寫您的郵箱。'
                        },
                        regexp: {
                            regexp: /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/,
                            message: '郵箱格式不正確。'
                        },
                        identical: {
                            field: 'useremailAgain',
                            message: '兩次填寫的郵箱地址不一樣'
                        }
                    }
                },
                useremailAgain: {
                    validators: {
                        notEmpty: {
                            message: '請確認您的郵箱地址。'
                        },
                        identical: {
                            field: 'useremail',
                            message: '兩次填寫的郵箱地址不一樣'
                        }
                    }
                },
                usertel: {
                    validators: {
                        notEmpty: {
                            message: '請填寫正確的手機電話。'
                        },
                        regexp: {
                            regexp: /^(\d{8}|\d{11})$/,
                            // regexp: /((\d{11})|^((\d{7,8})|(\d{4}|\d{3})-(\d{7,8})|(\d{4}|\d{3})-(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1})|(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1}))$)/,
                            message: '手機電話格式不正確。'
                        }
                    }
                },
                userpwd: {
                    validators: {
                        notEmpty: {
                            message: '請填寫密碼。'
                        },
                        regexp: {
                            regexp: /^(?![0-9]+$)(?![a-zA-Z]+$)(?!([^(0-9a-zA-Z)]|[\(\)])+$)([^(0-9a-zA-Z)]|[\(\)]|[a-zA-Z]|[0-9]){6,}$/,
                            message: '密碼格式不正確。'
                        },
                        identical: {
                            field: 'userpwdAgain',
                            message: '兩次密碼填寫不一樣。'
                        }
                    }
                },
                userpwdAgain: {
                    validators: {
                        notEmpty: {
                            message: '請確認填寫的密碼。'
                        },
                        identical: {
                            field: 'userpwd',
                            message: '兩次密碼填寫不一樣。'
                        }
                    }
                },
                userIdEn: {
                    validators: {
                        notEmpty: {
                            message: '請填寫身份證上的英文名。'
                        }
                    }
                },
                userIdCh: {
                    validators: {
                        notEmpty: {
                            message: '請填寫身份證上的中文名。'
                        }
                    }
                },
                // userHomeAddr: {
                //     validators: {
                //         notEmpty: {
                //             message: '請填寫固定住宅完整地址。'
                //         }
                //     }
                // },
                userHomeArea: {
                    validators: {
                        notEmpty: {
                            message: '請填寫固定住宅地址區域。'
                        }
                    }
                },
                userNickName: {
                    validators: {
                        notEmpty: {
                            message: '請填寫暱稱或英文名。'
                        }
                    }
                },
                tutor_class_price:{
                    validators: {
                        notEmpty: {
                            message: '請填寫每小時學費。'
                        }
                    }
                },
                tutor_hschool2:{
                    validators: {
                        notEmpty: {
                            message: '請填寫就讀中學。'
                        }
                    }
                },
                userSex: {
                    validators: {
                        notEmpty: {
                            message: '請選擇性別。'
                        }
                    }
                },
                userBirth: {
                    validators: {
                        notEmpty: {
                            message: '請選擇出生年份。'
                        }
                    }
                },
                // id_card: {
                //     validators: {
                //         notEmpty: {
                //             message: '請填寫身份證首4位。'
                //         },
                //         regexp: {
                //             regexp: /^\w{4}$/,
                //             message: '請填寫4位數字或字母'
                //         }
                //     } 
                // },
                userMainLan: {
                    validators: {
                        notEmpty: {
                            message: '請選擇主要語言。'
                        }
                    }
                },
                tutorPrice: {
                    validators: {
                        notEmpty: {
                            message: '請填寫每堂價錢範圍。'
                        },
                        regexp: {
                            regexp: /\d/,
                            message: '請填寫數字。'
                        }
                    }
                },
                userHighEd: {
                    validators: {
                        notEmpty: {
                            message: '請填寫最高教育程度。'
                        }
                    }
                },
                userCurOct: {
                    validators: {
                        notEmpty: {
                            message: '請選擇現時職業。'
                        }
                    }
                },
                userWorkExp: {
                    validators: {
                        notEmpty: {
                            message: '請選擇工作經驗。'
                        }
                    }
                },
                userTutorClass: {
                    validators: {
                        notEmpty: {
                            message: '請選擇最高可補年級。'
                        }
                    }
                },
                userTutorExp: {
                    validators: {
                        notEmpty: {
                            message: '請選擇補習經驗及年資。'
                        }
                    }
                },
                userEdiExp: {
                    validators: {
                        notEmpty: {
                            message: '請選擇教育機構經驗。'
                        }
                    }
                }
            }
        });
        //其他技能表单初始配置
/*         $('#otherSkiiForm').bootstrapValidator({
            message: '這個欄位必須填寫。',
            fields: {
                userProNote: {
                    validators: {
                        notEmpty: {
                            message: '請選擇是否可以提供筆記。'
                        }
                    }
                },
                // userTchEn: {
                //     validators: {
                //         notEmpty: {
                //             message: '請選擇是否可以教授英文拼音。'
                //         }
                //     }
                // },
                // userTchPt: {
                //     validators: {
                //         notEmpty: {
                //             message: '請選擇是否可以教授普通話。'
                //         }
                //     }
                // },
                userTchMui: {
                    validators: {
                        notEmpty: {
                            message: '請選擇可教授的樂器。'
                        }
                    }
                }
            }
        }); */
        //初始設置
        $('.tab-panel-content .tab-panel-item').first().addClass('active');
        StepBtn($('.tab-panel-content .tab-panel-item.active').index());
		$('.tutor-Remove').click(function () {
            var ColumnCount1 = document.querySelectorAll(".cate-item-1 > .cate-item-list");
            var ColumnCount2 = document.querySelectorAll(".cate-item-2 > .cate-item-list");
            var ColumnCount3 = document.querySelectorAll(".cate-item-3 > .cate-item-list");
            // console.log(ColumnCount1.length); 
            // console.log(ColumnCount2.length); 
            // console.log(ColumnCount3.length); 
            if(ColumnCount1.length >1 && ColumnCount2.length >1 && ColumnCount3.length >1){
                $(ColumnCount1[ColumnCount1.length-1]).remove();
                $(ColumnCount2[ColumnCount2.length-1]).remove();
                $(ColumnCount3[ColumnCount3.length-1]).remove();
            }

        })
        var status = 0; //1 = show 0 = hide
        $('.Extend').click(function () {
            if(status == 1){
                var Secondory = document.querySelectorAll(".panel-bread-content");
                $(".panel-bread-content").css('display','none'); 
                $(".panel-bread-grid").css('display','none');
                $('.Extend').text("顯示");
                status = 0;
            }
            else if(status == 0){
                var Secondory = document.querySelectorAll(".panel-bread-content");
                $(".panel-bread-content").css('display','flex');
                $(".panel-bread-grid").css('display','block');
                $('.Extend').text("縮小");
                status = 1;
            }
        })
        //導師類別添加
        $('.tutor-add').click(function () {
            var str='<div class="cate-item-list"></div>';
            var str1 = $(this).siblings().find('.cate-item-1').children("div:last-child").html();
            var str2 ="<div class='cate-item-list'><select class='form-control TchItem' name='TchItem'><option value=''></option></select></div>";
            var str3 ="<div class='cate-item-list'><select  class='form-control TchLevel' name='TchLevel'><option value=''></option></select></div>";
            $(this).siblings().find('.cate-item-1').append(str);
            $(this).siblings().find('.cate-item-2').append(str2);
            $(this).siblings().find('.cate-item-3').append(str3);
            $(this).siblings().find('.cate-item-1').children("div:last-child").append(str1);

            $(".tutor-cate-content").on("change",".form-control",function(){        //重新绑定事件
                    // alert(123);
                    var th = $(this);
                    var name = $(this).attr('name');            //获取下拉框的name名
                    if(name == 'TchCate'){
                         var v = $(this).val();
                            var index=$(".TchCate").index(this);            //当前元素的索引
                            // alert(index);
                            $.post('./index.php?m=teachers&c=index&a=TchItem_change',{tt_id:v},function(data){

                                var ThisSelectObj = th.parent().parent().next().children().eq(index+1).find('select');
                                var nextselectobj=th.parent().parent().next().next().children().eq(index+1).find('select');
                                      //获取下一级下拉框节点
                               // console.log(ThisSelectObj.html());
                                ThisSelectObj.html(' ');
                                nextselectobj.html("");
                                var text=" <option disabled='disabled' selected='selected'></option>";
                                ThisSelectObj.append(text);
                                nextselectobj.append(text);
                                if(data.length!==0){
                                    for(i in data){
                                    var course_option="<option value='"+data[i].tt_id+"'>"+data[i].tt_name+"</option>" ;  
                                    ThisSelectObj.append(course_option);
                                    }
                                }
                            },'json');
                    }else if(name == 'TchItem'){
                         var v = $(this).val();
                            var index=$(".TchItem").index(this);   //当前元素的索引

                            $.post('./index.php?m=tparents&c=index&a=stu_reg',{tt_id:v},function(data){
                                // $(".TchLevel").html(' ');
                                 var ThisSelectObj = th.parent().parent().next().children().eq(index+1).find('select');    //获取下一级下拉框节点
                                ThisSelectObj.html(' ');
                                var text=" <option disabled='disabled' selected='selected'></option>";
                                ThisSelectObj.append(text);
                                if(data.length!==0){
                                    for(i in data){
                                    var course_option="<option value='"+data[i].tt_id+"'>"+data[i].tt_name+"</option>" ;   
                                    ThisSelectObj.append(course_option);
                                    }
                                    
                                }
                            },'json');
                    }
                    // alert(a);
             });          
        })
        //下一步
        $(document).on('click','.step-btn .step-next',function () {
            var index = $('.tab-panel-content').find('.tab-panel-item.active').index();
             switch(index) {
                case 0://导师规则
                    if($('.agree-check').prop("checked")){
                        index++;
                    }else{
                        $('.error-rule').show();
                    }
                    break;
                case 1://導師類別
                    var ty = 0;
                    var level3=[];
                    $('.TchLevel option:selected').each(function () {
                        level3.push($(this).val());  
                    });
/*                     if(!level3.join("")){
                        $('.tutor-cate-box .error-area').text('請至少選擇一個教授項目');
                        $('.tutor-cate-box .error-area').show();
                        ty = 1;
                    }else{
                        $(".TchLevel option:selected").each(function(){
                        if($(this).val()== ""){
                        var index=$(".cate-item-3 .cate-item-list").index($(this).parent().parent());
                            if($(".cate-item-1 .cate-item-list").eq(index).children().find("option:selected").val()!==""){
                                $('.tutor-cate-box .error-area').text('請選擇完整');
                                $('.tutor-cate-box .error-area').show();
                                  ty = 1;
                            }                   
                        }
                        }); 
                    } */
                    
                    if(ty == 0){
                         $('.tutor-cate-box .error-area').hide();
                            index++;
                    }
                break;
                
                case 2://授课地区
                    var flag = checkNum();
                    var date = checkdate();
                    if(flag && date) {
                        
                        index++;   
                        window.scrollTo(0,300);   
                    }
                    break;    
                case 3://个人资料
                    // Validate the form manually
                    $('#personForm').bootstrapValidator('validate');
                    //表單驗證成功
                    
                    if ($("#personForm").data('bootstrapValidator').isValid()) {

                        checkusertel();
                        // checkReferee();
                        checkusermail();
                        if(emailstatus && telstatus ){
                            index++;
                        }
                    }

                    $('*[data-bv-field]').each(function () {
                        if($(this).parents('.form-group').hasClass('has-error')) {
                            $(this).focus();
                            return false;
                        }
                    });


                    break;     
                case 4://音乐及其他技能
                    $('#otherSkiiForm').bootstrapValidator('validate');
                    //表單驗證成功
                    if ($("#otherSkiiForm").data('bootstrapValidator').isValid()) {
                        //下一步
                        index++;
                        //顯示驗證碼部分 
                        $('.msg_code').show();
                    }
                    
                    break;
                case 5://自我簡介
                    var peLength = $('.pr-text').val()
                    if(false) {
                    //if(peLength.length < 50) {
                        $('.produce-box .error-area').text("自我介紹請不要少於50字。");
                        $('.produce-box .error-area').show();
                    }else{
                        //获取上课时间数据
                        var mon_time=[];
                        var tues_time=[];
                        var wed_time=[];
                        var thur_time=[];
                        var fri_time=[];
                        var sat_time=[];
                        var sun_time=[];
                        $('.mon option:selected').each(function () {  
                            mon_time.push($(this).val());  
                        });
                        $('.tues option:selected').each(function () {  
                            tues_time.push($(this).val());  
                        });
                        $('.wed option:selected').each(function () {  
                            wed_time.push($(this).val());  
                        });
                        $('.thur option:selected').each(function () {  
                            thur_time.push($(this).val());  
                        });
                        $('.fri option:selected').each(function () {  
                            fri_time.push($(this).val());  
                        });
                        $('.sat option:selected').each(function () {  
                            sat_time.push($(this).val());  
                        });
                        $('.sun option:selected').each(function () {  
                            sun_time.push($(this).val());  
                        });
                        
                        var time_arr = [];
                        time_arr[0]=mon_time;
                        time_arr[1]=tues_time;
                        time_arr[2]=wed_time;
                        time_arr[3]=thur_time;
                        time_arr[4]=fri_time;
                        time_arr[5]=sat_time;
                        time_arr[6]=sun_time;
                        var time=JSON.stringify(time_arr);
                        //获取上课时间数据結束



                        var msg_code=$('input[name="msg_code"]').val();
                        var Referee=$('input[name="Referee"]').val();
                        var userimage=$('input[name="userimage"]').val();
                        var type=[];
                        var sub=[];
                        var level=[];
                        var area=[]; 
                        var scores=[];  
                        $('.TchCate option:selected').each(function () {  
                            type.push($(this).val());  
                        });
                        $('.TchItem option:selected').each(function () {  
                            sub.push($(this).val());  
                        });
                        $('.TchLevel option:selected').each(function () {  
                            level.push($(this).val());  
                        });
                        $('input:checkbox[name="TchArea"]:checked').each(function () {  
                            area.push($(this).val());  
                        });
                        $('.scores option:selected').each(function () {  
                            var key=$(this).parent().attr("data-info");
                            scores.push(key+"-"+$(this).val());      
                        }); 
                        var self_introduction=$('textarea[name=pr-text]').val();
                        var perdetail=$('#personForm').serialize();
                        var skill=$('#otherSkiiForm').serialize();
                        var data = perdetail+'&'+skill+'&type='+type+'&sub='+sub+'&level='+level+'&area='+area+'&scores='+scores+'&self_introduction='+self_introduction+'&msg_code='+msg_code+'&Referee='+Referee+'&userimage='+userimage+'&teachertime='+time;
                        $.ajax({
                            url:"<?php echo APP_PATH;?>index.php?m=teachers&c=index&a=tutor_reg",
                            type:"post",
                            data:data,
                            asyn:false,
                            dataType:"json",
                            success: function(data){
                                if(data == 'msg_empty_error'){
                                    alert('請填寫驗證碼!');
                                    return false;
                                }else if(data == 'msg_code_error'){
                                    alert('驗證碼錯誤!');
                                    return false;
                                }else if(data == 'msg_phone_error'){
                                    alert('發送短信手機號與註冊手機號不同!');
                                    return false;
                                }else if(data == 'duplicate_error'){
                                    alert('請不要重複註冊');
                                    return false;
                                }else if(data == 'fail_error'){
                                    alert('用戶註冊失敗');
                                    return false;
                                }else{
                                    index++;
                                    $('.step-box').remove();
                                    $('.msg_code').hide();
                                    $('.tab-panel-content .tab-panel-item').eq(index).addClass('active').siblings().removeClass('active');
                                    $('.panel-tab .panel-tab-item').eq(index).addClass('active').siblings().removeClass('active');
                                    StepBtn(index);
                                }
                                
                                if(data!=""){                            
                                var content='<div class="col-md-6 col-sm-6 course-col" style="margin-left:25%">'
                                            +'<div class="course-item">'
                                            +'<div class="course-head"></div>'
                                            +'<div class="course-content student-content">'  
                                            +' <div class="course-person">'
                                            +'<img src="'+data.userimage+'" alt="">'
                                            +'<p class="course-contain">學生</p></div>'
                                            +'<div class="course-file">'
                                            +'<dl class="course-file-item">'
                                            +'<dd>個案編號</dd>'
                                            +'<dt>'+data.st_id+'</dt></dl>'
                                            +'<dl class="course-file-item">'
                                            +'<dd>補習地區</dd>'
                                            +'<dt>'+data.loc_name+'</dt></dl>'
                                            +'<dl class="course-file-item"><dd>科目</dd><dt>'+data.tt_name+'</dt></dl>'
                                            +'<dl class="course-file-item"><dd>學生人數</dd><dt>'+data.st_num_join+'</dt></dl>'
                                            +'<dl class="course-file-item"><dd>要求上課日期</dd><dt class="class_time">'+data.stu_time+'</dt></dl>'
                                            +'</div><div class="course-dec"><a href="<?php echo APP_PATH;?>index.php?m=tparents&c=index&a=stu_detail&stuid='+data.userid+'&tran_id='+data.st_id+'">查看詳情</a></div><div class="course-meta">'
                                            +'<div class="rating">'
                                            +'<div class="star-rating">'
                                            +'<span class="star-show" style="width:'+data.star+'%"></span>'
                                            +'</div>'
                                            +'</div>'
                                            +'<div class="course-three">$'+ data.st_tutor_fee+'</div></div>'+data.match_area
                                             +'</div></div> </div>';
 
                                $('.student').html(content);
                                }else{
                                    $('.finish-title').html('暫無學生適合你的要求，未有推薦個案。');
                                }

                            }
                        });
                    }
                    break;
             }

            $('.tab-panel-content .tab-panel-item').eq(index).addClass('active').siblings().removeClass('active');
            $('.panel-tab .panel-tab-item').eq(index).addClass('active').siblings().removeClass('active');
            StepBtn(index);
        });
        //上一步
        $(document).on('click','.step-btn .step-prev',function () {
            var index = $('.tab-panel-content').find('.tab-panel-item.active').index() - 1;
            $('.tab-panel-content .tab-panel-item').eq(index).addClass('active').siblings().removeClass('active');
            $('.panel-tab .panel-tab-item').eq(index).addClass('active').siblings().removeClass('active');
            StepBtn(index);
            $('.msg_code').hide();
        })

        //勾選條款操作
        $('.rule-checkbox .agree-check').click(function () {
            if($('.rule-checkbox .agree-check:checked')){
                $('.error-rule').hide();
            }
        });
        //上一步下一步的變化操作
        function StepBtn(index) {
            if(index != 0) {
                $('.step-btn .step-prev').show();
            }else{
                $('.step-btn .step-prev').hide();
            }
        }
        $('.item-panel-list input[type=checkbox]').click(function () {
            checkNum();
        })
        function checkNum() {
            var length = $('.item-panel-list input[type=checkbox]:checked').length;
            if(false){ //if(length == 0){
                $('.area-panel .error-area').text('請選擇授課區域');
                $('.area-panel .error-area').show();
                return false
            }
            // else if(length > 10 ) {
            //     $('.area-panel .error-area').text('請不要選擇超過十個地區');
            //     $('.area-panel .error-area').show();
            //     return false;
            // }
            else{
                $('.area-panel .error-area').hide();
                return true;
            }
        }

        function checkdate(){
            $('.info-list .error-tip').remove();//重置
            var flag = $(this).formConfirm();//表單驗證正確的標誌
            //时间选择必须完整
            var to_hour=[] ;
            var from_hour=[] ;
            $(".to_hour option:selected").each(function () {  
                to_hour.push($(this).text());  
            });
            $(".from_hour option:selected").each(function () {  
                from_hour.push($(this).text());  
            });
            for (var i = 0; i < from_hour.length; i++) {
                if ((from_hour[i]=="" && to_hour[i] != "") || (to_hour[i]=="" && from_hour[i] != "")) {
                    flag=false;
                    $(".to_hour").eq(i).parent().parent().append('<span class="error-tip">請輸入完整的時間。</span>');
                    $(".to_hour").eq(i).focus();//獲取焦點
                    $("#re_flag").val("");
                    var re_flag=$("#re_flag").val();
                    sessionStorage.setItem('Data',re_flag);
                }
                var t11=from_hour[i].split(":");
                var t21=to_hour[i].split(":");
                var sj1 = parseInt(t11[0])*24 + t11[1];
                var sj2 = parseInt(t21[0])*24 + t21[1];
                if (sj1 >= sj2) {    
                    flag=false;
                    $(".to_hour").eq(i).parent().parent().append('<span class="error-tip">請輸入正確的時間段。</span>');
                    $(".to_hour").eq(i).focus();//獲取焦點
                    $("#re_flag").val("");
                    var re_flag=$("#re_flag").val();
                    sessionStorage.setItem('Data',re_flag);
                }
            };
            return flag;
        }
    </script>

</body>
</html>